package gfx;

import module.Module;
import ui.Text;
import world.World;

public class ModuleRenderer extends Renderer{
	
	public Module m;
	public ModuleRenderer(Module m){
		this.m= m;
	}	

	@Override
	public void addToWorldRenderGroup(World world){
		world.rgMisc.add(this);
	}
		
	@Override
	public void render(Camera cam){
		cam= cam.clone();
		cam.trans.tx-= -.9f;
		cam.trans.ty-= .9f;
		//TODO deshit
		Text.renderLn(m.toString(), cam, m.trans());
	}
	
}
