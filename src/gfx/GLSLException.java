package gfx;

public class GLSLException extends Exception {
	private static final long serialVersionUID = 5258124161347307216L;
	
	private final String log;
	public GLSLException(String log){
		
		this.log= "\n"+log;
	}
	
	@Override
	public void printStackTrace(){
		super.printStackTrace();
		System.err.println(log);
	}
}
