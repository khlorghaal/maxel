package gfx;

import math.Transformation;

import org.lwjgl.opengl.GL20;

import core.Settings;
import entity.Light;


/**Renders the visible scene by referrencing the geomaterial texture.</br>
 * Renders every light one at a time and blends results additively*/
public class LightRenderer{

	static GLProgram prog;
	
	static GLProgram progflare;
	public static void initGL(){
		prog= new GLProgram("lightPoint");

		prog.addUniform1i("rgb", RenderCore.RGB);
		prog.addUniform1i("material", RenderCore.MATERIAL);
		prog.addUniform1i("normal", RenderCore.NORMAL);
				
		progflare= new GLProgram("flare");
	}
	
	public static void renderAll(Iterable<Light> lights, Camera cam){
		prog.use();
		synchronized(lights){//TODO view culling
			for(Light l : lights)
				render(l, cam);
			progflare.use();
//			for(Light l : )
//				renderFlare(l);
		}
	}
	
	public static void render(Light l, Camera cam){
		//intensity(distance)= min = mag*exp(1-pow1/2(1+pow2(dist/a)))
		//dist= (((ln(min/mag)-1)^2-1)^1/2
		float min= Settings.graphics.minbright+0.01f;
		float maxlen= (float)Math.sqrt((Math.pow(Math.log(min/Math.abs(l.mag))-1,2)-1))*l.a;
		Transformation m= l.trans().clone();
		m.setScale(maxlen);
		cam.setUniform(m, prog.uloc("cam"));
		GL20.glUniform1f(prog.uloc("z"), l.z);
		GL20.glUniform1f(prog.uloc("depth"), -1);
		GL20.glUniform1f(prog.uloc("maxlen"), maxlen/l.a);
		GL20.glUniform1f(prog.uloc("aspect"), RenderCore.displayAspect);
		GL20.glUniform3f(prog.uloc("emission"), l.mag*l.color[0], l.mag*l.color[1], l.mag*l.color[2]);
		GL20.glUniform1f(prog.uloc("seh"), Settings.graphics.speculareyeheight);
		Shapes.quad();
	}
	public static void renderFlare(Light l){
//		double sc= l.mag/Math.log(1+RenderCore.cameraPrime.trans.scaley/l.mag);
//		if(sc/RenderCore.cameraPrime.trans.scaley<.01)
//			return;
//		Transformation mv= l.trans.clone();
//		mv.setScale(sc);
//		RenderCore.cameraPrime.setUniform(mv, ulocfcam);
//		GL20.glUniform3f(ulocfmag, l.mag*l.color[0], l.mag*l.color[1], l.mag*l.color[2]);
//		RenderCore.drawFullscreenQuad();
	}
}
