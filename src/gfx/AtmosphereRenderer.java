package gfx;

import math.Maths;
import math.Transformation;

import org.lwjgl.opengl.GL20;

import core.Client;
import entity.celestial.Atmosphere;


public class AtmosphereRenderer{
	
	static GLProgram progSky, progNightlight;
	public static void initGL(){
		progSky= new GLProgram("sky");
		progNightlight= new GLProgram("sky", "night");
	}
	
	/**Rendered post lighting, forward, intensity on distance from star
	 * Foward rendered*/
	public static void renderSkies(){
		progSky.use();
		for(Atmosphere a: Atmosphere.instances){
			Transformation nt= a.attached.trans().clone();
			nt.scale(1.5);
			Client.cam.setUniform(nt, progSky.uloc("mvp"));
			
			float solnx= (float)(a.sun.trans().tx-nt.tx);
			float solny= (float)(a.sun.trans().ty-nt.ty);
			float soll= (float)Maths.len(solnx, solny);
			solnx/= soll;
			solny/= soll;
			GL20.glUniform2f(progSky.uloc("soln"), solnx, solny);
			
			float[] s= a.getSkyColor();
			GL20.glUniform3f(progSky.uloc("color"), s[0], s[1], s[2]);

			float fade= (float)(Client.cam.trans.scaley/a.attached.trans().scaley);
			fade= Maths.lerp(fade*1.4f, 1,0);
			fade= Maths.clamp(fade, 0,1);
//			z=1;
			float[] t= a.getDuskColor(fade);
			float[] t2= a.getDawnColor(fade);
			GL20.glUniform3f(progSky.uloc("colordusk"), t[0], t[1], t[2]);
			GL20.glUniform3f(progSky.uloc("colordawn"), t2[0], t2[1], t2[2]);

			Shapes.circle();
		}
	}
	/**Dusk/dawn and night
	 * Applied as multiplicative mask of lit
	 * after solar light, before other lights*/
	public static void renderNight(){
		progNightlight.use();
		for(Atmosphere a: Atmosphere.instances){
			Transformation nt= a.attached.trans().clone();
			nt.scale(1.5);
			Client.cam.setUniform(nt, progNightlight.uloc("mvp"));
			
			float solnx= (float)(a.sun.trans().tx-nt.tx);
			float solny= (float)(a.sun.trans().ty-nt.ty);
			float soll= (float)Maths.len(solnx, solny);
			solnx/= soll;
			solny/= soll;
			GL20.glUniform2f(progNightlight.uloc("soln"), solnx, solny);

			GL20.glUniform1f(progNightlight.uloc("daymag"), a.weather);
			GL20.glUniform1f(progNightlight.uloc("nightmag"), .025f);
			
			float fade= (float)(Client.cam.trans.scaley/a.attached.trans().scaley);
			fade= Maths.lerp(fade*1.4f, 1,0);
			fade= Maths.clamp(fade, 0,1);
//			fade=0;
			float[] t= a.getDawnColor(fade);
			GL20.glUniform3f(progNightlight.uloc("gold"), t[0], t[1], t[2]);
			GL20.glUniform3f(progNightlight.uloc("blue"), -1f/7, -.5f/7, 0);
			

			Shapes.circle();
		}		
	}
}
