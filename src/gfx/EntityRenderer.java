package gfx;

import math.Kinematic;
import math.Transformation;
import entity.Entity;

public abstract class EntityRenderer extends Renderer{
	
	public static void initGL(){
		ChunkRenderer.init();
		SpriteRenderer.init();
		ExplosionRenderer.init();
		SunRenderer.init();
		RenderCore.checkError();
	}
	
	protected Entity e;
	protected EntityRenderer(Entity e){
		super();
		this.e= e;
	}
	
	public static void setmvp(Kinematic kin, Camera cam, int uloc){
		Transformation model= kin.trans.clone();
		//interpolate TODO
//				model.tx+= kin.vx*cam.pT;
//				model.ty+= kin.vy*cam.pT;
//				model.theta+=kin.w*cam.pT;
//				model.update();
		
		cam.setUniform(model, uloc);
	}
	public void setmvp(Camera cam, int uloc){
		setmvp(e.kin, cam, uloc);
	}
}

