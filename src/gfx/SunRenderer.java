package gfx;

import org.lwjgl.opengl.GL20;

import world.World;
import entity.celestial.EntitySun;
import gfx.texture.NoiseTextures;

public class SunRenderer extends Renderer{
	
	static GLProgram prog;
	public static class Group extends RenderGroup{
		@Override
		public void equip(){
			prog.use();
			NoiseTextures.texPerlin3d.bind(0);
		}
	}
	
	
	public static void init(){
		prog= new GLProgram("sun");
		prog.addUniform1i("noise", 0);
	}
	
	final EntitySun sun;
	public SunRenderer(EntitySun sun){
		this.sun= sun;
	}

	@Override
	public void render(Camera cam){
		prog.use();
		cam.setUniform(sun.trans(), prog.uloc("mvp"));
		GL20.glUniform3f(prog.uloc("color"), 1, 1, 1);
		float t= (System.currentTimeMillis()%10000/10000f);
		if(t>.5f)
			t= 1-t;
		NoiseTextures.texPerlin3d.setMagLinear(false);
		GL20.glUniform1f(prog.uloc("t"), t);
//		GL20.glUniform1f(prog.uloc("zoom"), (float)cam.getScale());
		Shapes.circle();
	}

	@Override
	public void addToWorldRenderGroup(World world){
		world.rgSun.add(this);
	}
	
	
}
