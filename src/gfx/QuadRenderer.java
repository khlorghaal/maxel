package gfx;



import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;

public class QuadRenderer {
	public static final List<Quad> quads= new ArrayList<Quad>();
	
	public static int VBO= GL15.glGenBuffers();
	public static int VAO= GL30.glGenVertexArrays();
	static{
		//xy      uvh   sr
		//2float  3int  2float
		//2*4  +  3*4 + 2*4;
		final int VSIZ= 2*8 + 3*4 + 2*4;
		GL30.glBindVertexArray(VAO);
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, VBO);
		GL15.glBufferData(GL15.GL_ARRAY_BUFFER, 0xffff, GL15.GL_DYNAMIC_DRAW);
		GL20.glEnableVertexAttribArray(0);
		GL20.glEnableVertexAttribArray(1);
		GL20.glEnableVertexAttribArray(2);
		GL20.glVertexAttribPointer(0, 2, GL11.GL_FLOAT, false, VSIZ, 0);
		GL30.glVertexAttribIPointer(1, 3, GL11.GL_UNSIGNED_INT, VSIZ, 2*4);
		GL20.glVertexAttribPointer(2, 2, GL11.GL_FLOAT, false, VSIZ, 2*4 + 3*4);
		
		GL30.glBindVertexArray(0);
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, 0);
	}

	public static GLProgram QUADPROGRAM= new GLProgram("quad");
	static final int CALLDRAW;
	static{//compile draw list
		CALLDRAW= GL11.glGenLists(1);
		GL11.glNewList(CALLDRAW, GL11.GL_COMPILE);

		GL11.glEndList();
	}


	




	public static void drawAll(){		
		QUADPROGRAM.use();
		GL30.glBindVertexArray(VAO);
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, VBO);
		populateBuffer(quads);
		GL15.glBufferSubData(GL15.GL_ARRAY_BUFFER, 0, buf);

		GL11.glDrawArrays(GL11.GL_POINTS, 0, count);

		GL30.glBindVertexArray(0);
	}
	protected static final ByteBuffer buf= BufferUtils.createByteBuffer(0xffff);
	private static int count;
	private static void populateBuffer(List<Quad> quads){
		buf.clear();
		count=0;
		for(Quad quad : quads){
			quad.addToBuffer(buf);
			count++;
		}
		buf.rewind();
		
		quads.clear();
	}



	public static void flush(){

	}
}
