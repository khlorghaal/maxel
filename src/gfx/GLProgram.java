package gfx;



import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL32;
import org.lwjgl.opengl.GL40;
import org.lwjgl.opengl.GL43;

import core.FileUtil;
import core.Settings;

public class GLProgram {
	public int ptr;
	private static int ptrInUse;
	
	public int vsh, gsh, fsh, tesc, tese;
	String vnam, gnam, fnam, tcnam, tenam;
	protected boolean hasGeom, hasFrag, hasTess;
	
	private boolean compiled= false;
	
	public static final String PATH= "glsl/";
	
	/**for reloading*/
	static LinkedList<GLProgram> instances= new LinkedList<GLProgram>();
	{
		instances.add(this);
	}
	
	
	
	public GLProgram(String name){		
		vnam= name+".vert";
		gnam= name+".geom";
		fnam= name+".frag";
		tcnam= name+".tesc";
		tenam= name+".tese";
		
		hasGeom= getActualFile(gnam)!=null;
		hasFrag= getActualFile(fnam)!=null;
		hasTess= getActualFile(tenam)!=null;

		postconstruct();
	}
	public GLProgram(String vert, String frag){
		vnam= vert+".vert";
		fnam= frag+".frag";
		
		hasGeom= false;
		hasFrag= true;

		postconstruct();
	}
	public GLProgram(String vert, String geom, String frag){		
		vnam= vert+".vert";
		gnam= geom+".geom";
		fnam= frag+".frag";
		
		hasGeom= true;
		hasFrag= true;

		postconstruct();
	}
	public GLProgram(String vert, String tess, String frag, boolean nothing){
		hasTess= true;
		hasFrag= true;
		
		tcnam= tess+".tesc";
		tenam= tess+".tese";
		
		postconstruct();
	}

	
	protected void postconstruct(){
		if(getActualFile(vnam)==null)
			System.err.println("No vertex shader: "+vnam);
		this.ptr= GL20.glCreateProgram();
		includeIncludes();
		build();
	}
	protected void build(){
		try{
			vsh= loadShader(vnam);
			GL20.glAttachShader(this.ptr, vsh);
			if(hasGeom){
				gsh= loadShader(gnam);
				GL20.glAttachShader(this.ptr, gsh);
			}
			if(hasFrag){
				fsh= loadShader(fnam);
				GL20.glAttachShader(this.ptr, fsh);
			}
			if(hasTess){
				tesc= loadShader(tcnam);
				tese= loadShader(tenam);
				GL20.glAttachShader(this.ptr, tesc);
				GL20.glAttachShader(this.ptr, tese);
			}
		}
		catch(GLSLException e){e.printStackTrace();}
		
		preLink();
		GL20.glLinkProgram(ptr);
		GL20.glValidateProgram(ptr);
		
		if(GL20.glGetProgrami(ptr, GL20.GL_LINK_STATUS)!= GL11.GL_TRUE){
			final String pil= GL20.glGetProgramInfoLog(this.ptr, 0xfff);
			new GLSLException("SHLINKAGE ERROR\n"
			+vnam+"\n"+fnam+"\n"+pil+"\n_______").printStackTrace();
		}
		compiled= true;
		purgeShaderObjects();
		mapUlocs();
	}

	public void preLink(){}
	
	protected final void purgeShaderObjects(){
		GL20.glDetachShader(ptr, vsh);
		GL20.glDeleteShader(vsh);
		if(hasGeom){
			GL20.glDetachShader(ptr, gsh);
			GL20.glDeleteShader(gsh);
		}
		if(hasFrag){
			GL20.glDetachShader(ptr, fsh);
			GL20.glDeleteShader(fsh);
		}
		if(hasTess){
			GL20.glDetachShader(ptr, tesc);
			GL20.glDetachShader(ptr, tese);
			GL20.glDeleteShader(tesc);
			GL20.glDeleteShader(tese);
		}
	}
	
	public static int getShaderTypeFromFilename(String filename){
		switch(FileUtil.getExtension(filename)){
			case "vert":
				return GL20.GL_VERTEX_SHADER;
			case "frag":
				return GL20.GL_FRAGMENT_SHADER;
			case "geom":
				return GL32.GL_GEOMETRY_SHADER;
			case "comp":
				return GL43.GL_COMPUTE_SHADER;
			case "tesc":
				return GL40.GL_TESS_CONTROL_SHADER;
			case "tese":
				return GL40.GL_TESS_EVALUATION_SHADER;
			default: return -1;
		}
	}
	
	private static String[] subdirs;
	static{
		File base= new File(PATH);
		ArrayList<String> tsubdirs= new ArrayList<>();
		for(File f : base.listFiles())
			if(f.isDirectory())
				tsubdirs.add(f.getName()+'/');
		subdirs= tsubdirs.toArray(new String[0]);
	}
	private File getActualFile(String filename){
		File f= new File(PATH+filename);
		int i=0;
		while(!f.exists()){
			if(i>=subdirs.length)
				return null;
			f= new File(PATH+subdirs[i++]+filename);
		}
		return f;
	}
	public int loadShader(String filename) throws GLSLException{
		final int type= getShaderTypeFromFilename(filename);
		final int shptr= GL20.glCreateShader(type);
		
		final File file= getActualFile(filename);
		if(file==null){
			new FileNotFoundException(filename).printStackTrace();
			//
		}
		try(FileReader fr = new FileReader(file)) {
			final char[] chararr= new char[(int)file.length()];
			fr.read(chararr);
			String str= String.copyValueOf(chararr);
			str= includes()+str;
			
			GL20.glShaderSource(shptr, str);
		} catch (IOException e){e.printStackTrace();}
		
		
		GL20.glCompileShader(shptr);
		
		if(GL20.glGetShaderi(shptr, GL20.GL_COMPILE_STATUS)!= GL11.GL_TRUE){
			final String il= GL20.glGetShaderInfoLog(shptr, 0xffff);
			throw(new GLSLException(file.getPath()+" SHCOMPILE ERROR:\n "+il+"\n______"));
		}
		
		return shptr;
	}
	
	
	public static void reloadAll(){
		for(GLProgram p : instances)
			p.reload();
	}
	public void reload(){
		build();
		use();
		
		mapUlocs();
		
		//rebind constant unis
		for(int i=0; i!=uninames.size(); i++)
			GL20.glUniform1i(uloc(uninames.get(i)), univals.get(i));
		RenderCore.checkError();
	}
	
	
	
	//Uniform Management
	private final Map<String, Integer> ulocmap= new HashMap<>(4);
	//these enable unis to be preserved when reload()ing
	private final ArrayList<String> uninames= new ArrayList<String>();
	private final ArrayList<Integer> univals= new ArrayList<Integer>();
	
	private void mapUlocs(){
		ulocmap.clear();
		int c= GL20.glGetProgrami(ptr, GL20.GL_ACTIVE_UNIFORMS);
		int chbufsiz= GL20.glGetProgrami(ptr, GL20.GL_ACTIVE_UNIFORM_MAX_LENGTH);
		for(int i=0; i!=c; i++){
			ulocmap.put(GL20.glGetActiveUniform(ptr, i, chbufsiz), i);
		}
	}
	/**Will be preserved if the program gets reloaded.
	 * Should be used only when initializing*/
	public void addUniform1i(String name, int val){
		use();
		if(!ulocmap.containsKey(name)){
			System.err.println("Uniform "+ name+" not in program "+this);
			return;
		}
		uninames.add(name);
		univals.add(val);
		GL20.glUniform1i(uloc(name), val);
	}
	public void clearUniforms(){
		uninames.clear();
		univals.clear();
	}

	public int uloc(String name){
		Integer got= ulocmap.get(name);
		if(got==null){
				System.err.println("Uniform "+name+" not in program "+this);
			return -1;
		}
		return got;
	}
	
	
	public static final ArrayList<Include> includesState= new ArrayList<>();	
	private final ArrayList<Include> includes= new ArrayList<>();
	private void includeIncludes(){
		if(RenderCore.glversion<33){
			includes.add(new Include("#version 150 core"));
			includes.add(new Include("#extension GL_ARB_explicit_attrib_location : enable"));
		}
		else
			includes.add(new Include("#version "+RenderCore.glversion+"0"));

		for(Include i : includesState)
			this.includes.add(i);
		
		this.includes.add(new Include("#line 0\n"));
	}
	String includes(){
		StringBuilder b= new StringBuilder();
		for(Include d : includes)
			b.append(d);
		return b.toString();
	}
	/**Used for including into the shader src of each stage.
	 * Overrride toString to achieve conditional or arbitrary behavior
	 * 
	 * Subclasses are for overriding on() to check a condition upon loading*/
	public static class Include{
		public String str;
		public Include(String str){
			this.str= str+'\n';
		}

		public boolean on(){ return true;}
		
		@Override
		public final String toString(){
			if(on())
				return str;
			else
				return "";
		}
		
		public static final Include BLOOM= new Include("#define BLOOM"){
			@Override
			public boolean on(){
				return Settings.graphics.bloom;
			}
		};
		public static final Include REFRACTION= new Include("#define REFRACTION"){
			@Override
			public boolean on(){
				return Settings.graphics.refraction;
			}
		};
	}
	
	

	
	@Override
	public String toString(){
		String ret= "";
		ret+= this.vnam;
		if(hasGeom)
			ret+=" "+this.gnam;
		if(hasFrag)
			ret+= " "+this.fnam;
		return ret;
	}
	
	public void use(){
		if(ptrInUse!=ptr)//only switch program if current is inactive
			GL20.glUseProgram(ptr);		
	}
}
