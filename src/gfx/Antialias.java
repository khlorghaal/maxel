package gfx;

import static gfx.RenderCore.depths_rb;
import static gfx.RenderCore.lit_tex_antialiased;
import static gfx.RenderCore.lit_tex_drawn;
import static gfx.RenderCore.ssh;
import static gfx.RenderCore.ssw;
import gfx.texture.Texture;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL30;

import core.Profiler;
import core.Settings;

public class Antialias{
	public enum AA{NONE, SUPERSAMPLE, FASTAPPROXIMATE, MORPHOLOGICAL}
	static AA aa;
	static int lvl=1;
	
	static Texture lit_ss_downsampleintermediate;
	
	static GLProgram prog;
	public static GLProgram fxaaprog, mlaaprog, ds4xprog, ds9xprog;
	
	static void init(){
		fxaaprog= new GLProgram("posuv", "fxaa");
		mlaaprog= new GLProgram("posuv", "mlaa");
		ds4xprog= new GLProgram("posuv", "downsample4x");
		ds9xprog= new GLProgram("posuv", "downsample9x");		
	}
	
	static void set(){
		aa= Settings.graphics.aatyp;
		lvl= Settings.graphics.aalvl;
		
		switch(aa){
			case NONE:
				prog= null;
				break;
			case FASTAPPROXIMATE:
				prog= fxaaprog;
				break;
			case MORPHOLOGICAL:
				prog= mlaaprog;
				break;
			case SUPERSAMPLE:
				switch(lvl){
					case 2:
					case 4:
						prog= ds4xprog;
					break;
					case 3: 
						prog= ds9xprog;
					break;
					default:
						new Exception("Invalid SSAA level\n").printStackTrace();
						return;
				}
				break;
		}
	}
	
	
	static void resize(){
		Profiler.start("init aa");

		if(lit_tex_drawn!=null){
			lit_tex_drawn.delete();
			lit_tex_drawn=null;
		}
		
		if(lit_ss_downsampleintermediate!=null){
			lit_ss_downsampleintermediate.delete();
			lit_ss_downsampleintermediate= null;
		}
		
		if(aa!=AA.NONE){
			lit_tex_drawn= new Texture(GL30.GL_RGBA16F, GL11.GL_RGBA);
			lit_tex_drawn.allocate(ssw,ssh);
			lit_tex_drawn.bind_framebuffer();
			GL30.glFramebufferRenderbuffer(GL30.GL_FRAMEBUFFER, GL30.GL_DEPTH_STENCIL_ATTACHMENT, 
					GL30.GL_RENDERBUFFER, depths_rb);
			
			if(aa==AA.SUPERSAMPLE && lvl==4){
				lit_ss_downsampleintermediate= new Texture(GL30.GL_RGBA16F, GL11.GL_RGBA);
				lit_ss_downsampleintermediate.allocate(ssw/2,ssh/2);
			}
		}
		else
			lit_tex_drawn= lit_tex_antialiased;
		
		Profiler.stop();
	}
	
	static void invoke(){
		if(aa==AA.NONE)
			return;
		
		prog.use();
		lit_tex_drawn.bind();
		if(lvl==4){
			lit_ss_downsampleintermediate.bind_framebuffer();
			Shapes.quad();
			lit_ss_downsampleintermediate.bind();
		}
		lit_tex_antialiased.bind_framebuffer();
		Shapes.quad();
	}
}
