package gfx;

import static gfx.RenderCore.bindDefaultfb;
import static gfx.RenderCore.checkError;
import static gfx.RenderCore.h;
import static gfx.RenderCore.lit_tex_antialiased;
import static gfx.RenderCore.timer;
import static gfx.RenderCore.w;
import gfx.GLProgram.Include;
import gfx.Renderer.RenderGroup;
import gfx.texture.Texture;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;

import core.Profiler;
import core.Settings;

public class Postprocess{

	static Texture bloom_texprevious, bloom_tex0, bloom_tex1,
	bloom_downsampleintermediate0, bloom_downsampleintermediate1,
	pp_tex;
	
	static GLProgram progpp, progtelos, progbloom0;
	
	public static GLProgram progblur, progmultiplier;
	
	public static final RenderGroup group= new RenderGroup(){};
	
	static void init(){
		checkError();
		Profiler.start("init pp");
		bloom_downsampleintermediate0= new Texture(GL30.GL_RGBA16F, GL11.GL_RGBA);
		bloom_downsampleintermediate1= new Texture(GL30.GL_RGBA16F, GL11.GL_RGBA);
		bloom_tex0= new Texture(GL30.GL_RGBA16F, GL11.GL_RGBA);
		bloom_tex1= new Texture(GL30.GL_RGBA16F, GL11.GL_RGBA);
		bloom_texprevious= new Texture(GL30.GL_RGBA16F, GL11.GL_RGBA);
		pp_tex= new Texture(GL30.GL_RGBA16F, GL11.GL_RGBA);
		
		progpp= new GLProgram("posuv", "postprocess");
		GLProgram.includesState.add(Include.BLOOM);
		progtelos= new GLProgram("posuv", "telos");
		GLProgram.includesState.clear();
		progtelos.addUniform1i("light", 0);
		progtelos.addUniform1i("bloom", 1);
		
		progbloom0= new GLProgram("posuv", "bloom0");
		
		progblur= new GLProgram("posuv", "blur");
		
		progmultiplier= new GLProgram("posuv", "multiplier");
		
		checkError();
		Profiler.stop();
	}
	static int bw, bh;
	static void resize(){
		pp_tex.allocate(w,h);
		if(Settings.graphics.bloom){
			bloom_tex0.setMagLinear(Settings.graphics.bloomlinearmag);
			bloom_downsampleintermediate0.allocate(w/2, h/2);
			bloom_downsampleintermediate1.allocate(w/4, h/4);
			bw= w/8;
			bh= h/8;
			bloom_tex0.allocate(bw, bh);
			bloom_tex1.allocate(bw, bh);
			bloom_texprevious.allocate(bw, bh);
			
			bloom_texprevious.bind_framebuffer();
			GL11.glClear(GL11.GL_COLOR_BUFFER_BIT);
		}
		else{
			bloom_downsampleintermediate0.allocate(0,0);
			bloom_downsampleintermediate1.allocate(0,0);
			bloom_tex0.allocate(0,0);
			bloom_tex1.allocate(0,0);
			bloom_texprevious.allocate(0,0);

			bloom_downsampleintermediate0.delete_framebuffer();
			bloom_downsampleintermediate1.delete_framebuffer();
			bloom_tex0.delete_framebuffer();
			bloom_tex1.delete_framebuffer();
			bloom_texprevious.delete_framebuffer();
		}
	}
	
	/**Applies bloom and converts from hdr to ldr*/
	public static void invokeTelos(){
//		GL11.glPointSize(300);
//		Brush.begin();
//		Brush.color(0xffffffff);
//		Brush.vert(0, 0);
//		Brush.end(GL11.GL_POINTS);
		progpp.use();
		lit_tex_antialiased.bind();
		pp_tex.bind_framebuffer();
		Shapes.quad();
		
		if(Settings.graphics.bloom){
			//downsample
			Antialias.ds4xprog.use();
			pp_tex.bind();
			bloom_downsampleintermediate0.bind_framebuffer();
			Shapes.quad();
			bloom_downsampleintermediate0.bind();
			bloom_downsampleintermediate1.bind_framebuffer();
			Shapes.quad();
			bloom_downsampleintermediate1.bind();
			bloom_tex1.bind_framebuffer();
			Shapes.quad();
			
			//discard dims
			progbloom0.use();
			GL20.glUniform1f(progbloom0.uloc("lummax"), Settings.graphics.maxbright);
			bloom_tex1.bind();
			bloom_tex0.bind_framebuffer();
			Shapes.quad();
			
			progblur.use();
			GL20.glUniform1i(progblur.uloc("r"), Settings.graphics.bloomradius);
			for(int i=0; i!=2; i++){
				//pass1 horizontal blur
				bloom_tex0.bind();
				bloom_tex1.bind_framebuffer();
				GL20.glUniform1i(progblur.uloc("axis"), 0);
				Shapes.quad();
				//pass2 vertical
				bloom_tex1.bind();
				bloom_tex0.bind_framebuffer();
				GL20.glUniform1i(progblur.uloc("axis"), 1);
				Shapes.quad();
			}
			
			//burnw
			if(Settings.graphics.burn!=0){
				progmultiplier.use();
				//decay/sec = u^fps
				//(decay/sec)^spf = u
				GL20.glUniform1f(progmultiplier.uloc("mul"), (float)Math.pow(Settings.graphics.burn, timer.spf));
				GL11.glEnable(GL11.GL_BLEND);
				GL11.glBlendFunc(GL11.GL_ONE, GL11.GL_ONE);
				bloom_texprevious.bind();
				Shapes.quad();
				GL11.glDisable(GL11.GL_BLEND);
				//save curbloom as prevbloom
				GL30.glBindFramebuffer(GL30.GL_READ_FRAMEBUFFER, bloom_tex0.fbo);
				GL30.glBindFramebuffer(GL30.GL_DRAW_FRAMEBUFFER, bloom_texprevious.fbo);
				GL30.glBlitFramebuffer(0, 0, bw, bh, 0, 0, bw, bh, GL11.GL_COLOR_BUFFER_BIT, GL11.GL_NEAREST);
			}
			
			GL13.glActiveTexture(GL13.GL_TEXTURE1);//is normally bound to tex0
			GL11.glBindTexture(GL11.GL_TEXTURE_2D, bloom_tex0.ptr);
		}
		
		pp_tex.bind();
		bindDefaultfb();
		progtelos.use();
		GL20.glUniform1f(progtelos.uloc("t"), timer.tick/10f);
		GL20.glUniform1f(progtelos.uloc("lummin"), Settings.graphics.minbright);
		GL20.glUniform1f(progtelos.uloc("lumrange"), Settings.graphics.maxbright-Settings.graphics.minbright);
		GL20.glUniform1f(progtelos.uloc("saturation"), 1);//TODO saturation based on lum
		Shapes.quad();
	}
}
