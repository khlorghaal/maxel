package gfx;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;

public class Brush{
	public static GLProgram progbrush;
	static DBO dbo;
	static int vao;
	static final int STRIDE= 2*4 + 4*1;
	static void initGL(){
		progbrush= new GLProgram("brush");
		dbo= new DBO(4*3*10);
		
		vao= GL30.glGenVertexArrays();
		GL30.glBindVertexArray(vao);
		dbo.bind();
		GL20.glEnableVertexAttribArray(0);
		GL20.glEnableVertexAttribArray(1);
		GL20.glVertexAttribPointer(0, 2, GL11.GL_FLOAT, false, STRIDE, 0);
		GL20.glVertexAttribPointer(1, 4, GL11.GL_UNSIGNED_BYTE, true, STRIDE, 2*4);
	}
	
	static int count;
	public static void begin(){
		progbrush.use();
		dbo.bind();
		count=0;
	}
	static byte r,g,b,a;
	public static void color(int rgba){
		//uses bytes instead of floats
		r=(byte)((rgba&0xFF000000)>>0x18);
		g=(byte)((rgba&0x00FF0000)>>0x10);
		b=(byte)((rgba&0x0000FF00)>>0x8);
		a=(byte)((rgba&0x000000FF)>>0x0);
	}
	public static void verts(float[] xy){
		assert(xy.length%2==0);
		for(int i=0; i!=xy.length;)
			vert(xy[i++], xy[i++]);
	}
	public static void vert(float x, float y){
		DBO.buf.putFloat(x);
		DBO.buf.putFloat(y);
		DBO.buf.put(r);
		DBO.buf.put(g);
		DBO.buf.put(b);
		DBO.buf.put(a);
		count++;
	}
	public static void end(int drawmode){
		dbo.push();
		GL30.glBindVertexArray(vao);
		GL11.glDrawArrays(drawmode, 0, count);
	}
}
