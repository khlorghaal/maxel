package gfx;


import java.nio.FloatBuffer;

import math.Transformation;
import math.TransformationScaleRotate;
import math.vec2;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL20;

import ui.InputCore;
import core.Settings;

public class Camera {
	/**The transformation of the viewport*/
	public Transformation trans;
	/**Partial tick*/
	public static float pT;
	
	protected float zoomyness= .1f;
	/**translation relative to current zoom, unrelative to rotation*/
	protected vec2 pan= new vec2(0);
	public boolean doPan= Settings.pan;
	
	/**State read by some renderers during their render method*/
	public boolean unlit= false;

	public Camera(){
		trans= new TransformationScaleRotate();
	}
	@Override
	public Camera clone(){
		Camera ret= new Camera();
		ret.trans= this.trans.clone();
		ret.pT= this.pT;
		ret.zoomyness= this.zoomyness;
		ret.pan= this.pan.dup();
		ret.unlit= this.unlit;
		ret.pan= this.pan;
		ret.ignoreModelTranslation= this.ignoreModelTranslation;
		return ret;
	}
	
	protected void updateAspect(){
				trans.scalex= trans.scaley*RenderCore.displayAspect;
				trans.update();
	}

	public void zoomloc(int w, vec2 mp){
		mp= mp.dup().mul(RenderCore.displayAspectInverse).add(pan);
		final float mul= (float)Math.pow(1-zoomyness, w);
		if(Settings.cursorzoom==2 ||(Settings.cursorzoom==1 && mul<1)){
			//mg0 = mg1
			//t0 + m*r0 = t1 + m*r1
			//t1 = t0 + m*r0 - m*r1
			//t+= m*r0 - m*r1 = m*(r0-r1)
			double xx0= trans.mxx, xy0= trans.mxy, yx0= trans.myx, yy0= trans.myy;
			trans.scale(mul);
			trans.tx+= mp.x*(xx0-trans.mxx) + mp.y*(xy0-trans.mxy);
			trans.ty+= mp.x*(yx0-trans.myx) + mp.y*(yy0-trans.myy);
		}
		else
			trans.scale(mul);
	}



	//	protected static int ubo;
	private static FloatBuffer cambuf;

	static void initGL(){

		//		ubo= GL15.glGenBuffers();
		cambuf= BufferUtils.createFloatBuffer(3*3);
		//		
		//		GL15.glBindBuffer(GL31.GL_UNIFORM_BUFFER, ubo);
		//		GL15.glBufferData(GL31.GL_UNIFORM_BUFFER, cambuf, GL15.GL_DYNAMIC_DRAW);
		//		GL30.glBindBufferBase(GL31.GL_UNIFORM_BUFFER, 0, ubo);
		//		GL15.glBindBuffer(GL31.GL_UNIFORM_BUFFER, 0);
	}
	/**length of y row*/
	public double getScale(){
//		return Math.sqrt( (trans.mxx+trans.mxy)*(trans.myx+trans.myy)/2 );
//		double xsq= (trans.mxx*trans.mxx+trans.mxy*trans.mxy);
//		double ysq= (trans.myx*trans.myx+trans.myy*trans.myy);
//		return Math.sqrt(xsq+ysq);
		return Math.sqrt(trans.myx*trans.myx+trans.myy*trans.myy);
	}
	public boolean ignoreModelTranslation= false;
	/**scalexy are undefined*/
	public Transformation getModelviewprojeciton(Transformation model){
		//I hope i never have to touch this again
		Transformation p= trans.clone();
		
		if(ignoreModelTranslation){
			model= model.clone();
			model.tx= model.ty= 0;
		}
		
		p.invert();
		p= Transformation.product(p, model);
		
		if(doPan){
			pan= InputCore.mp.dup();
			if(Settings.panDeadzone!=0){
				float dz= Settings.panDeadzone;
				if(Math.abs(pan.x)<dz)
					pan.x=0;
				else
					pan.x= (pan.x - Math.signum(pan.x)*dz) / (1-dz);
				if(Math.abs(pan.y)<dz)
					pan.y=0;
				else
					pan.y= (pan.y - Math.signum(pan.y)*dz) / (1-dz);
			}
			if(Settings.panNonlinear){
				pan.x*= pan.x*Math.signum(pan.x);
				pan.y*= pan.y*Math.signum(pan.y);
			}
		}
		else{
			pan.set(0);
		}
		p.tx-=pan.x;
		p.ty-=pan.y;
		return p;
	}

	/**@param object the global transformation of the thing being rendered*/
	public void setUniform(Transformation model, int uloc) {
		final Transformation mvp= getModelviewprojeciton(model);

		cambuf.clear();
		cambuf.put((float) mvp.mxx);
		cambuf.put((float) mvp.mxy);
		cambuf.put((float) mvp.tx);
		cambuf.put((float) mvp.myx);
		cambuf.put((float) mvp.myy);
		cambuf.put((float) mvp.ty);
		cambuf.put(0);
		cambuf.put(0);
		cambuf.put(1);
		cambuf.clear();

		GL20.glUniformMatrix3(uloc, true, cambuf);
		//transpose because column major	
	}

	public vec2 getGlobalMousecoord(vec2 screenp){
		return trans.use(screenp.dup().add(pan));
	}
}
