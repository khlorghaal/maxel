package gfx;

import java.nio.ByteBuffer;
import java.nio.IntBuffer;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;

public class Shapes{
	
	public static void bar(float x, float y, float w, float h, int color){
		GL20.glUseProgram(0);
		Brush.begin();
		Brush.color(color);
		Brush.vert(x, y);
		Brush.vert(x+w, y);
		Brush.vert(x, y+h);
		Brush.vert(x+w, y+h);
		Brush.end(GL11.GL_TRIANGLE_STRIP);
	}
	
	final static float r= 1f;
	/**the xy of two triangles composing a quad*/
	public static final float[] quadTriVerts= new float[]{
		//cannot be a strip because not necessarily adjacent
		
		//2 3    -+ ++
		//0 1    -- +-
		
		//1 0 2
		+r,-r, -r,-r, -r,+r,
		//1 2 3
		+r,-r, -r,+r, +r,+r
		
	};
	
	public static final float[] quadTriVerts_pos_uv= new float[]{
		//2 3    -+ ++   01 11
		//0 1    -- +-   00 10
		
		//1 0 2
		+r,-r,1,0, -r,-r,0,0, -r,+r,0,1,
		//1 2 3
		+r,-r,1,0, -r,+r,0,1, +r,+r,1,1
	};
	
	
	static int 
	quadVBO, quadVAO, 
	quadPosUVVBO, quadPosUVVAO,
	circleVBO, circleVAO;
	static final int CIRCLE_COUNT= 128;
	public static void init(){
		//Quad
		quadVBO= GL15.glGenBuffers();
		quadPosUVVBO= GL15.glGenBuffers();
		DBO.preBufferWrite(4*4*6);
		ByteBuffer buf= DBO.buf;
		
		buf.limit(4*2*6);
		buf.asFloatBuffer().put(quadTriVerts);
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, quadVBO);
		GL15.glBufferData(GL15.GL_ARRAY_BUFFER, buf, GL15.GL_STATIC_DRAW);
		quadVAO= GL30.glGenVertexArrays();
		GL30.glBindVertexArray(quadVAO);
		GL20.glEnableVertexAttribArray(0);
		GL20.glVertexAttribPointer(0, 2, GL11.GL_FLOAT, false, 4*2, 0);
		
		buf.limit(4*4*6);
		buf.asFloatBuffer().put(quadTriVerts_pos_uv);
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, quadPosUVVBO);
		GL15.glBufferData(GL15.GL_ARRAY_BUFFER, buf, GL15.GL_STATIC_DRAW);
		quadPosUVVAO= GL30.glGenVertexArrays();
		GL30.glBindVertexArray(quadPosUVVAO);
		GL20.glEnableVertexAttribArray(0);
		GL20.glEnableVertexAttribArray(1);
		GL20.glVertexAttribPointer(0, 2, GL11.GL_FLOAT, false, 4*4, 0);
		GL20.glVertexAttribPointer(1, 2, GL11.GL_FLOAT, false, 4*4, 4*2);
		
		//circle
		circleVBO= GL15.glGenBuffers();
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, circleVBO);
		DBO.preBufferWrite(CIRCLE_COUNT*4*2);
		for(float theta=0; theta<=Math.PI*2; theta+=Math.PI*2/CIRCLE_COUNT ){
			DBO.buf.putFloat((float)Math.cos(theta));
			DBO.buf.putFloat((float)Math.sin(theta));
		}
		DBO.buf.flip();
		GL15.glBufferData(GL15.GL_ARRAY_BUFFER, DBO.buf, GL15.GL_STATIC_DRAW);
		
		circleVAO= GL30.glGenVertexArrays();
		GL30.glBindVertexArray(circleVAO);
		GL20.glEnableVertexAttribArray(0);
		GL20.glVertexAttribPointer(0, 2, GL11.GL_FLOAT, false, 0, 0);
		
		llibo= GL15.glGenBuffers();
		bindlineIBO(1024);
		GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, 0);
	}
	
	static int llibo, llibolen=0;
	public static void bindlineIBO(int count){
		assert(count>1);
		GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, llibo);		
		DBO.preBufferWrite(4*2*(count+1));
		IntBuffer ib= DBO.buf.asIntBuffer();

		if(count>llibolen){
			//c-1,0, 0,1, 1,2, ...c-2, c-1
			ib.put(count-1);
			ib.put(0);
			for(int i=0; i!=count; i++){
				ib.put(i);
				ib.put(i+1);
			}
			ib.flip();
			GL15.glBufferData(GL15.GL_ELEMENT_ARRAY_BUFFER, ib, GL15.GL_STATIC_DRAW);
			llibolen= count;
		}
		else{
			ib.put(count-1);
			ib.flip();
			GL15.glBufferSubData(GL15.GL_ELEMENT_ARRAY_BUFFER, 0, ib);
		}
	}
	
	public static void quad(){
		GL30.glBindVertexArray(quadPosUVVAO);
		GL11.glDrawArrays(GL11.GL_TRIANGLES, 0, 6);
	}
	public static void circle(){
		GL30.glBindVertexArray(circleVAO);
		GL11.glDrawArrays(GL11.GL_TRIANGLE_FAN, 0, CIRCLE_COUNT);		
	}
	public static void point(){
		GL11.glDisable(GL11.GL_DEPTH_TEST);
		GL11.glPointSize(512*2);
		Brush.begin();
		Brush.color(0x00ffff8f);
		Brush.vert(0, 0);
		Brush.end(GL11.GL_POINTS);
	}
}
