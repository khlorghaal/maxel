package gfx;

import gfx.texture.NoiseTextures;
import gfx.texture.Texture;
import material.Material;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.lwjgl.opengl.GL31;
import org.lwjgl.opengl.GL32;

import core.Profiler;
import core.Settings;


/**Generates the geomaterial texture array*/
public class MaterialRenderer {
	static Texture atlasn;
	static int trgbs, tmats;
	static DBO brgbs, bmats;
	static GLProgram prog;
	static final int
	COLORSIZ=4*4,
	COESIZ=3*4,
	NFSIZ= 2*4,
	STRIDE= COLORSIZ+2*(COESIZ+NFSIZ);
	
	static int VBO, VAO;
	static void initGL(){
		RenderCore.checkError();
		prog= new GLProgram("material");
		
		VBO= GL15.glGenBuffers();
		VAO= GL30.glGenVertexArrays();
		GL30.glBindVertexArray(VAO);
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, VBO);
		GL20.glEnableVertexAttribArray(0);
		GL20.glEnableVertexAttribArray(1);
		GL20.glEnableVertexAttribArray(2);
		GL20.glEnableVertexAttribArray(3);
		int mark=0;
		GL20.glVertexAttribPointer(0, 3, GL11.GL_FLOAT, false, STRIDE, mark);//surface0
		mark+= COESIZ;
		GL20.glVertexAttribPointer(1, 2, GL11.GL_FLOAT, false, STRIDE, mark);//nf0
		mark+= NFSIZ;
		GL20.glVertexAttribPointer(2, 3, GL11.GL_FLOAT, false, STRIDE, mark);//surface1
		mark+= COESIZ;
		GL20.glVertexAttribPointer(3, 2, GL11.GL_FLOAT, false, STRIDE, mark);//nf1
		mark+= NFSIZ;
		GL30.glBindVertexArray(0);
		

		brgbs= new DBO();
		trgbs= GL11.glGenTextures();
		GL13.glActiveTexture(Texture.RGB+Texture.BASE);
		GL11.glBindTexture(GL31.GL_TEXTURE_BUFFER, trgbs);
		GL31.glTexBuffer(GL31.GL_TEXTURE_BUFFER, GL11.GL_RGBA8, brgbs.ptr);

		bmats= new DBO();
		tmats= GL11.glGenTextures();
		GL13.glActiveTexture(Texture.MATERIAL+Texture.BASE);
		GL11.glBindTexture(GL31.GL_TEXTURE_BUFFER, tmats);
		GL31.glTexBuffer(GL31.GL_TEXTURE_BUFFER, GL30.GL_R16UI, bmats.ptr);
		
		RenderCore.checkError();
	}
	/**puts onto render thread*/
	public static void queueRenderAll(){
		RenderCore.exec.execute(new Runnable(){
			@Override
			public void run(){
				renderAll();
			}
		});
	}
	private static void renderAll(){
		Profiler.start("Render Materials");
		RenderCore.checkError();
		int resolution= 1<<Settings.graphics.materialResolutionExp;
		synchronized (Material.array){
			int depth= Material.array.size();

			
			if(atlasn!=null)
				atlasn.delete();
			atlasn= new Texture(Texture.NORMAL, GL30.GL_TEXTURE_2D_ARRAY, GL30.GL_RGB16F, GL11.GL_RGBA);
			atlasn.allocate(resolution, resolution, depth);
			atlasn.setMagLinear(Settings.graphics.normalLinearMagnificationFilter);
			int fbo= GL30.glGenFramebuffers();
			GL30.glBindFramebuffer(GL30.GL_FRAMEBUFFER, fbo);
			GL32.glFramebufferTexture(GL30.GL_FRAMEBUFFER, GL30.GL_COLOR_ATTACHMENT0, atlasn.ptr, 0);
						
			NoiseTextures.texNoises.bind();
			DBO.preBufferWrite(depth*STRIDE);
			for(Material m : Material.array){
				if(m==null)
					m= Material.NULLMAT;
				for(float f:m.noisedata)
					DBO.buf.putFloat(f);
			}
			DBO.buf.flip();
			GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, VBO);
			GL15.glBufferData(GL15.GL_ARRAY_BUFFER, DBO.buf, GL15.GL_STATIC_DRAW);
			
			GL11.glViewport(0, 0, resolution, resolution);
			GL11.glDisable(GL11.GL_DEPTH_TEST);
			GL11.glDisable(GL11.GL_BLEND);
			prog.use();
			GL20.glUniform1i(prog.uloc("res"), resolution);
			GL30.glBindVertexArray(VAO);
			GL11.glDrawArrays(GL11.GL_POINTS, 0, depth);

			atlasn.mapmips(5);
			
			

			brgbs.prewrite(Material.array.size()*4);
			for(Material m : Material.array){
				DBO.buf.put(m.red);
				DBO.buf.put(m.green);
				DBO.buf.put(m.blue);
				DBO.buf.put((byte)0);
			}
			brgbs.push();
			bmats.prewrite(Material.array.size()*2);
			for(Material m : Material.array)
				DBO.buf.putShort(m.gfxdata);
			bmats.push();

			RenderCore.checkError();
			RenderCore.bindDefaultfb();
			GL30.glDeleteFramebuffers(fbo);
		}
		Profiler.stop();
	}
	
	public static void bindAtlases(){
		GL13.glActiveTexture(Texture.RGB+Texture.BASE);
		GL11.glBindTexture(GL31.GL_TEXTURE_BUFFER, trgbs);
		GL13.glActiveTexture(Texture.MATERIAL+Texture.BASE);
		GL11.glBindTexture(GL31.GL_TEXTURE_BUFFER, tmats);
		atlasn.bind();		
	}
}
