package gfx;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import material.Chunk.Node;
import material.Material;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;

import world.World;
import entity.Entity;
import gfx.texture.Texture;

public class ChunkRenderer extends EntityRenderer{
	public static class Group extends RenderGroup{
		@Override
		public void equip(){
			prog.use();
			MaterialRenderer.bindAtlases();
		}
	}
	
	static GLProgram prog, progUnlit;
	
	static final int POSSIZ=3*4, UVSSIZ=4, IDSIZ=4;
	static final int STRIDE = POSSIZ+UVSSIZ+IDSIZ;
	public static final int QUADSIZE = STRIDE * 6;
	
	static void init() {
		prog= new GLProgram("block");
		prog.addUniform1i("brgb", Texture.RGB);
		prog.addUniform1i("bmat", Texture.MATERIAL);
		prog.addUniform1i("atlasn", Texture.NORMAL);

		progUnlit= new GLProgram("block", "block_unlit");
		progUnlit.addUniform1i("brgb", Texture.RGB);
		progUnlit.addUniform1i("bmat", Texture.MATERIAL);
		progUnlit.addUniform1i("atlasn", Texture.NORMAL);
	}
	public static int genVAO(int vbo){
		final int ret= GL30.glGenVertexArrays();
		GL30.glBindVertexArray(ret);
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vbo);
		
		GL20.glEnableVertexAttribArray(0);
		GL20.glEnableVertexAttribArray(1);
		GL20.glEnableVertexAttribArray(2);
		int mark=0;
		GL20.glVertexAttribPointer(0, 3, GL11.GL_FLOAT, false, STRIDE, mark);//pos
		mark+= POSSIZ;
		GL20.glVertexAttribPointer(1, 1, GL11.GL_FLOAT, false, STRIDE, mark);//uvscale
		mark+= UVSSIZ;
		GL30.glVertexAttribIPointer(2, 1, GL11.GL_INT, STRIDE, mark);//id
		mark+= IDSIZ;
		
		return ret;
	}

	int vao;
	public DBO dbo;
	public ChunkRenderer(Entity e){
		super(e);
		dbo= new DBO(0);
		vao= genVAO(dbo.get());
		e.chunk.changedVBO= true;
	}
	@Override
	public void onDestruct(){
		GL30.glDeleteVertexArrays(vao);
		dbo.delete();
	}
	@Override
	public void addToWorldRenderGroup(World world){
		world.rgChunk.add(this);
	}
	
	@Override
	public void render(Camera cam){
		GLProgram p= prog;
		if(cam.unlit){
			progUnlit.use();
			p= progUnlit;
		}
		
		setmvp(e.kin, cam, p.uloc("mvp"));
		
		if(e.chunk.changedVBO)
			refreshVBO();
		GL30.glBindVertexArray(vao);
		GL11.glDrawArrays(GL11.GL_TRIANGLES, 0, quadcount*6);
	}
	
	
	int quadcount;
	void refreshVBO() {
		List<Node> visibleNodes= new ArrayList<Node>();
		synchronized(e.chunk){
			
			for(Node n : e.chunk.getAllNodes()){
				if(n.getID()!=0)
					visibleNodes.add(n);
			}
			quadcount= visibleNodes.size();
			e.chunk.changedVBO= false;
			e.chunk.changedGrid= true;
		}
		dbo.prewrite(quadcount*QUADSIZE);
		loadBuffer(visibleNodes, (float)e.trans().scaley);
		dbo.push();
		visibleNodes.clear();
	}
	/**@param scale for texture*/
	public static void loadBuffer(Collection<Node> nodes, float scale){
		for(Node n : nodes){//iter quads
			int id= n.getID();//localvar because material array clamp
			if(id>Material.array.size()-1)
				id=0;
			int j=0;//iter quadgroup attribs
			for(int k=0; k!=6; k++){//iter verticies
				DBO.buf.putFloat((n.x+Shapes.quadTriVerts[j++]*n.scale));
				DBO.buf.putFloat((n.y+Shapes.quadTriVerts[j++]*n.scale));
				DBO.buf.putFloat(0);//z
				DBO.buf.putFloat(scale);
				DBO.buf.putInt(id);
			}
		}
	}

}