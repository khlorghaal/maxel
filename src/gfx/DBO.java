package gfx;

import java.nio.ByteBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL15;

/**Dynamic Buffer Object
 * Expands as necessary.
 * Always call prewrite before and push after writing*/
public class DBO {
	public static ByteBuffer buf= BufferUtils.createByteBuffer(0xff);
	
	public final int ptr= GL15.glGenBuffers();
	public int get(){return ptr;}
	int target= GL15.GL_ARRAY_BUFFER;
	int size=0;
	
	public DBO(){
		this(0);
	}
	public DBO(int initsize){
		if(initsize<64)//is arbitrary to prevent rapid reallocation
			initsize=64;		
		prewrite(initsize);
	}
	public DBO(int initsize, int target){
		this(initsize);
		this.target= target;
	}
	
	static int expand(int in){
		return in + in/2;
	}
	public static void preBufferWrite(int writesize){
		if(writesize>buf.capacity()){//bytebuffer expand
			buf= BufferUtils.createByteBuffer(expand(writesize));
		}
		buf.clear();
	}
	public void prewrite(int writesize){
		bind();
		preBufferWrite(writesize);
		if(writesize>size){//bufferobject expand
			size= expand(writesize);
			GL15.glBufferData(target, size, GL15.GL_DYNAMIC_DRAW);
		}
	}
	/**Must be bound by prewrite*/
	public void push(){
		buf.flip();
		GL15.glBufferSubData(target, 0, buf);
		buf.clear();
	}
	
	public void bind(){
		GL15.glBindBuffer(target, ptr);		
	}
	public void bind(int target){
		GL15.glBindBuffer(target, ptr);
	}
	public void delete(){
		GL15.glDeleteBuffers(ptr);		
	}
}
