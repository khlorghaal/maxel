package gfx;

import java.util.List;
import java.util.Vector;

import world.World;


public abstract class Renderer{
	
	public Renderer(){}

	public abstract void render(Camera cam);
	
	private RenderGroup group= null;
	public abstract void addToWorldRenderGroup(World world);
	
	public final void destruct(){
		if(group!=null)
			group.remove(this);
		onDestruct();
	}
	public void onDestruct(){}
	
	
	
	public static abstract class RenderGroup{
		private final List<Renderer> instances= new Vector<>();
		public final void add(Renderer r){
			instances.add(r);
			r.group= this;
		}
		public final void remove(Renderer r){
			instances.remove(r);
			r.group= null;
		}
		
		public final void renderAll(Camera cam){
			if(instances.size()==0)
				return;
			
			equip();
			for(Renderer r : instances)
				r.render(cam);
			dequip();
		}

		public void equip(){}
		public void dequip(){}
	}
	

//	/**Used to differentiate from null*/
//	public static final Renderer UNCONSTRUCTED= new Renderer(){
//		@Override
//		public void render(Camera cam){
//			assert(false);
//		}
//		@Override
//		public void addToWorldRenderGroup(World world){
//			assert(false);
//		}
//	};
}