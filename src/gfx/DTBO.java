package gfx;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL31;

public class DTBO extends DBO{
	int texunit;
	int tex;
	public DTBO(int internalformat, int texunit){
		super(0);
		this.texunit= texunit;
		this.tex= GL11.glGenTextures();
		GL13.glActiveTexture(texunit+GL13.GL_TEXTURE0);
		GL11.glBindTexture(GL31.GL_TEXTURE_BUFFER, tex);
		GL31.glTexBuffer(GL31.GL_TEXTURE_BUFFER, internalformat, ptr);
	}
	
	public void bindTex(){
		GL13.glActiveTexture(texunit+GL13.GL_TEXTURE0);
		GL11.glBindTexture(GL31.GL_TEXTURE_BUFFER, tex);
	}
	
	@Override
	public void delete(){
		super.delete();
		GL11.glDeleteTextures(tex);
	}
}
