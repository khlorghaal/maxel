package gfx;

import world.World;
import entity.Entity;
import gfx.texture.Texture;

public class SpriteRenderer extends EntityRenderer{
	public static class Group extends RenderGroup{
		@Override
		public void equip(){
			prog.use();	
		}
	};
	
	static GLProgram prog;
	static final int MTU=0, GTU=1;
	static final int STRIDE= 3*4 + 2*4;
	static final int QUADSIZE= STRIDE*6;
	
	static void init(){
		prog= new GLProgram("sprite");
		prog.addUniform1i("texm", MTU);
		prog.addUniform1i("texg", GTU);
	}
	
	Texture material, geometry;
	/**textures must be on appropriate units 0,1 respectively*/
	public SpriteRenderer(Entity e){
		super(e);
	}
	
	@Override
	public void addToWorldRenderGroup(World world){
		world.rgSprite.add(this);
	}
	
	@Override
	public void render(Camera cam){
		if(e.sprite==null)
			return;
		setmvp(e.kin, cam, prog.uloc("mvp"));

		Shapes.quad();
	}
}