package gfx;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL30;

/***/
public abstract class TransformFeedback{
	public static TransformFeedback knots;
	
	public static void init(){
		
//		knots= new TransformFeedback(
//				new GLProgram("knot"){
//			@Override
//			public void preLink(){
//				GL30.glTransformFeedbackVaryings(ptr, new String[]{"pos","norm"}, GL30.GL_INTERLEAVED_ATTRIBS);
//				super.preLink();
//			}
//		}){
//			@Override
//			public void bindAttribs(){
//				GL20.glEnableVertexAttribArray(0);
//				GL20.glEnableVertexAttribArray(1);
//				GL20.glEnableVertexAttribArray(2);
//				GL20.glVertexAttribPointer(0, 2, GL11.GL_FLOAT, false, 2*4, 2*4*0);
//				GL20.glVertexAttribPointer(1, 2, GL11.GL_FLOAT, false, 2*4, 2*4*1);
//				GL20.glVertexAttribPointer(2, 2, GL11.GL_FLOAT, false, 2*4, 2*4*2);
//			}
//		};
		
		
		
	}
	
	GLProgram prog;
	public TransformFeedback(GLProgram prog){
		this.prog= prog;
	}
	
	public void use(int in, int out, int first, int count){
		prog.use();
		
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, in);
		GL30.glBindVertexArray(0);
		bindAttribs();
		GL30.glBindBufferBase(GL30.GL_TRANSFORM_FEEDBACK_BUFFER, 0, out);
		
		GL11.glEnable(GL30.GL_RASTERIZER_DISCARD);
		GL11.glDrawArrays(GL11.GL_POINTS, first, count);
		GL11.glDisable(GL30.GL_RASTERIZER_DISCARD);
	}
	
	public abstract void bindAttribs();
}
