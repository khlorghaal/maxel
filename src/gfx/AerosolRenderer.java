package gfx;

import gfx.texture.NoiseTextures;

import java.nio.FloatBuffer;

import org.lwjgl.opengl.GL20;
import org.lwjgl.util.vector.Matrix3f;

public class AerosolRenderer{
	static GLProgram prog;
	public static void initGL(){
		prog= new GLProgram("aerosol");
		
	}
	
	public static void render(){		
		Matrix3f mvp= new Matrix3f();
		mvp.m00= RenderCore.displayAspectInverse;
		DBO.preBufferWrite(9*4);
		FloatBuffer fb= DBO.buf.asFloatBuffer();
		mvp.store(fb);
		fb.flip();
		
		
		prog.use();
		GL20.glUniformMatrix3(prog.uloc("mvp"), false, fb);
		GL20.glUniform4f(prog.uloc("color"), 1, 1, 1, 1);
		NoiseTextures.texPerlin3d.bind();
		NoiseTextures.texPerlin3d.setMagLinear(true);
		Shapes.quad();
	}
}
