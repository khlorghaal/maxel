package gfx;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.lwjgl.opengl.GL40;

import world.World;
import core.Settings;
import entity.Explosion;
import entity.Explosion.EntityShockwave;

public class ExplosionRenderer extends Renderer{
	public static class Group extends RenderGroup{
		@Override
		public void equip(){
			progsw.use();
			
		}
	};
	
	static GLProgram progsw;
	
	static boolean tess= false;
	public static void init(){
		if(RenderCore.glversion>=40 && Settings.graphics.tessellation!=0){
			tess= true;
			progsw= new GLProgram("shockwave");
		}
		else
			progsw= new GLProgram("shockwave_notess", "shockwave");
		PlumeRenderer.init();
	}
	
	public final Explosion expl;
	final PlumeRenderer pr;
	final DBO dbo= new DBO(64*4);
	
	final int vao;
	public ExplosionRenderer(Explosion explosion){
		super();
		this.expl= explosion;
		
		dbo.bind();
		vao= GL30.glGenVertexArrays();
		GL30.glBindVertexArray(vao);
		GL20.glEnableVertexAttribArray(0);
		GL20.glEnableVertexAttribArray(1);
		GL20.glEnableVertexAttribArray(2);
		GL20.glVertexAttribPointer(0, 2, GL11.GL_FLOAT, false, 0, 2*4*0);
		GL20.glVertexAttribPointer(1, 2, GL11.GL_FLOAT, false, 0, 2*4*1);
		GL20.glVertexAttribPointer(2, 2, GL11.GL_FLOAT, false, 0, 2*4*2);
		GL30.glBindVertexArray(0);
		pr= new PlumeRenderer(explosion);
	}
	@Override
	public void onDestruct(){
		if(expl.sws.size()!=0)
			return;//will be called again
		dbo.delete();
		pr.destruct();
	}	
	@Override
	public void addToWorldRenderGroup(World world){
		world.rgShockwave.add(this);
	}
	
	@Override
	public void render(Camera cam){
		if(expl.sws.size()<2)
			return;
		
		cam.setUniform(expl.trans, progsw.uloc("mvp"));
		if(tess){
			int t= Settings.graphics.tessellation;
			GL20.glUniform2i(progsw.uloc("res"), RenderCore.w/t, RenderCore.h/t);
			GL20.glUniform1f(progsw.uloc("w"), 6);
		}
		GL20.glUniform1f(progsw.uloc("z"), 0);
		
		int siz;
		synchronized(expl){
			siz= expl.sws.size();
			if(siz<2)
				return;
			dbo.prewrite(2*4*siz);
			putpos(siz-1);
			for(int i=0; i!=siz; i++)
				putpos(i);
			putpos(0);
			dbo.push();
		}
		
		GL30.glBindVertexArray(vao);
		Shapes.bindlineIBO(siz);
		if(tess){
			GL40.glPatchParameteri(GL40.GL_PATCH_VERTICES, 2);
			GL11.glDrawElements(GL40.GL_PATCHES, siz*2, GL11.GL_UNSIGNED_INT, 0);
			GL15.glBindBuffer(GL15.GL_ELEMENT_ARRAY_BUFFER, 0);
		}
		else{
			GL11.glLineWidth(64);
			GL11.glDrawElements(GL11.GL_LINES, siz*2, GL11.GL_UNSIGNED_INT, 0);
		}
	}
	final void putpos(int i){
		final EntityShockwave s= expl.sws.get(i);
		DBO.buf.putFloat((float)(s.trans().tx-expl.trans.tx));
		DBO.buf.putFloat((float)(s.trans().ty-expl.trans.ty));
	}
	
	
	public static class PlumeRenderer extends Renderer{
		public static class Group extends RenderGroup{
			@Override
			public void equip(){
				//				progplume.use();
			}
		};
		
		static GLProgram progplume;
		public static void init(){
			//			progplume= new GLProgram("explosion");
		}
		
		Explosion explosion;
		public PlumeRenderer(Explosion explosion){
			this.explosion= explosion;
		}
		
		@Override
		public void addToWorldRenderGroup(World world){
			world.rgPlume.add(this);
		}
		
		@Override
		public void render(Camera cam){
			Brush.begin();
			Brush.vert(0, 0);
			Brush.end(GL11.GL_POINTS);			
		}		
	}
}
