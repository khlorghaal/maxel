package gfx;

import gfx.texture.NoiseTextures;

import java.nio.FloatBuffer;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;

import ui.InputCore;
import core.Client;


public class Render3DTex{
	static GLProgram prog;
	
	static DBO dbo= new DBO(10000);
	static int edge, count;
	public static void initGL(){
		prog= new GLProgram("3dTex");
		prog.addUniform1i("tex", 0);
		
		edge= 200;
		count= edge*edge*edge;
		dbo.prewrite(count*4*3);
		FloatBuffer fb= DBO.buf.asFloatBuffer();
		float d= 2f/edge;
		for(float x=-1; x<1; x+=d)
			for(float y=-1; y<1; y+=d)
				for(float z=-1; z<1; z+=d)
					fb.put(new float[]{x,y,z});
		DBO.buf.position(count*4*3);
		dbo.push();
	}
	
	static Matrix4f mv;
	static Matrix4f p;
	public static void render(){		
		mv= new Matrix4f();
		mv.m32= (float)Client.cam.trans.scaley;
		float t= ((RenderCore.timer.tick*1000f)/1000000f)%1f;
		mv.rotate(-.7f+(float)InputCore.mp.y, new Vector3f(1, 0, 0));
		mv.rotate(-(float)InputCore.mp.x*(float)(Math.PI)/2, new Vector3f(0, 1, 0));
//		mv.translate(new Vector3f(.5f, .5f, .5f));
		
		p= new Matrix4f();
		p.m00= RenderCore.displayAspectInverse;
//		p.m00= (float)Math.sqrt(RenderCore.displayAspectInverse);
//		p.m11= (float)Math.sqrt(RenderCore.displayAspect);
		float far= 10f;
		p.m22= 2f/far;//zz
		p.m32= -1f/far;//zw=-zz/2
		p.m23= 1f;//wz
		p.m33= 0f;//ww
		
		
		
		Matrix4f mvp= new Matrix4f();
		Matrix4f.mul(p, mv, mvp);
		
		DBO.preBufferWrite(16);
		FloatBuffer fb= DBO.buf.asFloatBuffer();
		mvp.store(fb);
		fb.flip();
		
		
		GL11.glClear(GL11.GL_COLOR_BUFFER_BIT|GL11.GL_DEPTH_BUFFER_BIT);
		GL11.glEnable(GL11.GL_DEPTH_TEST);
//		GL11.glEnable(GL11.GL_BLEND);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE);

		NoiseTextures.texPerlin3d.bind();
		NoiseTextures.texPerlin3d.setMagLinear(true);
		prog.use();		
		GL20.glUniformMatrix4(prog.uloc("mvp"), false, fb);
//		GL20.glUniform3f(prog.uloc("texoffs"), 0, 0, t);
		
		GL30.glBindVertexArray(0);
		dbo.bind();
		GL20.glEnableVertexAttribArray(0);
		GL20.glDisableVertexAttribArray(1);
		GL20.glDisableVertexAttribArray(2);
		GL20.glVertexAttribPointer(0, 3, GL11.GL_FLOAT, false, 0, 0);
		GL11.glDrawArrays(GL11.GL_POINTS, 0, count);
	}
}
