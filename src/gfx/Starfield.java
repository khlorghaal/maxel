package gfx;

import java.nio.FloatBuffer;
import java.util.Random;

import math.Maths;
import math.Transformation;
import math.TransformationScaleRotate;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;

import core.Client;
import core.Settings;

public class Starfield{
	static GLProgram prog;
	static int vao, vbo;
	final static int STRIDE= 4*(2+3);
	public static void initGL(){
		prog= new GLProgram("starfield");
		
		vbo= GL15.glGenBuffers();
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vbo);
		DBO.preBufferWrite(siz);
		DBO.buf.asFloatBuffer().put(genPoints());
		DBO.buf.limit(siz);
		GL15.glBufferData(GL15.GL_ARRAY_BUFFER, DBO.buf, GL15.GL_STATIC_DRAW);
		
		vao= GL30.glGenVertexArrays();
		GL30.glBindVertexArray(vao);
		GL20.glEnableVertexAttribArray(0);
		GL20.glEnableVertexAttribArray(1);
		GL20.glVertexAttribPointer(0, 2, GL11.GL_FLOAT, false, STRIDE, 0);
		GL20.glVertexAttribPointer(1, 3, GL11.GL_FLOAT, false, STRIDE, 2*4);
	}
	final static int count= Settings.graphics.starcount;
	final static int siz= count*STRIDE;
	final static int len= count*STRIDE/4;
	private static float[] genPoints(){
		float[] ret= new float[len];
		final Random r= new Random(0);//pattern is persistent
		
		final int rse= len/2;
		for(int i=0; i!=rse;){//random scattering
			ret[i++]= r.nextFloat()*2-1;
			ret[i++]= r.nextFloat()*2-1;
			float[] c= getColor(r.nextFloat());
			ret[i++]= c[0];
			ret[i++]= c[1];
			ret[i++]= c[2];
		}
		final int b1e=len*3/4;
		for(int i=rse; i!=b1e;){//galactic band 1
			float x= r.nextFloat()*2-1;
			float y= (float)(r.nextGaussian());
			y= y*.033f + x*.19f+.1f;
			ret[i++]= x;
			ret[i++]= y;
			float[] c= getColor(0f);
			ret[i++]= c[0];
			ret[i++]= c[1];
			ret[i++]= c[2];
		}
		for(int i=b1e; i!=len;){//galactic band 2
			float x= r.nextFloat()*2-1;
			float y= (float)(r.nextGaussian());
			y= y*.15f + x*.2f +.1f;
			ret[i++]= x;
			ret[i++]= y;
			float[] c= getColor(1f);
			ret[i++]= c[0];
			ret[i++]= c[1];
			ret[i++]= c[2];
		}
		
		return ret;
	}
	
	static float[][] cols= new float[][]{
		Maths.rgbIntToFloat(0x354272),//
		Maths.rgbIntToFloat(0x604D73),//
		Maths.rgbIntToFloat(0x48598D),//
		Maths.rgbIntToFloat(0x4C63A5),//
		Maths.rgbIntToFloat(0x888DBF),//
	};
	static float lum= .3f;
	static float[] lums= new float[]{
		1f,
		1f,
		1f,
		1f,
		1f,};
	static{
		for(int i=0; i!=cols.length; i++)
			for(int j=0; j!=3; j++)
				cols[i][j]*=lums[i]*lum;
	}
	
	private static float[] getColor(float t){
		t-=.001f;
		int i= (int)(t*cols.length);
		return cols[i];
	}
	
	public static void render(){
		prog.use();
		float tri= (float)Math.sqrt(2);
		float sx= tri;
		float sy= tri;
		if(RenderCore.displayAspect>1)
			sy*= RenderCore.displayAspect;
		else
			sx*= RenderCore.displayAspectInverse;
		Transformation c= new TransformationScaleRotate(0, 0, -Client.cam.trans.theta, sx, sy);
		DBO.preBufferWrite(4*4);
		FloatBuffer b= DBO.buf.asFloatBuffer();
		b.put((float)c.mxx);
		b.put((float)c.mxy);
		b.put((float)c.myx);
		b.put((float)c.myy);
		b.flip();
		GL20.glUniformMatrix2(prog.uloc("cam"), true, b);
		
		GL11.glPointSize(4);
		GL30.glBindVertexArray(vao);
		GL11.glDrawArrays(GL11.GL_POINTS, 0, count);
	}
}
