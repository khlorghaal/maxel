package gfx;

import java.nio.IntBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.lwjgl.opengl.GL32;
import org.lwjgl.opengl.PixelFormat;
import org.lwjgl.util.glu.GLU;

import ui.gui.GUICore;
import world.World;
import core.Client;
import core.ExecutorQueue;
import core.Main;
import core.Profiler;
import core.Settings;
import core.Timer;
import entity.Light;
import gfx.Antialias.AA;
import gfx.GLProgram.Include;
import gfx.texture.NoiseTextures;
import gfx.texture.Texture;

public class RenderCore {
	public static int glversion;
	
	public static Timer timer= new Timer(Settings.graphics.fps);
	public static float pT;
	
	public static float displayAspect=1, displayAspectInverse=1, displayMarginNormal;
	
	public static final int GEOMETRY_STENCIL= 1, ATMOS_STENCIL= 2;
	
	/**After the current frame*/
	public static final ExecutorQueue exec= new ExecutorQueue();
	
	public static Thread thread;
	public static void start(){
		thread= new Thread(new Runnable(){
			@Override
			public void run() {
				try{
					if(Settings.undecorated)
						System.setProperty("org.lwjgl.opengl.Window.undecorated", "true");
					Profiler.start("init display");
					Display.create(new PixelFormat(0, 0, 0).withSRGB(true));
					Display.makeCurrent();
					Display.setTitle("Maxel");
					Display.setResizable(!Settings.undecorated);
					Display.setVSyncEnabled(Settings.graphics.vsync);
					
					Profiler.stop();
				} catch (LWJGLException e) {e.printStackTrace();}
				initGL();
				
				checkError();
				
				Client.cdl.countDown();
				try{
					Client.cdl.await();
				}catch(InterruptedException e){e.printStackTrace();}
				try{
					Main.worldCdl.await();
				}catch(InterruptedException e){e.printStackTrace();}
				
				while(!Display.isCloseRequested() && run){
					
					pT= 1f/timer.getRelativeRate(World.instance.timer);
					pT= 0;
					renderAll();
					exec.flush();
					checkError();
					Display.update();
					if(Display.wasResized())
						onResizeDisplay();
					
					timer.invoke();
					Client.cam.pT= timer.getRelativeRate(World.instance.timer);
				}
				Display.destroy();
				Client.terminate();
			}
		}, "Render");
		thread.start();
	}
	private static boolean run= true;
	public static void terminate(){
		run= false;
		timer.dehalt();
	}
	
	public static void waitOnWorldTranslation(World world){
		//wait on World to finish updating positions if doing so
		//World may still desynch positions while rendering Entities after this synch,
		//but this drastically reduces that chance
		while(world.translatingEntities.val){
			synchronized(world.translatingEntities){
				try{
					world.translatingEntities.wait();
				}catch(InterruptedException e1){e1.printStackTrace();}
			}
		}//do not maintain lock, World has absolute priority
	}
	//////////////////////////
	// MAIN RENDER LOOP //
	//////////////////////////
	public static void renderAll(){
		
		//clear depth and lit color
		//geometry doesnt need its color cleared because of stencil and unblended
		bindGeomaterialfb();
		GL11.glDepthMask(true);
		GL11.glClear(GL11.GL_DEPTH_BUFFER_BIT);
		lit_tex_drawn.bind_framebuffer();
		GL11.glClear(GL11.GL_COLOR_BUFFER_BIT);
		
		waitOnWorldTranslation(World.instance);
		Client.cam.trans.tx= World.instance.localplayer.trans().tx;
		Client.cam.trans.ty= World.instance.localplayer.trans().ty;
		//		Client.cam.trans.rotate(.01);
		
		beginGeometryRendering(true);
		World.instance.rgChunk.renderAll(Client.cam);
		World.instance.rgSprite.renderAll(Client.cam);
		World.instance.rgMisc.renderAll(Client.cam);
		endGeometryRendering();
				
		//background
		if(displayedFB==DisplayedFB.LIT){
			//lit_drawn is fbobound
			GL11.glDepthMask(true);
			GL11.glBlendFunc(GL11.GL_ONE, GL11.GL_ONE);
			GL11.glDisable(GL11.GL_BLEND);
			World.instance.rgSun.renderAll(Client.cam);
			GL11.glEnable(GL11.GL_BLEND);
			Starfield.render();
			GL11.glBlendFunc(GL11.GL_ONE, GL11.GL_ONE_MINUS_SRC_ALPHA);
			AtmosphereRenderer.renderSkies();
		}
		
		beginGeometryRendering(false);
		World.instance.rgPlume.renderAll(Client.cam);
		World.instance.rgShockwave.renderAll(Client.cam);
		endGeometryRendering();
		
		
//		beginGeometryRendering(false);
//		WeatherRenderer.RainRenderder.render();
//		endGeometryRendering();

		
		GL11.glDepthMask(false);
		if(displayedFB==DisplayedFB.LIT){
			GL11.glEnable(GL11.GL_BLEND);
			GL11.glEnable(GL11.GL_DEPTH_TEST);
			Postprocess.group.renderAll(Client.cam);
			
			GL11.glDisable(GL11.GL_BLEND);
			GL11.glDisable(GL11.GL_DEPTH_TEST);
			Antialias.invoke();
			Postprocess.invokeTelos();
		}
				
		//UI
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		bindDefaultfb();
		GUICore.renderAll();
	}
	private static boolean opaqueGeometryPass;
	public static void beginGeometryRendering(boolean opaque){
		bindGeomaterialfb();
		GL11.glStencilMask(0xff);
		GL11.glClear(GL11.GL_STENCIL_BUFFER_BIT);
		//no need to clear color because stencil
		
		GL11.glEnable(GL11.GL_STENCIL_TEST);
		GL11.glStencilOp(GL11.GL_KEEP, GL11.GL_KEEP, GL11.GL_REPLACE);//used with mask, no need to or
		GL11.glStencilFunc(GL11.GL_ALWAYS, GEOMETRY_STENCIL, GEOMETRY_STENCIL);
		GL11.glStencilMask(GEOMETRY_STENCIL);
		
		opaqueGeometryPass= opaque;
		GL11.glEnable(GL11.GL_DEPTH_TEST);
		GL11.glDepthMask(opaque);
		GL11.glDisable(GL11.GL_BLEND);
	}
	public static void endGeometryRendering(){
		GL11.glStencilOp(GL11.GL_KEEP, GL11.GL_KEEP, GL11.GL_KEEP);
		GL11.glStencilFunc(GL11.GL_EQUAL, GEOMETRY_STENCIL, GEOMETRY_STENCIL);
		GL11.glDepthMask(false);
		
		bindGeomaterialTextures();
		
		displayedFB= DisplayedFB.LIT;
		//				displayedFB= DisplayedFB.MATERIALS;
		//				displayedFB= DisplayedFB.NORMALS;
		if(displayedFB==DisplayedFB.LIT){
			
			//pass1 lighting
			if(opaqueGeometryPass)
				lit_tex_drawn.bind_framebuffer();
			else{
				//render the non opaque surfaces which will be blended
				lit_tex_drawn2.bind_framebuffer();
				GL11.glClear(GL11.GL_COLOR_BUFFER_BIT);
			}
			GL11.glEnable(GL11.GL_BLEND);
			GL11.glBlendFunc(GL11.GL_ONE, GL11.GL_ONE);
			LightRenderer.renderAll(Light.LightStar.instances, Client.cam);
			if(!Settings.nightvision){
				GL11.glBlendFunc(GL11.GL_DST_COLOR, GL11.GL_ZERO);				
				AtmosphereRenderer.renderNight();
			}
			
			GL11.glBlendFunc(GL11.GL_ONE, GL11.GL_ONE);
			LightRenderer.renderAll(Light.instances, Client.cam);
			
			//pass2 transparency and refraction
			if(!opaqueGeometryPass){
				//lit contains all previous passes this frame
				lit_tex_drawn.bind(LIT);
				//drawn2 is fbobound
				GL11.glDisable(GL11.GL_DEPTH_TEST);//stencil guarantees depth pass
				GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
				progTransparency.use();//src is refracted, dst is surface 
				Shapes.quad();
				
				//write current pass over lit
				lit_tex_drawn2.bind();
				lit_tex_drawn.bind_framebuffer();
				GL11.glDisable(GL11.GL_BLEND);
				progunlit.use();//blit because stencil test
				Shapes.quad();
			}
			
		}
		else{//geomaterial views
			bindDefaultfb();
			GL11.glDisable(GL11.GL_DEPTH_TEST);
			GL11.glDisable(GL11.GL_BLEND);
			if(displayedFB==DisplayedFB.MATERIALS)
				progmaterialdisplay.use();
			else if(displayedFB==DisplayedFB.NORMALS)
				prognormaldisplay.use();
			//stencil test prevents black overwrite
			Shapes.quad();
		}
		
		GL11.glDisable(GL11.GL_STENCIL_TEST);
	}
	//////////////
	//////////////
	//////////////
	
	private static void initGL(){
		checkError();
		String ver= GL11.glGetString(GL11.GL_VERSION).replaceAll("\\.", "").substring(0, 2);
		glversion= Integer.parseInt(ver);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		GL11.glEnable(GL11.GL_DEPTH_TEST);
		GL11.glDisable(GL11.GL_CULL_FACE);
		GL11.glEnable(GL20.GL_POINT_SPRITE);
		GL11.glEnable(GL20.GL_VERTEX_PROGRAM_POINT_SIZE);
		GL11.glClearColor(0, 0, 0, 0);
		GL11.glClearStencil(0);
		
		Shapes.init();
		NoiseTextures.initGL();
		Brush.initGL();
		GUICore.initGL();
		TransformFeedback.init();
		
		Camera.initGL();
		LightRenderer.initGL();
		MaterialRenderer.initGL();
		EntityRenderer.initGL();
		
		Postprocess.init();
		
		AtmosphereRenderer.initGL();
		Starfield.initGL();
		
		Render3DTex.initGL();
		
		AerosolRenderer.initGL();
		WeatherRenderer.initGL();
		
		
		//programs
		progunlit= new GLProgram("posuv","unlit");
		progunlitarray= new GLProgram("posuv","unlit_array");
		progunlit3D= new GLProgram("posuv","unlit_3d");
		progmaterialdisplay= new GLProgram("posuv", "materialvisualizer");
		progmaterialdisplay.addUniform1i("materials", RGB);
		prognormaldisplay= new GLProgram("posuv", "normalvisualizer");
		prognormaldisplay.addUniform1i("normals", NORMAL);
		GLProgram.includesState.add(Include.REFRACTION);
		progTransparency= new GLProgram("posuv", "transparency");
		progTransparency.addUniform1i("material", MATERIAL);
		progTransparency.addUniform1i("normal", NORMAL);
		progTransparency.addUniform1i("opaque", LIT);
		GLProgram.includesState.clear();
		
		Antialias.init();
		
		//place this block last or will gen nulls
		geomat_fbo= GL30.glGenFramebuffers();
		depths_rb= GL30.glGenRenderbuffers();
		
		rgb_tex= new Texture(RGB, GL11.GL_TEXTURE_2D, GL11.GL_RGBA8, GL11.GL_RGBA);
		mat_tex= new Texture(MATERIAL, GL11.GL_TEXTURE_2D, GL30.GL_R16UI, GL30.GL_RED_INTEGER);
		geom_tex= new Texture(NORMAL, GL11.GL_TEXTURE_2D, GL30.GL_RGBA16F, GL11.GL_RGBA);
		
		lit_tex_antialiased= new Texture(0, GL11.GL_TEXTURE_2D, GL30.GL_RGBA16F, GL11.GL_RGBA);
		lit_tex_drawn2= new Texture(0, GL11.GL_TEXTURE_2D, GL30.GL_RGBA16F, GL11.GL_RGBA);
		
		onResizeDisplay();//must resize geomat and lit base
		
		checkError();
	}
	
	public static void reload(){
		Display.destroy();
		start();
	}
	
	
	public static enum DisplayedFB {LIT, MATERIALS, NORMALS}
	public static DisplayedFB displayedFB;
	public static GLProgram progunlit, progunlitarray, progunlit3D;
	public static GLProgram prognormaldisplay;
	public static GLProgram progmaterialdisplay;
	public static GLProgram progTransparency;
	
	////////////////////
	//Framebuffery stuff
	static final int RGB=0, MATERIAL=1, NORMAL=2, LIT=3;
	static int geomat_fbo, depths_rb;
	/**lit acts as a pointer to either ds or ss. ss will be null if not supersampling*/
	static Texture 
	rgb_tex, mat_tex, geom_tex,  
	lit_tex_drawn, lit_tex_antialiased, //lit_drawn has alpha
	lit_tex_drawn2;
	
	/**wh of display*/
	public static int w,h;
	/**supersample wh. if no supersampling then = wh*/ 
	public static int ssw,ssh;
	
	public static void onResizeDisplay(){
		Antialias.set();
		w= Display.getWidth();
		h= Display.getHeight();
		ssw= w;
		ssh= h;
		if(Antialias.aa==AA.SUPERSAMPLE){
			ssw*= Antialias.lvl;
			ssh*= Antialias.lvl;
		}
		
		displayAspect= (float)ssw/(float)ssh;
		displayAspectInverse= 1f/displayAspect;
		displayMarginNormal= (1-1f/displayAspect)/2;
		Client.cam.updateAspect();
		
		checkError();
		
		//framebuffers
		//allocate
		GL30.glBindRenderbuffer(GL30.GL_RENDERBUFFER, depths_rb);
		GL30.glRenderbufferStorage(GL30.GL_RENDERBUFFER, GL30.GL_DEPTH24_STENCIL8, ssw, ssh);
		//geomat
		rgb_tex.allocate(ssw,ssh);
		geom_tex.allocate(ssw,ssh);
		mat_tex.allocate(ssw,ssh);
		GL30.glBindFramebuffer(GL30.GL_FRAMEBUFFER, geomat_fbo);
		GL32.glFramebufferTexture(GL30.GL_FRAMEBUFFER, GL30.GL_COLOR_ATTACHMENT0, rgb_tex.ptr, 0);
		GL32.glFramebufferTexture(GL30.GL_FRAMEBUFFER, GL30.GL_COLOR_ATTACHMENT1, mat_tex.ptr, 0);
		GL32.glFramebufferTexture(GL30.GL_FRAMEBUFFER, GL30.GL_COLOR_ATTACHMENT2, geom_tex.ptr, 0);
		GL30.glFramebufferRenderbuffer(GL30.GL_FRAMEBUFFER, GL30.GL_DEPTH_STENCIL_ATTACHMENT, GL30.GL_RENDERBUFFER, depths_rb);
		setDrawbuffers(3);
		
		Postprocess.resize();
		Antialias.resize();
		
		lit_tex_antialiased.allocate(w,h);
		//lit_drawn is allocated by aaresize if !=lit_final
		lit_tex_antialiased.bind_framebuffer();
		GL30.glFramebufferRenderbuffer(GL30.GL_FRAMEBUFFER, GL30.GL_DEPTH_STENCIL_ATTACHMENT, 
				GL30.GL_RENDERBUFFER, depths_rb);
		
		lit_tex_drawn2.allocate(ssw,ssh);
		
		bindDefaultfb();
		checkError();
		
		GUICore.onDisplayResize();
	}
	
	static void bindGeomaterialTextures(){
		rgb_tex.bind();
		mat_tex.bind();
		geom_tex.bind();
	}
	static void bindDefaultfb(){
		GL11.glViewport(0, 0, w, h);	
		GL30.glBindFramebuffer(GL30.GL_FRAMEBUFFER, 0);		
	}
	/**bind the framebuf with material and normal*/
	static void bindGeomaterialfb(){
		GL11.glViewport(0, 0, ssw, ssh);
		GL30.glBindFramebuffer(GL30.GL_FRAMEBUFFER, geomat_fbo);
	}
	
	///////
	//Utils
	public static int checkError(){
		final int glerr= GL11.glGetError();
		if(glerr!=0){
			String err= "GL ERROR: "+glerr+' '+GLU.gluErrorString(glerr)+'\n';
			StackTraceElement[] st= Thread.currentThread().getStackTrace();
			for(int i=2; i<st.length; i++)
				err+= st[i].toString()+'\n';
			err+= '\n';
			System.err.println(err);
			//MAKE SURE BREAKPOINT IS ON CORRECT LINE
			//IT LIKES TO BE A FAGGOT
			return glerr;
		}
		return glerr;
	}
	

	static IntBuffer db_clattch= BufferUtils.createIntBuffer(8);
	public static void setDrawbuffers(int n){
		db_clattch.clear();
		for(int i=0; i!=n; i++)
			db_clattch.put(GL30.GL_COLOR_ATTACHMENT0+i);
		db_clattch.flip();
		GL20.glDrawBuffers(db_clattch);
	}
}