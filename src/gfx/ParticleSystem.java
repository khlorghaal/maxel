package gfx;

import java.nio.FloatBuffer;

import math.Transformation;

import org.lwjgl.BufferUtils;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.lwjgl.util.vector.Matrix4f;

import ui.InputCore;

/**Contains transform shaders*/
public class ParticleSystem{
	static final int POSSIZ= 3*4, VSIZ= POSSIZ, 
			STRIDE= POSSIZ+VSIZ;
	
	TransformProgram prog;
	int tbuf, buf0= GL15.glGenBuffers(), buf1= GL15.glGenBuffers();
	int vao, vao0= GL30.glGenVertexArrays(), vao1= GL30.glGenVertexArrays();
	int start, count;
	public ParticleSystem(String vshname, int count){
		prog= new TransformProgram(vshname);
		
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, buf0);
		GL30.glBindVertexArray(vao0);
		GL20.glEnableVertexAttribArray(0);
		GL20.glEnableVertexAttribArray(1);
		GL20.glVertexAttribPointer(0, 3, GL11.GL_FLOAT, false, STRIDE, 0);
		GL20.glVertexAttribPointer(1, 3, GL11.GL_FLOAT, false, STRIDE, POSSIZ);
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, buf1);
		GL30.glBindVertexArray(vao1);
		GL20.glEnableVertexAttribArray(0);
		GL20.glEnableVertexAttribArray(1);
		GL20.glVertexAttribPointer(0, 3, GL11.GL_FLOAT, false, STRIDE, 0);
		GL20.glVertexAttribPointer(1, 3, GL11.GL_FLOAT, false, STRIDE, POSSIZ);
		vao= vao0;
		
		allocate(count);
	}
	public void allocate(int count){
		this.count= count;
		DBO.preBufferWrite(count*STRIDE);
		DBO.buf.clear();
//						float countCuRecip= (float) Math.pow(count, -1f/3f)*2.1f;
//						for(float x=-1; x<1; x+= countCuRecip){
//							for(float y=-1; y<1; y+= countCuRecip){
//								for(float z=-1; z<1; z+= countCuRecip){
//									DBO.buf.putFloat(x);
//									DBO.buf.putFloat(y);
//									DBO.buf.putFloat(z);
//									DBO.buf.putFloat(0);
//									DBO.buf.putFloat(0);
//									DBO.buf.putFloat(0);
//								}
//							}
//						}
		float countCuRecip= (float) Math.pow(count, -1f/2f)*2.1f;
		for(float x=-1; x<1; x+= countCuRecip){
			for(float z=-1; z<1; z+= countCuRecip){
				DBO.buf.putFloat(x);
				DBO.buf.putFloat((float)(Math.sin((x*z)*Math.PI*7)*Math.sqrt(1-x*x-z*z)));
				DBO.buf.putFloat(z);
				DBO.buf.putFloat(0);
				DBO.buf.putFloat(0);
				DBO.buf.putFloat(0);
			}
		}
		//		float countrecip= 1f/count;
		//		for(float t=0; t<1; t+=countrecip){
		//			float divs= 3;
		//			float mod= t%(1/divs);
		//			DBO.buf.putFloat((t)*2-1);
		//			DBO.buf.putFloat((t)*2-1);
		//			DBO.buf.putFloat(0);
		//			DBO.buf.putFloat(0);
		//			DBO.buf.putFloat(0);
		//			DBO.buf.putFloat(0);
		//		}
		
		DBO.buf.flip();
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, buf0);
		GL15.glBufferData(GL15.GL_ARRAY_BUFFER, DBO.buf, GL15.GL_DYNAMIC_DRAW);
		
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, buf1);
		GL15.glBufferData(GL15.GL_ARRAY_BUFFER, count*STRIDE, GL15.GL_DYNAMIC_DRAW);
	}
	
	boolean pong;
	public void tick(){		
		if(Keyboard.isKeyDown(Keyboard.KEY_RETURN)){
			allocate(count);
			pong=false;//becomes false
		}
		
		pong=!pong;//order is important, as buf1 will be empty first pass
		if(pong){
			vao= vao0;
			tbuf= buf1;
		}
		else{
			vao= vao1;
			tbuf= buf0;
		}
		//		if(!Mouse.isButtonDown(3))
		//			return;
		
		GL30.glBindBufferBase(GL30.GL_TRANSFORM_FEEDBACK_BUFFER, 0, tbuf);
		
		prog.use();
		int mul= 1;
		if(Mouse.isButtonDown(0))
			mul*=-1;
		if(Mouse.isButtonDown(1))
			mul*=8;
		GL20.glUniform4f(prog.uloc("attractor"), 
				(float)(InputCore.mg.x), 
				(float)(InputCore.mg.y), 0,
				mul*.0000125f);
		
		GL11.glEnable(GL30.GL_RASTERIZER_DISCARD);
		GL30.glBindVertexArray(vao);
		GL30.glBeginTransformFeedback(GL11.GL_POINTS);
		GL11.glDrawArrays(GL11.GL_POINTS, start, count);
		GL30.glEndTransformFeedback();
		GL11.glDisable(GL30.GL_RASTERIZER_DISCARD);
	}
	
	GLProgram rprog= new GLProgram("particle");
	FloatBuffer mbuf= BufferUtils.createFloatBuffer(4*4);
	float o= .01f;
	public void render(Camera cam){
		rprog.use();
		mbuf.clear();
		
		
		Matrix4f mat= new Matrix4f();
		//		mat= mat.rotate(InputCore.mx*1.57f, new Vector3f(0, 1,0));
		//		mat= mat.rotate(InputCore.my*1.57f, new Vector3f(-1, 0,0));
		mat.m00= RenderCore.displayAspectInverse;
		cam.getModelviewprojeciton(new Transformation());
		mat.m03= -(float)(cam.trans.tx-o+cam.pan.x);
		mat.m13= -(float)(cam.trans.ty+cam.pan.y);
		mat.m23= (float)cam.trans.scaley;
		mat.m30= mat.m20;//set w==z
		mat.m31= mat.m21;
		mat.m32= mat.m22;
		mat.m33= mat.m23;
		mat.m20*= .02f;//far
		mat.m21*= .02f;
		mat.m22*= .02f;
		mat.m23*= .02f;
		
		mat.store(mbuf);
		mbuf.clear();
		GL20.glUniformMatrix4(rprog.uloc("mvp"), false, mbuf);
		
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glDisable(GL11.GL_DEPTH_TEST);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE);
		GL30.glBindVertexArray(vao);
		GL11.glDrawArrays(GL11.GL_POINTS, start, count);
	}
	
	public static class TransformProgram extends GLProgram{
		public TransformProgram(String name){
			super(name);
		}
		@Override
		public void preLink(){
			GL30.glTransformFeedbackVaryings(ptr, new String[]{"pos","v"}, GL30.GL_INTERLEAVED_ATTRIBS);
			super.preLink();
		}
	}
	
	public static final class ParticleSystem3D extends ParticleSystem{
		public ParticleSystem3D(String vshname, int count){
			super(vshname, count);
		}
		
		@Override
		public void render(Camera cam){
			GL11.glViewport(0, 0, RenderCore.ssw/2, RenderCore.ssh);
			super.render(cam);
			o=-o;
			GL11.glViewport(RenderCore.ssw/2, 0, RenderCore.ssw/2, RenderCore.ssh);			
			super.render(cam);
			o=-o;
		}
	}
	
	public static final ParticleSystem INSTANCE_ATTRACTOR= new ParticleSystem(
			"tgravity", 2<<22);
	
}
