package gfx.texture;

import gfx.DBO;
import gfx.RenderCore;

import java.util.Random;

import math.Noise;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;
import org.lwjgl.opengl.GL30;
import org.lwjgl.opengl.GL31;

import core.Profiler;
import core.Settings;

/**Makes the 2d noise textures</br>
 * Also contains a 3d perlin float array*/
public class NoiseTextures{	
	public static Texture texSin2D, texNoises, texPerlin3d;
	
	//Are never modified without being rereferenced
	private static Noise nperlin2;
	public static float[][] perlin2;
	private static Noise nperlin3;
	public static float[][][] perlin3;
	private static Noise nworley2;
	public static float[][] worley;
	
	public static int res, zres;
	private static int res3d;
	
	public static void init(){
		Settings.graphics.materialResolutionExp= 6;
		res= 2<<Settings.graphics.materialResolutionExp;
		res3d= res/(1<<Settings.graphics.cloudResolutionFraction);
		res3d= 20;//TODO
		zres= Settings.graphics.cloudZResolution;
		zres= 10;

		int seed= new Random().nextInt();
		int gres= 1<<4;
		nperlin2= new Noise.Perlin2(gres, gres, seed);
		nperlin3= new Noise.Perlin3(gres, gres, 1<<4, seed);
		nworley2= new Noise.Worley2(32);
		
		perlin2= new float[res][res];
		perlin3= new float[res3d][res3d][zres];
		worley= new float[res][res];

		makePerlin();
	}
	
	public static void initGL(){
		Profiler.start("init Noise Textures");
		RenderCore.checkError();
		
		if(texNoises!=null)
			texNoises.delete();
		texNoises= new Texture(0, GL30.GL_TEXTURE_2D_ARRAY, GL31.GL_R8_SNORM, GL11.GL_RED);
		texNoises.allocate(res,res,2);
		
		if(texPerlin3d!=null)
			texPerlin3d.delete();
		texPerlin3d= new Texture(0, GL12.GL_TEXTURE_3D, GL31.GL_R8_SNORM, GL11.GL_RED);
		texPerlin3d.allocate(res3d, res3d, zres);
		
		int r= res*res > res3d*res3d*zres ? res*res : res3d*res3d*zres;
		DBO.preBufferWrite(r*4*3);
		makePerlinTex();
		makeWorleyTex();
		
		Profiler.stop();
	}

	public static void makeWorleyTex(){
		texNoises.bind();
		DBO.buf.clear();
		for(int y=0; y!=res; y++)
			DBO.buf.asFloatBuffer().put(worley[y]);
		DBO.buf.limit(res*res*4);
		GL12.glTexSubImage3D(GL30.GL_TEXTURE_2D_ARRAY, 0, 0, 0, 1, res, res, 1, GL11.GL_RED, GL11.GL_UNSIGNED_BYTE, DBO.buf);
		worley= null;
	}
	
	public static void makePerlin(){
		float d= 1f/res;
		for(float x=0; x<1; x+=d){
			int ix= (int)(x*res);
			for(float y=0; y<1; y+=d){
				int iy= (int)(y*res);
				perlin2[ix][iy]= nperlin2.get(x,y,0);
			}
		}
		
		d= 1f/res3d;
		float dz= 1f/zres;
		for(float x=0; x<1; x+=d){
			int ix= (int)(x*res3d);
			for(float y=0; y<1; y+=d){
				int iy= (int)(y*res3d);
				for(float z=0; z<1; z+=dz){
					int iz= (int)(z*zres);
					perlin3[ix][iy][iz]= nperlin3.get(x,y,z);
				}
			}
		}

	}
	public static void makePerlinTex(){
		texNoises.bind();
		
		DBO.buf.clear();
		for(int y=0; y!=res; y++)
			for(int x=0; x!=res; x++)
				DBO.buf.putFloat(perlin2[x][y]);
		DBO.buf.limit(res*res*4);
		DBO.buf.position(0);
		GL12.glTexSubImage3D(GL30.GL_TEXTURE_2D_ARRAY, 0, 0, 0, 0, res, res, 1, GL11.GL_RED, GL11.GL_FLOAT, DBO.buf);
		
		texPerlin3d.bind();
		DBO.buf.clear();
		for(int z=0; z!= zres; z++)
			for(int y=0; y!=res3d; y++)
				for(int x=0; x!=res3d; x++)
					DBO.buf.putFloat(perlin3[x][y][z]);
		DBO.buf.limit(res3d*res3d*zres);
		DBO.buf.position(0);
		GL12.glTexSubImage3D(GL12.GL_TEXTURE_3D, 0, 0, 0, 0, res3d, res3d, zres, GL11.GL_RED, GL11.GL_FLOAT, DBO.buf);
	}
	
	public static void makeSine(){
		if(texSin2D!=null)
			texSin2D.delete();
		texSin2D= new Texture(GL31.GL_R16_SNORM,GL11.GL_RED);
		texSin2D.allocate(res, res);
		
		float[] dat= new float[res*res]; int ind=0;
		float x=0, y=0;
		float d= 1f/res;
		for(int i=0; i!=res; i++){
			for(int j=0; j!=res; j++){
				dat[ind++]=(float)( Math.cos(x*Math.PI*2) * Math.cos(y*Math.PI*2) );
				x+= d;
			}
			x=0;
			y+= d;
		}
		texSin2D.push(dat);
	}
}
