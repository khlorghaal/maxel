package gfx.texture;

import gfx.DBO;
import gfx.RenderCore;
import gfx.Shapes;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.ByteBuffer;

import javax.imageio.ImageIO;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL14;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;
import org.lwjgl.opengl.GL32;

import core.Profiler;
import core.Settings;
import de.matthiasmann.twl.utils.PNGDecoder;

public class Texture {
//	/**The assignment of texture units for their purposes during different phases*/
	public static final int BASE= GL13.GL_TEXTURE0,
//			//rows are groupings used in a single shader
			RGB=0, MATERIAL=1, NORMAL=2;//deferred, lighting phase
	
	public int ptr;
	private int texunit;
	
	private final int internalformat, format;
	
	protected final int type;
	/**width, height, depth. must be 1 if that dim is unusued*/
	public int w,h,d;
	
	
	/**@param textunit
	 * @param type GL_TEXTURE_* */
	public Texture(int texunit, int type, int internalformat, int format){
		this.ptr= GL11.glGenTextures();
		this.texunit= texunit+BASE;	
		this.type= type;
		this.internalformat= internalformat;
		w=h=d=1;
		this.format= format;
		bind();
		GL11.glTexParameteri(type, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_NEAREST);
		GL11.glTexParameteri(type, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_NEAREST);
	}
	public Texture(int internalformat, int format){
		this(0, GL11.GL_TEXTURE_2D, internalformat, format);
	}
	
	/**autoloads 2d from file*/
	public Texture(String filename, int texunit){
		this(texunit, GL11.GL_TEXTURE_2D, GL11.GL_RGBA8, GL11.GL_RGBA);
		String filenam= "image\\"+filename+".png";
		try {
			PNGDecoder dec= new PNGDecoder(new FileInputStream(new File(filenam)));
			w= dec.getWidth();
			h= dec.getHeight();
			d=1;
			DBO.preBufferWrite(w*h*4);
			DBO.buf.clear();
			dec.decode(DBO.buf, w*4, PNGDecoder.Format.RGBA);
			DBO.buf.clear();
			GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, internalformat, w, h, 0, GL11.GL_RGBA, GL11.GL_UNSIGNED_BYTE, DBO.buf);
			mapmips();
		}catch (FileNotFoundException e) {
			System.err.println("Cannot find "+filenam);
			this.ptr= NULLTEX.ptr;
			this.texunit= NULLTEX.texunit; 
			return;
		} catch (IOException e){e.printStackTrace();}
	}
	public static final Texture NULLTEX= new Texture("unfound", 0);
	
	private boolean allocated= false;
	/**Allocates storage for a texture, contents are undefined
	 * @param components number of rgba
	 * @param componentsize in bytes of each component*/
	public void allocate(){
		bind();
		//cant use gltexstorage because it requires mipmap count
		//also high api level, and this works fine
		if(d<=1){
			if(h<=1)
				GL11.glTexImage1D(type, 0, internalformat, w, 0, format, GL11.GL_UNSIGNED_BYTE,(ByteBuffer)null);
			else
				GL11.glTexImage2D(type, 0, internalformat, w, h, 0, format, GL11.GL_UNSIGNED_BYTE,(ByteBuffer)null);
		}
		else
			GL12.glTexImage3D(type, 0, internalformat, w, h, d, 0, format, GL11.GL_UNSIGNED_BYTE,(ByteBuffer)null);
		
		RenderCore.checkError();
		allocated= true;
	}
	public void allocate(int w, int h, int d){
		this.w=w;
		this.h=h;
		this.d=d;
		allocate();
	}public void allocate(int w, int h){
		allocate(w, h, 1);
	}
		
	public void push(int datatype, ByteBuffer data){
		if(!allocated){
			System.err.println("Texture not allocated");
			return;
		}			
		bind();
		if(d<=1){
			if(h<=1)
				GL11.glTexSubImage1D(type, 0, 0,w, format, datatype, data);
			else
				GL11.glTexSubImage2D(type, 0, 0,0,w,h, format, datatype, data);
		}
		else
			GL12.glTexSubImage3D(type, 0, 0,0,0,w,h,d, format, datatype, data);
	}
	public void push(byte[] data){
		DBO.preBufferWrite(data.length);
		DBO.buf.clear();
		DBO.buf.put(data);
		DBO.buf.flip();
		push(GL11.GL_UNSIGNED_BYTE, DBO.buf);
	}
	public void push(float[] data){
		DBO.preBufferWrite(data.length*4);
		DBO.buf.clear();
		DBO.buf.asFloatBuffer().put(data);
		DBO.buf.limit(data.length*4);
		push(GL11.GL_FLOAT, DBO.buf);
	}
	public void push(int[] data){
		DBO.preBufferWrite(data.length*4);
		DBO.buf.clear();
		DBO.buf.asIntBuffer().put(data);
		DBO.buf.limit(data.length*4);
		push(GL11.GL_INT, DBO.buf);
	}
	
	public void bind(){
		GL13.glActiveTexture(texunit);
		GL11.glBindTexture(type, ptr);
	}
	public void bind(int texunit){
		GL13.glActiveTexture(BASE+texunit);
		GL11.glBindTexture(type, ptr);		
	}
		
	public void mapmips(){
		mapmips(Settings.graphics.mipmaps);
	}
	public void mapmips(int maxlevel){
		bind();
		GL11.glTexParameteri(type, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_NEAREST_MIPMAP_NEAREST);
		GL11.glTexParameteri(type, GL12.GL_TEXTURE_BASE_LEVEL, 0);
		GL11.glTexParameteri(type, GL12.GL_TEXTURE_MAX_LEVEL, maxlevel);
		GL11.glTexParameterf(type, GL14.GL_TEXTURE_LOD_BIAS, 0f);
		GL30.glGenerateMipmap(type);
	}
	
	public void setMagLinear(boolean yes){
		bind();
		if(yes){
			GL11.glTexParameteri(type, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_LINEAR);
		}
		else{
			GL11.glTexParameteri(type, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_NEAREST);
		}
	}
	
	public void render(){
		bind();
		RenderCore.progunlit.use();
		Shapes.quad();
	}
	public void renderLayer(int layer){
		bind();
		RenderCore.progunlitarray.use();
		GL20.glUniform1i(RenderCore.progunlitarray.uloc("layer"), layer);
		GL11.glDisable(GL11.GL_DEPTH_TEST);
		Shapes.quad();
	}
	public void renderSlice(float z){
		bind();
		RenderCore.progunlit3D.use();
		GL20.glUniform1f(RenderCore.progunlit3D.uloc("layer"), z);
		GL11.glDisable(GL11.GL_DEPTH_TEST);
		Shapes.quad();		
	}
	
	public int fbo= 0;
	public void bind_framebuffer(){
		if(fbo==0){
			fbo= GL30.glGenFramebuffers();
			GL30.glBindFramebuffer(GL30.GL_FRAMEBUFFER, fbo);
			GL32.glFramebufferTexture(GL30.GL_FRAMEBUFFER, GL30.GL_COLOR_ATTACHMENT0, ptr, 0);
		}
		GL30.glBindFramebuffer(GL30.GL_FRAMEBUFFER, fbo);		
		GL11.glViewport(0, 0, w, h);
	}
	public void delete_framebuffer(){
		if(fbo!=0)
			GL30.glDeleteFramebuffers(fbo);
		fbo= 0;
	}
	
	public void save(final String filename){
		bind();
		DBO.preBufferWrite(w*h*d*4);
		DBO.buf.clear();
		GL11.glGetTexImage(type, 0, GL11.GL_RGBA8, GL11.GL_UNSIGNED_BYTE, DBO.buf);
		DBO.buf.flip();
		final byte[] bytes= new byte[DBO.buf.remaining()];
		DBO.buf.get(bytes);
		new Thread(new Runnable(){
			@Override
			public void run(){
				try{
					Profiler.start("save texture "+filename);
					BufferedImage bimg= new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB);
					int[] argb= new int[bytes.length*4];
					for(int i=0; i!=bytes.length; i+=4){
						argb[i  ]= bytes[i+3];
						argb[i+1]= bytes[i  ];
						argb[i+2]= bytes[i+1];
						argb[i+3]= bytes[i+2];
					}
					bimg.setRGB(0, 0, w, h, argb, 0, w*h);
					ImageIO.write(bimg, "PNG", new File(filename+".png"));
					Profiler.stop();
				}catch(IOException e){e.printStackTrace();}
			}
			
		});
	}
	
	public void delete(){
		GL11.glDeleteTextures(ptr);
		delete_framebuffer();
	}


	public void enableClamp(){
		bind();
		GL11.glTexParameteri(type, GL11.GL_TEXTURE_WRAP_S, GL11.GL_CLAMP);
		GL11.glTexParameteri(type, GL11.GL_TEXTURE_WRAP_T, GL11.GL_CLAMP);
	}
	public void enableWrap(){
		bind();
		GL11.glTexParameteri(type, GL11.GL_TEXTURE_WRAP_S, GL11.GL_REPEAT);
		GL11.glTexParameteri(type, GL11.GL_TEXTURE_WRAP_T, GL11.GL_REPEAT);
	}
	public void enableWrapMirrored(){
		bind();
		GL11.glTexParameteri(type, GL11.GL_TEXTURE_WRAP_S, GL14.GL_MIRRORED_REPEAT);
		GL11.glTexParameteri(type, GL11.GL_TEXTURE_WRAP_T, GL14.GL_MIRRORED_REPEAT);
	}
}
