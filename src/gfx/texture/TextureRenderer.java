package gfx.texture;

import gfx.RenderCore;

import java.nio.FloatBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;

public class TextureRenderer{
		Texture t;
		int vbo, vao;
		public TextureRenderer(){
			vbo= GL15.glGenBuffers();
			GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vbo);
			GL15.glBufferData(GL15.GL_ARRAY_BUFFER, 16*4, GL15.GL_DYNAMIC_DRAW);
			
			vao= GL30.glGenVertexArrays();
			GL30.glBindVertexArray(vao);
			GL20.glEnableVertexAttribArray(0);
			GL20.glEnableVertexAttribArray(1);
			GL20.glVertexAttribPointer(0, 2, GL11.GL_FLOAT, false, 8, 0);//non interleaved
			GL20.glVertexAttribPointer(1, 2, GL11.GL_FLOAT, false, 8, 32);			
		}
		public void destroy(){
			GL15.glDeleteBuffers(vbo);
			GL30.glDeleteVertexArrays(vao);
		}
		
		public void set(Texture t){
			this.t= t;
		}

		static final float[] uvs= new float[]{
			0,0,
			1,0,
			0,1,
			1,1
		};
		public void setVerts(float[] verts){
				FloatBuffer fbuf= BufferUtils.createFloatBuffer(16);
				fbuf.put(verts);
				fbuf.put(uvs);
				fbuf.clear();
				GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vbo);
				GL15.glBufferSubData(GL15.GL_ARRAY_BUFFER, 0, fbuf);
		}
		
		public void render(){
			if(t==null)
				return;
			t.bind(0);
			RenderCore.progunlit.use();
			GL30.glBindVertexArray(vao);
			GL11.glDrawArrays(GL11.GL_TRIANGLE_STRIP, 0, 4);
		}
	
}
