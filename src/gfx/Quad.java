package gfx;

import java.nio.ByteBuffer;

/**Should only be used for particles. 
 * Objects consisting of more than a quad should manage their own vbo*/
public class Quad extends QuadRenderer{
	
	//XY is global coords, shaders do all transformations
	protected double x,y;
	protected int u,v,h;
	protected float scale,theta;
	protected int layer;
	protected int listindx;
	
	public Quad(int layer, double x, double y, int u, int v, int h, float scale){
		this.layer= layer;
		
		this.x= x;
		this.y= y;
		this.u= u;
		this.v= v;
		this.h= h;
		this.scale= scale;
		
		this.listindx= quads.size();
		quads.add(this);
		this.theta=0;
	}
	public Quad(int layer, double px, double py, int u, int v, int h, float scale, float rotation){
		this(layer, px,py, u,v,h, scale);
		this.theta= rotation;
	}
	
	public void addToBuffer(ByteBuffer buff){
		final int p= buff.position();
//		
		buff.asDoubleBuffer().put(new double[]{x,y});
		buff.position(p+8*2);
		buff.asIntBuffer().put(new int[]{u,v,h});
		buff.position(p+8*2+4*3);
		buff.asFloatBuffer().put(new float[]{scale,theta});
		buff.position(p+8*2+4*3+4*2);
	}
	
	public void setLayer(int l){
		
	}
	
	public void setPosition(float x, float y){
		
	}
}