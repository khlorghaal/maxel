package gfx;

import java.util.Random;

import math.Transformation;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;

import world.World;
import core.Client;

public class WeatherRenderer{
	public static void initGL(){
		RainRenderder.initGL();
		SnowRenderer.initGL();
		LightningRenderer.initGL();
	}
	
	public static class RainRenderder{
		public static final int RAINCOUNT= 0x100000;
		
		static GLProgram prog;
		static int vao;
		private static void initGL(){
			prog= new GLProgram("rain");

			vao= GL30.glGenVertexArrays();
			GL30.glBindVertexArray(vao);
			
			int vbo= GL15.glGenBuffers();
			GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vbo);
			DBO.preBufferWrite(RAINCOUNT*2*2);
			Random r= new Random();
			for(int i=0; i!=RAINCOUNT; i++){
				DBO.buf.putShort((short)r.nextInt());
				DBO.buf.putShort((short)r.nextInt());
			}
			DBO.buf.flip();
			GL15.glBufferData(GL15.GL_ARRAY_BUFFER, DBO.buf, GL15.GL_STATIC_DRAW);
			
			GL20.glEnableVertexAttribArray(0);
			GL20.glVertexAttribPointer(0, 2, GL11.GL_SHORT, true, 2*2, 0);
		}
		
		public static void render(){
			prog.use();
			float t= ((System.currentTimeMillis()%1000000)+RenderCore.pT)/1000000f;
			GL20.glUniform1f(prog.uloc("t"), 100*t);
			Client.cam.setUniform(new Transformation(), prog.uloc("mvp"));
			GL20.glUniform1f(prog.uloc("scale"), (float)Client.cam.getScale());
			GL20.glUniform1i(prog.uloc("count"), RAINCOUNT);
			GL30.glBindVertexArray(vao);
			GL11.glDrawArrays(GL11.GL_POINTS, 0, RAINCOUNT);
		}
	}
	
	public static class SnowRenderer{
		static GLProgram prog;
		private static void initGL(){
			prog= new GLProgram("snow");
			
		}		
	}
	
	public static class LightningRenderer{
		static GLProgram prog;
		private static void initGL(){
			prog= new GLProgram("lightning");
			
		}
	}
}
