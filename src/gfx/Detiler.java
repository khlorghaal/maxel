package gfx;


public class Detiler{
	
	/**Takes an atlas and rearranges it into an array of tiles of twh
	 * @param in the full raster
	 * @param inwh wh of the raster in pixels
	 * @param twh wh of a tile
	 * @param wh of the raster in tiles*/
	public static byte[] DetileRGBA(byte[] in, int inw, int inh, int tw, int th){
		byte[] ret= new byte[in.length];
		
		int i=0, x=0, y=0;
		while(i<in.length){
			
			ret[i++]= in[(x+inw*y)*4    ];
			ret[i++]= in[(x+inw*y)*4 + 1];
			ret[i++]= in[(x+inw*y)*4 + 2];
			ret[i++]= in[(x+inw*y)*4 + 3];
			x++;

			if((x%tw)==0){//endrow of tile
				x-=tw;
				y++;
				if((y%th)==0){//end of tile
					if(x==inw-tw){//end of tilerow
						x=0;
					}
					else{
						x+=tw;
						y-=th;
					}
				}
			}
		}
		return ret;
	}
}
