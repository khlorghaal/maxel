package module;

import world.SpawnData;
import entity.Entity;
import gfx.Renderer;

public class EntityModuleAnchor extends Entity{

	public EntityModuleAnchor(SpawnData spawndata){
		super(spawndata);
	}
	
	@Override
	public void onUpdatePre(){
		
	}
	
	@Override
	public Renderer constructRenderer(){
		Renderer r= moduleget(0).renderer;		
		return r;
	}
}
