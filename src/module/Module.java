package module;

import java.io.DataInputStream;
import java.io.DataOutputStream;

import math.Transformation;
import core.Saveable;
import core.UpdateBus.Updateable;
import entity.Aim;
import entity.Buttons;
import entity.Entity;
import gfx.ModuleRenderer;
import gfx.RenderCore;
import gfx.Renderer;

public abstract class Module implements Saveable, Updateable{
	protected Entity attached;
	public Transformation trans(){ return attached.trans(); }
	protected boolean active;
	
	public String name;
	protected Aim aim;
	protected Buttons buttons;
	public Module(Entity attached, Aim aim, Buttons buttons){
		assert(attached!=null);
		this.attached= attached;
		attached.moduleAdd(this);
		this.aim=aim;
		this.buttons= buttons;
	}
	public synchronized final void destroy(){
		attached.moduleRemove(this);
		onDestroy();
	}
	protected void onDestroy(){}
	
	public synchronized final void use(boolean state){
		if(state){
			if(!active)
				startUse();
		}
		else{
			if(active)
				stopUse();
		}
	}
	protected final void startUse(){
		active= true;
		startAction();
	}
	protected final void stopUse(){
		active= false;
		stopAction();
	}
	protected void startAction(){}
	protected void stopAction(){}

	@Override
	public synchronized final void update(){
		updateAction();
		if(active)
			activeUpdateAction();
	}
	protected void updateAction(){}
	protected void activeUpdateAction(){}
	
	public double getVolume(){return 0;}
	public double getMass(){return 0;}
	
	/**remove from world*/
	public synchronized void store(){
		stopUse();
	}
		
	@Override
	public abstract void save(DataOutputStream out);
	public static Module load(DataInputStream in){
		return null;
	}
	
	public Renderer renderer= null;
	{
		RenderCore.exec.execute(new Runnable(){
			@Override
			public void run(){
				renderer= makeRenderer();
			}
		});
	}

	public Renderer makeRenderer(){
		return new ModuleRenderer(this);		
	}
	
	
	
	@Override
	public String toString(){
		return this.getClass().getSimpleName().replace("Tool", "");
	}
}
