package module.adminhax;

import java.io.DataOutputStream;

import module.Module;
import entity.Aim;
import entity.Buttons;
import entity.Entity;

public class Gravitator extends Module{

	public Gravitator(Entity attached, Aim aim, Buttons buttons){
		super(attached, aim, buttons);
	}
	
	@Override
	public void startAction(){
		Entity e= attached.world.getEntityAtPoint(aim.p, 0);
		if(e!=null)
			e.kin.gravimetry= !e.kin.gravimetry;
	}

	@Override
	public void save(DataOutputStream out){
		// TODO Auto-generated method stub
		
	}
	
}
