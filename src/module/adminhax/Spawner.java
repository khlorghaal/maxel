package module.adminhax;

import java.io.DataOutputStream;

import material.Chemistry.Quantity;
import material.Material;
import math.Transformation;
import module.Module;
import world.SpawnData;
import entity.Aim;
import entity.Buttons;
import entity.Entity;
import entity.EntityDust;

public class Spawner extends Module{
	Material mat;
	double volume;
	double angle;
	public Spawner(Material mat, Entity attached, Aim aim, Buttons buttons){
		super(attached, aim, buttons);
		this.mat= mat;
		this.volume= 1;
	}
	//TODO constructor takes spawn byte[] allowing for dynamy

	
	@Override
	public void startAction(){
		Transformation t= new Transformation(aim.p);
		t.setScale(Math.sqrt(volume));
		new Entity(new SpawnData(attached.world, t), mat);
	}
	
	@Override
	public void save(DataOutputStream out){
		
	}
	
	public static class SpawnerDust extends Module{
		Quantity q;
		public SpawnerDust(Quantity q, Entity attached, Aim aim, Buttons buttons){
			super(attached, aim, buttons);
			this.q= q;
		}

		@Override
		public void startAction(){
			Transformation t= new Transformation(aim.p);
			t.setScale(Math.sqrt(q.amount));
			new EntityDust(new SpawnData(attached.world, t), q);
		}

		@Override
		public void save(DataOutputStream out){			
		}
	}
}
