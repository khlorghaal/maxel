package module.adminhax;

import java.io.DataOutputStream;

import math.vec2;
import module.Module;
import entity.Aim;
import entity.Buttons;
import entity.Entity;

public class Dragger extends Module{
	Entity grabbed;
	vec2 graboff;
	public Dragger(Entity attached, Aim aim, Buttons buttons){
		super(attached, aim, buttons);
	}

	@Override
	public void startAction(){
		grabbed= attached.world.getEntityAtPoint(aim.p, 0);
		if(grabbed!=null){
			graboff= grabbed.trans().t().sub(aim.p);
		}
		else
			this.active= false;
	}
	@Override
	public void activeUpdateAction(){
		grabbed.trans().translate( aim.p.dup().add(graboff));
	}
	
	@Override
	public void save(DataOutputStream out){
		
	}
	
}
