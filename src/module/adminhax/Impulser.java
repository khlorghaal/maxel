package module.adminhax;

import java.io.DataOutputStream;

import math.Transformation;
import math.vec2;
import module.Module;
import entity.Aim;
import entity.Buttons;
import entity.Entity;

public class Impulser extends Module{
	Entity grabbed;
	vec2 clickedl;
	public boolean rotational= false;
	public double magnitude= .001;
	public Impulser(Entity attached, Aim aim, Buttons buttons){
		super(attached, aim, buttons);
	}
	

	@Override
	public void startAction(){
		grabbed= attached.world.getEntityAtPoint(aim.p, 0);
		if(grabbed!=null){
			vec2 dg= aim.p.dup();
			Transformation t= grabbed.trans().inverse();
			clickedl= t.use(dg);
		}
		else
			this.active= false;
	}
	@Override
	public void activeUpdateAction(){
		if(grabbed==null){
			active= false;
			return;
		}	
		vec2 grabg= grabbed.trans().use(clickedl);
		vec2 j= aim.p.dup().sub(grabg);
		j.mul(magnitude);
		if(rotational){
			grabbed.kin.impulseGlobal(clickedl, j);
		}
		else{
			double m= grabbed.kin.m;
			grabbed.kin.v.add(j.dup().div(m));
		}
	}
	
	@Override
	public void save(DataOutputStream out){
		
	}
	
}
