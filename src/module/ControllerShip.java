package module;

import java.util.Hashtable;
import java.util.Map;

import math.vec2;
import module.Thruster.ThrusterGroup;
import world.Controlled;

public class ControllerShip implements Controlled{
	ThrusterGroup thr;
	public ControllerShip(ThrusterGroup thr){
		this.thr= thr;
	}
	
	
	public Map<Integer, Module> mappings= new Hashtable<>();
	@Override
	public void setButton(int button, boolean state){
		Module got= mappings.get(button);
		if(got!=null)
			got.use(state);
	}
	@Override
	public void setMouse(vec2 g, int dw){
		thr.mouse(g, dw);
	}
	
}
