package module;

import java.io.DataOutputStream;

import material.Material;
import math.Maths;
import math.Transformation;
import math.vec2;
import world.World;
import entity.Aim;
import entity.Buttons;
import entity.Entity;

public class Gun extends Module{
	
	public boolean auto= true;
	public int cd, cdr=1;
	public double lx, ly= 5;
	public Gun(Entity attached, Aim aim, Buttons buttons){
		super(attached, aim, buttons);
	}
	
	@Override
	protected void startAction(){
		active= auto;
		if(!auto){
			fire();
		}
	}
	
	@Override
	public void updateAction(){
		cd--;
		//cd may be <0 because why not
	}
	
	@Override
	public void activeUpdateAction(){
		tryFire();
	}
	
	public void tryFire(){
		if(ready()){
			onFire();
			fire();
		}
	}
	public boolean ready(){
		System.out.println(cd);
		cdr= 10;
		return cd<1;		
	}
	void onFire(){
		cd= cdr;
	}
	public void fire(){
		Entity b= getBullet();
		Transformation bt= b.trans();
		Transformation trans= attached.trans().clone();
		
		trans.translateRelative(lx, ly);
		bt.tx= trans.tx;
		bt.ty= trans.ty;
		bt.theta= trans.theta;
		bt.scale(1);
		
		double m= b.kin.m*10;
		vec2 n= aim.p.sub(trans.t()).nrm();
		vec2 j= n.dup();
		b.kin.v.add(j.dup().div(m));
		//attached.kin.impulse(lx, ly, 0, j);
		attached.kin.impulseGlobal(trans.t(), j);
		
		World.instance.addEntity(b);
	}
	
	public Entity getBullet(){
		Entity e= new Entity(null);
		e.chunk.root.set(Material.get("wood"));
		e.trans().setScale(.1);
		e.kin.m= 1;
		return e;
	}
	
	@Override
	public void save(DataOutputStream out){
		// TODO Auto-generated method stub
		
	}
	
}
