package module;

import java.io.DataOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

import material.Chemistry.Quantity;
import world.SpawnData;
import core.Saveable;
import core.Selector;
import entity.Entity;
import entity.EntityDust;

public final class Inventory implements Saveable{
	public double maxVolume=-1;
	private double volume, mass;
	public double getMass(){return mass;}
	public double getVolume(){return volume;}
	
	public final ArrayList<Entity> contents= new ArrayList<>();
	private int slotcount, w;
	{
		pad();
	}
	void pad(){
		w=8;
		slotcount= w*w;
		while(contents.size()<slotcount)
			contents.add(null);
	}
	public Selector selector= new Selector(slotcount, w);
	
	public Inventory(double maxVolume){
		this.maxVolume= maxVolume;
	}
	
	
	private int getFirstEmptySlot(){
		int ret=0;
		int siz= contents.size();
		while(true){
			if(ret==siz){
				contents.add(null);
				break;
			}
			if(contents.get(ret)==null)
				break;
			ret++;
		}
		return ret;
	}
	
	public int put(Entity e){
		if(e instanceof EntityDust)
			return put((EntityDust)e);
		
		double nv= volume+e.chunk.getVolume(-1);
		if(nv<=maxVolume){
			volume= nv;
			int slot= getFirstEmptySlot();
			contents.set(slot, e);
			if(e.world!=null)
				e.store();
			return slot;
		}
		return -1;
	}
	public int put(EntityDust e){
		Quantity[] qs= e.quantities.toArray(new Quantity[0]);
		if(qs.length>1)
			Arrays.sort(qs, new Comparator<Quantity>(){
				@Override
				public int compare(Quantity a, Quantity b){
					return (int)(a.amount-b.amount);
				}
			});
		for(int i=0; i!=qs.length; i++){
			double nv= volume+ qs[i].amount;
			if(nv<=maxVolume){
				volume= nv;
				int slot= getFirstEmptySlot();
				contents.set(slot, e);
				if(e.world!=null && i>=qs.length-1)
					e.store();
				return slot;//TODO merging				
			}
			return -1;
		}
		return -1;
	}
	public int put(Entity e, int slot){
		double nv= volume+e.chunk.getVolume(-1);
		if(nv<=maxVolume){
			volume= nv;
			
			//move the occupant to the end
			//this is better than shifting, as shifting will invalidate binds
			Entity occupying= contents.get(slot);
			if(occupying!=null)
				contents.set(getFirstEmptySlot(), occupying);
			
			contents.set(slot, e);
			e.store();
			return slot;
		}
		return -1;		
	}
	
	public Entity withdrawSelected(){
		if(!selector.active)
			return null;
		return withdraw(selector.pos());
	}
	/**Does not unstore item. The caller must do so.*/
	public Entity withdraw(int slot){
		Entity e= contents.get(slot);
		contents.set(slot, null);
		return e;
	}
	
	public Entity eject(int slot, SpawnData sd){
		//TODO
		Entity ret= contents.get(slot);
		if(ret==null)
			return null;
		contents.set(slot, null);
		ret.unstore(sd);
		return ret;
	}
	
	
	
	@Override
	public void save(DataOutputStream b) throws IOException{
		b.writeDouble(maxVolume);
		b.writeDouble(volume);
		for(Entity e: contents)
			e.save(b);
	}
	public static Inventory load(ByteBuffer b){
		return null;//TODO
	}
}
