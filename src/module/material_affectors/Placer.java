package module.material_affectors;

import java.io.DataOutputStream;

import material.Material;
import entity.Aim;
import entity.Buttons;
import entity.Entity;

public class Placer extends MaterialAffector{
	public int mid;
	public Placer(Material material, Entity attached, Aim aim, Buttons buttons){
		this(material.id, attached, aim, buttons);
	}
	public Placer(int mid, Entity attached, Aim aim, Buttons buttons){
		super(attached, aim, buttons);
		this.mid= mid;
	}
	@Override
	protected void activeUpdateAction(){
		if(ableToPlace())
			place();
	}
	protected boolean ableToPlace(){
		return focused!=null;
				//&& (align.aligned.chunk.getID(aim.x, aim.y)==0);
	}
	protected void place(){
		if(depth>-1)
			align.aligned.chunk.setg(mid, aim.p, depth);
		else
			align.focusedNode.set(mid);
	}
	@Override
	public int getGhostNodeID(){
		return mid;
	}
	@Override
	public void save(DataOutputStream out){
		// TODO Auto-generated method stub
		
	}
}