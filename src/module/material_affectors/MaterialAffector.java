package module.material_affectors;

import material.Chunk;
import material.Chunk.Node;
import module.Module;
import ui.EntityAlignment;
import ui.KeyMap;
import entity.Aim;
import entity.Buttons;
import entity.Entity;


public abstract class MaterialAffector extends Module{
	public EntityAlignment align= new EntityAlignment();
	Node focused= null;
	public int depth=0;
	int pw;
	
	public MaterialAffector(Entity attached, Aim aim, Buttons buttons){
		super(attached, aim, buttons);
	}
	public Node makeGhostNode(){
		//create localized dummy node as to not affect the chunk
		return align.aligned.chunk.new Node(){
			@Override
			public void set(int id){
				this.typ= id;
			}
			{
				set(getGhostNodeID());
				depth= MaterialAffector.this.depth;
				scale= Chunk.depthToScale(
						depth!=-1?
								depth : align.focusedNode!=null? align.focusedNode.depth: 0);
				x= (float)align.l.x;
				y= (float)align.l.y;
				//snap to grid
				float gs= scale*2;
				float o= depth==0?.5f:0;
				x= (float)((Math.floor(x/gs+o)-o+.5f)*gs);
				y= (float)((Math.floor(y/gs+o)-o+.5f)*gs);
			}
		};
	}
	protected abstract int getGhostNodeID();
	
	@Override
	protected void updateAction(){
		int w= buttons.wheel;
		float dw= w-pw;
		pw= w;
		if(buttons.array[KeyMap.SHIFT]){
			if(dw!=0){
				depth-= dw;
				if(depth>Chunk.MAXIMUM_DEPTH)
					depth= Chunk.MAXIMUM_DEPTH;
				if(depth<-1)
					depth= -1;
			}
		}
		
		align.update(attached.world, aim.p);
		if(align.aligned!=null)
			focused= align.focusedNode;
		else
			focused= null;
	}
}
