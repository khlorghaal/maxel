package module.material_affectors;

import java.io.DataOutputStream;

import entity.Aim;
import entity.Buttons;
import entity.Entity;

public class Remover extends MaterialAffector{
	public Remover(Entity attached, Aim aim, Buttons buttons){
		super(attached, aim, buttons);
	}
	@Override
	protected void activeUpdateAction(){
		if(depth==-1 && align.focusedNode!=null)
			align.focusedNode.set(0);
		else if(align.aligned!=null)
			align.aligned.chunk.setg(0, aim.p, depth);
	}
	@Override
	public int getGhostNodeID(){
		return 0;
	}
	@Override
	public void save(DataOutputStream out){
		// TODO Auto-generated method stub
		
	}
}