package module.material_affectors;

import java.io.DataOutputStream;

import math.Maths;
import math.vec2;
import entity.Aim;
import entity.Buttons;
import entity.Entity;

public class Chisel extends MaterialAffector{
	vec2 pl= new vec2(0);
	static final int FREE=0, LOCKED=1;
	int mode= FREE;
	public Chisel(Entity attached, Aim aim, Buttons buttons){
		super(attached, aim, buttons);
	}
	@Override
	protected void stopAction(){
		if(focused!=null){
			if(mode==FREE){
				vec2 l= align.aligned.trans().use(aim.p);
				vec2 n= l.dup().sub(pl).nrm();
				
				pl= l.dup();
			}
			else if(mode==LOCKED){
				
			}
		}
	}
	@Override
	public int getGhostNodeID(){
		return align.focusedNode.getID();
	}
	@Override
	public void save(DataOutputStream out){
		// TODO Auto-generated method stub
		
	}
}