package module;

import java.util.ArrayList;

import math.Kinematic;
import math.vec2;

public class Thruster{
	public float f, th=0;
	protected Kinematic k;
	public Thruster(Kinematic k, float forceMax, float theta){
		this.f= forceMax;
		this.k= k;
		setTheta(theta);
	}
	private vec2 n;
	public void setTheta(float theta){
		n= new vec2( Math.cos(theta), Math.sin(theta) );
	}

	public void impulse(){
		
	}
	
	public static class ThrusterGroup{
		public ArrayList<Thruster> thrusters= new ArrayList<>();
		public final Kinematic k;
		public ThrusterGroup(Kinematic k){
			this.k= k;
		}
		
		public void mouse(vec2 g, int dw){
			
		}
		
		public void accel(float th){
			
		}
		public void strafe(float th){
			
		}
		public void yaw(float target){
			
		}
	}
}
