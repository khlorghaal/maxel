package ai;

import java.util.LinkedList;
import java.util.List;

public class Token{	
	
	
	String str;
	public Token(String str){
		this.str= str;
	}

	public static List<Token> tokenize(String str){
		final List<Token> tokens= new LinkedList<Token>();

		int i= 0;

		//pass over front spaces
		while(str.charAt(i)==' '){
			if(i== str.length())
				return tokens;
			i++;
		}
		
		char mark= str.charAt(i);

		while(i<str.length()){
			
			String substr="";
			while(mark!=' '){
				substr+= mark;
				mark= str.charAt(i++);
			}
			tokens.add(new Token(substr));
			
			while(mark==' '){//pass over middle spaces
				i++;
				if(i==str.length())
					return tokens;
				mark= str.charAt(i);
			}	
		}
		return tokens;
	}

	boolean isNoun(){
		return false;
	}
	boolean isCharacter(){
		return false;
	}

	boolean isVerb(){
		return false;		
	}
	boolean isTask(){
		return Task.names.contains(str);
	}
	boolean isBehavior(){
		return Behavior.names.contains(str);
	}

	boolean isAdverb(){
		return false;
	}
}