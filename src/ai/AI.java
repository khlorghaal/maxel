package ai;

import java.util.LinkedList;
import java.util.Queue;

import world.Controlled;

public class AI {

	public Controlled body;
	
	protected Queue<Task> tasks= new LinkedList<Task>();
	public AI(Controlled controlled) {
		this.body= controlled;
	}

	public void addtask(Task cmd){
		//TODO prioritization based off reputation etc
		tasks.add(cmd);
	}
	
	/**@return the next cmd able to be done*/
    protected Task nexttask(){
		if(tasks.size()==0){
			return null;
		}
		
		Task look= tasks.poll();
		
		final LinkedList<Task> rejects= new LinkedList<Task>();
		while(tasks.size()!=0){
			//look through tasks until one is able
			if(willing(look) && able(look))
				return look;
			else{
				look.timesRejected++;
				rejects.add(look);
				look= tasks.poll();
			}
		}
		return null;
	}
    
	protected boolean willing(Task cmd){
		return true;//TODO
	}
	protected boolean able(Task cmd){
		return true;//TODO
	}
}
