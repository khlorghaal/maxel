package entity;

import java.util.ArrayList;
import java.util.List;

import math.Transformation;
import entity.celestial.EntitySun;

public class Light {
	public static final List<Light> instances= new ArrayList<Light>();
	
	public Entity attached;
	public Transformation trans(){return attached.trans();}
	
	/**fade(x/A)*/
	public float z=1f;
	public float mag, a=1;
	
	public int shadowSamples;
	public float penumbra;
	public float[] color;
	public int layer;
		
	{
		setColor(1,1,1);
		setMagnitude(1);
		instances.add(this);
	}
//	public Light(){
//		trans= new Transformation();
//	}
	public Light(Entity attached){
		this.attached= attached;
	}
	public void remove(){
		instances.remove(this);
	}

	/**@param rgb recommend as clamped [0-1]*/
	public Light setColor(float r, float g, float b){
		this.color= new float[]{r,g,b};
		return this;
	}
	/**Sets absolute magnitude*/
	public Light setMagnitude(float mag){
		this.mag= mag;
		return this;
	}
	
	/**Sets scale such that intensity(dist)=magd, intensity(0)==mag
	 * @param dist distance being looked at
	 * @param magd magnitude at dist, must be less than mag*/
	public Light setScale(double dist, double magd){
		//intensity(dist)= magd/2 = mag*exp(1-pow1/2(1+pow2(dist/a)))
		double b=magd;
		b/=Math.abs(mag);
		b= Math.log(b);
		b-= 1;
		b*= b;
		b-=1;
		b= Math.sqrt(b);
		b/= dist;
		b= 1/b;
		
		a= (float)b;
		return this;
	}
	
	public static class LightStar extends Light{
		public static final ArrayList<Light> instances= new ArrayList<>();

		public LightStar(EntitySun attached, double height1){
			super(attached);
			instances.add(this);
			Light.instances.remove(this);
			setMagnitude(20);
			setScale(height1, 1);
		}
		@Override
		public void remove(){
			instances.remove(this);
		}
	}
}
