package entity;

import material.Material;
import material.Solution;
import math.Maths;
import world.SpawnData;

public class Liquid{
	public final Solution sol= new Solution();

	public Liquid(SpawnData sd, Material base){
		
	}
	
	//Each vertex
	public final class LiquidVertex extends Entity{
		public LiquidVertex left, right, down;
		public LiquidVertex(SpawnData spawndata){
			super(spawndata);
			// TODO Auto-generated constructor stub
		}
	
		final double maxDot= Math.cos( Maths.TAU/20 );
		
		boolean flowing= true;
		
		@Override
		public void onUpdatePre(){
			if(!flowing)
				return;
			
			if(Math.abs(getDot())>maxDot)
				fracture();
		}
		void fracture(){
			
		}
		
		public double getDot(){
			double dxL= this.trans().tx-left.trans().tx;
			double dyL= this.trans().ty-left.trans().ty;
			double l= Math.sqrt(dxL*dxL+dyL*dyL);
			dxL/= l;
			dyL/= l;
			double dxR= this.trans().tx-right.trans().tx;
			double dyR= this.trans().ty-right.trans().ty;
			l= Math.sqrt(dxR*dxR+dyR*dyR);
			dxR/= l;
			dyR/= l;
			return dxR*dxL + dyR*dyL;
		}
		
	}
}
