package entity.celestial;

import material.Planetology;
import world.SpawnData;

public class EntityPlanetoid extends EntityCelestial{
	
	Atmosphere a;
	Planetology p;
	public EntityPlanetoid(SpawnData sd, Planetology p){
		super(sd, p.g);
		a= new Atmosphere(this, world.sun);
		this.p= p;
		p.gen(chunk.root);
	}

	@Override
	public void onRemove(){
		super.onRemove();
		a.remove();
	}
}
