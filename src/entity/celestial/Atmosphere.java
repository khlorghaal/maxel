package entity.celestial;

import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

import math.Maths;
import entity.Entity;

public class Atmosphere{
	public static final Set<Atmosphere> instances= new CopyOnWriteArraySet<>();
	
	public Entity attached, sun;
	private final int rgbDay= 0x3C90EE;
	private final int rgbWeather= 0x1e2225;
	public float weather=1f;
	/**start and end of fade*/
	public int radius0, radius1;
	public Atmosphere(Entity attached, EntitySun sun){
		this.attached= attached;
		this.sun= sun;
		instances.add(this);
	}
	public void remove(){
		instances.remove(this);
	}
	
	public static void dragAll(){
		for(Atmosphere a :instances)
			a.drag();
	}
	public void drag(){
		//TODO
	}
	
	public float[] getSkyColor(){
		float[] d= Maths.rgbIntToFloat(rgbDay);
		float[] w= Maths.rgbIntToFloat(rgbWeather);
		float[] s= new float[3];
		for(int i=0; i!=3; i++){
			s[i]= Maths.lerp(weather, w[i], d[i]);
		}
		return s;
	}
	public float[] getDuskColor(float z){
		float[] ret= getSkyColor();
		ret[0]*= 1.5f*z*weather;
		ret[1]*= 0.0f*z*weather;
		ret[2]*= .25f*z*weather;
		return ret;
	}
	public float[] getDawnColor(float z){
		float[] ret= getSkyColor();
		for(int i=0; i!=3; i++)
			ret[i]= (1-ret[i])*weather*z;
		return ret;
	}
}