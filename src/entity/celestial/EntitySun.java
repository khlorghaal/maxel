package entity.celestial;

import world.SpawnData;
import entity.Light;
import entity.Light.LightStar;
import gfx.Renderer;
import gfx.SunRenderer;

public class EntitySun extends EntityCelestial{

	public LightStar l;
	public EntitySun(SpawnData sd, double gmag, double height1){
		super(sd, gmag);
		this.l= new Light.LightStar(this, height1);
	}
	@Override
	public void onRemove(){
		super.onRemove();
		l.remove();
	}
	
	@Override
	protected Renderer constructRenderer(){
		return new SunRenderer(this);
	}
}
