package entity.celestial;

import world.GravityWell;
import world.SpawnData;
import entity.Entity;

public class EntityCelestial extends Entity{
	public GravityWell g;
	public EntityCelestial(SpawnData sd, double gmag){
		super(sd);
		g= new GravityWell.GravityWellEntity(gmag, this);
	}
	
	@Override
	public void onRemove(){
		super.onRemove();
		g.remove();
	}
}
