package entity;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.util.HashSet;
import java.util.Set;

import math.Transformation;
import math.vec2;
import module.Inventory;
import world.SpawnData;
import world.World;
import core.Saveable;
import core.Ticker;
import core.UpdateBus.Updateable;

public class Hand implements Saveable{
	private Entity held; public Entity getHeld(){return held;}
	public boolean isEmpty(){ return held==null; }
	/**The inventory index held was retrieved from. 
	 * Will put back into that slot if it is empty*/
	private int grabbedFrom= -1;
	private Inventory defaultinv;
	
	public Entity body;
	public Hand(Entity body){
		this.body= body;
	}
	public Hand(Entity body, Inventory defaultinv){
		this.defaultinv= defaultinv;
		this.body= body;
	}
	
	public Transformation trans(){
		Transformation ret= body.trans().clone();
		ret.tx+=1;
		ret.ty-=1;
		return ret;
	}
	
	
	
	public static final int 
	INVOKE=0, 
	SWING=1, 
	JAB=2, 
	THROW=3, 
	EAT=4, 
	PUT=5,
	TAKE=6,
	GRAB=7,
	CYCLE=8,//only used for UI rendering 
	ASDF=9;
	public static final boolean[] persistence= new boolean[]{
		true,//invoke
		true,//swing
		true,//jab
		false,//throw
		false,//eat
		false,//put
		false,//take
		false,//grab
		false,//cycle
		true,//
	};
	public static final Set<Integer> 
	heldActions= new HashSet<>(),
	unheldActions= new HashSet<>();
	static{
		heldActions.add(INVOKE);
		heldActions.add(SWING);
		heldActions.add(JAB);
		heldActions.add(THROW);
		heldActions.add(EAT);
		heldActions.add(PUT);
		
		unheldActions.add(SWING);
		unheldActions.add(JAB);
		unheldActions.add(TAKE);
		unheldActions.add(GRAB);
	}
	
	public static int[] cycledActionsHeld= new int[]{
		INVOKE, SWING, JAB		
	};
	public static int[] cycledActionsUnheld= new int[]{
		GRAB, SWING, JAB
	};

	int action= GRAB,
	actionLastHeld= INVOKE, actionLastUnheld= GRAB;//last persistent action used
	boolean actionPersistent= false;
	public int getAction(){return action;}
	
	IHandActionListener actionListener= null;
	public void setActionListener(IHandActionListener l){ 
		this.actionListener= l;
		l.set(action);
	}
	public static interface IHandActionListener{ public void set(int i); }
	
	public void setAction(int action){
		boolean held= !isEmpty();
		
		final Set<Integer> a= held? heldActions:unheldActions; 
		if(!a.contains(action))
			action= held? actionLastHeld:actionLastUnheld;

		actionPersistent= persistence[action];
		if(actionPersistent){
			if(held)
				actionLastHeld= action;
			else
				actionLastUnheld= action;
		}
		this.action= action;
		
		if(actionListener!=null)
			actionListener.set(action);
	}
	
	void popAction(){
		//after using temporary action, returns to last persistent
		if(!isEmpty())
			setAction(actionLastHeld);
		else
			setAction(actionLastUnheld);
	}
	
	int cycle= 0, cycleUnheld= 0;
	public void cycleAction(){
		int a;
		if(!isEmpty()){
			if(++cycle >= cycledActionsHeld.length)
				cycle=0;
			a= cycledActionsHeld[cycle];
		}
		else{
			if(++cycleUnheld >= cycledActionsUnheld.length)
				cycleUnheld=0;
			a= cycledActionsUnheld[cycleUnheld];
		}
		setAction(a);
	}
	
	
	
	boolean using= false;
	/**Accepts duplicates*/
	public void use(boolean state){
		if(using==state)
			return;
		using= state;
		
		
		//TODO remove dead chunks, not here but on entity death
		//setHeld(null);
		
		switch(action){
			case INVOKE:
				invoke(state);
				return;
			case SWING:
				swing(state);
				return;
			case JAB:
				jab(state);
				return;
			case THROW:
				throw_(state);
				return;
			case EAT:
				eat(state);
				return;
			case PUT:
				if(state)
					put();
				return;
			case TAKE:
				if(state)
					take();
				return;
			case GRAB:
				if(state)
					grab();
				return;
		}
		popAction();
	}
	
	void invoke(boolean state){
		if(isEmpty())
			return;
		held.moduleStateAll(state);
	}
	void swing(boolean state){
		//TODO
	}
	void jab(boolean state){
		//TODO		
	}
	
	void throw_(boolean state){
		if(state)
			throw_= new Throw_(); 
		else if(throw_!=null){
			throw_.stop();
			throw_= null;
		}
	}
	private Throw_ throw_= null;
	private class Throw_ implements Updateable{
		float power= 0;
		Ticker ticker;
		Throw_(){
			ticker= new Ticker(this, World.instance.updateBus);			
		}
		public void stop(){
			ticker.destroy();
			ticker= null;
			
			Entity throwing= drop();
			if(throwing!=null){
				throwing.kin.v.y-= .05*power;
			}
			
			power= 0;
		}
		@Override
		public void update(){
			power+= .1f;
			System.out.println(power);
		}
	}
	void eat(boolean state){
		
	}
	
	
	
	/**@return slot put into*/
	int put(){ return put(defaultinv); }
	int put(Inventory inv){
		if(isEmpty())
			return -2;
		
		int put;
		if(grabbedFrom!=-1)
			put= inv.put(held, grabbedFrom);
		else
			put= inv.put(held);
		
		if(put!=-1){
			setHeld(null);
			return put;
		}
		return put;
	}

	/**From inventory*/
	public int take(){ return take(defaultinv, defaultinv.selector.pos() ); }
	/**From an inventory*/
	public int take(Inventory inv, int slot){
		int put= put();
		if(put==-1)//could not put in held
			return put;
		SpawnData sd= new SpawnData(body);
		sd.kin.trans.ty+= 2;
		sd.kin.trans.tx+= 2;
		setHeld(inv.eject(slot, sd));
		if(held==null)
			return -1;
		grabbedFrom= slot;
		grab(held);
		return put;
	}
	
//	public Entity grab(World world, double gx, double gy){
//		if(put()!=-2)
//			return null;
//		List<Entity> l= world.getEntitiesInLocation(gx, gy, 0);
//		if(l.size()!=0){
//			setHeld(l.get(0));
//			grab(held);
//			return held;
//		}
//		return null;
//	}	
	private Joint j= null;
	/**physically binds held to hand with a joint*/
	void grab(Entity e){
		defaultinv.selector.active= false;
		j= new Joint(body, held, new vec2(0), new vec2(4,0));
	}
	/**determines entity to be grabbed from hand's world position*/
	void grab(){
		defaultinv.selector.active= false;
		//TODO
	}
	
	/**releases joint*/
	public Entity drop(){
		if(isEmpty())
			return null;
		
		Entity ret= held;
		setHeld(null);
		j.destroy();
		return ret;
	}
	
	void setHeld(Entity e){
		held= e;
		if(isEmpty())
			setAction(actionLastUnheld);
		else
			setAction(actionLastHeld);
	}
	
	
	public static Hand load(DataInputStream in){
		return null;
	}
	@Override
	public void save(DataOutputStream out){
		
	}
}
