package entity;

import gfx.ExplosionRenderer;
import gfx.RenderCore;
import gfx.Renderer;

import java.util.ArrayList;

import material.Material;
import math.Kinematic;
import math.Maths;
import math.Transformation;
import math.vec2;
import world.SpawnData;
import world.World;

public class Explosion{
	public ArrayList<EntityShockwave> sws= new ArrayList<>();
	public Transformation trans;
	public long lastRenderTick=0;
	public int particleCount;
	
	public Renderer renderer= null;
	
	public static final int PARTICLES= 64;
	public Explosion(World world, vec2 g, vec2 v, 
			double ke){
		trans= new Transformation(g);
		final int particles= PARTICLES;
		this.particleCount= particles;
		
		double res= Maths.TAU/(particles);
		synchronized(this){
			for(int i=0; i<particles; i++){
				double a= i*res;
				Transformation t= new Transformation();
				double scale0= .1;
				//			double eta= scale0/7*particles;
				double eta= 4;
				//circ= tau*eta
				double mass= 1f/particles;//make sure mass isnt 0
				vec2 vi= new vec2(Math.cos(a), Math.sin(a));
				t.tx= vi.x*eta + g.x;
				t.ty= vi.y*eta + g.y;
				vi= v.dup().mul(ke);
				t.theta= a+Maths.TAU/8;
				t.setScale(scale0);
				
				Kinematic k= new Kinematic(t, 1);
				k.v.set(vi.dup().add(v));
				sws.add(new EntityShockwave(new SpawnData(world, k)));
			}
		}
		
		RenderCore.exec.execute(new Runnable(){	
			@Override
			public void run(){
				renderer= new ExplosionRenderer(Explosion.this);
			}
		});
	}
	
	
	public class EntityShockwave extends Entity{
		private final double mass0, scale0, vmagmin;
		private EntityShockwave(SpawnData sd){
			super(sd, Material.EXPLOSION);
			this.mass0= this.kin.m;
			this.scale0= trans().scaley;
			this.vmagmin= kin.v.len()/10;
		}
		vec2 displacement= new vec2(1);
		@Override
		public void onUpdatePre(){
			displacement.add(kin.v);
			//		trans.scale(1+displacement);
			kin.m= mass0/(displacement.len()+.00001);
			assert(kin.m!=0);
			
			if(lifetime>300 || kin.v.len()<vmagmin)
				destroy();
		}
		
		@Override
		public void onRemove(){
			synchronized(Explosion.this){
				sws.remove(this);
			}
		}
		
		@Override
		public Renderer constructRenderer(){
			if(Explosion.this.renderer==null){
				makeRenderer();
				//simply requeue instead of doing any wait/lock bs
			}
			return Explosion.this.renderer;
		}
	}
}
