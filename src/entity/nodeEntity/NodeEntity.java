package entity.nodeEntity;

import material.Chunk.Node;
import material.Material;

public abstract class NodeEntity{
	public Node n;
	public NodeEntity(Node n){
		this.n= n;
	}
	
	public abstract Material getMaterial();
	
	/**Altering a chunk does not affect NodeEntities.</br>
	 * This is to avoid perf issues with removing low depth parents.</br>
	 * Thus each NodeEntity must do checks to see if it 
	 * has been removed or altered*/
	public void checkIntact(){
		if(n.getMaterial()!=getMaterial())
			kill();
	}
	protected void kill(){}
}
