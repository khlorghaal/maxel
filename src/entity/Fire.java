package entity;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

import material.Chunk;
import material.Chunk.Node;

public class Fire{
	public final static List<Fire> instances= new CopyOnWriteArrayList<>();
	public final static Map<Chunk, Fire> map= new Hashtable<>();
	public static void updateAll(){
		for(Fire f : instances)
			f.update();
	}
	
	public final Chunk attached;
	public List<Node> affected= new ArrayList<>();
	
	private Fire(Chunk attached){
		this.attached= attached;
		instances.add(this);
		map.put(attached, this);
	}
	private void destroy(){
		instances.remove(this);
		map.remove(this);
	}
	
	
	private void update(){
		//heat change
		
		//burn node
	}
	
	public static void ignite(Chunk c, Node n){
		Fire f= map.get(c);
		if(f==null)
			f= new Fire(c);
		
		f.affected.add(n);
	}
	public static void extinguish(Chunk c, Node n){
		Fire f= map.get(c);
		if(f==null)
			return;
		
		f.affected.remove(n);
	}
}
