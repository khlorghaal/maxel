package entity;

import gfx.ChunkRenderer;
import gfx.RenderCore;
import gfx.Renderer;

import java.io.DataOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import material.Chunk;
import material.Chunk.Hit;
import material.Chunk.Push;
import material.Material;
import math.Kinematic;
import math.Transformation;
import math.vec2;
import module.Module;
import world.Controlled;
import world.SpawnData;
import world.World;
import core.Saveable;

public class Entity implements Saveable{
	public long id=-1;
	public World world;
	public int lifetime;
	
	public final Chunk chunk;
	protected Chunk makeChunk(){return new Chunk(this, Material.get("stone").id);}
	
	public Sprite sprite= null;

	public Kinematic kin;	
	public final Transformation trans(){return kin.trans;}
	public final void trans(Transformation t){
		kin.trans= t;
	}	
	
	public boolean collided, hasUpdated;
	public List<Entity> collisions= new ArrayList<>();
	public Push push, pushl;
	
	/**@param spawndata may be null to not spawn*/
	public Entity(SpawnData spawndata){
		if(spawndata==null){
			kin= new Kinematic(new Transformation(), 1);
			world= null;
		}
		else
			spawndata.apply(this);
		chunk= makeChunk();
		
		if(world!=null)
			world.addEntity(this);
		//id set by add
	}
	public Entity(SpawnData spawndata, Material fill){
		this(spawndata);
		this.chunk.root.set(fill.id);
	}
	private boolean destroyed= false;
	
	public void destroy(){
		removeFromWorld();
		destroyed= true;
		for(Joint j : joints.toArray(new Joint[0]))
			j.destroy();
		for(Module m : mods.toArray(new Module[0]))
			m.destroy();
	}
	
	public void onAddToWorld(){
		if(renderer!=null){
			RenderCore.exec.execute(new Runnable(){
				@Override
				public void run(){
					if(renderer!=null)
						renderer.destruct();					
				}
			});
		}
		makeRenderer();
	}
	
	public final void preUpdate(){
		pushl= push;
		push= null;
		onUpdatePre();
		for(Module m: mods)
			m.update();
	}
	/**Before any kinematic updates 
	 * For things which require kinematic information*/
	public void onUpdatePre(){}
	
	/**After kinematic translation 
	 * For sounds and packets*/
	int c=0;
	public void onPostUpdate(){
//		new Emitter("kitty", this.kin);
	}
		
	/**Most items are used through Bindings, 
	 * only special cases should have a controller.*/
	Controlled controller= new ControllerBlank();
	public static class ControllerBlank implements Controlled{
		@Override
		public void setMouse(vec2 g, int dw){}
		@Override
		public void setButton(int button, boolean state){}
	}
	
	final ArrayList<Module> mods= new ArrayList<>();
	/**May only be used by Module*/
	public void moduleAdd(Module mod){
		this.mods.add(mod);
	}
	public boolean moduleRemove(Module mod){
		return mods.remove(mod);
	}
	public Module moduleRemove(int ind){
		return mods.remove(ind);
	}
	public Module moduleget(int ind){
		return mods.get(ind);
	}
	public Iterable<Module> moduleIter(){
		return mods;
	}
	public final void moduleStateAll(boolean state){
		for(Module mod : mods)
			mod.use(state);
	}
	public final void moduleState(int mod, boolean state){
		mods.get(mod).use(state);
	}

	
	
	public Renderer renderer= null;
	/**called by world upon adding*/
	public final void makeRenderer(){
		if(hasRenderer){
			RenderCore.exec.execute(new Runnable(){
				@Override
				public void run(){
					renderer= constructRenderer();
					if(renderer!=null){
						if(world!=null)
							renderer.addToWorldRenderGroup(world);
					}
				}
			});
		}
	}
	protected boolean hasRenderer= true;
	/**Ran in render thread*/
	protected Renderer constructRenderer(){
		return new ChunkRenderer(this);
	}
	
	public final List<Joint> joints= new ArrayList<>();
	
//	public boolean noclip= false;
	public final List<Entity> noclips= new ArrayList<>();
	public boolean noclip= false;
	/**collision check only needs ran once per pair
	 * each entity tracks the collisions it has already done
	 * if that has already checked this, this continues*/
	private final Collection<Entity> collisionsChecked= new ArrayList<>();
	public synchronized void move(){
		if(noclip)//TODO broadphase will handle noclips
			return;
		collided= false;
		final List<Entity> nears= world.getEntitiesInLocation(trans().t(),
				trans().scaley+Math.abs(kin.v.len()));
		nears.remove(this);
		nears.removeAll(noclips);
		
		collisionsChecked.clear();
		for(Entity that : nears){
			if(that.noclip)
				continue;
			if(!that.collisionsChecked.contains(this)){
				this.collisionsChecked.add(that);
				that.collisionsChecked.add(this);
				Hit h= this.chunk.collide(that.chunk, kin, that.kin);
				if(h!=null){
					this.collided= true;
					that.collided= true;
					this.collisions.add(that);
					that.collisions.add(this);
					this.collide(that, h);
					that.collide(this, h);
				}
			}
			else{
				this.collided= that.collided;
			}
		}
	}
	private final void collide(Entity e, Hit h){
		onCollide(e, h);
		//TODO sound
	}
	public void onCollide(Entity e, Hit h){}
	
	
	public final void removeFromWorld(){
		onRemove();
		noclips.clear();
		collisions.clear();
		if(world!=null)
			world.removeEntity(this);
		
		//dont nullcheck here as construction is async
		RenderCore.exec.execute(new Runnable(){
			@Override
			public void run(){
				if(renderer!=null)
					renderer.destruct();
				renderer= null;
			}
		});

		this.kin.v.set(0);
		this.trans().tx= this.trans().ty= 0;
	}
	protected void onRemove(){}
	
	/**0 relative velocity when unstoring*/
	public void store(){
		removeFromWorld();
	}
//	public void store(Entity container){
//		remove();
//		//store relative velocity
//		this.trans.tx= this.trans.ty= 0;
//		this.kin.vx= this.kin.vx - container.kin.vx;
//		this.kin.vy= this.kin.vy - container.kin.vy;
//	}
	public void unstore(SpawnData d){
		//v when stored is relative velocity
		vec2 rv= this.kin.v.dup();
		double mass= this.kin.m;
		d.apply(this);
		this.kin.v.add(rv);
		this.kin.m= mass;
		world.addEntity(this);
	}
	
	@Override
	public String toString(){
		return super.toString()+", "+world+'\n'+kin;
	}
	
	@Override
	public void save(DataOutputStream b) throws IOException{
		b.writeLong(id);
		b.writeInt(world.id);
		b.writeInt(lifetime);
		trans().save(b);
		kin.save(b);
		chunk.loadman.unloadAll();
	}
	
	public static Entity loadFromDisk(long id){
		return null;
	}
	public static Entity load(ByteBuffer b){
		return null;
	}
}
