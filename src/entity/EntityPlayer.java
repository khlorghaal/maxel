package entity;

import material.Chemistry.Quantity;
import material.Chunk;
import material.ChunkSprite.ChunkHuman;
import material.Material;
import math.vec2;
import module.EntityModuleAnchor;
import module.Gun;
import module.Inventory;
import module.adminhax.Dragger;
import module.adminhax.Gravitator;
import module.adminhax.Impulser;
import module.adminhax.Spawner;
import module.material_affectors.Placer;
import module.material_affectors.Remover;
import ui.Bindings;
import ui.KeyMap;
import world.Controlled;
import world.SpawnData;

public class EntityPlayer extends Entity implements Controlled{	
	
	public Aim aim= new Aim();
	public Buttons buttons= new Buttons();
	public double reach= 1;
	public Inventory inv= new Inventory(1024);
	public Hand lhand= new Hand(this, inv), rhand= new Hand(this, inv);
	public Bindings binds= new Bindings(inv, lhand, rhand);
	@Override
	protected Chunk makeChunk(){return new ChunkHuman(this);}
	
	protected Sprite sprite;
	public EntityPlayer(SpawnData sd){
		super(sd, Material.BIOLOGICAL);
		sprite= new Sprite();

		inv.put(new Entity(null));
		Entity s1= new EntityModuleAnchor(null);
		new Spawner.SpawnerDust(new Quantity(Material.get("dirt"), 1), s1, aim, buttons);
		inv.put(s1);
		Entity spawner= new EntityModuleAnchor(null);
		new Spawner(Material.get("stone"), spawner, aim, buttons);
		Entity placer= new EntityModuleAnchor(null);
		new Placer(Material.get("stone"), placer, aim, buttons);
		Entity remover= new EntityModuleAnchor(null);
		new Remover(remover, aim, buttons);
		Entity dragger= new EntityModuleAnchor(null);
		new Dragger(dragger, aim, buttons);
		Entity gravyater= new EntityModuleAnchor(null);
		new Gravitator(gravyater, aim, buttons);
		Entity impulser= new EntityModuleAnchor(null);
		new Impulser(impulser, aim, buttons);
		Entity gat= new EntityModuleAnchor(null);
		new Gun(gat, aim, buttons);
		inv.put(spawner);
		inv.put(placer);
		inv.put(remover);
		inv.put(dragger);
		inv.put(gravyater);
		inv.put(impulser);
		inv.put(gat);
//		lhand.grab(0);
//		rhand.grab(5);
	}
	
	@Override
	public void onUpdatePre(){
		if(buttons.array[KeyMap.W])
			kin.v.y+=.001;
		if(buttons.array[KeyMap.S])
			kin.v.y-=.001;
		if(buttons.array[KeyMap.A])
			kin.v.x-=.001;
		if(buttons.array[KeyMap.D])
			kin.v.x+=.001;
	}
	
	
	@Override
	public void setMouse(vec2 g, int dw){
		aim.p= g.dup();
		buttons.wheel+= dw;
	}
	
	@Override
	public void setButton(int index, boolean state){
		buttons.array[index]= state;
		
		binds.setButton(index, state);
		
		if(index==KeyMap.SPACE && state){
			vec2 p= trans().t().add(4);
			new Explosion(world, p, kin.v, .75);
		}
	}
}

