package entity;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.StandardOpenOption;
import java.util.Stack;

import material.Chunk;
import material.Chunk.Node;

public class EntityFile{
	//filename - entityname
	
	//struct{
	//int entitydatalength
	//byte[] entitydata
	//node[]
	
	//struct node{
	//ulong5 chptr
	//short id
	//ubyte durability
	//ubyte temperature
	//byte normalx
	//byte normaly
	
	//chptr is a pointer to an array of 4 child nodes
	//when null
	//when saving children, is allocated to end of file
	//when loading, returns null
	
	public File file;
	FileChannel fch;
	int edatlen;
	static final int edatptr= 4;
	int rootptr;
	static final int nodedatlen= 5+2+1+1+1+1;
	
	public EntityFile(Entity e){
		try{
			String folder= "";
			if(e instanceof EntityPlayer)
				folder= "players\\";
			
			file= new File("\\worlds\\"+e.world.name+"\\entities\\"
				+folder+e.id+".entity");
			
			fch= FileChannel.open(file.toPath(), 
					StandardOpenOption.CREATE,
					StandardOpenOption.READ,
					StandardOpenOption.WRITE);
			

			fch.position(0);
			ByteBuffer buf= ByteBuffer.allocate(4);
			fch.read(buf);
			buf.clear();
			edatlen= buf.getInt();
			rootptr= edatptr+edatlen;
		}catch(IOException ex){ex.printStackTrace();}
	}

	private static abstract class ReaderInterfaceThingy{
		abstract void read(ByteBuffer buf, long pos) throws IOException;
		
		static ReaderInterfaceThingy make(final byte[] dat){
			return new ReaderInterfaceThingy(){
				@Override
				void read(ByteBuffer buf, long pos) throws IOException{
					if(pos>Integer.MAX_VALUE)
						throw new EOFException();
					int lim= (int)(pos)+buf.remaining();
					for(int i=(int)pos; i!=lim; i++)
						buf.put(dat[i]);
						
				}
			};		
		}
		static ReaderInterfaceThingy make(final FileChannel fch){
			return new ReaderInterfaceThingy(){
				@Override
				void read(ByteBuffer buf, long pos) throws IOException{
					fch.read(buf, pos);
				}
			};
		}
	}

	public void loadNodes(Chunk c, Node parent, byte[] path, int childDepth) throws IOException{
		loadNodes(c, parent, path, childDepth,  ReaderInterfaceThingy.make(fch), rootptr);
	}
	public static void loadNodes(Chunk c, Node parent, byte[] path, int childDepth, byte[] data, int rootptr) throws IOException{
		loadNodes(c, parent, path, childDepth, ReaderInterfaceThingy.make(data), rootptr);
	}
	/**@param path the child[] index per each step
	 * @param childdepth the DIFFERENCE of the smallest child's depth and node's depth
	 * depth(data) = 2^log4(data/nodedatlen) 
	 * @return node at path and its children to depth*/
	private static void loadNodes(Chunk c, Node parent, byte[] path, int childDepth, ReaderInterfaceThingy ch, int rootptr) throws IOException{
		byte[] dat= new byte[nodedatlen];
		ByteBuffer buf= ByteBuffer.wrap(dat);
		buf.limit(5);
		long parentptr= rootptr;//not yet set
		for(int i=0; i!=path.length; i++){
			ch.read(buf, parentptr);
			buf.position(0);
			parentptr= constructULong5(dat);
		}
		//parentptr is now set at end of path	
		buf.clear();
		
		//for each level, load all nodes that are not already loaded
		Stack<Long> pending= new Stack<>();//pointers that havent been looked at yet
		pending.add(parentptr);
		while(!pending.isEmpty()){
			parentptr= pending.pop();
			buf.position(0);
			buf.limit(5);
			ch.read(buf, parentptr);//read chptr
			long chptr= constructULong5(dat);
			if(chptr==0)//no ch in data
				continue;
			
			parent.children= new Node[4];
			for(int i=0; i!=4; i++){
				
			}
		}
	}
	
	public static Node loadNode(Chunk c, Node parent, int index, byte[] data) throws IOException{
		DataInputStream dat= new DataInputStream(new ByteArrayInputStream(data));
		int typ= dat.readShort();
		float dur= dat.readUnsignedByte()/255f;
		float temp= dat.readUnsignedByte();
		byte n= dat.readByte();
		Node ret= c.new Node(parent, typ, index);
		ret.health= dur;
		ret.temp= temp;
		ret.normal= n;
		ret.reset();
		return ret;
	}
	public static byte[] nodeToBytes(Node n) throws IOException{
		ByteArrayOutputStream baos= new ByteArrayOutputStream();
		DataOutputStream dat= new DataOutputStream(baos);
		dat.writeShort(n.getID());
		dat.writeByte((byte)(n.health*255));
		dat.writeByte((byte)n.temp);
		dat.writeByte(n.normal);
		return baos.toByteArray();
	}

	public static long constructULong5(byte[] dat){
		long ret=0;
		ret+= dat[0]<<32;
		ret+= dat[1]<<24;
		ret+= dat[2]<<16;
		ret+= dat[3]<<8;
		ret+= dat[4]<<0;
		return ret;
	}
	public static byte[] deconstructULong5(long dat){
		return new byte[]{
				(byte)((dat&0xFF00000000l)>>32),
				(byte)((dat&0xFF000000)>>24),
				(byte)((dat&0xFF0000)>>16),
				(byte)((dat&0xFF00)>>8),
				(byte)((dat&0xFF)>>0)};
	}
	
	public DataInputStream getEntityData() throws IOException{
		byte[] dat= new byte[edatlen];
		ByteBuffer buf= ByteBuffer.wrap(dat);
		fch.read(buf,edatptr);
		return new DataInputStream(new ByteArrayInputStream(dat));
	}
	public void saveEntityData(byte[] dat) throws IOException{
		//write length
		ByteBuffer buf= ByteBuffer.allocate(4);
		buf.putInt(dat.length);
		buf.position(0);
		fch.position(0);
		fch.write(buf);
		
		buf= ByteBuffer.wrap(dat);		
		fch.position(edatptr);
		fch.write(buf);
	}
	
	/**Loads ALL nodes, saves them, then clips end of file, thus defragmenting*/
	public void consolidateChunkdata(){
		byte[] p= new byte[0];
//		Node root= getNode(p, Chunk.MAXIMUM_DEPTH);
	}
}
