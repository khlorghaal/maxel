package entity;

import java.util.ArrayList;

import math.Transformation;
import math.vec2;

public class Joint{
	public final Entity m, e;
	protected vec2 mp, ep;
	public double strength;
	
	/**@param m entity mounted on
	 * @param e entity being mounted onto m
	 * @param mxy of the joint on m
	 * @param exy of the joint on e
	 * @param theta of e relative to m*/
	public Joint(Entity m, Entity e, vec2 mp, vec2 ep, double theta){
		this.m=m;
		this.e=e;
		this.mp= mp.dup();
		this.ep= ep.dup();

		e.joints.add(this);
//		m.joints.add(this);

		m.noclips.add(e);
		e.noclips.add(m);
	}
	public void destroy(){
		e.joints.remove(this);
		m.joints.remove(this);
		m.noclips.remove(e);
		e.noclips.remove(m);
	}
	public Joint(Entity m, Entity e, vec2 mp, vec2 ep){
		this(m,e,mp,ep,0);
	}
	
	public void update(){
		//get point dpos
		//subtract rel v //damping. do not dot, that would allow sliding
		//
		Transformation mt= m.trans();
		Transformation et= e.trans();
		vec2 mg= mt.use(mp);
		vec2 eg= et.use(ep);
		vec2 dp= mg.dup().sub(eg);
		

		vec2 vm= m.kin.getV(mp);
		vec2 ve= e.kin.getV(ep);
		vec2 dv= vm.dup().sub(ve);
		dp.add(dv);
		
		vec2 dmp= dp.dup().neg().div(2000);
		vec2 dep= dp.dup().div(2000);

//		double[] vxm= m.kin.getV(mx, my);
//		double[] vxe= e.kin.getV(ex, ey);

//		dx= Math.signum(dx);
//		dy= Math.signum(dy);
//		m.kin.vx-= dx;
//		m.kin.vy-= dx;
//		e.kin.vx+= dx;
//		e.kin.vy+= dy;
//		m.kin.impulseGlobal(mg, dmp);
		e.kin.impulseGlobal(eg, dep);
	}
	
	public static class JointGroup{
		final ArrayList<Joint> group= new ArrayList<>();
		public void solve(){
			
		}
	}
}
