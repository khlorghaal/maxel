package entity;

import java.util.ArrayList;

import material.Chemistry.Quantity;
import material.Chunk.Hit;
import material.Material;
import world.SpawnData;

public class EntityDust extends Entity{
	/**volumetric amounts*/
	public final ArrayList<Quantity> quantities= new ArrayList<>();
	public EntityDust(SpawnData s, Quantity q){
		super(s, Material.DUST);
		add(q);
		noclip= true;
	}
	
	@Override
	public void onCollide(Entity e, Hit h){
		if(e instanceof EntityDust)
			merge(this, (EntityDust)e);
		else if(e instanceof EntityPlayer){
			EntityPlayer p= (EntityPlayer)e;
			p.inv.put(this);
		}
	}
	public void add(Quantity q){
		quantities.add(q);
		trans().setScale( trans().scaley );
		kin.m+= q.amount*q.mat.density;
	}
	public Quantity remove(Quantity q){
		int i= 0;
		while(true){
			if(i==quantities.size())
				return null;
			if(quantities.get(++i).mat == q.mat)
				break;
		}
		
		Quantity ret;
		Quantity contained= quantities.get(i);
		if(contained.amount < q.amount)
			ret= contained;
		else
			ret= q;
		
		return ret;
	}
	
	public static void merge(EntityDust a, EntityDust b){
		EntityDust big, small;
		if(a.trans().scaley > b.trans().scaley){
			big= a;
			small= b;
		}
		else{
			big= b;
			small= a;
		}
		
		for(Quantity q : small.quantities)
			big.add(q);
		small.destroy();
	}
	
	//	@Override
	//	public Renderer constructRenderer(){
	//		return null;
	//	}
}
