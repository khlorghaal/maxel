package ui;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import module.Inventory;
import core.Threaderator.Function;
import entity.Entity;
import entity.Hand;
import entity.Hand.IHandActionListener;
import gfx.Camera;
import gfx.MaterialRenderer;

/**Serve any arbitrary action upon a keypress, are meant to be customized on the fly
 * 
 * Keypresses are recieved directly from the player listener.
 * Mappings are controlled by buttonBindingMap, populated by Binding constructor*/
public class Bindings{
	Inventory inv;
	Hand left, right;
	
	public Bindings(Inventory inv, Hand left, Hand right){
		this.inv= inv;
		this.left= left;
		this.right= right;
		
		new BindingHand(KeyMap.LMB, left);
		new BindingHand(KeyMap.RMB, right);
		new BindingHandCycle(KeyMap.TAB);
		new BindingHandTempAction(KeyMap.THROW, Hand.THROW);
		new BindingHandTempAction(KeyMap.EAT, Hand.EAT);
		new BindingHandTempAction(KeyMap.PUT, Hand.PUT);
	}
	
	Map<Integer, Binding> buttonBindingMap= new Hashtable<>();
	/**As stored in the bar*/
	private final List<Binding> bindings= new ArrayList<>();
	/**Used for rendering and saving*/
	public Binding[] getBindings(){ return bindings.toArray(new Binding[0]); }

	public void setButton(int button, boolean state){
		Binding bind= buttonBindingMap.get(button);
		if(bind!=null){
			bind.invoke(state);
			return;
		}
				
		if(state && KeyMap.isArrow(button) ){//move the swap selector
			new Action(Hand.TAKE);
			if(!inv.selector.active){
				inv.selector.active= true;
			}
			else{
				if(button==KeyMap.LEFT)
					inv.selector.left();
				else if(button==KeyMap.RIGHT)
					inv.selector.right();
				else if(button==KeyMap.DOWN)
					inv.selector.up();
				else if(button==KeyMap.UP)
					inv.selector.down();
				
			}
			return;
		}
	}
	
	
	public abstract class Binding{
		public Binding(int key){
			buttonBindingMap.put(key, this);
			bindings.add(this);
		}
		public abstract void invoke(boolean state);

		public abstract void render(Camera cam);
	}
	
	public class BindingHand extends Binding{
		final Hand hand;
		public BindingHand(int key, Hand hand){
			super(key);
			this.hand= hand;
		}
		@Override
		public void invoke(boolean state){
			if(isPendingHandAction())
				pendingHandAction.use(hand);//may begin hand use
			else
				hand.use(state);
		}
		
		@Override
		public void render(Camera cam){
			Entity h= hand.getHeld();
			if(h!=null && h.renderer!=null){
				cam.trans.scaleTransPreserving(h.trans().scaley);
				MaterialRenderer.bindAtlases();
				h.renderer.render(cam);
			}
		}
	}

	
	
	Function<Hand> pendingHandAction= null;
	public IHandActionListener pendingActionListener= null;
	public boolean isPendingHandAction(){ return pendingHandAction!=null; }
	public class Action extends Function<Hand>{
		final int a;
		public Action(int a){
			this.a= a;
			pendingHandAction= this;
			pendingActionListener.set(a);
		}
		@Override
		public void use(Hand t){
			t.setAction(a);
			t.use(true);
			pendingHandAction= null;
			pendingActionListener.set(-1);
		}	
	}
	public class ActionCycle extends Function<Hand>{
		public ActionCycle(){
			pendingHandAction= this;
			pendingActionListener.set(Hand.CYCLE);
		}
		@Override
		public void use(Hand t){
			t.cycleAction();
			pendingHandAction= null;
			pendingActionListener.set(-1);
		}	
	}
	public class BindingHandCycle extends Binding{
		final int[] actions= new int[]{
		};
		int i= 0;
		public BindingHandCycle(int key){
			super(key);
		}

		@Override
		public void invoke(boolean state){
			if(state)
				new ActionCycle();
		}

		@Override
		public void render(Camera cam){
			// TODO Auto-generated method stub
			
		}
		
	}
	public class BindingHandTempAction extends Binding{
		int action;
		public BindingHandTempAction(int key, int action){
			super(key);
			this.action= action;
		}

		@Override
		public void invoke(boolean state){
			if(state)
				new Action(action);
		}

		@Override
		public void render(Camera cam){
			// TODO Auto-generated method stub
			
		}
		
	}
	
	
	public abstract class BindingInventory extends Binding{
		public int invIndex;
		BindingInventory(int key, int invIndex){
			super(key);
			this.invIndex= invIndex;
		}			

		@Override
		public void render(Camera cam){
			
		}
	}
	
	public class BindingSwap extends BindingInventory{
		BindingSwap(int key, int invIndex){
			super(key, invIndex);
		}
		@Override
		public void invoke(boolean state){
			if(state){
				inv.selector.set(invIndex);
				inv.selector.active= true;
				//TODO
			}
		}

		@Override
		public void render(Camera cam){
			
		}
	}
	
	public class BindingUse extends BindingInventory{
		BindingUse(int key, int invIndex){
			super(key, invIndex);
		}
		@Override
		public void invoke(boolean state){
			// TODO Auto-generated method stub
		}

		@Override
		public void render(Camera cam){
			
		}
	}
}
