package ui.gui;


public class Button extends Focusable{
	public Button(Frame parent, float x, float y, float w, float h){
		super(parent, x, y, w, h);
	}
	
	@Override
	protected void onFocus(){}
	@Override
	protected void button(int index, boolean state){}
}
