package ui.gui;

import entity.Entity;
import gfx.Camera;
import gfx.MaterialRenderer;

public class FrameEntity extends Frame{
	static float pad= .1f;
	static float size= 1f-2*pad;
	
	public FrameEntity(Frame parent, float x, float y, float s){
		super(parent, x+s*pad, y+s*pad, s*size, s*size);
	}
	
	private Entity e;
	public void set(Entity e){
		this.e= e;
		if(e!=null && e.renderer==null)
			e.makeRenderer();
		//this is a very ineficcient way to reconstruct a large renderer
		//however it avoids all sorts of design changes 
		//which would be required to preserve the renderer object
	}
	@Override
	public void render(){
		super.render();
		if(e==null)
			return;
		if(e.renderer==null){
			e.makeRenderer();
			return;
		}
		Camera c= makeCamera();
		c.trans.scaleTransPreserving(e.trans().scaley);
		MaterialRenderer.bindAtlases();
		e.renderer.render(c);
	}
}