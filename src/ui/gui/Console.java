package ui.gui;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import ui.Text;
import ui.console.Command;
import core.Settings;

public class Console extends Focusable{
	public static Console instance;
	public Textarea textout;
	public Textbox textin;
	public Console(){
		super(ROOT, 0, 0, 1, .95f);
		if(instance!=null)
			instance.destroy();
		instance= this;
		
		textout= new Textarea(this, 0, 0, 1, 1-Settings.fontsize*1.25f);
		textout.setHeightNonsquare();
		textout.setWidthNonsquare();
		textout.visible= false;
		
		textin= new Textbox(this, 0, 0, 1);
		textin.align= Alignment.BOTTOMLEFT;
		textin.setWidthNonsquare();
		textin.color= 0x1f;
		textin.setConsumer(Command.Parser.instance);
		
		this.color= 0x111111cc;
		textout.scrollbar.color= 0x01010155;
		textout.scrollbar.bar.color= 0xeeeeee44;

		textout.text= outtext;
		out.flush();
		
		Focusable.setFocus(this);
	}
	
	public static void toggle(){
		if(instance==null){
			new Console();
		}
		else{
			instance.destroy();
			instance= null;
		}
	}
	
	@Override
	public void render(){
		super.render();
	}
	
	public static final PrintStream out= new PrintStream(
			new OutputStream(){
				final List<Character> acc= new ArrayList<>();
				
				@Override
				public void write(int b) throws IOException{
					if(b=='\r')
						return;
					acc.add((char)b);
				}
				
				@Override
				public void flush(){
					if(instance==null || acc.size()==0)
						return;
					
					for(char c : acc)
						outtext.add(c);
					acc.clear();
					
					//prune
					

					instance.textout.onResizeMove();
				}
				
			}, true);
	private static final Text outtext= new Text();
	
	
	@Override
	public void onFocus(){
		Focusable.setFocus(textin);
	}
}
