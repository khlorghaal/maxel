package ui.gui;

public class Expander{
	public Frame f;
	public float dx, dy, kw, kh;
	public Expander(Frame f, float dx, float dy, float kw, float kh){
		this.f= f;
		this.dx= dx;
		this.dy= dy;
		this.kw= kw;
		this.kh= kh;
	}
	
	private float px, py, pw, ph;
	public void expand(){
		px= f.getOffsetX();
		py= f.getOffsetY();
		pw= f.getWidth();
		ph= f.getHeight();
		f.setPos(px+dx, py+dy);
		f.mulWidthHeight(kw, kh);
	}
	public void retract(){
		f.setPos(px, py);
		f.setWidthHeight(pw, ph);
	}
}
