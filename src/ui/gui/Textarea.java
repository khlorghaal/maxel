package ui.gui;

import gfx.RenderCore;
import ui.Text;
import core.Settings;


public class Textarea extends FrameScrollable{	
	public Text text= new Text();
	int charsw, charsh;
	/**The first and last characters rendered*/
	int first, last;

	boolean lock= false;
	
	public Textarea(Frame parent, float x, float y, float w, float h){
		super(parent, x, y, w, h);
		clean();
	}
	@Override
	public void onDestroy(){
		text.destroy();
	}
	
	@Override
	public void onResizeMove(){
		if(body==null)
			return;
		Frame b= scrollbar.visible? body:this;
		charsw= (int)((b.verts[6]-b.verts[0])/Settings.fontsize*RenderCore.displayAspect);
		charsh= (int)((b.verts[1]-b.verts[7])/Settings.fontsize);

		if(scrollbar.get()!=1){//scrolllock
			//TODO
		}
		scrollbar.setNumPages((float)(text.getLines(charsw))/charsh);
		System.out.println(charsh);
	}
	
	@Override
	public void render(){
		if(body==null)
			return;
		super.render();
		float lf= scrollbar.getPage()*charsh;
		int li= (int)lf;
		text.render(li, charsh, charsw, 
				body.verts[0], body.verts[1]);
	}
}
