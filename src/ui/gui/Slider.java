package ui.gui;

import ui.KeyMap;
import core.Settings;

public class Slider extends Focusable{
	private float amount, pages= 1;
	public boolean ignoreArrowPresses= false;
	protected final Frame bar; 
	{
		bar= new Frame(this, 0, 0, 1, .5f);
		bar.color= Settings.uiColorSecond;
		bar.setWidthNonsquare();
		bar.setHeightNonsquare();
		setNumPages(0);
	}
	public Slider(Frame parent){
		super(parent, 0, 0, .025f, 1);
		align= Alignment.TOPRIGHT;
		setWidthAbsolute();
		setHeightNonsquare();
	}
	public Slider(Frame parent, float x, float y, float w, float h){
		super(parent, x, y, w, h);
	}
	/**Number of wheel clicks to scroll all the way*/
	public void setNumPages(float count){
		if(count<1)
			count= 1;
		pages= count;
		float h= 1f/count;
		if(h<.06f)
			h= .06f;
		bar.setHeight(h);
		this.visible= bar.visible= count>1;
	}
	public void set(float amount){
		if(amount<0)
			amount=0;
		if(amount>1)
			amount=1;
		this.amount=amount;
		
		float h= bar.getHeightNonsquare();
		float y= amount*(1-h);
		bar.setPos(0, y);
		
	}
	public float get(){ return amount; }
	public float getPage(){ return amount*(pages-1); }
	
	float grab;
	@Override
	protected void onFocus(){
		grab= amount;
		if(bar.visible)
			bar.highlight= true;
	}
	@Override
	protected void onDefocus(){ bar.highlight= false; }
	@Override
	protected void drag(){
		//bar is not focusable, it doesnt intercept this
		
		float m= (float)(GUICore.mp.y-GUICore.mDown.y);
		m/= getAbsoluteHeight()*(1-bar.getHeightNonsquare());
		set( m+grab );
	}
	@Override
	protected void wheel(int wheel){
		set(amount-wheel/pages*Settings.scrollyness);
	}
	
	@Override
	protected void button(int index, boolean state){
		if(state){
			if(index==KeyMap.DOWN || index==KeyMap.RIGHT){
				if(!ignoreArrowPresses) 
					wheel(-1);
			}
			else if(index==KeyMap.UP || index== KeyMap.LEFT){
				if(!ignoreArrowPresses) 
					wheel(1);
			}
			else if(index==KeyMap.PAGEUP)
				wheel((int)(1/Settings.scrollyness));
			else if(index==KeyMap.PAGEDOWN)
				wheel((int)(-1/Settings.scrollyness));
		}
	}
	
	@Override
	protected void onMouseOver(boolean state){
		if(Focusable.mousedOver==this && parent instanceof Focusable){
			((Focusable)parent).onMouseOver(state);
		}
	}
}
