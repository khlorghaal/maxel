package ui.gui;

import java.util.ArrayList;

import module.Inventory;
import world.World;

public class FrameInventory extends FrameScrollable{
	public long msSelected;
	private final ArrayList<FrameEntity> slots= new ArrayList<>();
	private final Inventory inv;
	private final Grid grid;
	public FrameInventory(float x, float y, float w, float h){
		super(ROOT, x, y, w, h);
		scrollbar.ignoreArrowPresses= true;
		
		inv= World.instance.localplayer.inv;

		grid= new Grid(inv.selector.w);
		
		refreshSlots();
	}
	private void refreshSlots(){
		if(slots.size()==inv.contents.size())
			return;
		
		float pad= .1f;
		int n= inv.contents.size();

		body.mulWidthHeight(1f-pad/inv.selector.w, 1f-pad/inv.selector.w);
		for(int i=0; i!=n; i++){
			FrameEntity f= new FrameEntity(body, 0f, 0f, 1);
			slots.add(f);
			grid.conformFrame(f, i, pad);
		}
		scrollbar.setNumPages(1.6f);
	}
	
	Expander exp= new Expander(this, 0, -getHeightNonsquare()*7, 1, 8f);
	Expander exp1= new Expander(scrollbar, 0, 0, .125f, .125f);
	Expander exp2= new Expander(body, 0, 0, .125f, .125f);
	
	@Override
	protected void onMouseOver(boolean state){
		super.onMouseOver(state);
		if(state){
			exp.expand();
//			exp1.expand();
//			exp2.expand();
		}
		else{
			exp.retract();
//			exp1.retract();
//			exp2.retract();
			Focusable.defocus();
		}
	}
	
	int prevhighlight= -1;
	@Override
	public void render(){

		//selector highlight
		if(prevhighlight!=-1)
			slots.get(prevhighlight).highlight= false;
		if(inv.selector.active){
			int s= inv.selector.pos();
			slots.get(s).highlight= true;
			prevhighlight= s;
		}

		refreshSlots();
		
		//refresh entityframes
		//probably not best way todo this, but easiest
		for(int i=0; i!= inv.contents.size(); i++)
			slots.get(i).set( inv.contents.get(i) );
			
		super.render();
		
		//tool label
//		if(msSelected!=-1){
//			int msSinceSelected= (int)(System.currentTimeMillis() - msSelected);
////			String name= tools.get(activeSlot).name;
//			String color= Integer.toHexString(Settings.uiColorSecond);
////			FontRenderer.render(color+name, 0, 0, 256);
//		}
		
	}
}
