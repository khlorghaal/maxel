package ui.gui;

import entity.Light;
import gfx.RenderCore;
import material.Chunk;
import math.vec2;

import org.lwjgl.opengl.ATIMeminfo;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.NVXGpuMemoryInfo;

import ui.InputCore;
import ui.KeyMap;
import ui.Text;
import world.Controlled;
import world.World;
import audio.Sound;
import core.Settings;

/**Manages all windowy stuff. Distributes inputs to UI*/
public class GUICore implements Controlled{
	public static final GUICore instance= new GUICore();
	static{ InputCore.clientInputBus.register(instance);}	
	
	static vec2 mp= new vec2(), pmp= new vec2(), mDown= new vec2();
	
	//	static int VBO;
	//	static int VAO;
	//	static final int VBOSIZ= 2*4*4*2;//two sets * 4verts * fvec2
	//	static FloatBuffer vbuf= BufferUtils.createFloatBuffer(VBOSIZ/4);
	public static void initGL() {
		Cursor.init();
		Frame.initGL();
		Text.initGL();
		ModuleUIRenderer.initGL();
		
		//brush is used for drawing
		//		VBO= GL15.glGenBuffers();
		//		VAO= GL30.glGenVertexArrays();
		//		
		//		GL30.glBindVertexArray(VAO);
		//		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, VBO);
		//		vbuf.clear();
		//		vbuf.position(VBOSIZ/2 /4);
		//		vbuf.put(new float[]{0,0,  1,0, 0,1,1,1});//UV, invariant
		//		vbuf.clear();
		//		GL15.glBufferData(GL15.GL_ARRAY_BUFFER, vbuf, GL15.GL_DYNAMIC_DRAW);
		//		
		//		GL20.glEnableVertexAttribArray(0);
		//		GL20.glEnableVertexAttribArray(1);
		//		GL20.glVertexAttribPointer(0, 2, GL11.GL_FLOAT, false, 0, 0);//xy
		//		GL20.glVertexAttribPointer(1, 2, GL11.GL_FLOAT, false, 0, VBOSIZ/2);//uv
		
		RenderCore.checkError();
	}
	
	static Frame aaaa= new Frame(Frame.ROOT, 0, 0, .5f, 1);
	
	private static String vidmemprev="";
	private static String getVidmem(){
		if(RenderCore.timer.tick % 120!=0)
			return vidmemprev;
		
		String vendor= GL11.glGetString(GL11.GL_VENDOR).toLowerCase();
		int total, avail;
		if(vendor.contains("nvidia")){
			total= GL11.glGetInteger(NVXGpuMemoryInfo.GL_GPU_MEMORY_INFO_DEDICATED_VIDMEM_NVX);
			avail= GL11.glGetInteger(NVXGpuMemoryInfo.GL_GPU_MEMORY_INFO_CURRENT_AVAILABLE_VIDMEM_NVX);
			total/= 1000;
			avail/= 1000;
			vidmemprev= (total-avail)+"/"+total+" VRAM";
			return vidmemprev;
		}
		else if(vendor.contains("ati")){
			avail= GL11.glGetInteger(ATIMeminfo.GL_TEXTURE_FREE_MEMORY_ATI);
			vidmemprev= (avail/1000)+ "free VRAM (does ATI meminfo work?)";
			return vidmemprev;
		}
		else{
			vidmemprev= "meminfo not avail";
			return vidmemprev;
		}
	}
	
	public static void renderAll(){
		ModuleUIRenderer.render();
		
		if(Settings.profilerShow){
			int l= 0;
			float s= -Settings.fontsize;
			String fps= String.format("%3d",RenderCore.timer.getAverageFps());
			String spf= String.format("%5.1f", RenderCore.timer.getAverageSpf()*1000);
			Text.renderLn("fps|"+fps+" |"+spf+"ms"
					+"  "+RenderCore.ssw+"x"+RenderCore.ssh,-1, Text.getPosFromTop(l++));
			String tps= String.format("%3d",World.instance.timer.getAverageFps());
			String spt= String.format("%5.1f", World.instance.timer.getAverageSpf()*1000);
			Text.renderLn("tps|"+tps+" |"+spt+"ms",-1, Text.getPosFromTop(l++));

			Text.renderLn("Entities| "+World.instance.getEntityCount(), -1, Text.getPosFromTop(l++));
			String ccbs= String.format("%4d", Chunk.pccheck_basic/20);
			String ccms= String.format("%4d", Chunk.pccheck_macro/20);
			String cchs= String.format("%4d", Chunk.pccheck_heavy/20);
			Text.renderLn("broad|"+ccms+" basic|"+ccbs+" precise|"+cchs,-1, Text.getPosFromTop(l++));
			
			Text.renderLn("Lights| "+(Light.instances.size()+Light.LightStar.instances.size()),
					-1, Text.getPosFromTop(l++));

			Text.renderLn("Sounds| "+Sound.count, -1, Text.getPosFromTop(l++));
			
			String sysinfo= Runtime.getRuntime().availableProcessors()+"core   "
					+Runtime.getRuntime().totalMemory()/1000000
					+'/'+Runtime.getRuntime().maxMemory()/1000000+" heap   "
					+getVidmem()+'\n'
					
					+GL11.glGetString(GL11.GL_RENDERER)+' '
					+GL11.glGetString(GL11.GL_VERSION)+'\n';
			Text.renderLn(sysinfo, 0, 1f);
			
		}
		
		for(Frame f : Frame.ROOT.children){//dont render root itself
			f.render();//children should be naturally depth sorted
		}
		
		Cursor.render();
	}
	
	/**Coords are normally a square stretched to display height
	 * These ignore the square and are absolutes on the screen*/
	public static float margin=0, top, bottom, left, right;
	public static void onDisplayResize(){
		margin= 1-RenderCore.displayAspectInverse;
		if(margin<0)
			margin= 0;
		if(Frame.ROOT!=null)
			Frame.ROOT.clean();//cleans all Frames
	}
	
	/**Destroys the entire UI*/
	public static void purge(){
		Frame.ROOT.destroy();
	}
	
	@Override
	public void setMouse(vec2 g, int dw){
		pmp= mp.dup();
		mp= InputCore.mp.dup();
		
		//Mirrored with Frame's cleanVerts
		if(RenderCore.displayAspect>1)
			mp.x*= RenderCore.displayAspect;
		else{
			mp.y*= RenderCore.displayAspectInverse;
			mp.y-= 1-RenderCore.displayAspectInverse;//move to bottom
		}
		mp.div(2).add(.5);
		mp.y= -mp.y +1;
		
		Focusable.mouseMove((float)mp.x,(float)mp.y, dw);
	}
	
	@Override
	public void setButton(int index, boolean state){
		if(state){
			if( index==KeyMap.LMB || index==KeyMap.RMB ){
				Focusable.refocus((float)mp.x, (float)mp.y);
				mDown= mp.dup();
			}
			else if(index==KeyMap.CONSOLE){
				Console.toggle();
			}
		}
		Focusable.buttonPress(index, state);
	}
}
