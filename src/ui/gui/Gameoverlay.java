package ui.gui;

import entity.EntityPlayer;
import gfx.RenderCore;

public class Gameoverlay{

	public static void make(final EntityPlayer player){
		RenderCore.exec.execute(new Runnable(){//to prevent cmod
			@Override
			public void run(){
				GUICore.purge();
				
				float w= 1f/13;
				float y= 1f-w;
				float sp= .005f;
				new FrameBindings(0,y-w-sp, w);
				new FrameInventory(0, y, 1, w);
				
				
			}
		});
	}

}
