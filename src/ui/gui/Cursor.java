package ui.gui;

import gfx.DBO;
import gfx.RenderCore;
import gfx.texture.Texture;

import java.nio.FloatBuffer;
import java.nio.IntBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.LWJGLException;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;

import ui.InputCore;
import core.Settings;


public class Cursor{
	static Texture tex;
	static int vao, vbo;
	public static void init(){
		try{//make native cursor invisible to hide input lag
			IntBuffer cbuf= BufferUtils.createIntBuffer(1);
			Mouse.setNativeCursor(new org.lwjgl.input.Cursor(1, 1, 0, 0, 1, cbuf, null));
			
			texSet(Settings.cursor);
			vbo= GL15.glGenBuffers();
			GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vbo);
			GL15.glBufferData(GL15.GL_ARRAY_BUFFER, 64, GL15.GL_DYNAMIC_DRAW);
			vao= GL30.glGenVertexArrays();
			GL30.glBindVertexArray(vao);
			GL20.glEnableVertexAttribArray(0);
			GL20.glEnableVertexAttribArray(1);
			GL20.glVertexAttribPointer(0, 2, GL11.GL_FLOAT, false, 16, 0);
			GL20.glVertexAttribPointer(1, 2, GL11.GL_FLOAT, false, 16, 8);
		}catch(LWJGLException e){
			e.printStackTrace();
			try{ 
				Mouse.setNativeCursor(null); 
			}catch(LWJGLException e1){
				e1.printStackTrace();
			}
		}
	}
	public static void texSet(String cursorSuffix){
		if(tex!=null)
			tex.delete();
		String nam= "cursor"+cursorSuffix;
		tex= new Texture(nam, 0);
	}
	
	public static void render(){
		if(tex==null)
			return;
		
		tex.bind();
		RenderCore.progunlit.use();
		float x= (float)InputCore.mp.x;
		float y= (float)InputCore.mp.y;
		float w= 64f/RenderCore.w;
		float h= 64f/RenderCore.h;
		DBO.preBufferWrite(64);
		FloatBuffer fb= DBO.buf.asFloatBuffer();
		fb.put(
				new float[]{
						x+w,y-h,
						1,0,
						x+w,y+h,
						1,-1,
						x-w,y-h,
						0,0,
						x-w,y+h,
						0,-1,				});
		fb.flip();
		GL15.glBindBuffer(GL15.GL_ARRAY_BUFFER, vbo);
		GL15.glBufferSubData(GL15.GL_ARRAY_BUFFER, 0, fb);
		GL11.glPointSize(100);
		GL30.glBindVertexArray(vao);
		GL11.glDrawArrays(GL11.GL_TRIANGLE_STRIP, 0, 4);

//		GL11.glPointSize(200);
//		Brush.begin();
//		Brush.vert(x, y);
//		Brush.end(GL11.GL_POINTS);
	}
}
