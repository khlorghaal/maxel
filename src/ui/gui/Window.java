package ui.gui;




/**A moveable and scaleable ext of a regular GUI<br/>
 * All GUI coordinates, ever, are proportional*/
public class Window extends Frame{	
	public Window(float x, float y, float w, float h) {
		super(ROOT, x, y, w, h);
	}


	private class CloseButton extends Button{
		public CloseButton(Window parent){
			super(parent, .9f, 0, .1f, .1f);
		}
		@Override
		protected void onFocus(){
			this.parent.destroy();
		}

		@Override
		public void render(){
			super.render();
			//TODO
		}
	}

	private class MoveButton extends Button{
		public MoveButton(Window parent){
			super(parent, 0, 0, .9f, .1f);
		}
		
		@Override
		public void onFocus(){
			this.highlight= true;
		}
		@Override
		protected void onDefocus(){
			this.highlight= false;
		}
		
		@Override
		protected void drag(){
			float ldx= (float)(GUICore.mDown.x)-Window.this.getAbsoluteX();
			float ldy= (float)(GUICore.mDown.y)-Window.this.getAbsoluteY();
			Window.this.setPos(ldx, ldy);//Cannot have parent other than root
		}
		
		@Override
		public void render(){
			super.render();
			//TODO
		}
	}

	private class ResizeButton extends Button{
		public ResizeButton(Window parent){
			super(parent, .95f, .95f, .05f, .05f);
		}
		@Override
		protected void drag(){
			this.parent.setCorner((float)GUICore.mp.x, (float)GUICore.mp.y);}
		
		@Override
		public void render(){
			super.render();
			//TODO
		}
	}

	private final CloseButton cb= new CloseButton(this);
	private final ResizeButton rb= new ResizeButton(this);
	private final MoveButton mb= new MoveButton(this);
}
