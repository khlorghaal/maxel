package ui.gui;

import gfx.texture.Texture;
import gfx.texture.TextureRenderer;

public class FrameTextured extends Frame{
		
	public TextureRenderer tr;
	public FrameTextured(Frame parent, float x, float y, float w, float h){
		super(parent, x, y, w, h);
	}
	@Override
	protected void init(){
		tr= new TextureRenderer();
	}
	@Override
	public void onDestroy(){
		tr.destroy();
	}
	
	public void set(Texture t){
		this.tr.set(t);
	}
	
	@Override
	public void onResizeMove(){
		tr.setVerts(verts);
	}
	@Override
	public void render(){
		super.render();
		if(tr==null)
			return;
		tr.render();
	}
}
