package ui.gui;

import gfx.Brush;
import gfx.Camera;
import gfx.RenderCore;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import org.lwjgl.opengl.GL11;

import core.Settings;

public class Frame {
	/**Root's "parent" used to avoid null checking*/
	private static final Frame NORMAL= new Frame(null, 0, 0, 1, 1);
	static{
		NORMAL.as= NORMAL.aw= NORMAL.ah= 1;
	}
	public static final Frame ROOT= new Frame(NORMAL, 0, 0, 1, 1);
	
	public Frame parent;
	public final List<Frame> children= new CopyOnWriteArrayList<Frame>();
	
	public boolean visible=true, highlight= false;
	public int color= Settings.uiColorPrime;
	public boolean border= Settings.uiBorders;
	
	public enum Alignment { TOPLEFT, TOPRIGHT, BOTTOMLEFT, BOTTOMRIGHT, CENTER }
	
	public Alignment align= Alignment.TOPLEFT;
	/**Offsets relative to align and squareness.
	 * Use absolutes for anything nonprivate whenever possible.*/
	private float x,y, ax,ay;
	private float w,h, aw,ah;
	/**Whether to use the small or large axis as this.scale*/
	public boolean scaleMajor= false;
	private float as, s, smin,smaj;
	public float getOffsetX(){return x;}
	public float getOffsetY(){return y;}
	public final float getAbsoluteX(){return ax;}
	public final float getAbsoluteY(){return ay;}
	/**As a proportion of parents scale*/
	public float getWidth(){return w;}
	/**As a proportion of parents scale*/
	public float getHeight(){return h;}
	/**As a proportion of parents width*/
	public float getWidthNonsquare(){ return aw/parent.aw; }
	/**As a proportion of parents height*/
	public float getHeightNonsquare(){ return ah/parent.ah; }
	public float getAbsoluteWidth(){return aw;}
	public float getAbsoluteHeight(){return ah;}
	public float getScaleMinor(){return smin;}
	public float getScaleMajor(){return smaj;}
	public float getAbsoluteScale(){return as;}
	public float getAbsoluteAspect(){ return aw/ah; }
	
	public static void initGL(){
		
	}
	
	/**All coords are relative to inside of parent
	 * Width and Height are square, scaled to the smallest of parent's w|h*/
	public Frame(Frame parent, float x, float y, float w, float h){
		this.parent= parent;
		this.x= x;//*2-1;
		this.y= y;//)*2-1;
		this.w= w;
		this.h= h;
		this.smin=  w<h? w:h;
		this.smaj= w>h? w:h;
		this.s= smin;
		
		init();
		if(parent!=null){//normal has null parent
			parent.children.add(this);
			clean();
		}
	}
	/**Stuff that must be done pre-clean*/
	protected void init(){}
	
	/**Sets wh inverse of parents absolute wh 
	 * so that all children will have square positioning*/
	public void squareify(){
		float asp= parent.getAbsoluteAspect();
		if(asp>1)//parent w>h
			this.w/= asp;			
		else
			this.h*= asp;
		clean();
	}
	//	/**Scales this by parent's major scale instead of minor.
	//	 * Nonpersistent.*/
	//	public void bigify(){
	//		float exp= parent.sm/parent.s;
	//		this.w*= exp;
	//		this.h*= exp;
	//		clean();
	//	}
	/**Nonpersistent*/
	public void setWidthAbsolute(){
		//height*k=1*k=absheight=.01
		//height=absheight/k
		w= w/parent.as;
		setWidthSquare();
	}
	/**Nonpersistent*/
	public void setHeightAbsolute(){
		h= h/parent.as;
		setHeightSquare();
	}
	
	/**local coords*/
	public void setPos(float x, float y){
		this.x= x;
		this.y= y;
		clean();
	}
	public void addPos(float x, float y){
		this.x+= x;
		this.y+= y;
		clean();		
	}
	/**local coords*/
	public void setWidthHeight(float w, float h){
		this.w= w;
		this.h= h;
		clean();
	}
	public void mulWidthHeight(float w, float h){
		this.w*= w;
		this.h*= h;
		clean();		
	}
	public void setWidth(float w){
		this.w= w;
		clean();
	}
	public void setHeight(float h){
		this.h= h;
		clean();
	}
	
	private boolean widthSquare= true, heightSquare= true;
	public void setWidthSquare(){
		widthSquare= true;
		clean();
	}
	public void setWidthNonsquare(){
		widthSquare= false;
		clean();
	}
	public void setHeightSquare(){
		heightSquare= true;
		clean();
	}
	public void setHeightNonsquare(){
		heightSquare= false;
		clean();
		
	}
	
	private boolean posSquare= false;
	public void setPosNonsquare(){
		posSquare= false;
		clean();
	}
	public void setPosSquare(){
		posSquare= true;
		clean();
	}
	
	/**Sets wh so this's corner opposite the position corner touches point*/
	public void setCorner(float cornerX, float cornerY){
		w=cornerX-x;
		h=cornerY-y;
		clean();
	}
	public void pad(float pad){
		addPos(pad, pad);
		setPosSquare();
		setHeight(1-2*pad);
		setWidth(parent.getAbsoluteAspect()-2*pad);
		setHeightSquare();
		setWidthSquare();
	}
	
	/**Refeshes absolute coordinates and verts*/
	public final void clean(){
		smin=  w<h?w:h;
		smaj= w>h?w:h;
		s= scaleMajor? smaj:smin;
		as= resetAbsoluteScale();
		aw= widthSquare?  parent.as*w : parent.aw*w;//as/s= parents s, cant parent.s because root
		ah= heightSquare? parent.as*h : parent.ah*h;
		
		//wh affect position scaling but not scale scaling
		ax= getAbsoluteTopLeft(0);
		ay= getAbsoluteTopLeft(1);
		switch(align){
			default:
			case TOPLEFT:
				break;
			case TOPRIGHT:
				ax= parent.aw-ax-aw;
				break;
			case BOTTOMLEFT:
				ay= parent.ah-ay-ah;
				break;
			case BOTTOMRIGHT:
				ax= parent.aw-ax-aw;
				ay= parent.ah-ay-ah;
				break;
			case CENTER:
				ax= ax+(parent.aw-aw)/2;
				ay= ay+(parent.ah-ah)/2;
				break;
		}
		
		cleanVerts();
		onResizeMove();
		for(Frame c : children)
			c.clean();
	}
	/**Called when positioned or scaled, including construction*/
	protected void onResizeMove(){}
	
	/**Product of this and each of its parents' scale</br>
	 * So a half frame within a half frame will ret 1/4*/
	protected final float resetAbsoluteScale(){
		return this.s*parent.as;
	}
	/**Sums each window's relative coord times its parent's absolute wh</br>
	 * So the third frame of pos=0.5 scale=0.25 inside 2 others likewise, 
	 * Will return .5*1 + .5*.25 + .5*.25*.25 */
	protected final float getAbsoluteTopLeft(int axis){
		if(posSquare){
			if(axis==0)
				return parent.ax + parent.as*x;
			else
				return parent.ay + parent.as*y;			
		}
		else{
			if(axis==0)
				return parent.ax + parent.aw*x;
			else
				return parent.ay + parent.ah*y;
		}
	}
	
	
	/**Takes screen coordinates [0,1] and converts them into this's coordinates*/
	public float rootToLocalCoordX(float gx){
		return (gx-ax)/aw;
	}
	/**Takes screen coordinates [0,1] and converts them into this's coordinates*/
	public float rootToLocalCoordY(float gy){
		return (gy-ay)/ah;
	}
	
	
	public final float[] verts= new float[4*2];
	public final float[] triangleVerts= new float[6*2];
	protected final void cleanVerts(){
		float x= ax;
		float y= ay;
		float mx= x+aw;
		float my= y+ah;
		
		y= 1-y;//set y top
		my= 1-my;
		
		x= x*2-1;//map xy from [0, 1] to [-1, 1]  
		y= y*2-1;
		mx= mx*2-1;
		my= my*2-1;
		
		//do this last
		//mirrored with GUICore's setMouse
		if(RenderCore.displayAspect>1){
			x*= RenderCore.displayAspectInverse;
			mx*= RenderCore.displayAspectInverse;
		}
		else{
			y+= 1-RenderCore.displayAspectInverse;//move to bottom
			my+= 1-RenderCore.displayAspectInverse;
			y*= RenderCore.displayAspect;
			my*= RenderCore.displayAspect;
		}
		
		verts[0]= x;
		verts[1]= y; 
		verts[2]= mx; 
		verts[3]= y; 
		verts[4]= x; 
		verts[5]= my; 
		verts[6]= mx; 
		verts[7]= my;
		
		triangleVerts[0 ]= verts[0];//0
		triangleVerts[1 ]= verts[1];
		triangleVerts[2 ]= verts[2];//1
		triangleVerts[3 ]= verts[3];
		triangleVerts[4 ]= verts[4];//2
		triangleVerts[5 ]= verts[5];
		triangleVerts[6 ]= verts[2];//1
		triangleVerts[7 ]= verts[3];
		triangleVerts[8 ]= verts[4];//2
		triangleVerts[9 ]= verts[5];
		triangleVerts[10]= verts[6];//3
		triangleVerts[11]= verts[7];
	}
	
	public Camera makeCamera(){
		Camera ret= new Camera();
		ret.doPan= false;
		ret.unlit= true;
		ret.ignoreModelTranslation= true;
		
		ret.trans.setScale(1f/this.getAbsoluteScale()*1.2);
		ret.trans.scale(RenderCore.displayAspect, 1);
		ret.trans.translateRelative(
				-(verts[0]+verts[2])/2,
				-(verts[1]+verts[5])/2
		);
		
		return ret;
	}
	
	public void render(){
		if(visible){
			Brush.begin();
			Brush.color(this.color);
			Brush.verts(verts);
			Brush.end(GL11.GL_TRIANGLE_STRIP);
			if(this.border){	
				Brush.begin();
				Brush.color(Settings.uiColorSecond);
				int[] n= new int[]{0,2,6,4,0};//reorder
				for(int i=0; i!=5; i++)
					Brush.vert(verts[n[i]],verts[n[i]+1]);
				GL11.glLineWidth(3);
				Brush.end(GL11.GL_LINE_STRIP);
			}
		}
		if(highlight){
			Brush.begin();
			Brush.color(0xffffff44);
			Brush.verts(verts);
			Brush.end(GL11.GL_TRIANGLE_STRIP);			
		}
		for(Frame c : children)
			c.render();
	}
	
	/**recurses all children*/
	public final void destroy(){
		//cannot rely on deleting the root reference
		//as frames may be referenced externally
		parent.children.remove(this);
		Frame[] ct= children.toArray(new Frame[0]);
		for(Frame c : ct)
			c.destroy();
		onDestroy();
	}
	public void onDestroy(){}
}
