package ui.gui;

import ui.Bindings.Binding;
import world.World;
import entity.EntityPlayer;
import entity.Hand.IHandActionListener;
import gfx.texture.Texture;

public class FrameBindings extends Frame{
	EntityPlayer player;
	
	FrameHandAction frameHandActionL, frameHandActionR, frameHandActionPending;
	public FrameBindings(float x, float y, float w){
		super(Frame.ROOT, x, y, w, w);
		this.visible= false;

		frameHandActionL= new FrameHandAction(this, 0,-1, 1,1);
		frameHandActionR= new FrameHandAction(this, 1,-1, 1,1);
		frameHandActionPending= new FrameHandAction(this,-1,-1, 1,1);
		World.instance.localplayer.lhand.setActionListener(frameHandActionL);
		World.instance.localplayer.rhand.setActionListener(frameHandActionR);
		World.instance.localplayer.binds.pendingActionListener= frameHandActionPending;
		
		player= World.instance.localplayer;		
		Binding[] bs= player.binds.getBindings();
		for(int i=0; i!= bs.length; i++){
			new FrameBinding(bs[i], this, i, 0, 1);
		}
	}
	
	public static class FrameHandAction extends FrameTextured implements IHandActionListener{
		static final Texture[] icons= new Texture[]{
			new Texture("hand_invoke", 0),
			new Texture("hand_swing", 0),
			new Texture("hand_jab", 0),
			new Texture("hand_throw", 0),
			new Texture("hand_eat", 0),
			new Texture("hand_put", 0),
			new Texture("hand_take", 0),
			new Texture("hand_grab", 0),
			new Texture("hand_cycle", 0),
		};
		
		public FrameHandAction(Frame parent, float x, float y, float w, float h){
			super(parent, x, y, w, h);
			this.visible= false;
		}
		
		@Override
		public void set(int action){
			if(action==-1)
				set(null);
			else
				set(icons[action]);
		}
	}
	
	public static class FrameBinding extends Frame{
		private final Binding b;
		public FrameBinding(Binding bind, Frame parent, float x, float y, float s){
			super(parent, x, y, s, s);
			this.b= bind;
		}
		
		@Override
		public void render(){
			super.render();
			b.render(makeCamera());
		}
	}
}
