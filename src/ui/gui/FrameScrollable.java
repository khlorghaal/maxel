package ui.gui;

import gfx.RenderCore;

import org.lwjgl.opengl.GL11;


public class FrameScrollable extends Focusable{

	public Slider scrollbar;
	public Frame body;
	public FrameScrollable(Frame parent, float x, float y, float w, float h){
		super(parent, x, y, w, h);
		this.scaleMajor= true;
		this.clean();
		scrollbar= new Slider(this);
		float d= 1-scrollbar.getWidth();
		body= new Frame(this, 0,0, d, d);
		body.visible= false;
		body.setWidthAbsolute();
		body.setHeightAbsolute();
		body.clean();
	}
	@Override
	protected void onMouseOver(boolean state){
		Focusable.setFocus(this);
		if(Focusable.mousedOver==this)
			scrollbar.onMouseOver(state);
	}
	
	//fwd stuff to scrollbar
	@Override
	protected void wheel(int wheel){
		scrollbar.wheel(wheel);
	}
	@Override
	protected void onFocus(){
		scrollbar.onFocus();
	}
	@Override
	protected void onDefocus(){
		scrollbar.onDefocus();
	}
	@Override
	protected void drag(){
		scrollbar.drag();
	}
	@Override
	protected void button(int index, boolean state){
		scrollbar.button(index, state);
	}
	
	@Override
	public void render(){
		//scroll offset
		body.setPos(0, -scrollbar.getPage());
		
		int[] dpverts= new int[4];
		dpverts[0]= (int)((verts[0]/2+.5)*RenderCore.w);
		dpverts[1]= (int)((verts[5]/2+.5)*RenderCore.h);
		dpverts[2]= (int)((getAbsoluteWidth())*RenderCore.w);
		dpverts[3]= (int)((getAbsoluteHeight())*RenderCore.h);
		GL11.glEnable(GL11.GL_SCISSOR_TEST);
		GL11.glScissor(dpverts[0], dpverts[1], dpverts[2], dpverts[3]);
		super.render();
		GL11.glDisable(GL11.GL_SCISSOR_TEST);
	}
}
