package ui.gui;

import gfx.RenderCore;

import java.util.List;

import org.lwjgl.input.Keyboard;

import ui.InputCore;
import ui.KeyBoardListener;
import ui.Text;
import ui.Text.StylizedCharacter;
import ui.console.StringConsumer;
import core.Settings;

public class Textbox extends Focusable implements KeyBoardListener{
	
	private int charsw= 10;
	private int caret= 0;
	private int window= 0;
//	private char[] rarr;
	private final Text content= new Text();
	private final Text renderContent= new Text();

	private static final float pad= .5f;
	private final Frame body= new Frame(this, 0, 0, 1, 1);
	public Textbox(Frame parent, float x, float y, float w){
		super(parent, x, y, w, Settings.fontsize/2*(1+2*pad));
		setHeightAbsolute();
		
		body.pad(pad/(1+2*pad));
		body.visible= false;
	}

	@Override
	public void onResizeMove(){
		if(body==null)
			return;
		charsw= (int)(
				(body.verts[6]-body.verts[0])
				/Settings.fontsize*RenderCore.displayAspect
				);
		if(content!=null)//constructed
			cleanText();
	}
	
	@Override
	public void onFocus(){
		InputCore.kbl= this;
	}
	@Override
	public void key(int key, char c){
		switch(key){
			case Keyboard.KEY_LEFT:
				caret--;
				break;
			case Keyboard.KEY_RIGHT:
				caret++;
				break;
			case Keyboard.KEY_UP:
				caret-=charsw;
				break;
			case Keyboard.KEY_DOWN:
				caret+=charsw;
				break;
				
			case Keyboard.KEY_END:
				caret= content.size();
				break;
			case Keyboard.KEY_HOME:
				caret= 0;
				break;
			
			case Keyboard.KEY_BACK:
				if(caret==0)
					break;
				caret--;
				content.remove(caret);
				break;
			case Keyboard.KEY_DELETE:
				if(caret==content.size())
					break;
				content.remove(caret);
				break;
				
			case Keyboard.KEY_RETURN:
				enter();
				break;
				
			default:
			case 0:
				if(c==0)
					break;
				if(caret==content.size())
					content.add(c);
				else
					content.add(caret, c);
				caret++;
		}
		cleanText();
	}
	
	private StringConsumer out= null;
	public void setConsumer(StringConsumer sc){ out= sc; }
	public void enter(){
		out.consume(content.toString());
		content.clear();
		window=0;
		caret=0;
		cleanText();
	}
	
	private void cleanText(){
		if(caret<0)
			caret=0;
		else if(caret>content.size())
			caret= content.size();
		
		if(caret<window)
			window= caret;
		
		float w=0;
		int count=0;
		for(int i=window; i!=content.size(); i++){
			float nw= w+content.get(i).kern;
			if(nw>charsw)
				break;
			w= nw;
			count++;
		}
		int end= window+count;
		
		if(caret>end){
			count=1;
			w=2;
			for(int i=caret-1; i!=0; i--){//trace back from end
				w+= content.get(i).kern;
				count++;
				if(w>charsw)
					break;
			}
			window= caret-count;
			end= window+count;
		}
		
		List<StylizedCharacter> sl= content.subList(window, end);
		renderContent.clear();
		renderContent.addAll(sl);
	}
	
	@Override
	public void render(){
		if(body==null)
			return;
		super.render();
		float x= body.verts[0];
		float y= body.verts[1];
		renderContent.render(0, 1, 0xffff, x, y);
		Text.renderLn(new String("_"), 
				x+renderContent.getWidthTotalAbsolute(0, caret-window),
				y);
		
//		FontRenderer.render("_", 
//				verts[0]+(caret-window)
//				*FontRenderer.getSize()
//				verts[1], charsw);
	}
}
