package ui.gui;

import entity.Entity;
import entity.EntityPlayer;
import gfx.Camera;
import gfx.ChunkRenderer;
import gfx.DBO;
import gfx.GLProgram;
import gfx.MaterialRenderer;
import gfx.texture.Texture;

import java.util.Arrays;
import java.util.Collection;

import material.Chunk.Node;
import math.Maths;
import math.Transformation;
import module.Module;
import module.material_affectors.MaterialAffector;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;

import world.World;
import core.Client;
import core.Settings;

/**For rendering UIs projected by a tool such as a build grid*/
public class ModuleUIRenderer{
	
	static GLProgram proggrid;
	static int vaogrid;
	static DBO dbogrid;
	static GLProgram progblockghost;
	static int vaobs;
	static DBO dboghost;
	public static void initGL(){
		proggrid= new GLProgram("tool_grid");
		dbogrid= new DBO(512);
		dbogrid.bind();
		vaogrid= GL30.glGenVertexArrays();
		GL30.glBindVertexArray(vaogrid);
		GL20.glEnableVertexAttribArray(0);
		GL20.glVertexAttribPointer(0, 2, GL11.GL_FLOAT, false, 0, 0);
		
		progblockghost= new GLProgram("block", "blockghost");
		progblockghost.addUniform1i("atlasm", Texture.MATERIAL);
		progblockghost.addUniform1i("atlasn", Texture.NORMAL);
		dboghost= new DBO(0);
		vaobs= ChunkRenderer.genVAO(dboghost.get());
	}
	
	public static void render(){
		EntityPlayer p= World.instance.localplayer;
		Entity lh= p.lhand.getHeld();
		Entity rh= p.rhand.getHeld();

		if(lh!=null)
			for(Module mod : lh.moduleIter())
				render(mod);
		if(rh!=null)
			for(Module mod : rh.moduleIter())
				render(mod);
	}
	private static void render(Module m){
			if(m instanceof MaterialAffector){
				MaterialAffector tma= (MaterialAffector)(m);
				Entity aligned= tma.align.aligned;
				if(aligned==null)
					return;
				if(Settings.toolgrid)
					renderGrid(Client.cam, aligned );

				Node gn= tma.makeGhostNode();
				renderBlockShadow(Client.cam, gn, aligned.trans());
			}
			if(m instanceof Object){
				
			}
	}
	
	private static void renderBlockShadow(Camera cam, Node n, Transformation t){
		progblockghost.use();
		cam.setUniform(t, progblockghost.uloc("mvp"));
		GL20.glUniform1i(progblockghost.uloc("invalid"), 0);
		MaterialRenderer.bindAtlases();
		dboghost.prewrite(ChunkRenderer.QUADSIZE);
		ChunkRenderer.loadBuffer(Arrays.asList(n), (float)t.scaley);
		dboghost.push();
		
		GL30.glBindVertexArray(vaobs);
		GL11.glDrawArrays(GL11.GL_TRIANGLES, 0, 6);
	}
	
	private static int count;
	private static Entity lastAligned;
	private static void renderGrid(Camera cam, Entity aligned){
		GL11.glLineWidth(1);
		proggrid.use();
		cam.setUniform(aligned.trans(), proggrid.uloc("mvp"));
		float[] color= Maths.rgbaIntToFloat(Settings.uiColorSecond);
		GL20.glUniform4f(proggrid.uloc("color"), color[0], color[1], color[2], .7f);
		
		if(aligned!=lastAligned){
			aligned.chunk.changedGrid= true;
			lastAligned= aligned;
		}
		if(aligned.chunk.changedGrid){
			count=0;
			Collection<Node> allnodes= aligned.chunk.getAllNodes();
			float[] verts= new float[allnodes.size()*4*2*2];
			for(Node n: allnodes){
				float s= n.scale;
				float x= n.x;
				float y= n.y;
				verts[count++]= x+s;//++
				verts[count++]= y+s;
				verts[count++]= x-s;//-+
				verts[count++]= y+s;
				verts[count++]= x-s;//-+
				verts[count++]= y+s;
				verts[count++]= x-s;//--
				verts[count++]= y-s;
				verts[count++]= x-s;//--
				verts[count++]= y-s;
				verts[count++]= x+s;//+-
				verts[count++]= y-s;
				verts[count++]= x+s;//+-
				verts[count++]= y-s;
				verts[count++]= x+s;//++
				verts[count++]= y+s;
			}
			
			dbogrid.prewrite(count*4);
			DBO.buf.asFloatBuffer().put(verts);
			DBO.buf.position(count*4);
			dbogrid.push();
			aligned.chunk.changedGrid= false;
		}
		
		GL30.glBindVertexArray(vaogrid);
		GL11.glDrawArrays(GL11.GL_LINES, 0, count/2);
	}
}
