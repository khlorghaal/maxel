package ui.gui;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import ui.InputCore;
import ui.KeyMap;

/**For all UIs that intercept keypresses*/
public abstract class Focusable extends Frame{

	/**Instead of trying to ref from outside, make handle methods*/
	protected static Focusable focus;
	public static void setFocus(Focusable f){ focus= f;	f.onFocus();}
	public static boolean hasFocus(){return focus!=null;}

	public static final List<Focusable> instances= new CopyOnWriteArrayList<Focusable>();
	@Override
	public void onDestroy(){
		super.onDestroy();
		instances.remove(this);
	}

	public Focusable(Frame parent, float x, float y, float w, float h){
		super(parent, x, y, w, h);
		instances.add(this);
	}

	static Focusable mousedOver=null;
	public static void mouseMove(float x, float y, int wheel){		
		//drag
		if(focus!=null){
			if(InputCore.localButtons.array[KeyMap.LMB] || InputCore.localButtons.array[KeyMap.RMB])
				focus.drag();
			if(wheel!=0){
				focus.wheel(wheel);
			}
		}
		
		//mouseover
		Focusable m= null;
		for(Focusable f:instances){
			if(f.clickedInside(x, y)){
				m= f;
				continue;
			}
		}
		if(m==mousedOver)//on same frame
			return;
		if(mousedOver!=null)//move off of frame
			mousedOver.onMouseOver(false);
		mousedOver= m;
		if(mousedOver!=null)//move onto
			mousedOver.onMouseOver(true);
	}
	protected void onMouseOver(boolean state){}
	
	public static void refocus(float x, float y){
		int highest=-1;//for overlapping panels
		for(int i=0; i!=instances.size(); i++){
			if(instances.get(i).clickedInside(x, y)){
				highest= i;//instances is sorted
				continue;
			}
		}
		//System.out.println(focus);
		if(highest==-1)//clicked nothing
			defocus();
		else{
			focus= instances.get(highest);
			//invoke click
			//System.out.println(focus);

			//move focus panel to top
			Focusable.instances.remove(focus);
			Focusable.instances.add(focus);
			//moved focus's children on top
			for(Frame f : focus.children){
				if(f instanceof Focusable){
					Focusable.instances.remove(f);
					Focusable.instances.add((Focusable) f);
				}
			}
			focus.onFocus();
		}
	}
	public static void defocus(){
		if(focus!=null)
			focus.onDefocus();
		focus=null;
	}
	protected void onFocus(){}
	protected void onDefocus(){}
	
	/**@param xy root coordinates*/
	boolean clickedInside(float x, float y){
		float ax= getAbsoluteX(), ay= getAbsoluteY();
		float aw= getAbsoluteWidth();
		float ah= getAbsoluteHeight();
		return x>ax && x<ax+aw && y>ay && y<ay+ah;
	}

	protected void drag(){}
	protected void wheel(int wheel){}

	/**unrelated to Button class*/
	static void buttonPress(int index, boolean state) {
		if(focus==null)
			return;
		focus.button(index, state);
	}
	protected void button(int index, boolean state){}
}
