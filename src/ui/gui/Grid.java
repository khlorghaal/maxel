package ui.gui;

public class Grid{
	int row;
	float s;
	/**@param row cells per row
	 * @param aspect of the parent frame. determines vertical spacing*/
	public Grid(int row){
		this.row= row;
		this.s= 1f/row;
	}

	public float[] getxy(int index){
		float x= index%row;
		float y= index/row;
		x*=s;
		y*=s;
		return new float[]{x,y};
	}
	
	/**Sets frame position to lie on grid</br>
	 * Padding is implied</br>
	 * Parent must be square, otherwise use an invisible wrapper that is*/
	public void conformFrame(Frame frame, int index, float pad){
		float[] xyw= getxy(index);
		float wh= s*(1f-pad);
		frame.setWidthHeight(wh, wh);
		pad*=s;
		frame.setPos(xyw[0]+pad, xyw[1]+pad);
	}
}
