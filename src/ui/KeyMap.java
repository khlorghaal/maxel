package ui;

import java.util.HashSet;
import java.util.Set;

import org.lwjgl.input.Keyboard;

public class KeyMap {
	/**indicies of default keyboard keys, entries of default keyindexs
	 * Used strictly to populate defaults - always use clientmappings*/
	static int[] defaultmappings= new int[Keyboard.KEYBOARD_SIZE];

	/**indicies of mapped keyboard keys, entries of keyindexs*/
	static int[] clientmappings= new int[Keyboard.KEYBOARD_SIZE];

	/**Only use when polling keyboard.*/
	public static int getKeyIndex(int keyboardKey){
		return clientmappings[keyboardKey];
	}

	/**These are the keyindexs, named as their default key.<br/>
	 * Always use defualt keys to identify keyindexs*/
	public static final int
	MOUSEOFFSET= Keyboard.KEYBOARD_SIZE-20,
	NULL=0,
	LMB,
	RMB,
	MMB,
	TAB,
	SHIFT,
	CTRL,
	W,	A,	S,	D,
	RIGHT, LEFT, UP, DOWN,
	PAGEUP, PAGEDOWN,
	SPACE,
	ESC,

	ONE, TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, ZERO,
	
	THROW, EAT, PUT,
	
	CONSOLE;

	static int i=1;
	/**length of any bool[] holding keys*/
	public static final int SIZE;
	/////////////////////////////////
	//DEFAULT KEY-KEYINDEX ASSIGNMENT
	static{
		//mouse buttons have fixed values
		defaultmappings[MOUSEOFFSET + 0]=	LMB= i++;
		defaultmappings[MOUSEOFFSET + 1]=	RMB= i++;
		defaultmappings[MOUSEOFFSET + 2]= MMB= i++;

		defaultmappings[Keyboard.KEY_TAB]= TAB= i++;
		defaultmappings[Keyboard.KEY_LSHIFT]= SHIFT= i++;
		defaultmappings[Keyboard.KEY_LCONTROL]= CTRL= i++;
		
		defaultmappings[Keyboard.KEY_W]= W= i++;		
		defaultmappings[Keyboard.KEY_A]= A= i++;
		defaultmappings[Keyboard.KEY_S]= S= i++;
		defaultmappings[Keyboard.KEY_D]= D= i++;
		
		defaultmappings[Keyboard.KEY_RIGHT]= RIGHT= i++;
		defaultmappings[Keyboard.KEY_LEFT ]= LEFT= i++;
		defaultmappings[Keyboard.KEY_UP   ]= UP= i++;
		defaultmappings[Keyboard.KEY_DOWN ]= DOWN= i++;
		
		defaultmappings[Keyboard.KEY_PRIOR]= PAGEUP= i++;
		defaultmappings[Keyboard.KEY_NEXT ]= PAGEDOWN= i++;

		defaultmappings[Keyboard.KEY_SPACE]= SPACE= i++;
		
		defaultmappings[Keyboard.KEY_ESCAPE]= ESC= i++;
		
		defaultmappings[Keyboard.KEY_1]= ONE= i++;
		defaultmappings[Keyboard.KEY_2]= TWO= i++;
		defaultmappings[Keyboard.KEY_3]= THREE= i++;
		defaultmappings[Keyboard.KEY_4]= FOUR= i++;
		defaultmappings[Keyboard.KEY_5]= FIVE= i++;
		defaultmappings[Keyboard.KEY_6]= SIX= i++;
		defaultmappings[Keyboard.KEY_7]= SEVEN= i++;
		defaultmappings[Keyboard.KEY_8]= EIGHT= i++;
		defaultmappings[Keyboard.KEY_9]= NINE= i++;
		defaultmappings[Keyboard.KEY_0]= ZERO= i++;
		

		defaultmappings[Keyboard.KEY_F]= PUT= i++;
		defaultmappings[Keyboard.KEY_G]= THROW= i++;
		defaultmappings[Keyboard.KEY_T]= EAT= i++;
		
		defaultmappings[Keyboard.KEY_GRAVE]= CONSOLE= i++;
		
		SIZE=i;
		
		load();
	}

	static Set<Integer> arrows= new HashSet<>();
	static{
		arrows.add(LEFT);
		arrows.add(RIGHT);
		arrows.add(UP);
		arrows.add(DOWN);
	}
	public static boolean isArrow(int button){ return arrows.contains(button); }
	
	
	public static void mapDefaults(){
		System.arraycopy(defaultmappings, 0, clientmappings, 0, Keyboard.KEYBOARD_SIZE);
	}
	
	static void save(){

	}
	static void load(){
		mapDefaults();
	}
}
