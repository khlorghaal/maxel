package ui.console;

import ui.gui.Console;
import entity.Entity;

public abstract class Command{
	
	public static class Parser implements StringConsumer{
		public static final Parser instance= new Parser();
		@Override
		public void consume(String str){
			Console.out.println(str);
		}
	}
	
	
	public abstract Object invoke(Entity caller);
	

}
