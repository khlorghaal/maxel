package ui;

import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.util.HashSet;
import java.util.Set;

import material.Material;
import math.vec2;

import org.lwjgl.LWJGLException;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.Display;

import ui.gui.Console;
import ui.gui.Gameoverlay;
import world.Controlled;
import world.World;
import core.Client;
import core.Settings;
import core.Timer;
import entity.Buttons;
import gfx.GLProgram;
import gfx.RenderCore;
import gfx.texture.NoiseTextures;


public class InputCore {
	private static Timer timer= new Timer(64);
	public static InputBus clientInputBus= new InputBus();
	
	public static boolean processing= false; 
	
	public static int mxi, myi;
	/**normalized, no aspect*/
	public static vec2 mp= new vec2(), pmp= new vec2();
	/**Global stuff*/
	public static vec2 mg= new vec2(), lmg= new vec2(), dmg= new vec2();
	public static int dw;
	public static Buttons localButtons= new Buttons();
	
	public static void start(){
		thread.setDaemon(true);
		thread.start();
	}
	public static Thread thread= new Thread(new Runnable(){
		@Override
		public void run(){
			
			Client.cdl.countDown();
			try{
				Client.cdl.await();
			}catch(InterruptedException e){e.printStackTrace();}
			
			while(!Client.terminal){
				poll();
				timer.invoke();
			}
		}
	}, "Input");
	
	//only one thing has typing focus in any sane scenario
	public static KeyBoardListener kbl;
	
	public static void poll(){
		processing= true;
		Keyboard.poll();
		//KEYS
		while(!Client.terminal && Keyboard.next()){
			final int ek= Keyboard.getEventKey();
			final boolean keystate= Keyboard.getEventKeyState();
			
			//typing
			if(keystate && kbl!=null){
				kbl.key(ek, Keyboard.getEventCharacter());
			}
			
			//bound keys
			if(!Keyboard.isRepeatEvent()){
				final int index= KeyMap.getKeyIndex(ek);
				localButtons.array[index]= keystate;
				//nonbound keys still gen events 
				clientInputBus.broadcastButton(index, keystate);
				//localplayer
				if(World.instance!=null && World.instance.localplayer!=null)
					World.instance.button(World.instance.localplayer, index, keystate);
				
				if(keystate){
					//debug stuff, nonformal bindless
					if(ek==Keyboard.KEY_F5){
						Console.out.println("_________\nRefreshing");
						Gameoverlay.make(World.instance.localplayer);
						RenderCore.exec.execute(new Runnable(){
							@Override
							public void run(){
								NoiseTextures.init();
								GLProgram.reloadAll();
							}
						});
						RenderCore.exec.execute(new Runnable(){
							@Override
							public void run(){
								NoiseTextures.init();
								NoiseTextures.initGL();
							}
						});
						Material.loadAll(World.instance);
						Text.loadKerning();
					}
					else if(ek==Keyboard.KEY_F11){
						RenderCore.exec.execute(new Runnable(){
							@Override
							public void run(){
								try {
									Settings.fullscreen= !Settings.fullscreen;
									Display.setFullscreen(Settings.fullscreen);
									RenderCore.onResizeDisplay();
								} catch (LWJGLException e) {e.printStackTrace();}
							}
						});
					}
					else if(ek==Keyboard.KEY_PERIOD)
						World.instance.pauseToggle();
					else if(ek==Keyboard.KEY_COMMA)
						World.instance.step();
					else if(index==KeyMap.ESC){
						Client.terminate();
					}
				}
			}
		}
		
		//CLICKS
		while(Mouse.next()){
			if(Mouse.getEventButton()!=-1){
				final int index= KeyMap.getKeyIndex(Math.abs(Mouse.getEventButton()+KeyMap.MOUSEOFFSET));
				final boolean state= Mouse.getEventButtonState();
				localButtons.array[index]= state;
				clientInputBus.broadcastButton(index, state);
				//localplayer
				if(World.instance!=null && World.instance.localplayer!=null)
					World.instance.button(World.instance.localplayer, index, state);
			}
		}
		
		//WHEEL
		dw= Mouse.getDWheel()/120;
		if(InputCore.dw!=0 && !localButtons.array[KeyMap.SHIFT] && !localButtons.array[KeyMap.CTRL])
			Client.cam.zoomloc(InputCore.dw, InputCore.mp);
		
		//m position
		pmp= mp.dup();
		mxi= Mouse.getX();
		myi= Mouse.getY();
		final float w= Display.getWidth();
		final float h= Display.getHeight();
		mp.set(((mxi)/w)*2 -1, ((myi)/h)*2 -1 );
		
		lmg= mg.dup();
		//undo aspect warp because camera also does that itself
		mg= Client.cam.getGlobalMousecoord(mp);
		
		dmg= mg.dup().sub(lmg);
		
		clientInputBus.broadcastMouse(mg, dw);
		//localplayer
		if(World.instance!=null && World.instance.localplayer!=null)
			World.instance.mouse(World.instance.localplayer, mg, dw);
		
		processing= false;
	}
	
	public static boolean isCapslock(){
		return Toolkit.getDefaultToolkit().getLockingKeyState(KeyEvent.VK_CAPS_LOCK);
	}
	public static boolean isScrolllock(){
		return Toolkit.getDefaultToolkit().getLockingKeyState(KeyEvent.VK_SCROLL_LOCK);		
	}
	
	
	
	
	
	
	/**Icore bus used by Client for arbitrary client stuff.
	 * World input bus listens to client one.*/
	public static class InputBus{
		Set<Controlled> listeners= new HashSet<>();
		public void register(Controlled l){
			synchronized(listeners){
				listeners.add(l);
			}
		}
		public void remove(Controlled l){
			synchronized(listeners){
				listeners.remove(l);
			}
		}
		
		public void broadcastMouse(vec2 g, int dw){
			synchronized(listeners){
				for(Controlled c : listeners)
					c.setMouse(g.dup(), dw);
			}
		}
		public void broadcastButton(int index, boolean state){
			synchronized(listeners){
				for(Controlled c : listeners)
					c.setButton(index, state);
			}
		}
	}
}
