package ui;

import gfx.Camera;
import gfx.DBO;
import gfx.Detiler;
import gfx.GLProgram;
import gfx.RenderCore;
import gfx.texture.Texture;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Map;
import java.util.Scanner;

import math.Transformation;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL30;

import core.Profiler;
import core.Settings;
import de.matthiasmann.twl.utils.PNGDecoder;


/**Is intended exclusively for rendering.
 * Thus contains getters only for rendering.*/
public class Text extends ArrayList<Text.StylizedCharacter>{
	private static final long serialVersionUID= 3363377388530625241L;
	
	
	public Text(){}
	public Text(String str){
		add(str);
	}
	public void destroy(){
		RenderCore.exec.execute(new Runnable(){	
			@Override
			public void run(){
				if(dbo!=null){
					dbo.delete();
				}
			}
		});
	}
	
	public void add(String s){
		for(char c : s.toCharArray())
			add(c);
	}
	public void add(char c){
		StylizedCharacter sc= new StylizedCharacter(c);
		add(sc);
	}
	public void add(char[] c){
		for(char ch : c)
			add(ch);		
	}
	public void add(int index, char c){
		StylizedCharacter sc= new StylizedCharacter(c);
		add(index, sc);		
	}
	
	public int getLines(int width){
		int ret=0;
		float ll=0;
		for(int i=0; i!=size(); i++){
			float nll= ll+get(i).kern;
			if( nll>width || get(i).c=='\n' ){
				if(get(i).c=='\n')
					i++;
				i--;
				ll=0;
				ret++;
			}
			else{
				ll= nll;
			}
		}
		return ret;
	}
	public float getWidthTotal(int from, int to){
		if(to>size())
			to= size();
		float ret=0;
		for(int i=from; i!=to; i++){
			ret+= get(i).kern;
		}
		return ret;
	}
	public float getWidthTotalAbsolute(int from, int to){
		return getWidthTotal(from, to)*RenderCore.displayAspectInverse*Settings.fontsize*scale;
	}
	
	@Override
	public String toString(){
		StringBuilder b= new StringBuilder();
		for(StylizedCharacter c: this)
			b.append(c.c);
		return b.toString();
	}
	
	
	
	public static class StylizedCharacter{
		public final char c;
		public final byte modifiers;
		public final int color;
		public final float kern;
		
		public StylizedCharacter(char c, byte modifiers, int color){
			this.c= c;
			this.modifiers= modifiers;
			this.color= color;
			
			Float k= kernTable.get(c);
			if(k==null)
				kern= 1*.78f;
			else
				kern= k*.78f;
		}
		public StylizedCharacter(char c){
			this(c, (byte)0, Settings.fontColor);
		}
		public StylizedCharacter(char c, byte modifiers){
			this(c, modifiers, Settings.fontColor);
		}
		public StylizedCharacter(DataInputStream dis) throws IOException{
			this(
					dis.readChar(),
					dis.readByte(),
					dis.readInt()
					);
		}
		
		public static final byte
		BOLD= 1<<0,
		ITALIC= 1<<1,
		UNDERLINE= 1<<2,
		STRIKETHROUGH= 1<<3;
		public boolean hasModifier(byte m){
			return (modifiers&m)==m;
		}
		
	}
	static Map<Character, Float> kernTable= new Hashtable<>(512);
	static{ loadKerning(); }
	public static void loadKerning(){
		Profiler.start("init Kerning table");
		
		int line= -1;
		try(Scanner sc= new Scanner(new File("kerning.txt"))){
			while(sc.hasNextLine()){
				line++;
				String l= sc.nextLine();
				if(l.length()>=2 && l.substring(0,2).equals("//"))
					continue;
				String[] tokens= l.split(" ");
				if(tokens.length>2)
					throw new Exception("invalid syntax");
				if(tokens.length<2)
					continue;
				float x= Float.parseFloat(tokens[1]);
				
				String cstr= tokens[0];
				for(char c : cstr.toCharArray())
					kernTable.put(c, x);
			}				
		}catch(Exception e){
			System.err.println("Error in kerning.txt at line:"+line);
			e.printStackTrace();
		}
		
		Profiler.stop();
	}
	
	
	
	
	static Texture font;
	/**Number of characters in the font*/
	static final int FONT_LENGTH= 0x100, RES= 8, RESSQ= RES*RES;
	
	static GLProgram prog;	
	public static void initGL(){
		prog= new GLProgram("font");
		prog.use();
		prog.addUniform1i("tex", 0);
		
		FileInputStream is;
		try{
			DBO.preBufferWrite(4*RESSQ*FONT_LENGTH);
			ByteBuffer buf= DBO.buf;
			is= new FileInputStream("image\\font.png");
			new PNGDecoder(is).decode(buf, RES*16*4, PNGDecoder.Format.RGBA);
			is.close();
			buf.flip();
			
			byte[] rast= new byte[buf.limit()];
			buf.get(rast);
			
			buf.clear();
			buf.put(Detiler.DetileRGBA(rast, RES*16, RES*FONT_LENGTH/16,  RES,RES));
			buf.flip();
			font= new Texture(0, GL30.GL_TEXTURE_2D_ARRAY, GL11.GL_RGBA8, GL11.GL_RGBA);
			font.allocate(RES, RES, FONT_LENGTH);
			font.push(GL11.GL_UNSIGNED_BYTE, buf);
			
		}catch(IOException e){ e.printStackTrace(); }		
	}
	
	static final int STRIDE= 4*2+ 2+1+4;
	DBO dbo;
	int vao;
	{
		Runnable r= new Runnable(){
			@Override
			public void run(){
				dbo= new DBO(64*STRIDE);
				vao= GL30.glGenVertexArrays();
				GL30.glBindVertexArray(vao);
				dbo.bind();
				GL20.glEnableVertexAttribArray(0);
				GL20.glEnableVertexAttribArray(1);
				GL20.glEnableVertexAttribArray(2);
				GL20.glEnableVertexAttribArray(3);
				GL20.glVertexAttribPointer(0, 2, GL11.GL_FLOAT, false, STRIDE, 0);
				GL30.glVertexAttribIPointer(1, 1, GL11.GL_UNSIGNED_SHORT, STRIDE, 8);
				GL30.glVertexAttribIPointer(2, 1, GL11.GL_BYTE, STRIDE, 8+2);
				GL30.glVertexAttribIPointer(3, 1, GL11.GL_INT, STRIDE, 8+2+1);
			}
		};
		RenderCore.exec.execute(r);
	}
	
	public float scale= 1;
	
	/**@param lineWidth of 1size characters
	 * @param buf appends vbo data
	 * @param to last line index*/
	public void render(int line, int lineCount, float lineWidth, float x0, float y0){
		if(dbo==null)//lazy initialization
			return;
		
		float x=0;
		float y=0;
		int first= (int)(line*lineWidth);
		if(first>=size() || first<0)
			return;
		//		int iline=0;
		int count=0;
		int lines=0;
		dbo.prewrite((size()-first)*STRIDE);
		for(int i= first; i!=size(); i++){
			StylizedCharacter c= get(i);
			float nx= x+c.kern;
			if(c.c=='\n' || nx>lineWidth){//newline
				y-=1f;
				x= nx=0;
				if(++lines>lineCount)
					break;
				if(c.c=='\n')
					continue;
			}
			
			DBO.buf.putFloat(x);
			DBO.buf.putFloat(y);
			DBO.buf.putShort((short)c.c);
			DBO.buf.put(c.modifiers);
			DBO.buf.putInt(c.color);
			x= nx;
			count++;
		}
		dbo.push();
		
		prog.use();
		font.bind();
		float sx= scale*Settings.fontsize*RenderCore.displayAspectInverse;
		float sy= scale*Settings.fontsize;
		GL20.glUniform1i(prog.uloc("dp"), (int)(sy/2*RenderCore.h));
		GL20.glUniform2f(prog.uloc("scale"), sx, sy);
		GL20.glUniform2f(prog.uloc("corner"), x0+sx/2, y0-sy/2);
		GL30.glBindVertexArray(vao);
		GL11.glDrawArrays(GL11.GL_POINTS, 0, count);
	}
	
	static Text stat= new Text();
	public static void renderLn(final String str, float x0, float y0){
		stat.clear();
		stat.add(str);
		stat.render(0, 1, Integer.MAX_VALUE, x0, y0);
	}
	public static void renderLn(final String str, Camera cam, Transformation m){
		Transformation tr= cam.getModelviewprojeciton(m);
		
		stat.clear();
		stat.add(str);
		stat.render(0, 1, Integer.MAX_VALUE, (float)tr.tx, (float)tr.ty);
	}
	
	public static float getPosFromTop(float line){
		return 1-line*Settings.fontsize;
	}
}
