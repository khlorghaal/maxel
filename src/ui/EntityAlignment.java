package ui;

import java.util.List;

import material.Chunk.Node;
import math.Transformation;
import math.vec2;
import world.World;
import entity.Entity;

public class EntityAlignment{
	public Entity aligned;
	public Node focusedNode= null;
	public vec2 pl, l;
	public void update(World world, vec2 g){
		List<Entity> es= World.instance.getEntitiesInLocation(g, 0);
		//TODO sort Z
		
		Entity paligned= aligned;
		for(Entity e : es){
			if(e.chunk.getID(g)!=0){
				aligned= e;
			}
		}

		if(aligned!=null){
			Transformation tinv= aligned.trans().inverse();
			pl= l.dup();
			l= tinv.use(g);
		}
		if(paligned!=aligned)//reset p upon changing alignment
			pl= l.dup();
		
		if(aligned!=null && aligned.chunk!=null)
			focusedNode= aligned.chunk.getl(l);
	}
}
