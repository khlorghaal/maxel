package world;

import math.vec2;

public interface Controlled{
	public void setMouse(vec2 g, int dw);
	public void setButton(int button, boolean state);
}