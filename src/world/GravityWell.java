package world;

import java.util.concurrent.CopyOnWriteArraySet;

import math.Maths;
import entity.Entity;

public abstract class GravityWell{
	public static CopyOnWriteArraySet<GravityWell> instances= new CopyOnWriteArraySet<>();
	
	public abstract double getX();
	public abstract double getY();
	public double magnitude;
	
	public GravityWell(double magnitude){
		this.magnitude= magnitude;
		instances.add(this);
	}
	public void remove(){
		instances.remove(this);
	}

	public double getAcceleration(double r){
		return magnitude/(r*r);
	}
	public double getAcceleration(double x, double y){
		double r= Maths.len(getX()-x, getY()-y);
		return getAcceleration(r);
	}

	public double getStableOrbitSpeed(double r){
		//ac= v^2/r
		//v= (ac*r)^1/2
		return Math.sqrt(getAcceleration(r)*r);
	}
	public double[] getStableOrbitVelocity(double x, double y){
		double len= Maths.len(x, y);
		double xn= x/len;
		double yn= y/len;
		double mag= getStableOrbitSpeed(len);
		double vx= -yn*mag;
		double vy= xn*mag;
		return new double[]{vx,vy};
	}

	
	public static class GravityWellEntity extends GravityWell{
		Entity e;
		public GravityWellEntity(double magnitude, Entity e){
			super(magnitude);
			this.e= e;
		}
		@Override
		public double getX(){
			return e.trans().tx;
		}
		@Override
		public double getY(){
			return e.trans().ty;
		}
	}
}
