package world;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import material.Chunk;
import material.Chunk.Push;
import material.Material;
import material.Planetology;
import math.Kinematic;
import math.Transformation;
import math.vec2;
import ui.InputCore.InputBus;
import core.Client;
import core.ExecutorQueue;
import core.Main;
import core.ReferenceGlue.Tuple1;
import core.Timer;
import core.UpdateBus;
import entity.Entity;
import entity.EntityPlayer;
import entity.Joint;
import entity.Light;
import entity.celestial.EntityPlanetoid;
import entity.celestial.EntitySun;
import gfx.ChunkRenderer;
import gfx.ExplosionRenderer;
import gfx.Renderer.RenderGroup;
import gfx.SpriteRenderer;
import gfx.SunRenderer;

public class World implements Runnable{
	public static World instance;
	
	public final Thread thread;
	public final InputBus inputBus= new InputBus();
	
	public String name;
	public int id;
	
	public Entity planet;
	public EntityPlayer localplayer;
	public EntitySun sun;
	public Light l; public float solarheight=0x2000;
	
	public final RenderGroup 
	rgMisc= new RenderGroup(){},
	rgChunk= new ChunkRenderer.Group(),
	rgSprite= new SpriteRenderer.Group(),
	rgSun= new SunRenderer.Group(),
	rgShockwave=  new ExplosionRenderer.Group(),
	rgPlume= new ExplosionRenderer.PlumeRenderer.Group();
	
	public World(String name){
		instance= this;
		this.name= name;
		Material.loadAll(this);
		thread= new Thread(this, "World");
		
		thread.start();
	}
	
	/**Should only be used to process buttons and packets.
	 * Using for anything else means that should 
	 * be done in World thread to begin with*/
	public final ExecutorQueue exec= new ExecutorQueue();
	
	public final Timer timer= new Timer(60);
	private boolean pause;
	@Override
	public void run(){
		Transformation pt= new Transformation();
		pt.scale(.5);
		pt.update();
		localplayer= new EntityPlayer(new SpawnData(this, new Kinematic(pt, 1)));
		
		//constructed enough once player is spawned
		Main.worldCdl.countDown();
		Client.onWorldLoad(localplayer);
		
		Transformation st= new Transformation(0, solarheight);
		st.setScale(2000);
		sun= new EntitySun(new SpawnData(this, st), 1, solarheight);
		
		Transformation geot= new Transformation();
		geot.scale(256);
		geot.ty-=geot.scaley+1;
		Kinematic geok= new Kinematic(geot, 1000);
		planet= new EntityPlanetoid(new SpawnData(this, geok), Planetology.terran);
		
		while(!terminate){
			if(pause){
				synchronized(this){
					try{
						this.wait();
					}catch(InterruptedException e){e.printStackTrace();}
				}
			}
			exec.flush();
			this.update();
			exec.flush();
			timer.invoke();
		}	
	}
	public void pause(){
		pause= true;
	}
	public synchronized void step(){
		this.notify();
	}
	public synchronized void unpause(){
		pause= false;
		this.notify();
	}
	public void pauseToggle(){
		if(pause)
			unpause();
		else
			pause();
	}
	
	
	
	public UpdateBus updateBus= new UpdateBus();
	public Tuple1<Boolean> translatingEntities= new Tuple1<>();{translatingEntities.val= false;}
	public final List<Push> pushes= new ArrayList<>();
	public synchronized void update(){
		if((timer.tick)%20==0){
			Chunk.pccheck_macro= Chunk.ccheck_macro;
			Chunk.pccheck_basic= Chunk.ccheck_basic;
			Chunk.pccheck_heavy= Chunk.ccheck_heavy;
			Chunk.ppenetrations= Chunk.penetrations;
			Chunk.penetrations=
					Chunk.ccheck_macro=
					Chunk.ccheck_basic=
					Chunk.ccheck_heavy= 0;
		}
		
		//pre update is done before movement
		//as movement must access the best velocity of an entity
		for(Entity e : entities){
			e.lifetime++;
			e.preUpdate();
		}
		for(Entity e : entities)
			for(Joint j : e.joints)
				j.update();
		
		//collision detection, resolution
		//penetration detection
		for(Entity e : entities)//not using iterator excludes removed
			if(e.world!=null)
				e.move();
		
		//penetration resolution
		//synchro(pushes) unneeded
		for(Push p: pushes)
			p.propagate();
		for(Push p: pushes)
			p.enact();
		for(Entity e : entities)
			e.collisions.clear();
		pushes.clear();
		
		//apply forces
		for(Entity e : entities)
			e.kin.update1();
		
		translatingEntities.val= true;
		for(Entity e : entities)
			e.kin.update2();
		synchronized(translatingEntities){
			translatingEntities.val= false;
			translatingEntities.notifyAll();
		}
		
		for(Entity e : entities)
			e.onPostUpdate();
		
		cleanEntities();
		
		float ticksperday= 100000f;
		float t= (timer.tick%ticksperday)/ticksperday*6.28f;
		sun.trans().tx= Math.cos(t)*solarheight;
		sun.trans().ty= Math.sin(t)*solarheight;
		
		updateBus.broadcast();
	}
	
	
	
	
	private final ArrayList<Entity> entities= new ArrayList<>();
	public int getEntityCount(){ return entities.size(); }
	private final ArrayList<Entity> entitiespadd= new ArrayList<>();
	private final ArrayList<Entity> entitiesprem= new ArrayList<>();
	
	private final Map<Long, Entity> idmap= new HashMap<Long, Entity>();
	private long freeID= 0;//TODO save freeID
	protected final long getEntityID(){
		return freeID++;
	}
	
	public Entity getEntity(long id){
		synchronized(idmap){
			return idmap.get(id);
		}
	}
	public long addEntity(Entity entity){
		entity.world= this;
		synchronized(entitiespadd){
			entitiespadd.add(entity);
		}
		final long id;
		synchronized(idmap){
			if(entity.id==-1){
				id= getEntityID();
				entity.id= id;
			}
			else
				id= entity.id;
			
			idmap.put(id, entity);
		}
		return id;
	}
	public void removeEntity(Entity entity){
		synchronized(entitiesprem){
			entitiesprem.add(entity);
		}
	}
	void cleanEntities(){
		synchronized(entitiespadd){
			for(Entity e : entitiespadd){
				entities.add(e);
				e.onAddToWorld();
			}
			entitiespadd.clear();
		}
		
		synchronized(entitiesprem){
			synchronized(idmap){
				for(Entity e : entitiesprem){
					idmap.remove(e.id);
					entities.remove(e);
					e.world= null;
				}
				entitiesprem.clear();	
			}
		}
	}
	
	/**@return the topmost visible entity*/
	public Entity getEntityAtPoint(vec2 p, double z){
		List<Entity> l= getEntitiesInLocation(p, .1);
		List<Entity> passed= new ArrayList<>();
		for(Entity e : l){
			if(e.chunk.getID(p)!=0)
				passed.add(e);
		}
		Entity e= null;
		if(passed.size()!=0)
			e= passed.get(0);
		return e;
	}
	public List<Entity> getEntitiesInLocation(vec2 p, double r){
		final List<Entity> ret= new ArrayList<Entity>();
		for(Entity e: entities)//TODO spatial hashing
			ret.add(e);
		for(Entity e: ret.toArray(new Entity[0])){
			double re= r+e.trans().scaley+Math.abs(e.kin.v.len());
			re*= 1.45f;//sqrt(2)
			if( Math.abs(e.trans().tx-p.x)>re || Math.abs(e.trans().ty-p.y)>re )
				ret.remove(e);
		}
		return ret;
	}
	
	
	/**Takes from arbitrary thread and run on world thread*/
	public void button(final Controlled sender, 
			final int button, final boolean state){
		exec.execute(new Runnable(){
			@Override
			public void run(){
				sender.setButton(button, state);
			}
		});
	}
	/**Takes from arbitrary thread and run on world thread*/
	public void mouse(final Controlled sender, 
			final vec2 mg, final int dw){
		exec.execute(new Runnable(){
			@Override
			public void run(){
				sender.setMouse(mg, dw);
			}
		});
	}
	
	protected boolean terminate= false;
	public void terminate(){
		terminate= true;
		//TODO save all the things
	}
	protected void save(){
		
	}
	protected void load(){
		
	}
	
	
	//	public Entity ground= new EntityImmoble(this);
	//	{
	//		ground.trans.scale(100);
	//		ground.trans.ty-=50;
	//	}
}
