package world;

import math.Kinematic;
import math.Transformation;
import entity.Entity;

/**You should always apply transformations before spawning to avoid collision fucking*/
public class SpawnData{
	public World world;
	public Kinematic kin;//is cloned
	
	public SpawnData(World world){
		this(world, new Kinematic());
	}
	public SpawnData(World world, Transformation t){
		this(world, new Kinematic(t));
	}
	public SpawnData(World world, Kinematic kin){
		this.world= world;
		assert(world!=null);
		this.kin= kin.clone();
	}
	public SpawnData(Entity clone){
		this(clone.world, clone.kin);
	}
	
	public void apply(Entity e){
		e.world= world;
		e.kin= kin;
	}

}
