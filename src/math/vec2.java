package math;

public final class vec2{
	public double x,y;
	public vec2(){
	}
	public vec2(double x, double y){
		set(x, y);
	}
	public vec2(double s){
		set(s);
	}
	
	public vec2 dup(){
		return new vec2(x,y);
	}
	public vec2 set(vec2 s){
		this.x= s.x;
		this.y= s.y;
		return this;
	}
	public vec2 set(double x, double y){
		this.x= x;
		this.y= y;
		return this;
	}
	public vec2 set(double s){
		this.x= this.y= s;
		return this;
	}
	
	
	public vec2 add(vec2 t){
		x+= t.x;
		y+= t.y;
		return this;
	}
	public vec2 add(double s){
		x+= s;
		y+= s;
		return this;
	}
	public vec2 sub(vec2 t){
		x-= t.x;
		y-= t.y;
		return this;
	}
	public vec2 sub(double s){
		x-= s;
		y-= s;
		return this;
	}
	public vec2 mul(vec2 t){
		x*= t.x;
		y*= t.y;
		return this;
	}
	public vec2 mul(double s){
		x*= s;
		y*= s;
		return this;
	}
	public vec2 div(vec2 t){
		x*= t.x;
		y*= t.y;
		return this;
	}
	public vec2 div(double s){
		x/= s;
		y/= s;
		return this;
	}
	public vec2 recip(){
		x= 1/x;
		y= 1/y;
		return this;
	}
	
	public double len(){
		return Maths.len(x, y);
	}
	public vec2 nrm(){
		double l= len();
		assert(l!=0);
		x/= l;
		y/= l;
		return this;
	}
	
	public double dot(vec2 d){
		return x*d.x + y*d.y;
	}
	public vec2 cross(){
		double xt= x;
		x= -y;
		y= xt;
		return this;
	}
	public vec2 neg(){
		x= -x;
		y= -y;
		return this;
	}
	
	public boolean within(vec2 min, vec2 max){
		return this.x<min.x || this.x>max.x || this.y<min.y || this.y>max.y;
	}
	
	public boolean nan(){
		return Double.isNaN(x) || Double.isNaN(y);
	}
	public boolean inf(){
		return Double.isInfinite(x) || Double.isInfinite(y);
	}
	public boolean naninf(){
		return nan()||inf();
	}
}
