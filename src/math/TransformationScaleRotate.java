package math;

/**Scales then rotates</br>
 * Used mostly for the screen's camera*/
public class TransformationScaleRotate extends Transformation {
	public TransformationScaleRotate(){
		super();
	}
	public TransformationScaleRotate(double i, double j, double d, double e, double f) {
		super(i, j, d, e, f);
	}

	@Override
	protected void scaleApply(){
		mxx*=scalex;
		mxy*=scalex;
		myx*=scaley;
		myy*=scaley;
	}
	
	/**@return a cloned inverse matrix with no translation*/
	@Override
	public Transformation inverseR(){
		return new TransformationScaleRotate(0,0, -theta, 1/scalex,1/scaley);
	}
	
	@Override
	public TransformationScaleRotate clone(){
		final TransformationScaleRotate r= new TransformationScaleRotate(tx, ty, theta, scalex, scaley);
		return r;
	}
}
