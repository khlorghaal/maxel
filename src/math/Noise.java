package math;

import java.util.Random;


public abstract class Noise{
	
	public abstract float get(float x, float y, float z);
	
	
	public static class Perlin2 extends Noise{
		private final float[][][] g;
		private final int xgsiz, ygsiz;
		
		public Perlin2(int gx, int gy, int seed){
			xgsiz= gx;
			ygsiz= gy;
			g= new float[gx][gy][2];
			
			Random rand= new Random(seed);
			for(int x=0; x!=xgsiz; x++){
				for(int y=0; y!=ygsiz; y++){
					if(y==xgsiz-1){
						g[x][y][0]= g[x][0][0];
						g[x][y][1]= g[x][0][1];
						continue;
					}
					if(x==xgsiz-1){
						g[x][y][0]= g[0][y][0];
						g[x][y][1]= g[0][y][1];
						continue;
					}
						
					float vx= rand.nextFloat()*2-1;
					float vy= rand.nextFloat()*2-1;
					float mag= (float)Maths.len(vx,vy);
					g[x][y][0]= vx/mag;
					g[x][y][1]= vy/mag;
				}
			}
		}
		
		@Override
		public float get(float x, float y, float z){
			float vx= x*(xgsiz);
			float vy= y*(ygsiz);
			
			int gx= (int)(vx);
			int gy= (int)(vy);
			
			int gxp= gx+1;if(gxp==xgsiz) gxp=0;
			int gyp= gy+1;if(gyp==ygsiz) gyp=0;
			
			vx-= gx;
			vy-= gy;
			
			float vxp= 1-vx;
			float vyp= 1-vy;
			
			float s= Maths.smooth(vx);
			float t= Maths.smooth(vy);
			
			float g0x= g[gx ][gy ][0];
			float g0y= g[gx ][gy ][1];
			float g1x= g[gx ][gyp][0];
			float g1y= g[gx ][gyp][1];
			float g2x= g[gxp][gy ][0];
			float g2y= g[gxp][gy ][1];
			float g3x= g[gxp][gyp][0];
			float g3y= g[gxp][gyp][1];
			
			float dotnn= g0x*-vx + g0y*-vy;
			float dotnp= g1x*-vx + g1y*vyp;
			float dotpn= g2x*vxp + g2y*-vy;
			float dotpp= g3x*vxp + g3y*vyp;

			return Maths.lerp(s, t, dotnn, dotpn, dotnp, dotpp);
		}
	}
	
	
	public static class Perlin3 extends Noise{
		private final float[][][][] g;
		private final int xgsiz, ygsiz, zgsiz;
		
		public Perlin3(int gx, int gy, int gz, int seed){
			xgsiz= gx;
			ygsiz= gy;
			zgsiz= gz;
			g= new float[gx][gy][gz][3];
			
			Random rand= new Random(seed);
			for(int x=0; x!=xgsiz; x++){
				for(int y=0; y!=ygsiz; y++){
					for(int z=0; z!=zgsiz; z++){
						if(z==zgsiz-1){
							g[x][y][z][0]= g[x][y][0][0];
							g[x][y][z][1]= g[x][y][0][1];
							g[x][y][z][2]= g[x][y][0][2];
							continue;
						}
						if(y==xgsiz-1){
							g[x][y][z][0]= g[x][0][z][0];
							g[x][y][z][1]= g[x][0][z][1];
							g[x][y][z][2]= g[x][0][z][2];
							continue;
						}
						if(x==xgsiz-1){
							g[x][y][z][0]= g[0][y][z][0];
							g[x][y][z][1]= g[0][y][z][1];
							g[x][y][z][2]= g[0][y][z][2];
							continue;
						}
						float vx= rand.nextFloat()*2-1;
						float vy= rand.nextFloat()*2-1;
						float vz= rand.nextFloat()*2-1;
						float mag= (float)Maths.len(vx,vy,vz);
						g[x][y][z][0]= vx/mag;
						g[x][y][z][1]= vy/mag;
						g[x][y][z][2]= vz/mag;
					}
				}
			}
		}
		
		@Override
		public float get(float x, float y, float z){
			float vx= x*(xgsiz);
			float vy= y*(ygsiz);
			float vz= z*(zgsiz);
			
			int gx= (int)(vx);
			int gy= (int)(vy);
			int gz= (int)(vz);
			
			int gxp= gx+1;if(gxp==xgsiz) gxp=0;
			int gyp= gy+1;if(gyp==ygsiz) gyp=0;
			int gzp= gz+1;if(gzp==zgsiz) gzp=0;
			
			vx-= gx;
			vy-= gy;
			vz-= gz;
			
			float vxp= 1-vx;
			float vyp= 1-vy;
			float vzp= 1-vz;			
			
			float r= Maths.smooth(vx);
			float s= Maths.smooth(vy);
			float t= Maths.smooth(vz);
			
			float g0x= g[gx ][gy ][gz ][0];
			float g0y= g[gx ][gy ][gz ][1];
			float g0z= g[gx ][gy ][gz ][2];
			float g1x= g[gxp][gy ][gz ][0];
			float g1y= g[gxp][gy ][gz ][1];
			float g1z= g[gxp][gy ][gz ][2];
			float g2x= g[gx ][gyp][gz ][0];
			float g2y= g[gx ][gyp][gz ][1];
			float g2z= g[gx ][gyp][gz ][2];
			float g3x= g[gxp][gyp][gz ][0];
			float g3y= g[gxp][gyp][gz ][1];
			float g3z= g[gxp][gyp][gz ][2];
			float g4x= g[gx ][gy ][gzp][0];
			float g4y= g[gx ][gy ][gzp][1];
			float g4z= g[gx ][gy ][gzp][2];
			float g5x= g[gxp][gy ][gzp][0];
			float g5y= g[gxp][gy ][gzp][1];
			float g5z= g[gxp][gy ][gzp][2];
			float g6x= g[gx ][gyp][gzp][0];
			float g6y= g[gx ][gyp][gzp][1];
			float g6z= g[gx ][gyp][gzp][2];
			float g7x= g[gxp][gyp][gzp][0];
			float g7y= g[gxp][gyp][gzp][1];
			float g7z= g[gxp][gyp][gzp][2];
			
			float dot0= g0x*-vx + g0y*-vy + g0z*-vz;
			float dot1= g1x*vxp + g1y*-vy + g1z*-vz;
			float dot2= g2x*-vx + g2y*vyp + g2z*-vz;
			float dot3= g3x*vxp + g3y*vyp + g3z*-vz;
			float dot4= g4x*-vx + g4y*-vy + g4z*vzp;
			float dot5= g5x*vxp + g5y*-vy + g5z*vzp;
			float dot6= g6x*-vx + g6y*vyp + g6z*vzp;
			float dot7= g7x*vxp + g7y*vyp + g7z*vzp;
			
			return Maths.lerp(r, s, t, dot0, dot1, dot2, dot3, dot4, dot5, dot6, dot7);
		}
	}
	
	
	public static class Worley2 extends Noise{
		private final int count;
		private final float[][] points;
		public Worley2(int count){
			this.count= count;
			this.points= new float[count][2];
			
			Random r= new Random();
			for(int i=0; i!=count; i++){
				points[i][0]= r.nextFloat();
				points[i][1]= r.nextFloat();
			}
		}
		
		@Override
		public float get(float x, float y, float z){
			// TODO Auto-generated method stub
			return 0;
		}
	}
	
	
}