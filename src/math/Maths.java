package math;

public class Maths {
	public static final double PI= Math.PI;
	public static final double TAU= 2*PI;	
	public static final double SQRT2= Math.sqrt(2);
	
	public static float clamp(float t, float a, float b){
		if(t>b)
			return b;
		if(t<a)
			return a;
		return t;
	}
	public static double clamp(double t, double a, double b){
		if(t>b)
			return b;
		if(t<a)
			return a;
		return t;
	}
	
	public static float lerp(float t, float a, float b){
		return t*(b-a)+a;
	}
	
	/**cd ^ </br>
	 * ab t </br>
 	 * s >  </br>
	 * */
	public static float lerp(float s, float t, float a, float b, float c, float d){		
		float ab= lerp(s, a, b);
		float cd= lerp(s, c, d);		
		return lerp(t, ab, cd);
	}
	
	public static float lerp(float r, float s, float t,
			float a, float b, float c, float d, float e, float f, float g, float h){
		float top= lerp(r,s, a,b,c,d);
		float bot= lerp(r,s, e,f,g,h);
		return lerp(t, top, bot);
	}

	public static float smooth(float t){
		return t*t*t*(t*(t*6 - 15) + 10);
	}
	
	public static double len(double x, double y){
		return Math.sqrt(x*x+y*y);
	}
	public static double len(double x, double y, double z){
		return Math.sqrt(x*x+y*y+z*z);
	}
	
	public static double[] tangent(double lx, double ly){
		return new double[]{ -ly, lx };
	}
	public static float[] tangent(float lx, float ly){
		return new float[]{ -ly, lx };
	}
	
	public static float[] rgbaIntToFloat(int rgba){
		return new float[]{
				((rgba&0xFF000000)>>>0x18)/255f,
				((rgba&0x00FF0000)>>>0x10)/255f,
				((rgba&0x0000FF00)>>>0x8 )/255f,
				((rgba&0x000000FF)>>>0x0 )/255f,	
		};
	}
	public static float[] rgbIntToFloat(int rgb){
		return new float[]{
				((rgb&0x00FF0000)>>>0x10)/255f,
				((rgb&0x0000FF00)>>>0x8 )/255f,
				((rgb&0x000000FF)>>>0x0 )/255f,	
		};
	}
}
