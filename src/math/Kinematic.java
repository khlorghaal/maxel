package math;

import java.io.DataOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

import material.Chunk;
import material.Chunk.Node;
import core.Saveable;

public class Kinematic implements Saveable{
	/**Center of mass - rotational center, relative to chunkcoords 
	 * Is not the origin of the chunk*/
	public vec2 c= new vec2();
	public vec2 v= new vec2();
	
	public double m, w, I;
	
	public boolean gravimetry= false;
	static final float frict= .998f;
	
	/**true for one tick after a collision, to prevent jitter*/
	//	public boolean collided;
	
	public Transformation trans, ptrans;
	
	public Kinematic(){
		this.trans= new Transformation();
		this.ptrans= new Transformation();
		this.m= 1;
	}
	public Kinematic(Transformation trans){
		this(trans, 1);
	}
	public Kinematic(Transformation trans, double mass){
		this.trans= trans.clone();
		this.ptrans= trans.clone();//be sure to not alias >:C
		this.m= mass;
	}
	@Override
	public Kinematic clone(){
		Kinematic ret= new Kinematic(trans, m);
		ret.c= this.c.dup();
		ret.v= this.v.dup();
		ret.w= this.w;
		ret.gravimetry= this.gravimetry;
		return ret;
	}
	
	/**translational and rotational tangental*/
	public vec2 getV(vec2 l){
		return l.dup().cross().mul(w).add(v);
	}
	public double getMoment(vec2 p){
		return p.dup().sub(c).len()*I;
	}
	
	/**@param n normal to surface hit
	 * @param jmul impulse multiplier, 0 doesnt slow object, 1 stops, 2 bounces*/
	public void hit(vec2 g, vec2 n,	double jmul, Kinematic that){
		return;
		if(true)
			return;
		double mmul= 1/(1/m + 1/that.m);
		
		vec2 r1= this.trans.inverse().use(g);
		vec2 r2= that.trans.inverse().use(g);

		vec2 v1= getV(r1);
		vec2 v2= that.getV(r2);
		
		//j= 2n*dot(v,n)*m
		double vdotn= -(vxr*nx+vyr*ny);
		double jx= nx*vdotn;
		double jy= ny*vdotn;
		jx*= mmul;
		jy*= mmul;
		
		jx*=jmul;
		jy*=jmul;
		
		vx+= jx/m;
		vy+= jy/m;
		that.vx-= jx/that.m;
		that.vy-= jy/that.m;

		if( Double.isNaN(vx) || Double.isInfinite(vx)
				|| Double.isNaN(vy) || Double.isInfinite(vy)
				|| Double.isNaN(that.vx) || Double.isInfinite(that.vx)
				|| Double.isNaN(that.vx) || Double.isInfinite(that.vx)){
			assert(false);
		}
		
//		this.impulse(r1x, r1y, jx, jy);
//		that.impulse(r2x, r2y, -jx, -jy);		
	}
	public void impulseGlobal(vec2 g, vec2 gj){
		if(true)
			return;
		Transformation inv= trans.inverse();
		double lx= inv.useX(gx, gy);
		double ly= inv.useY(gx, gy);
		Transformation r= new Transformation().rotate(inv.theta);
		double jx= r.useRX(gjx, gjy);
		double jy= r.useRY(gjx, gjy);
		impulse(lx, ly, jx, jy);
	}
	public void impulse(double lx, double ly, double jx, double jy){
		if(true)
			return;
		vx+= jx/m;
		vy+= jy/m;
		
		if(true)//TODO
			return;
		
		double rx= lx-cx;
		double ry= ly-cy;
		double rmag= Maths.len(rx, ry);
		double rnx= rx/rmag;
		double rny= ry/rmag;
		
		double jmag= Maths.len(jx, jy);
		double jnx= jx/jmag;
		double jny= jy/jmag;
		
		//nr * impulse dot nr
		double trdot= rnx*jx+rny*jy;
		double jtx= rnx*trdot;
		double jty= rny*trdot;
		vx+= jtx/m;
		vy+= jty/m;
		
		
		//nimpulse dot (ncenter x zunit)
		double RcrossZ_x= rny;
		double RcrossZ_y= rnx;
		
		double jw= 1;
		//rotational v
		//		w+= jw/m;
	}
	
	/**Recalculates mass, center of mass, and rotational mass*/
	public void reevaluate(Chunk c){
		double mass=0;
		vec2 nc= new vec2();
		for(Node n: c.getNodes(4)){
			double nm= n.getMaterial().density*n.scale*n.scale;
			nc.add(new vec2(n.x, n.y).mul(nm));
			mass+= nm;
		}
		nc.div(mass);
		
		//adjust pos so new center doesnt leap the object
		trans.translate(nc.dup().sub(this.c));
		
		this.c= nc.dup();
	}
	
	/**position change is condensed into a seperate tight loop,
	 * so as to not tear when rendering*/
	public vec2 dp= new vec2();
	public double dth;
	public void update1(){
		dth+= w;
		dp.add(v);
		
		//TODO process impulses properly
		
		//TODO force fields
		if(gravimetry){
			v.y-= .0012;
		}
		if(frict!=0)
			v.mul(frict);
		ptrans.set(trans);
	}
	/**enacts and clears buffered position change*/
	public void update2(){		
		trans.translate(dp);
		trans.theta+= dth;
		dp.set(0);
		dth=0;
		trans.update();
	}
	
	
	@Override
	public String toString(){
		return 
				"m:"+m+'\n'+
				"vx:"+v.x+'\n'+
				"vy:"+v.y+'\n'+
				"w:"+w+'\n';
	}
	
	@Override
	public void save(DataOutputStream o) throws IOException{
		o.writeDouble(m);
		o.writeDouble(w);
		o.writeDouble(c.x);
		o.writeDouble(c.y);
		o.writeDouble(v.x);
		o.writeDouble(v.y);
	}
	public Kinematic load(ByteBuffer b, Transformation trans){
		Kinematic r= new Kinematic(trans, b.getDouble());
		r.c.x= b.getDouble();
		r.c.y= b.getDouble();
		r.v.x= b.getDouble();
		r.v.y= b.getDouble();
		return r;
	}
}
