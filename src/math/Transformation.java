package math;

import java.io.DataOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

import core.Saveable;

/**Always rotates then scales*/
public class Transformation implements Saveable{
	public double tx, ty; public vec2 t(){ return new vec2(tx,ty);}
	public void translate(vec2 t){ tx+= t.x; ty+= t.y; }
	public double mxx, mxy, myx, myy;

	/**Always call reset after writing these*/
	public double theta, scalex=1, scaley=1;
	
	/**Identity loaded*/
	public Transformation(){
		mxx= 1;
		myy= 1;
		this.scalex=1;
		this.scaley=1;
	}
	public Transformation(double theta){
		this.theta= theta;
		update();
	}
	public Transformation(double tx, double ty){
		this();
		this.tx= tx;
		this.ty= ty;
	}
	public Transformation(vec2 t){
		this();
		this.tx= t.x;
		this.ty= t.y;
	}
	public Transformation(double tx, double ty, 
			double theta, double scalex, double scaley){
		this(tx, ty);
		this.scalex= scalex;
		this.scaley= scaley;
		this.theta= theta;
		update();
	}
	public Transformation(double tx, double ty, 
			double mxx, double mxy, double myx, double myy){
		this(tx, ty);
		this.mxx=mxx;
		this.mxy=mxy;
		this.myx=myx;
		this.myy=myy;
	}

	@Override
	public Transformation clone(){
		final Transformation r= new Transformation();
		r.tx= this.tx;
		r.ty= this.ty;
		r.mxx= this.mxx;
		r.mxy= this.mxy;
		r.myx= this.myx;
		r.myy= this.myy;
		r.theta= this.theta;
		r.scalex= this.scalex;
		r.scaley= this.scaley;
		return r;
	}
	public void set(Transformation t){
		this.mxx= t.mxx;
		this.mxx= t.mxx;
		this.mxx= t.mxx;
		this.mxx= t.mxx;
		this.theta= t.theta;
		this.scalex= t.scalex;
		this.scaley= t.scaley;
	}

	public void load(Transformation tr){
		this.tx=     tr.tx;
		this.ty=     tr.ty;
		this.scalex= tr.scalex;
		this.scaley= tr.scaley;
		this.theta=  tr.theta;
		this.mxx=    tr.mxx;
		this.mxy=    tr.mxy;
		this.myx=    tr.myx;
		this.myy=    tr.myy;
	}

	/**For nonrelative use public txy*/
	public void translateRelative(double[] xy){ translateRelative(xy[0], xy[1]);}
	/**For nonrelative, use public txy*/
	public void translateRelative(double x, double y){
		this.tx+= x*mxx	+ y*mxy;
		this.ty+= y*myx + y*myy;
	}

	public Transformation rotate(double theta){this.theta+= theta; this.update(); return this;}
	
	/**Redoes matrix based off non mat values: trans, theta, then scale*/
	public void update(){
		this.theta%= Math.PI*2;
		//e^(it)= cos(t) + isin(t) = x, y
		//ma1 = cos(t) , sin(t)          = [  cos -sin ]
		//ma2 = cos(t+pi/4), sin(t+pi/4) = [  sin  cos ]

		final double
		cost= Math.cos(theta),
		sint= Math.sin(theta), 
		rxx= cost,
		rxy= -sint,
		ryx= sint,
		ryy= cost;
		//workaround scale
		
		//first row
		mxx= rxx;
		mxy= rxy;
		//second row
		myx= ryx;
		myy= ryy;
		
		scaleApply();
	}
	protected void scaleApply(){
		mxx*=scalex;
		myx*=scalex;
		myy*=scaley;
		mxy*=scaley;
	}

	public void setScale(double s){
		this.scalex= this.scaley= s;
		this.update();
	}
	public void setScale(double x, double y){
		this.scalex= x;
		this.scaley= y;
		this.update();
	}
	public void scale(double s){
		this.scalex*= s;
		this.scaley*= s;
		this.update();
	}
	public void scale(double x, double y){
		this.scalex*= x;
		this.scaley*= y;
		this.update();
	}
	/**Used for when a camera wants a screen-absolute center, 
	 * this preserves the camera's center*/
	public void scaleTransPreserving(double scale){

		double s1= scaley;//scalar
		scale(scale);
		//tAbs = t1/scl1 = t2*scl2
		//t2 = t1*scl2/scl1
		double k= scaley/s1;
		tx*= k;
		ty*= k;
		
	}

	
	public final double[] use(double[] xy){return use(xy[0], xy[1]);}
	public final double[] use(double x, double y){
		return new double[]{
				x*mxx + y*mxy + tx,
				x*myx + y*myy + ty
		};
	}
	public final double[] useR(double[] xy){return useR(xy[0], xy[1]);}
	public final double[] useR(double x, double y){
		return new double[]{
			x*mxx + y*mxy,
			x*myx + y*myy
		};
	}
	public final double useX(double x, double y){return mxx*x + mxy*y + tx;}
	public final double useY(double x, double y){return myx*x + myy*y + ty;}
	public final double useRX(double x, double y){return mxx*x + mxy*y;}
	public final double useRY(double x, double y){return myx*x + myy*y;}
	public final vec2 use(vec2 a){ return new vec2(mxx*a.x + mxy*a.y + tx, myx*a.x + myy*a.y + ty); }
	public final vec2 useR(vec2 a){ return new vec2(mxx*a.x + mxy*a.y, myx*a.x + myy*a.y); }
	
	public void invert(){
		this.load(this.inverse());
	}
	public Transformation inverse(){
		//use is local to global
		//= vector rotatescale then translate
		//= matrix translate then rotatescale
		//therefore
		//inverse is global to local
		//= vector detranslate then derotatescale
		//= matrix derotatescale then detranslate
		final Transformation inv= inverseR();
		inv.tx= -inv.useX(tx, ty);
		inv.ty= -inv.useY(tx, ty);
		return inv;
	}
	/**@return a cloned inverse matrix with no translation*/
	public Transformation inverseR(){
		return new Transformation(0,0, -theta, 1/scalex,1/scaley);
	}

	/**Translates additively. Invalidates scale*/
	public static Transformation productR(Transformation left, Transformation right){
		final Transformation ret= new Transformation();
		ret.tx= left.tx + right.tx;
		ret.ty= left.ty + right.ty;

		ret.scalex= Double.NaN;
		ret.scaley= Double.NaN;

		//11 10
		//01 00
		ret.mxx= left.mxx*right.mxx + left.mxy*right.myx;

		//10 11
		//00 01
		ret.mxy= left.mxx*right.mxy + left.mxy*right.myy;

		//01 00
		//11 10
		ret.myx= left.myx*right.mxx + left.myy*right.myx;

		//00 01
		//10 11
		ret.myy= left.myx*right.mxy + left.myy*right.myy;

		return ret;
	}
	/**Regular matrix multiply*/
	public static Transformation product(Transformation left, Transformation right){

		final Transformation ret= new Transformation();
		ret.mxx= left.mxx*right.mxx + left.mxy*right.myx;
		ret.mxy= left.mxx*right.mxy + left.mxy*right.myy;
		ret.myx= left.myx*right.mxx + left.myy*right.myx;
		ret.myy= left.myx*right.mxy + left.myy*right.myy;
		ret.tx= left.mxx*right.tx + left.mxy*right.ty + left.tx;
		ret.ty= left.myx*right.tx + left.myy*right.ty + left.ty;
		return ret;
	}

	/**Converts one local coordinate to another. Map should be used more > 1 invocation.*/
	public static double[] convert(double[] lxy, Transformation local, Transformation target){return convert(lxy[0], lxy[1], local, target);}
	/**Converts one local coordinate to another. Map should be used more > 1 invocation.*/
	public static double[] convert(double lx, double ly, Transformation local, Transformation target){
		final double[] global= local.use(lx, ly);
		return target.inverse().use(global);
	}

	public static Transformation map(Transformation local, Transformation that){
		//TODO does this even work?
		//to global
		//local*v
		//to that
		//thatinverse*(local*v)
		return Transformation.product(that.inverse(), local);
		//		return Transformation.productR(local,  that.inverse());
		//		return Transformation.product( that.inverse(), local);
	}


	public void put(ByteBuffer buf){
		buf.putDouble(mxx);
		buf.putDouble(mxy);
		buf.putDouble(tx);

		buf.putDouble(myx);
		buf.putDouble(myy);
		buf.putDouble(ty);
	}
	
	@Override
	public String toString(){
		return super.toString()+"\n"
				+ "tx: " +tx + "\n"
				+ "ty: " +ty + "\n"
				+ "theta: " +theta + "\n"
				+ "scalex: " +scalex + "\n"
				+ "scaley: " +scaley + "\n";
	}
	@Override
	public void save(DataOutputStream b) throws IOException{
		b.writeDouble(tx);
		b.writeDouble(ty);
		b.writeDouble(theta);
		b.writeDouble(scalex);
		b.writeDouble(scaley);
	}
	public static Transformation load(ByteBuffer b){
		return new Transformation(b.getDouble(), b.getDouble(),
				b.getDouble(),
				b.getDouble(), b.getDouble());
	}
}
