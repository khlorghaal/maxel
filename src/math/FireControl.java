package math;

public class FireControl{
	
	//TODO drag
	/**@param pxy_vxy_s position, current velocity, and launch speed of projectile
	 * @return change in targets position*/
	public static double[] aimd(Kinematic target, double[] pxy_vxy_s){
		double tx= target.trans.tx;
		double ty= target.trans.ty;
		
		double dx= tx-pxy_vxy_s[0];
		double dy= ty-pxy_vxy_s[1];
		
		//TODO

		return new double[]{dx,dy};
	}
	
	/**@return aim angle on global basis*/
	public static double thetag(Kinematic target, double[] pxy_vxy_s){
		double[] aimd= aimd(target, pxy_vxy_s);
		return Math.atan2(aimd[1]-pxy_vxy_s[1], aimd[0]-pxy_vxy_s[0]);
	}
}
