package material;

import java.util.ArrayList;
import java.util.Arrays;

public class Chemistry{
	
	public static Reaction getReaction(Material[] mats){
		return null;
	}
	
	public static boolean soluble(Material add, Material[] solution){
		return true;
	}
	
	
	public static class Quantity{
		public final Material mat;
		public float amount;
		public Quantity(Material mat, float amount){
			this.mat= mat;
			this.amount= amount;
		}
	}
	
	static final ArrayList<Reaction> reactions= new ArrayList<>(); 
	public static class Reaction{
		public final Quantity[] reagents, products;
		/**Amount = rate/concentration*/
		public final Quantity catalyst;
		public final float baseRate, activationTemp, enthalpy;
		public Reaction(Quantity[] reagents, Quantity[] products,
				Quantity catalyst,
				float baseRate,
				float activationTemp, float enthalpy){
			this.reagents= reagents;
			this.products= products;
			this.catalyst= catalyst;
			this.baseRate= baseRate;
			this.activationTemp= activationTemp;
			this.enthalpy= enthalpy;
		}
		
		public float rate(float temperature, float catalyst){
			if(temperature<activationTemp
					||( this.catalyst!=null && catalyst==0 ))
				return 0;
			return 1;
		}
		public boolean react(Solution s){
			if(!s.quantities.containsAll(Arrays.asList(reagents)))
				return false;
			
			return true;
		}
	}
}
