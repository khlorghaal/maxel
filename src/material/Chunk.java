package material;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.Stack;

import material.Chemistry.Quantity;
import math.Kinematic;
import math.Maths;
import math.Transformation;
import math.vec2;
import world.SpawnData;
import entity.Entity;
import entity.EntityDust;

/**Acts as intermediate between global coords or other chunk coords 
 * and this's quadtree*/
public class Chunk{
	public static final int MAXIMUM_DEPTH= 16;
	
	public final Entity e;
	public Transformation trans(){return e.trans();}
	
	public LoadManager loadman= LoadManager.make(this);
	
	public final Node root;
	public boolean changedVBO= true;
	public boolean changedGrid= true;
	
	/**blocks which have an exposed side which may collide*/
	public final List<Node> outsides= new LinkedList<Node>();
	

	public Chunk(Entity e){
		this.e= e;
		root= new Node(Material.NULLMAT.id, 0);
	}
	public Chunk(Entity e, int itype){
		this(e);
		root.typ= itype;
	}
	public synchronized void destroy(){
		this.e.destroy();
	}
	
	public static class Hit implements Comparable<Hit>{
		public Node a, b;
		public double t;
		public vec2 n, p;
		public Hit(Node a, Node b, double t, vec2 n, vec2 g){
			this.a= a;
			this.b= b;
			this.t= t;
			this.n= n.dup();
			this.p= g.dup();
		}
		@Override
		public int compareTo(Hit o){
			if(this.t<o.t)
				return -1;
			if(this.t>o.t)
				return 1;
			return 0;
		}
	}
	public static class Push implements Comparable<Push>{
		public Entity pusher, pushed;
		public vec2 dp;
		public double mag, weight;
		public Push(Entity pusher, Entity pushed, vec2 dp){
			this.pusher= pusher;
			this.pushed= pushed;
			this.dp= dp.dup();
			this.mag= dp.len();
		}
		public Push(Entity pusher, Entity pushed, vec2 dp, double mag){
			this.pusher= pusher;
			this.pushed= pushed;
			this.dp= dp.dup();
			this.mag= mag;
		}
		/**construction does not necessetate enactment, this does*/
		public void commit(){
			if(pusher.world==null || pushed.world==null)
				return;//if an entity is destroyed it will have a nulled world
			if(pushed.push==null){
				pushed.push= this;
				pushed.world.pushes.add(this);
			}
			else{//already has a push
				if(pushed.push.pusher==this.pusher)
					return;
				pushed.push.dp.add(this.dp);
				if(this.weight>pushed.push.weight)
					pushed.push.weight= this.weight;
			}
		}
		
		/**progress through everything pushing heavy and light.
		 * May penetrate into objects as a result of this, 
		 * but will resolve all existing penetration*/
		public void propagate(){
//			double biggest=0;

			if(pusher.push!=null){//all pushes on pusher affect this
				this.dp.add(pusher.push.dp);
			}


//			for(Entity collision : pushed.collisions){//push all touching objects
//				if(collision.trans.scaley<=pusher.trans.scaley){//smaller than this
//					collision.kin.dpx+= pushx;
//					collision.kin.dpy+= pushy;
//				}	
//			}
		}
		
		public void enact(){
			penetrations++;
			pushed.kin.dp.add(dp);
		}
		
		@Override
		public int compareTo(Push that){
			return (int)(this.mag-that.mag);
		}
	}

	public static int ccheck_macro=0, ccheck_basic=0, ccheck_heavy=0;
	public static int pccheck_macro=0, pccheck_basic=0, pccheck_heavy=0;
	public static int penetrations=0, ppenetrations=0;
	public final synchronized Hit collide(Chunk that, Kinematic kin0, Kinematic kin1){
		Hit ret= null;
		Push push= null;
		synchronized(that){
			ccheck_macro++;
			ArrayList<Hit> hits= new ArrayList<>();
			
			Transformation thatthis= Transformation.map(that.trans(), this.trans());
			Transformation thisthat= Transformation.map(this.trans(), that.trans());
			Transformation thisrsi= this.trans().inverseR();
			Transformation thatrsi= that.trans().inverseR();
			
			Stack<Node> thisstack= new Stack<>();
			thisstack.push(this.root);
			thisloop:while(!thisstack.empty()){
				Node thisnode= thisstack.pop();
				
				float 
				s0= thisnode.scale,
				x0= thisnode.x, y0= thisnode.y;
				float[] xy0= new float[]{
						x0-s0, y0-s0,
						x0-s0, y0+s0,
						x0+s0, y0-s0,
						x0+s0, y0+s0
				};
				
				Stack<Node> thatstack= new Stack<>();
				thatstack.push(that.root);
				thatloop:while(!thatstack.empty()){
					Node thatnode= thatstack.pop();
					
					ccheck_basic++;
					
					float 
					s1= thatnode.scale,
					x1= thatnode.x, y1= thatnode.y;
					float[] xy1= new float[]{
							x1-s1, y1-s1,
							x1-s1, y1+s1,
							x1+s1, y1-s1,
							x1+s1, y1+s1
					};
					vec2 vd= kin1.v.dup().sub(kin0.v);//TODO tangental, for each point
//					vnxd*=3;
//					vnyd*=3;
					vec2 vl= thisrsi.useR(vd).add(1.e-32);
					boolean hit= boundcheck(xy1, xy0, thatthis, vl);
					boolean thisSpace= hit;
					if(!hit){
						vd.neg();
						vl= thatrsi.useR(vd).add(+1.e-32);
						hit= boundcheck(xy0, xy1, thisthat, vl);
					}
					if(hit){
						//nodes are hitting
						//if nodes are parents, continue recursion
						if(thisnode.children!=null){
							for(Node n:thisnode.children)
								thisstack.push(n);
							continue thisloop;
							//aborts thatloop
						}
						if(thatnode.children!=null){
							for(Node n:thatnode.children)
								thatstack.push(n);
							continue thatloop;
						}
						//if nodes are terminal do a collision
						if(thisnode.typ!=0 && thatnode.typ!=0){
							ccheck_heavy++;
							//							kin0.vx=kin0.vy=kin1.vx=kin1.vy= 0;
							//							return;
							
							Transformation mapping= thisSpace? thatthis:thisthat;
							float[] 
									points= thisSpace? xy1:xy0, 
									corners= thisSpace? xy0:xy1;
							float
							uexn=corners[0],//unexpanded, priori
							uexp=corners[4],
							ueyn=corners[1],
							ueyp=corners[3],
							xn= uexn,
							xp= uexp,
							yn= ueyn,
							yp= ueyp;
							if(vl.x<0)
								xp-= vl.x*2;
							else
								xn-= vl.x*2;
							if(vl.y<0)
								yp-= vl.y*2;
							else
								yn-= vl.y*2;
							
							boolean penetrated= false;
							
							int p;
							for(p=0; p!=8;){
								float 
								lx= points[p++],
								ly= points[p++],
								tmpx= (float)mapping.useX(lx, ly),
								tmpy= (float)mapping.useY(lx, ly);
								lx= tmpx;
								ly= tmpy;
								
								if(lx>=xn && lx<=xp && ly>=yn && ly<=yp){
									//postvelocity point is inside
									//det intersecting edge
									//det intersection point
									
									//p.a + v.a*t = c.a + e.a*s
									//t= c.a-p.a+e.a*s / v.a
									//always hits nearest edge per axis
									Node n= thisSpace? thisnode:thatnode;
									double exn= n.x-n.scale;
									double exp= n.x+n.scale;
									double eyn= n.y-n.scale;
									double eyp= n.y+n.scale;
									double dxn= exn-lx;
									double dxp= exp-lx;
									double dyn= eyn-ly;
									double dyp= eyp-ly;
									boolean xng= dxn<dxp ^ (vl.x<0);
									boolean yng= dyn<dyp ^ (vl.y<0);
									double ex= xng? exn:exp;
									double ey= yng? eyn:eyp;
									//double ex= vlx>0? exn:exp;
									//double ey= vly>0? eyn:eyp;
									double tx= (ex-lx)/vl.x;
									double ty= (ey-ly)/vl.y;
									boolean axisIsX= Math.abs(tx)<Math.abs(ty);//the edge hitting, axis being offset
									double t= axisIsX? tx:ty;
									if(t<0){
										axisIsX= !axisIsX;
										t= axisIsX? tx:ty;
										if(t<0){//if still negative, reset to the smaller abs
											axisIsX= !axisIsX;
											t= axisIsX? tx:ty;
										}
									}
									if(Double.isInfinite(t))
										continue;
									if(Double.isNaN(t))
										assert(false);
									if(t>1)
										continue;
									
									vec2 hl= new vec2(lx, ly).add(vl.dup().mul(t));
									//make sure is on the square
									//and not just the edgeline 
									//nor within the velocity box erroneously
									if(axisIsX){
										if(hl.y<eyn || hl.y>eyp)
											continue;
									}
									else{
										if(hl.x<exn || hl.x>exp)
											continue;
									}

									//penetration
									//push out translationally
									if(t<0){
										penetrated= true;
									}
									
									
									vec2 nl= new vec2();
									if(axisIsX){
										nl.y=0;
										nl.x= -Math.signum(vl.x);
									}
									else{
										nl.x=0;
										nl.y= -Math.signum(vl.y);
									}
									Transformation local= thisSpace? this.trans():that.trans();
									vec2 ng= local.useR(nl).nrm();
									vec2 hg= local.use(hl);
									hits.add(
											new Hit(thisnode, thatnode, t, 
													ng, hg)
											);
									//dont break, two may hit
								}
							}

							if(penetrated){
								//pushing is based off center of nodes and rotated scale
								vec2 pushl;
								vec2 pushdp;
								
								//get working variables
								Node ln, rn;//local, remote
								double lnx, lny;
								double rnx, rny;
								double lns, rns;
								Transformation ltr, rmltr;//localspace, r->l mapping
								boolean movingthat= this.trans().scaley>that.trans().scaley;
								if(movingthat){
									ltr= this.trans();
									rmltr= thatthis;
									ln= thisnode;
									rn= thatnode;
								}
								else{
									ltr= that.trans();
									rmltr= thisthat;
									ln= thatnode;
									rn= thisnode;
								}
								//locations and scales
								lnx= ln.x;
								lny= ln.y;
								lns= ln.scale;
								rnx= rmltr.useX(rn.x, rn.y);
								rny= rmltr.useY(rn.x, rn.y);
								double mxxabs= Math.abs(rmltr.mxx);
								double myxabs= Math.abs(rmltr.myx);
								rns= Math.abs( mxxabs>myxabs? mxxabs:myxabs );
								rns*= rn.scale;
								//the maximum displacement from the rotatescale mapping
								
								//determine displacement and penetration
								//how much rn needs pushed away from ln
								double cx= rnx-lnx;//center distance
								double cy= rny-lny;
								double sx= Math.signum(cx)*(lns+rns);
								double sy= Math.signum(cy)*(lns+rns);
								double px= cx - sx;
								double py= cy - sy;
								px*=-1.01;
								py*=-1.01;
								//if abs(centers)>abs(scales), no penetration
								if(Math.abs(cx)>Math.abs(sx)) px=0;
								if(Math.abs(cy)>Math.abs(sy)) py=0;
								boolean pushingX= Math.abs(px)<Math.abs(py);
								
								//which axis is being pushed
								if(pushingX){
									pushl= new vec2(px, 0);
								}
								else{
									pushl= new vec2(0, py);
								}
								
								//local to global
								pushdp= ltr.useR(pushl);
								
								//one push per two-entity check
								double magpush= pushdp.len();
								if(push!=null && magpush<=push.mag)
										continue;
																
								if(movingthat)
									push= new Push(this.e, that.e, pushdp, magpush);
								else
									push= new Push(that.e, this.e, pushdp, magpush);
							}							
						}
					}
				}
			}
			
			if(hits.size()!=0){
				Collections.sort(hits);
				Hit h0= hits.get(0);
				
				
				////if(hits.size()>2)
				////h= hits.get(1);
				////is max instead of min because if hitting two squares
				////will hit outside edge of one and inner edge of other
				////depending on velocity, may have shorter time to inner edge
				////if this conflict arises, outer edge almost always has greater t
				//float vdotn= (float)(h.nx*(kin0.vx-kin1.vx) + h.ny*(kin0.vy-kin1.vy));
				//double mul= h.a.impact(h.b, vdotn);
				//double mul=.61f;
				//kin0.hit(h.x, h.y, h.nx,h.ny,
				//mul, kin1);
								
				int hs= hits.size();
				hs=1;
				for(Hit h : hits){
					if(h.t > h0.t+.2)
						break;
					vec2 dv= kin0.v.dup().sub(kin1.v);
					double vdotn= h.n.dot(dv);
//					vdotn/= hs;
					
//					double mul= h.a.impact(h.b, vdotn, h.x, h.y);
					double mul=.51f;
//					mul/= hs;
					kin0.hit(h.p, h.n,mul, kin1);
					ret= h;
//					break;
				}

				hits.clear();

				assert(!kin0.v.naninf());
			}
			
			if(push!=null)
				push.commit();
			
		}
		return ret;
	}
	
	/**cheap bounding algorithm to see if hitting*/
	private static boolean boundcheck(float[] points, float[] corners, Transformation tr,
			vec2 v){
		v= v.dup();
		v.mul(2);
		float //edges, not unique to these indicies 
		xn=corners[0],
		xp=corners[4],
		yn=corners[1],
		yp=corners[3];
		if(v.x<0)//expand in appropriate direction
			xp-= v.x;
		else
			xn-= v.x;
		if(v.y<0)
			yp-= v.y;
		else
			yn-= v.y;
		
		int p;
		for(p=0; p!=8;){
			float 
			x= points[p++],
			y= points[p++],
			tx= (float)tr.useX(x, y),
			ty= (float)tr.useY(x, y);
			x= tx;
			y= ty;
			
			if(x>=xn && x<=xp && y>=yn && y<=yp)
				return true;
		}
		return false;
	}
	
	
	public class Node {
		public Chunk chunk= Chunk.this;
		/**Arranged as </br>
		 * 2 3</br>
		 * 0 1*/
		public Node[] children= null;
		public Node parent;
		
		/**== -log2(scale)*/
		public int depth;
		/**signed normalized*/
		public float x,y, scale;
		/**Volume in global units*/
		public float getVolume(){
			float ret= scale*(float)chunk.trans().scaley;
			return ret*ret;
		}
		/**Not really a normal, spherical angles.
		 * 7-5 zenith [0,90] 7p ; 4-0 azimuth [0,360] 31p
		 * Sent to gpu as 3x3 rotation matrix*/
		public byte normal;
		public byte cleaveNumber= 0;
		public double getx(Transformation t){return t.useX(x, y);}
		public double gety(Transformation t){return t.useY(x, y);}
		
		public float health=1;
		public float temp;
		
		protected int typ; public int getID(){return typ;}
		public Material getMaterial(){
			int t= this.typ;//localvar incase desync
			if(t>=Material.array.size())
				return Material.NULLMAT;
			return Material.array.get(t);
		}
		/**should only be used for roots*/
		private Node(int typ, int depth){
			this.typ= typ;
			this.depth= depth;
			this.scale= depthToScale(depth);
		}
		/**For dummy nodes that dont interact with other things*/
		public Node(){}
		
		/**The proper constructor*/
		public Node(Node parent, int type, int index){
			this(type, parent.depth+1);
			this.parent= parent;
			parent.children[index]= this;
			//2 3
			//0 1
			x= parent.x;
			y= parent.y;
			switch(index){
				case 0:
					x-=scale; y-=scale;
					break;
				case 1:
					x+=scale; y-=scale;
					break;
				case 2:
					x-=scale; y+=scale;
					break;
				case 3:
					x+=scale; y+=scale;
					break;
			}
		}
		
//		@Override
//		public Node clone(){
//			assert(this.children==null);
//			Node ret= new Node();
//			ret.x= this.x;
//			ret.y= this.y;
//			ret.chunk= this.chunk;
//			ret.depth= this.depth;
//			ret.scale= this.scale;
//			ret.normal= this.normal;
//			ret.health= this.health;
//			ret.temp= this.temp;
//			ret.typ= this.typ;
//			
//			return ret;
//		}
		
		/**Resets depth, scale,volume, x,y based on tree location.
		 * Applied recursively for loaded children*/
		public void reset(){
			synchronized(Chunk.this){
				//TODO
				if(this.children!=null)
					for(Node ch : this.children)
						ch.reset();
			}
		}
		
		public final void set(Material mat){ set(mat.id); }
		/**Does not check for homogeneity; removes all children*/
		public void set(int id){
			if(id==typ && id!=0)//continue upon 0 because setting parents
				return;
			synchronized(Chunk.this){
				removeChildren();
				if(id==0){
					removeChildren();
					if(this==root)//TODO multilayer
						Chunk.this.destroy();
					Cleaver.cleave(Chunk.this, this);
				}
				typ= id;
				
				defragmentParents();
				changedVBO= true;
			}
		}
		private void removeChildren(){
			//			if(children!=null){
			//				children[0].remove();
			//				children[1].remove();
			//				children[2].remove();
			//				children[3].remove();
			this.children= null;
			//no references to children are maintained
			//			}
		}
		public void defragment(){
			if(children!=null && isHomogenousFragments())
				set(this.children[0].typ);
		}
		public void defragmentParents(){
			if(this.parent==null || !this.parent.isHomogenousFragments())
				return;
			//move up through parents until one is herterogenous
			//or until root is reached
			Node p= this.parent;
			while(p.parent!=null && p.parent.isHomogenousFragments()){
				p= p.parent;
			}
			
			p.set(typ);
		}
		/**@return has children && all of the same type*/
		private boolean isHomogenousFragments(){
			if(children==null)
				return false;
			
			//childrens' children will not be homogenous fragments
			//unless a node has been manually fragmented
			//therefore frament() is private
			if(children[0].children!=null
					||children[1].children!=null
					||children[2].children!=null
					||children[3].children!=null)
				return false;
			
			final int c0= children[0].typ;
			return children[1].typ== c0
					&& children[2].typ== c0
					&& children[3].typ== c0;
		}
		/**must be private to prevent homogenous children, 
		 * which would bork defragmenting*/
		private void fragment(){
			synchronized(Chunk.this){
				children= new Node[4];
				new Node(this, typ, 0);
				new Node(this, typ, 1);
				new Node(this, typ, 2);
				new Node(this, typ, 3);
				this.typ= 0;
				changedVBO= true;
			}
		}
		
		/**@return multiplier of collision impulse*/
		public float impact(Node that, float v, double gx, double gy){
			Material m1= this.getMaterial();
			Material m2= that.getMaterial();
			float elas= m1.elas>m2.elas? m1.elas:m2.elas;
			float mass1= (float)(this.chunk.e.kin.m);
			float mass2= (float)(that.chunk.e.kin.m);
			float ke= v* mass1<mass2? mass1:mass2;
//			ke= 10000;
			
			//normalized durability subtractor
			float dmg1= this.getDamage(ke);
			float dmg2= that.getDamage(ke);
			
			if(dmg1==0 && dmg2==0)
				return 1+elas;//fully elastic

			float srel= that.scale/this.scale;
			//one block breaking, damage the other = ke needed to break
			if(this.health-dmg1<0 || that.health-dmg2<0){//a node is breaking
				//solve for minimum ke needed to break one
				float breakke1= this.getBreakKE();
				float breakke2= that.getBreakKE();
				float breakke= Math.min(breakke1, breakke2)*1.0001f;
				this.damage( this.getDamage(breakke), gx, gy );
				that.damage( that.getDamage(breakke), gx, gy );
				return 1-(ke-breakke)/ke;//nondeflective, speed loss
			}
			else{//neither is breaking
				this.damage(dmg1, gx, gy);
				that.damage(dmg2, gx, gy);
				//ke-keabsorbed = keremain
				float keabsorbed= m1.getKE(dmg1, this.getVolume()) + m2.getKE(dmg2, that.getVolume());
				return 1 + elas*(ke-keabsorbed)/ke;//semielastic
			}
		}
		public float getDamage(float ke){
			return getMaterial().getDamage(ke, getVolume());
		}
		public float getBreakKE(){
			return getMaterial().getKE(health, getVolume());
		}
		
		public void damage(float amount, double gx, double gy){
			health-= amount;
			if(health<=0.01){
				Quantity q= new Quantity(getMaterial(), getVolume());
				Transformation t= e.trans().clone();
				t.translateRelative(x, y);
				t.scale(scale*.95);
				SpawnData sd= new SpawnData(e.world, t);
				
				set(0);
				new EntityDust(sd, q);
				
				//TODO fx
			}
		}

		
		@Override
		public String toString(){
			return super.toString()+" typ:"+Material.array.get(typ).name+" dep:"+depth;
		}
		
		public byte[] getData(){
			return null;
		}
	}
	
//	/**Checks all nodes to see if they have children which can be merged*/
//	public void consolidate(){
//		synchronized(Chunk.this){
//			Collection<Node> nodes= getAllNodes();
//			for(Node n : nodes)
//				n.defragment();
//			//TODO
//		}
//	}
	
	public class Seeker{
		private double x, y, s;
		public Node current;
		private boolean invalidCoords;
		public Seeker(vec2 l){
			checkBounds(l);
			if(invalidCoords)
				return;
			current= root;
			this.x= l.x;
			this.y= l.y;
			this.s= 1;
		}
		private void checkBounds(vec2 l){
			invalidCoords= l.within(new vec2(-1), new vec2(1));
		}
		public Node next(){
			int i=0;
			//2 3
			//0 1
			if(x>0)
				i+=1;
			if(y>0)
				i+=2;
			
			s/=2f;
			//recenter on current node
			switch(i){
				case 0:
					x+=s; y+=s;
					break;
				case 1:
					x-=s; y+=s;
					break;
				case 2:
					x+=s; y-=s;
					break;
				case 3:
					x-=s; y-=s;
					break;
			}
			current= current.children[i];
			return current;
		}
		public Node end(){
			while(current.children!=null)
				next();
			return current;
		}
	}
	
	/**Fragments deeply until a block of right depth is at location</br></br>
	 * If size < current location resolution.</br>

	 * If size > current location resolution,
	 * sets first node of size== invsize

	 * @param xy local coordinates of point
	 * @return the node of depth at location*/
	public synchronized Node setl(int typ, vec2 p, int depth){
		synchronized(Chunk.this){
			if(depth>=0){
				Seeker st= new Seeker(p);
				if(st.invalidCoords)
					return null;
				
				while(st.current.depth!=depth){
					if(st.current.children==null){
						if(st.current.typ==typ)
							return null;//dont fragment if frag parent same as typ
						st.current.fragment();
					}
					st.next();
				}
				st.current.set(typ);
				return st.current;
			}
			else{
				return null;
			}
		}
	}
	public synchronized Node setg(int typ, vec2 g, float scale){
		int d= scaleToDepth(scale);
		if(d>MAXIMUM_DEPTH)
			d= MAXIMUM_DEPTH;
		return setg(typ, g, d);
	}
	public synchronized Node setg(int typ, vec2 g, int depth){
		Transformation tinv= trans().inverse();
		return setl(typ, tinv.use(g), depth);
	}
	
	
	//ga global a
	//la local a
	//all functions which take a transformation inout global coordinates
	
	/**convenience for null check*/
	public synchronized int getID(vec2 g){
		Node gn= getg(g);
		if(gn==null)
			return 0;
		return gn.typ;
	}
	public synchronized Node getg(vec2 g){
		return getl(trans().inverse().use(g));
	}
	/**@param xy from [-1.0, 1.0] relative to this's scale
	 * @return the node exactly on xy*/
	public synchronized Node getl(vec2 l){
		Seeker st= new Seeker(l);
		if(st.invalidCoords)
			return null;
		return st.end();
	}
	/**@param xy from [-1.0, 1.0] relative to this's scale
	 * @param depth does not return nodes smaller than this, will return the parent
	 * @return the node exactly on xy*/
	public synchronized Node getl(vec2 l, int depth){
		Seeker st= new Seeker(l);
		if(st.invalidCoords)
			return null;
		while(st.current.children!=null && st.current.depth!= depth)
			st.next();
		return st.current;
	}
	
	//	/**@return all nodes within the quad*/
	//	List<Node> get(double gx0, double gy0, double gx1, double gy1){
	//		Transformation i= trans.inverse();
	//		double[] l0= i.use(gx0, gy0);
	//		double[] l1= i.use(gx0, gy0);
	//		return getl(l0[0], l0[1], l1[0], l1[1]);
	//	}
	//	/**@return all nodes within the quad*/
	//	public abstract List<Node> getl(double lx0, double ly0, double lx1, double ly1);
	
	/**@return all nodes of all scales touching this*/
	public synchronized Collection<Node> getAllNeighbors(Node n){
		final Collection<Node> ret= new ArrayList<>();
		for(int dir=0; dir!=4; dir++)
			ret.addAll(getNeightbors(n, dir));		
		return ret;
	}
	/**&nbsp&nbsp1</br>
	 * 2&nbsp&nbsp0</br>
	 * &nbsp&nbsp3*/
	public synchronized Collection<Node> getNeightbors(Node n, int dir){
		assert(n!=null);
		final float d= n.scale*2;
		float dx, dy;
		ArrayList<Node> ret= new ArrayList<>();

		//  1
		//2   0
		//  3
		switch(dir){
			case 0:
				dx= d;
				dy= 0;
				break;
			case 1:
				dx= 0;
				dy= d;
				break;
			case 2:
				dx= -d;
				dy= 0;
				break;
			case 3:
				dx= 0;
				dy= -d;
				break;
			default:
				assert(false);
				return ret;
		}
		
		final Node largn= getl( new vec2(n.x+dx, n.y+dy), n.depth);
		//the neighbor node of same size or bigger than c
		//absolute edge will always have a node >= the current
		if(largn==null)
			return ret;
		
		
		Stack<Node> looking= new Stack<>();
		looking.push(largn);
		
		//recurse through neighbor's children along dir,
		//adding touching end nodes
		while(!looking.isEmpty()){
			Node l= looking.pop();
			if(l.children==null){
				ret.add(l);
				continue;
			}
			
			//   2 3
			//2   1   3
			//  2   0
			//0   3   1
			//   0 1
			int i0;
			int i1;
			switch(dir){
				case 0:
					i0= 1;
					i1= 3;
					break;
				case 1:
					i0= 2;
					i1= 3;
					break;
				case 2:
					i0= 0;
					i1= 2;
					break;
				case 3:
					i0= 0;
					i1= 1;
					break;
				default:
					assert(false);
				i0=-1;
				i1=-1;
			}
			
			looking.push(l.children[i0]);
			looking.push(l.children[i1]);
		}
		return ret;
	}
	
	/**2^-(deep)*/
	public static float depthToScale(int deep){
		return 1f/(1<<(deep));
	}
	public static int scaleToDepth(double scale){
		return (int)(Math.log(1f/(scale))/Math.log(2));
	}
	public synchronized float getVolume(int mip){
		//TODO mips approximate well?
		float ret=0;
		for(Node n: getNodes(mip))
			ret+= (n.getVolume());
		return ret;
	}
		
	public synchronized Collection<Node> getAllNodes(){
		return getAllNodes(root);
	}
	public static Collection<Node> getAllNodes(Node first){
		Stack<Node> remaining= new Stack<>();
		List<Node> done= new ArrayList<>();
		remaining.push(first);
		//does a sort of recursion, adding all nodes including 0 nodes
		while(!remaining.isEmpty()){
			Node n= remaining.pop();
			done.add(n);
			if(n.children!=null)
				for(Node ch: n.children){
					if(ch==null)
						assert(false);
					remaining.push(ch);
				}
		}
		return done;
	}
	public synchronized Collection<Node> getNodes(int layer){
		Set<Node> remaining= new HashSet<>();
		List<Node> done= new ArrayList<>();
		remaining.add(root);
		
		for(int l=0; l!=layer && !remaining.isEmpty(); l++){
			Set<Node> rem_sub= new HashSet<>();//seperate from done for perf
			Set<Node> rem_add= new HashSet<>();
			for(Node n: remaining){
				if(l+1!=layer && n.children!=null)
					for(Node ch: n.children)
						rem_add.add(ch);
				rem_sub.add(n);
				done.add(n);
			}
			remaining.removeAll(rem_sub);
			remaining.addAll(rem_add);
		}
		return done;
	}
}
