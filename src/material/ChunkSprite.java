package material;

import entity.Entity;
import entity.EntityPlayer;
import gfx.texture.Texture;

/**Consists of nodes of constant, arbitrary shape.</br>
 * Rendered as a single texture*/
public class ChunkSprite extends Chunk{
	public ChunkSprite(Entity e){
		super(e, Material.BIOLOGICAL.id);
	}
	
	public Texture texmaterial, texgeometry;
	
	public static class ChunkHuman extends ChunkSprite{

		public ChunkHuman(EntityPlayer e){
			super(e);
			root.set(Material.BIOLOGICAL.id);
		}
		
	}
}
