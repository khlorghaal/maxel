package material;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import material.Chunk.Node;
import math.Noise;
import math.vec2;
import core.Profiler;

public abstract class Planetology{
	public static final Planetology terran= new Planetology(0){
		{
			g= 1;
			minscale= .25;
			seed= 0;
		}
		int grass= Material.get("grass").id;
		int dirt= Material.get("dirt").id;
		@Override
		protected void genBiomes(Chunk c, float x, float s, float delta, int depth){
			int y=0;
			for(int i=0; i!=2; i++){
				c.setl(grass, new vec2(x, s+delta*y--),depth);
			}
			for(int i=0; i!=4; i++){
				c.setl(dirt, new vec2(x, s+delta*y--),depth);
			}
		}
	};
	
	protected int seed;
	protected Noise surfaceNoise, caveNoise;
	
	public int 
	bulkMaterialID= Material.get("stone").id,
	mantleID= Material.get("leaf").id;
	public float mantleHeight= .5f;
	
	public double g, minscale;
	
	
	public Planetology(int seed){
		this.seed= seed;
		this.surfaceNoise= new Noise.Perlin2(40, 1, seed);
		this.caveNoise= new Noise.Perlin2(8, 64, seed);
	}
	
	private static ExecutorService genExec= Executors.newSingleThreadExecutor();
	static{
		genExec.execute(new Runnable(){
			@Override
			public void run(){
				Thread.currentThread().setName("Terrain Generator");
			}
		});
	}
	public static void terminate(){
		genExec.shutdownNow();
	}
	
	public final void gen(final Node partition){
		genExec.execute(new Runnable(){			
			@Override
			public void run(){
				Profiler.start("planet gen: "+partition.chunk.e);
				gen_(partition);
				Profiler.stop();
			}			
		});
	}
	protected void gen_(final Node partition){
		//TODO partition use
		final Chunk c= partition.chunk;
		partition.set(bulkMaterialID);
		
		float delta= (float)(minscale/(c.e.trans().scalex));
		int depth= Chunk.scaleToDepth(delta);
		delta*=2;
		
		for(float x= -1+delta/2; x<1; x+=delta){
			float s= getSurfaceHeight(x);
			genMantle(c, x, delta,depth);
			genBiomes(c, x,s, delta,depth);
			carveSurface(c, x,s, delta,depth);
			genCaves(c, x,s, delta,depth);
		}
	}
	
	public float getSurfaceHeight(float x){
		x= x/2+.5f;
		x%=1;
		float n= surfaceNoise.get(x, 0, 0)/2+.5f;
		return ((float)Math.pow( n*.9 , 5))*.5f+.75f;
	}
	
	protected void carveSurface(Chunk c, float x, float s, float delta, int depth){
		float y= s;
		c.setl(0, new vec2(x, y), depth);
		for(; y<1; y+=delta){
			c.setl(0, new vec2(x, y), depth); 
		}
	}
	protected void genCaves(Chunk c, float x, float s, float delta, int depth){
		for(float y=s; y>-1; y-=delta){
			float a= caveNoise.get(x/2+.5f,y/2+.5f,0);
			a= Math.abs(a);
			a= (float)Math.sqrt(a);
			a= .1f-a;
			//			if(a>0)
			//				c.setl(0, x, y, depth);
		}
	}
	protected void genMantle(Chunk c, float x, float delta, int depth){
		if(mantleHeight==0)
			return;
		float d= Chunk.depthToScale(depth);
		for(float y= mantleHeight*2-1; y>=-1; y-=d){
			c.setl(mantleID, new vec2(x, y), depth);
		}
	}
	
	protected abstract void genBiomes(Chunk c, float x, float s, float delta, int depth);
	
	public static class Mineral{
		public double sparsity, width, concentration;
		protected Noise noise;
		
		public int getConcentration(float x, float y){
			float n= noise.get(x, y, 0);
			n= .5f-Math.abs(n);
			if(n<0) n=0;
			return (int)(n*10);
		}
		
	}
}
