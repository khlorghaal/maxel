package material;

import java.util.ArrayList;

import material.Chemistry.Quantity;

public class Solution{
	
	public float temperature;
	
	final ArrayList<Quantity> quantities= new ArrayList<>();
	public Solution(){
		
	}
	
	public float getAmount(Material mat){
		int ind= -1;
		for(int i=0; i<quantities.size(); i++)
			if(quantities.get(i).mat == mat)
				ind= i;
		if(ind==-1)
			return 0;
		return quantities.get(ind).amount;
	}
	
	public float total(){
		float r= 0;
		for(Quantity q : quantities)
			r+= q.amount;
		return r;
	}
	
	public float getConcentration(Material mat){
		return getAmount(mat)/total();
	}
	
	
	public void add(Material mat, float quantity){
		
	}
	public void subtract(Material mat, float quantity){
		
	}
	
}
