package material;

import java.util.Set;

import material.Chunk.Node;
import core.Server;
import entity.EntityFile;

public abstract class LoadManager{

	private final Chunk chunk;
	protected LoadManager(Chunk chunk){
		this.chunk= chunk;
	}
	public static LoadManager make(Chunk chunk){
		if(Server.instance==null)
			return new LoadManagerClient(chunk);
		else
			return new LoadManagerServer(chunk);
	}
	private static class LoadManagerClient extends LoadManager{
		protected LoadManagerClient(Chunk chunk){super(chunk);}

		@Override
		protected void saveNode(Node n){}

		@Override
		protected Node loadNode(float lx, float ly, int depth){
			//TODO query server 
			return null;
		}
		
	}
	private static class LoadManagerServer extends LoadManager{
		EntityFile file;
		protected LoadManagerServer(Chunk chunk){
			super(chunk);
			file= new EntityFile(chunk.e);
		}

		@Override
		protected void saveNode(Node n){
			//TODO write 
		}

		@Override
		protected Node loadNode(float lx, float ly, int depth){
			//TODO load
			return null;
		}
		
	}

	Set<Node> active;
	public void loadRegion(Node n, int depth){
		
	}
	public void unloadAll(){
		synchronized(this.chunk){
			for(Node n : active)
				saveNode(n);
		}
	}
	
	protected abstract void saveNode(Node n);
	protected abstract Node loadNode(float lx, float ly, int depth);
}