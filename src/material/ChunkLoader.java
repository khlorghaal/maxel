package material;

import java.awt.image.Raster;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class ChunkLoader {
	public static String directory= ".\\image\\";
	public Raster loadRaster(String filename){
		try {
			ImageIO.read(new File(directory+filename));
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
}
