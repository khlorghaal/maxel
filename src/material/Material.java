package material;

import gfx.MaterialRenderer;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import world.World;
import core.FileUtil;
import core.NameMap;

public class Material{
	private static NameMap idmap= new NameMap();
	public static ArrayList<Material> array= new ArrayList<Material>();
	private static File file;
	
	public final int id;
	public final String name;
	public float 
	density, 
	elas,hard,dur=1, 
	heatcond, melt;
	
	public float[] noisedata;
	public byte red, green, blue;
	public short gfxdata;
	
	/**constructed before loading all*/
	public static Material NULLMAT, EXPLOSION, BIOLOGICAL, DUST;
	//	public static Material BIOLOGICAL;
	public static void makeStaticMats(){
		final float BIG= Float.MAX_VALUE/2;
		NULLMAT= new Material("NULL", "", 0, 0, 0,
				0xff00ff,.5f, 0, 0, 0, Noisedef.NULLDEF);
		EXPLOSION= new Material("Explosion", "", BIG, 0, 0,
				0xff0000,0, 0,0,0, Noisedef.NULLDEF);
		BIOLOGICAL= new Material("Biological", "", BIG, 0, 0, 
				0x888888,.5f, 0,0,0, Noisedef.NULLDEF);
		DUST= new Material("Dust", "", BIG, 0, 0, 0, 0, 0, 0, 0, Noisedef.NULLDEF);
	}
	
	/**@param hardness the physic property of impact resistance
	 * @param q heat resistance*/
	public Material(String name, String category,
			float hardness, float heatconductivity, float meltingpoint,
			int rgb, float specularity, 
			float transparency, int metallicity, int iridescence, 
			Noisedef... noisedefs){
		this.dur= hardness;
		this.hard= hardness/10;
		this.elas= .1f;
		this.id= idmap.popID(name);
		this.name= name;
		synchronized(array){
			while(array.size()<id+1)
				array.add(NULLMAT);
			array.set(id, this);
		}
		
		noisedata= new float[4 + 2*(3+2)];
		int i=0;
		red  = (byte)((rgb&0xff0000)>>16);
		green= (byte)((rgb&0x00ff00)>>8);
		blue = (byte)((rgb&0x0000ff));
		
		//material packing
		//iridescence2|metallicity2|transparency8|specularity5
		//ii|mm|tttttttt|sssss
		int iri  = iridescence <<(5+8+2);
		int metl = metallicity <<(5+8);
		int trnsp= (int)( transparency*((1<<8)-1)) <<(5);
		int spec = (int)( specularity*((1<<5)-1));
		gfxdata= (short)(iri|metl|trnsp|spec);
		
		
		Noisedef n;
		if(noisedefs.length>0)
			n= noisedefs[0];
		else
			n= Noisedef.NULLDEF;
		noisedata[i++]= n.data[0];
		noisedata[i++]= n.data[1];
		noisedata[i++]= n.data[2];
		noisedata[i++]= n.data[3];
		noisedata[i++]= n.data[4];
		if(noisedefs.length>1)
			n= noisedefs[1];
		else
			n= Noisedef.NULLDEF;
		noisedata[i++]= n.data[0];
		noisedata[i++]= n.data[1];
		noisedata[i++]= n.data[2];
		noisedata[i++]= n.data[3];
		noisedata[i++]= n.data[4];
		//		if(noisedefs.length>2)
		//			n= noisedefs[2];
		//		else
		//			n= Noisedef.NULL;
		//		gfxdata[i++]= n.data[0];
		//		gfxdata[i++]= n.data[1];
		//		gfxdata[i++]= n.data[2];
		//		gfxdata[i++]= n.data[3];
	}
	public static Material get(String name){
		return array.get(idmap.getID(name));
	}
	
	public float getDamage(float ke, float vol){
		float kemh= ke-hard;
		if(kemh<=0)
			return 0;
		return kemh/dur/vol;
	}
	public float getKE(float damage, float vol){
		return damage*dur*vol+hard;
	}
	
	public float getBounce(float ke){
		float kemh= ke-hard;
		if(kemh>0)
			return 0;
		return elas;
	}
		
	
	
	
	
	
	
	
	
	public static Material load(String text){
		//physical properties - gameplay affectors
		//color - diffuse rgb and specularity, negative specularity causes iridescence
		//noise - becomes the normal map - mag*noise(uv*scl,func))^exp
		//noise2 - added to the other noise.
		
		//name category hardness heatconductivity meltingpoint
		//rgb s
		//mag0 scl0 exp0 func0
		
		//Noise constants
		//defined by
		//!name mag scl exp func
		//referenced by
		//@name
		
		//Example file
		
		//stone rock 1 .1 200
		//a0a0a0 .2
		//1 .1 4 abs
		//@rough
		//
		//!rough .1 6 1 x 
		error:{try{
			String[] lines= text.split("\n");
			if(lines.length<2)
				break error;
			String[][] tokens= new String[2][];//tokens per line
			tokens[0]= lines[0].split(" ");
			tokens[1]= lines[1].split(" ");
			if(tokens[0].length!= 5 || tokens[1].length!= 2)
				break error;
			
			String 
			name= tokens[0][0],
			cat= tokens[0][1];
			float 
			hard= Float.parseFloat(tokens[0][2]),
			cond= Float.parseFloat(tokens[0][3]), 
			mp= Float.parseFloat(tokens[0][4]);
			int rgb= Integer.parseInt(tokens[1][0], 16);
			float spec= Float.parseFloat(tokens[1][1]);
			float transp= 0;
			int metl= 0;//TODO
			int iri= 0;
			
			Noisedef[] nds= new Noisedef[lines.length-2];
			for(int i=0; i!=nds.length; i++)
				nds[i]= Noisedef.load(lines[i+2]);
			return new Material(name, cat, hard, cond, mp, rgb,spec,transp,metl,iri, nds);
		}catch(NumberFormatException e){break error;}
	}
	errors.add(text);
	return NULLMAT;
	}
	
	static final File DEFAULTMATFILE= new File("materials.txt");
	static List<String> errors= new ArrayList<String>();
	static final String iddeclarator="#IDs:\n";
	static final String iddeclaratornative="#IDs:"+FileUtil.newline;
	static final int iddecll= iddeclarator.length();
	public static void loadAll(World world){
		idmap.clear();
		Material.array.clear();
		Noisedef.namemap.clear();
		
		//separate all defs into their own strings
		file= new File("worlds/"+world.name+"/materials.txt");
		if(!file.exists()){
			file= DEFAULTMATFILE;
		}
		
		try(FileChannel fch= new RandomAccessFile(file, "rw").getChannel()){
			fch.lock();
			byte[] data= new byte[(int)file.length()];
			ByteBuffer dbuf= ByteBuffer.allocate(data.length);
			fch.read(dbuf);
			dbuf.clear();
			dbuf.get(data);
			String dstr= new String(data);
			String[] blocks= FileUtil.blockify(data);
			List<String> mats= new ArrayList<String>();
			List<String> noises= new ArrayList<String>();
			List<String> iddefs= new ArrayList<String>();
			for(String block : blocks){
				
				if(block.charAt(0)=='!')
					noises.add(block);
				else if(Character.isAlphabetic(block.charAt(0)))
					mats.add(block);
				else{
					if(block.charAt(0)=='#'){
						block= block.replace(iddeclarator, "");
						iddefs.add(block);
					}
					else
						errors.add(block);
				}
			}
			//noisedefs may be clumped into blocks, declump
			List<String> tn= new ArrayList<String>();
			for(String n : noises)
				tn.addAll(Arrays.asList(n.split("\n")));
			noises= tn;
			
			synchronized(array){
				//order is important here
				for(String n : noises){
					Noisedef.load(n);
				}
				makeStaticMats();
				for(String iddefblock : iddefs){
					String iderr= 
							idmap.load(iddefblock);
					if(iderr!=null)
						errors.add(iderr);
				}
				NameMap loadedidmap= idmap.clone();
				for(String m : mats)
					Material.load(m);
				
				String diff= NameMap.diff(idmap, loadedidmap).toString();
				if(errors.size()==0 && diff.length()!=0){
					String fadd="";
					
					int mark=0;
					if(iddefs.size()==0){//contains no block at all
						fadd= FileUtil.newline+iddeclaratornative+diff;
						mark= dstr.length();
					}
					else{
						mark= dstr.indexOf(iddeclaratornative);
						if(mark!=-1){//typical addage
							mark+= iddeclaratornative.length();
							fadd= diff;//contains all newlines
						}
						else{
							String newlineless= iddeclaratornative.substring(0, iddeclaratornative.length()-FileUtil.newline.length());
							mark= dstr.indexOf(newlineless);
							if(mark!=-1){//doesnt contain newline
								mark+= newlineless.length();
								fadd= FileUtil.newline+diff;
							}
							else//should be impossible
								new Exception().printStackTrace();
						}
					}
					
					if(fadd.length()!=0){
						assert(mark<=dstr.length());
						if(mark<dstr.length()){//moving stuff
							String moving= dstr.substring(mark, dstr.length());
							fadd+= moving;
						}
						byte[] addarr= fadd.getBytes();
						if(dbuf.capacity()<addarr.length)
							dbuf= ByteBuffer.allocate(addarr.length);
						dbuf.clear();
						dbuf.put(addarr);
						dbuf.flip();
						fch.position(mark);
						fch.write(dbuf);
						fch.force(false);
					}
				}
			}
			MaterialRenderer.queueRenderAll();
		}catch (IOException e){errors.add(e.getLocalizedMessage());}
		
		if(errors.size()!=0){
			String message="Error"+(errors.size()>1?"s":"")+" in materials.txt\n\n";
			for(String e : errors)
				message+= e+"\n\n";
			errors.clear();
			//TODO Message.popup(message);
			try{
				throw new Exception();
			}catch(Exception e){e.printStackTrace();System.err.println('\n'+message);}
		}
	}
	
	/**This is a class for easy reuse, 
	 * as many materials will use the same noise pattern*/
	public static class Noisedef{
		static Map<String, Noisedef> namemap= new HashMap<String, Noisedef>();
		public String name;
		/**{diffuse rgb, specularity, nscl, nmagxy, n, f}*/
		public float[] data= new float[5];
		
		public static final Noisedef NULLDEF= new Noisedef(0, 0, 0, 0, -1);
		public static int nenum=0, fenum=0,
				PERLIN= nenum++,
				WORLEY= nenum++,
				
				X= fenum++,
				ABS= fenum++,
				XSQ= fenum++,
				XSQN= fenum++,
				SPL= fenum++;		
		
		/** @param rgb diffuse color, 0xrrggbb
		 * @param specularity shinyness
		 * @param mag intensity of surface bumps
		 * @param scl frequency of surface bumps*/
		public Noisedef(float mag, int sclx, int scly, int n, int f){
			data[0]= mag;
			data[1]= sclx;
			data[2]= scly;
			data[3]= n;
			data[4]= f;
		}
		public Noisedef(String name, float mag, int sclx, int scly, int n, int f){
			this(mag, sclx,scly, n,f);
			this.name= name;
			namemap.put(name, this);
		}
		
		/**@param line the line containing the def, excluding starting '!' and ending '\n'*/
		public static Noisedef load(String line){
			try{
				String[] tokens= line.split(" ");
				if(tokens.length==1){//referencing, dict lookup
					Noisedef got= namemap.get(tokens[0]);
					if(got==null){
						errors.add("Undefined noise definition\n"+line);
						return NULLDEF;
					}
					return got;
				}
				
				
				final String name;
				if(tokens[0].charAt(0)=='!'){//dict insert
					name= tokens[0].substring(1);
					
					//remove name from tokens, shifting rest >>1
					String[] ttemp= tokens;
					tokens= new String[tokens.length-1];
					System.arraycopy(ttemp, 1, tokens, 0, tokens.length);
				}
				
				
				else//not using dict
					name= null;
				
				//if manual then length is 5
				if(tokens.length!=5){
					errors.add("Invalid noisedef length\n"+line);
					return NULLDEF;
				}
				float mag= Float.parseFloat(tokens[0]);
				int sclx= Integer.parseInt(tokens[1]);
				int scly= Integer.parseInt(tokens[2]);
				int f= Integer.parseInt(tokens[3], 2);
				int n;
				switch(tokens[4]){
					case "p":
						n= PERLIN;
						break;
					case "w":
						n= WORLEY;
						break;
					default:
						errors.add("Invalid Noise\n"+line);
						return NULLDEF;
				}
				Noisedef ret;
				if(name==null)
					ret= new Noisedef(mag, sclx,scly, n,f);
				else
					ret= new Noisedef(name, mag, sclx,scly, n,f);
				return ret;
			}catch(NumberFormatException e){
				errors.add("Invalid number format\n"+line);
				return NULLDEF;
			}
		}
	}
	
	
	
	private static byte[][] nmatlut= new byte[256][];
	public static byte[] normalToMatrixLookup(byte n){
		return nmatlut[n&0xff];
	}
	public static void initNMatLUT(){
		for(int i=0; i!=nmatlut.length; i++){
			nmatlut[i]= normalToMatrix((byte)(i&0xff));
		}
	}
	private static byte[] normalToMatrix(byte n){
		byte theta= (byte)(n&0b00011111);
		byte phi=  (byte)((n&0b11100000)>>>5);
		
		float x= (float)Math.cos(theta);
		float y= (float)Math.sin(theta);
		float z= 0;
		
		float c = (float)Math.cos(theta);
		float s = (float)Math.sin(theta);
		float t = 1f-c;
		
		float m00 = c + x*x*t;
		float m11 = c + y*y*t;
		float m22 = c + z*z*t;    
		
		float tmp1 = x*y*t;
		float tmp2 = z*s;
		float m10 = tmp1 + tmp2;
		float m01 = tmp1 - tmp2;
		tmp1 = x*z*t;
		tmp2 = y*s;
		float m20 = tmp1 - tmp2;
		float m02 = tmp1 + tmp2;
		tmp1 = y*z*t;
		tmp2 = x*s;
		float m21 = tmp1 + tmp2;
		float m12 = tmp1 - tmp2;
		
		byte xx= (byte)(m00*127);
		byte xy= (byte)(m10*127);
		byte xz= (byte)(m20*127);
		byte yx= (byte)(m01*127);
		byte yy= (byte)(m11*127);
		byte yz= (byte)(m21*127);
		byte zx= (byte)(m02*127);
		byte zy= (byte)(m12*127);
		byte zz= (byte)(m22*127);
		
		return new byte[]{
				xx, xy, xz,
				yx, yy, yz,
				zx, zy, zz
		};
	}
	
	
	@Override
	public String toString(){
		return "Material:"+id+":"+name;
	}
}
