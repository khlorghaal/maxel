package audio;

import org.lwjgl.LWJGLException;
import org.lwjgl.openal.AL;
import org.lwjgl.openal.AL10;

import core.Client;
import core.ExecutorQueue;

public class AudioCore{
	
	private static Thread thread;
	private static final ExecutorQueue exec= new ExecutorQueue();
	protected static void exec(Runnable r){
		AudioCore.exec.execute(r);
		synchronized(AudioCore.exec){
			AudioCore.exec.notify();
		}
	}
	private static boolean run= true;
	public static void start(){
		thread= new Thread(new Runnable(){
			@Override
			public void run(){
				try{
					AL.create();
				}catch(LWJGLException e){
					e.printStackTrace();
				}
				Client.cdl.countDown();
				
				while(run){
					exec.flush();
					checkError();
					
					synchronized(exec){
						try{
							exec.wait();
						}catch(InterruptedException e){}
					}
				}
				
				AL.destroy();
			}
		}, "Audio");
		thread.start();
	}
	public static void terminate(){
		synchronized(exec){
			run= false;
			exec.notify();			
		}
	}
	
	public static void checkError(){
		int alerr= AL10.alGetError();
		if(alerr!=AL10.AL_NO_ERROR){
			String err= "AL ERROR: "+alerr+' ';
			String msg;
			switch(alerr){
				case AL10.AL_INVALID_OPERATION:
					msg= "Invalid Operation";
					break;
				case AL10.AL_INVALID_ENUM:
					msg= "Invalid Enum";
					break;
				case AL10.AL_INVALID_NAME:
					msg= "Invalid Name";
					break;
				case AL10.AL_INVALID_VALUE:
					msg= "Invalid Value";
					break;
				case AL10.AL_OUT_OF_MEMORY:
					msg= "Out of Memory. "+Sound.count+" sounds";
					break;
				default:
					msg= alerr+"";
			}
			err+= msg+'\n';
			StackTraceElement[] st= Thread.currentThread().getStackTrace();
			for(int i=2; i<st.length; i++)
				err+= st[i].toString()+'\n';
			err+= '\n';
			System.err.println(err);
		}
	}
	
	public static void onWorldTick(){
		Emitter.updateAll();
	}
}
