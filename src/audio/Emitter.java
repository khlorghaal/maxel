package audio;

import java.util.ArrayList;
import java.util.List;

import math.Kinematic;

import org.lwjgl.openal.AL10;

import world.World;

/**Destroys once done playing.
 * Autoupdates*/
public class Emitter{
	private static final List<Emitter> activeEmitters= new ArrayList<>();
	private static final List<Emitter> prem= new ArrayList<>();
	
	private final Kinematic attached;
	private Sound sound= null;//constructs async
	public Emitter(final String sound, final Kinematic attached){
		this.attached= attached;
		AudioCore.exec(new Runnable(){
			@Override
			public void run(){
				Emitter.this.sound= Sound.getImmediate(sound);
				activeEmitters.add(Emitter.this);
				Emitter.this.sound.playImmediate();
			}
		});
	}
	public void destroy(){
		sound.destroy();
		prem.add(this);
		
	}
	
	protected static void updateAll(){
		AudioCore.exec(new Runnable(){
			@Override
			public void run(){
				for(Emitter e : activeEmitters)
					e.update();
				activeEmitters.removeAll(prem);
				prem.clear();
			}
		});
	}
	
	
	/**Called each tick.
	 * @param k if null will use 0 pos 0 vel*/
	protected void update(){
		//TODO allow for multiworld sounds
		if(AL10.alGetSourcei(sound.source, AL10.AL_SOURCE_STATE)==AL10.AL_PLAYING){
			final float dpx= (float)( attached.trans.tx - World.instance.localplayer.trans().tx );
			final float dpy= (float)( attached.trans.ty - World.instance.localplayer.trans().ty );
			final float dvx= (float)( attached.v.x - World.instance.localplayer.kin.v.x );
			final float dvy= (float)( attached.v.y - World.instance.localplayer.kin.v.y );
			AL10.alSource3f(sound.source, AL10.AL_POSITION, dpx, dpy, 0);
			AL10.alSource3f(sound.source, AL10.AL_VELOCITY, dvx, dvy, 0);
		}
		else
			destroy();
	}
}
