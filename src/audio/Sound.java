package audio;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Hashtable;
import java.util.Map;

import org.lwjgl.openal.AL10;
import org.lwjgl.util.WaveData;

import core.ReferenceGlue.Tuple1;

public class Sound{
	
	protected final int source;
	public static int count= 0;
	protected Sound(final int buffer){
		this(buffer, false);	
	}
	protected Sound(final int buffer, final boolean loop){
		source= AL10.alGenSources();
		//this.buffer= buffer;
		
		AL10.alSourcei(source, AL10.AL_BUFFER, buffer);
	}
	protected void destroy(){
		stop();
		AudioCore.exec(new Runnable(){	
			@Override
			public void run(){
				AL10.alDeleteSources(source);
			}
		});
	}
	
	/**Null means pending. Result will always suceed or except*/
	public static void get(final String resource, final Tuple1<Sound> reference){
		reference.val= null;
		AudioCore.exec(new Runnable(){	
			@Override
			public void run(){
				reference.val= getImmediate(resource);
			}
		});
	}
	protected static Sound getImmediate(final String resource){
		return new Sound( SoundData.getBuffer(resource) );
	}
		
	private static class SoundData{
		private static final Map<String, Integer> rsrcmap= new Hashtable<>();
		
		protected static void purge(){
			for(Integer buf : rsrcmap.values())
				AL10.alDeleteBuffers(buf);
			rsrcmap.clear();
		}
		/**If sound is loaded, immediately sets ref.
		 * Else audio thread will load it and set ref asap*/
		public static int getBuffer(final String resource){
			Integer ret= rsrcmap.get(resource);
			if(ret!=null)
				return ret;
			
			File f= new File("sound/"+resource+".wav");
			if(f.exists()){
				ret= makeBufferWav(f);
			}
			else{
				f= new File("sound/"+resource+".ogg");
				if(f.exists()){
					ret= makeBufferOgg(f);
				}
				else{//file doesnt exist
					new FileNotFoundException(f.getAbsolutePath()).printStackTrace();
					assert(false);
					return -1;
				}
			}
			
			rsrcmap.put(resource, ret);
			return ret;
		}
		
		private static int makeBufferWav(File f){
			WaveData wd=null;
			try{
				wd= WaveData.create(new BufferedInputStream(new FileInputStream(f)));
			}catch(FileNotFoundException e){
				e.printStackTrace();
			}
			
			int buffer= AL10.alGenBuffers();
			AL10.alBufferData(buffer, wd.format, wd.data, wd.samplerate);
			wd.dispose();
			return buffer;
		}
		private static int makeBufferOgg(File f){
			//TODO
			return -1;
		}
	}
	
	public static void purgeBuffers(){
		AudioCore.exec(new Runnable(){
			@Override
			public void run(){
				SoundData.purge();
			}
		});
	}
	
	
	
//	
//	public void volume(float v){
//		
//	}
//	public void pitch(float p){
//		
//	}
//	public void doppler(float d){
//		
//	}
	
	
	public void play(){
		AudioCore.exec(new Runnable(){
			@Override
			public void run(){
				playImmediate();
				count++;
			}
		});
	}
	protected void playImmediate(){
		AL10.alSourcePlay(source);
	}
	
	//	private void emitterDefault(){
	//		final Transformation t= new Transformation();
	//		final Kinematic d= new Kinematic(t, 1);
	//		emission(d);
	//	}
	
	public void loop(final boolean loop){
		AudioCore.exec(new Runnable(){
			@Override
			public void run(){
				AL10.alSourcei(source, AL10.AL_LOOPING, loop?AL10.AL_TRUE:AL10.AL_FALSE);
			}
		});
	}
	
	public void pause(){
		AudioCore.exec(new Runnable(){
			@Override
			public void run(){
				AL10.alSourcePause(source);
				count--;
			}
		});	
	}
	
	public void stop(){
		AudioCore.exec(new Runnable(){
			@Override
			public void run(){
				AL10.alSourceStop(source);
				count--;
			}
		});
	}
}
