package net;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import net.Packet.ServerPacket;
import core.Main;
import entity.EntityPlayer;

public class Server{
	public static Set<ConnectionToClient> clients= new HashSet<>();
	
	private static List<ServerPacket> queue;
	public static void sendPacket(ServerPacket packet){
		synchronized(queue){
			queue.add(packet);
		}
		Server.class.notify();
	}
	
	public static void start(){
		new Thread(new Runnable(){
			@Override
			public void run(){
				try{
					while(Main.isRunning()){
						
						Server.class.wait();
						List<ServerPacket> dequeue= new ArrayList<>();
						synchronized(queue){
							dequeue.addAll(queue);
							queue.clear();
						}
						for(ServerPacket p : dequeue){
							//TODO :V
						}
						
					}
				}catch(InterruptedException e){
					e.printStackTrace();
				}
				
			}
		}, "Network Server").start();
	}
	

	public static class ConnectionToClient extends Connection{
		public EntityPlayer entity;
	}
}