package net;

import net.Server.ConnectionToClient;

public abstract class Packet{
	public byte[] data;
	/**mostly to prevent duplicates*/
	public short id;
	/**will overflow, that is expected*/
	public static short curID;
	public Packet(byte[] data){
		this.data= data;
		this.id= curID++;
	}
	
	public boolean validate(){return true;}
	
	public static class PacketSyn extends Packet{
		public PacketSyn(byte[] data){
			super(data);
		}
		
	}
	public static class PacketAck extends Packet{
		public PacketAck(byte[] data){
			super(data);
		}
		
	}
	public static class PacketAckAck extends Packet{
		public PacketAckAck(byte[] data){
			super(data);
		}
		
	}
	public static class PacketReq extends Packet{
		public PacketReq(byte[] data){
			super(data);

		}
	}

	
	/**Volatile packet from client to server*/
	public static class ClientPacket extends Packet{
		public static Connection server;
		public ClientPacket(byte[] data){
			super(data);

		}
	}
	/**Volatile packet from server to client*/
	public static class ServerPacket extends Packet{
		public ConnectionToClient reciever;
		public ServerPacket(byte[] data, ConnectionToClient reciever){
			super(data);
			this.reciever= reciever;
		}	
	}
	
	
	/**Client requests specific data from server
	 * If client does not recieve after time, rerequests*/
	public static class ClientRequestPacket extends ClientPacket{

		public ClientRequestPacket(byte[] data){
			super(data);
			// TODO Auto-generated constructor stub
		}
		
	}
	/**Sends data to client
	 * Client sends ack
	 * Server sends ackack*/
	public static class ServerPacketImportant extends ServerPacket{

		public ServerPacketImportant(byte[] data, ConnectionToClient c){
			super(data, c);
			// TODO Auto-generated constructor stub
		}
		
	}
}
