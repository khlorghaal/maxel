package core;

import java.io.DataOutputStream;
import java.io.IOException;


/**For writing to disk or packet</br>
 * Byte buffers are in Java default order.*/
public interface Saveable{
	public void save(DataOutputStream o) throws IOException;
}
