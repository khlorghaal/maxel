package core;

import java.util.Hashtable;
import java.util.Map;

/**Used for assigning unique ID numbers to a name*/
public class NameMap{
	private Hashtable<String, Integer> map= new Hashtable<>();
	private int freeID=0;
		
	public synchronized void clear(){
		map.clear();
		freeID=0;
	}
	
	/**Adds entry and revalidates freeID*/
	private void add(String key, int id){
		map.put(key, id);
		while(map.containsValue(freeID))
			freeID++;
	}
	/**Does a simple map get*/
	public synchronized int getID(String name){
		Integer ret= map.get(name);
		if(ret==null)
			ret= 0;
		return ret;
	}
	/**Does a map get, creates new id if not in map*/
	public synchronized int popID(String name){
		if(!map.containsKey(name)){
			add(name, freeID++);
		}
		return map.get(name);
	}
	
	/**Takes an iddef block in the format of</br>
	 * name id\n...
	 * @return if error*/
	public synchronized String load(String block){
		String[] lines= block.split("\\n");
		for(String line : lines){
			if(line.length()<3) //2ch+1wsp
				return line;
			String[] tokens= line.split(" ");
			if(tokens.length!=2)
				return "Invalid IDDEF\n"+line;
			try{
				int id= Integer.parseInt(tokens[0]);
				if(map.containsKey(tokens[1]) || map.contains(id)){
					String s="NameMap already contains ID\n";
					if(id==0)
						s="ID 0 is reserved\n";
					return s+line;
				}
				add(tokens[1], id);
			}catch(NumberFormatException e){return "NFE\n"+line;}
		}
		return null;
	}
	@Override
	public synchronized String toString(){
		String ret="";
		for(Map.Entry<String, Integer> e : map.entrySet())
			ret+= e.getValue().toString()+' '+e.getKey()+FileUtil.newline;
		return ret;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public synchronized NameMap clone(){
		NameMap n= new NameMap();
		n.freeID= this.freeID;
		n.map= (Hashtable<String, Integer>)this.map.clone();
		return n;
	}

	/**Big nor small may violate 1:1 key:value
	 * @return any IDs in big not in small</br>
	 * id name\\sysnewline*/
	public static synchronized NameMap diff(NameMap big, NameMap small){
		NameMap ret= new NameMap();
		//does small not contain key or id
		//if not, insert big's entry into ret
		for(String bigkey : big.map.keySet()){
			if(!small.map.containsKey(bigkey))
				ret.map.put(bigkey, big.map.get(bigkey));
		}
		return ret;
	}
}
