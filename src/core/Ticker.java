package core;

import core.UpdateBus.Updateable;

public class Ticker{
	Updateable tickee;
	UpdateBus b;
	/**@param l normally constructing class's this*/
	public Ticker(Updateable l, UpdateBus b){
		this.tickee= l;
		this.b= b;
		b.register(l);
	}
	public void destroy(){
		b.remove(tickee);
	}
}
