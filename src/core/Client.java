package core;

import entity.EntityPlayer;
import gfx.Camera;
import gfx.RenderCore;

import java.util.concurrent.CountDownLatch;

import ui.InputCore;
import ui.gui.Gameoverlay;
import audio.AudioCore;


public class Client{
	public static CountDownLatch cdl;
	public static Camera cam= new Camera();
		
	public static void init(){
		cdl= new CountDownLatch(3);
		RenderCore.start();
		AudioCore.start();
		InputCore.start();

		try{
			cdl.await();
		}catch(InterruptedException e){e.printStackTrace();}
	}
	
	public static boolean terminal= false;
	public static void terminate(){
		if(terminal)//prevent recursion
			return;
		terminal= true;
		RenderCore.terminate();
		AudioCore.terminate();
		//Settings save dynamically
		//GUIs dont save anything, 
		//persistent things like checkboxes are Settings
		Main.onClientClose();
		try{
//			InputCore.thread.stop();
//			RenderCore.thread.stop();
		}catch(ThreadDeath d){}
	}
	
//	private static boolean worldLoaded= false;
//	public static boolean worldLoaded(){ return worldLoaded; }
	public static void onWorldLoad(EntityPlayer localplayer){
		Gameoverlay.make(localplayer);
	}
	
}
