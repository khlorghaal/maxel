package core;

import gfx.Camera;
import gfx.RenderCore;
import gfx.Renderer;

import java.util.concurrent.CopyOnWriteArraySet;

import material.Chunk.Hit;
import math.Transformation;
import world.World;

public abstract class DebugMarker{
	public static final CopyOnWriteArraySet<DebugMarker> instances= new CopyOnWriteArraySet<>();
	{
		instances.add(this);
	}
	public void destruct(){
		instances.remove(this);
	}
	
	Transformation t;
	
	Renderer r= null;
	{
		RenderCore.exec.execute(new Runnable(){
			@Override
			public void run(){
				r= makeRenderer();
			}
		});
	}
	public abstract Renderer makeRenderer();
	
	public static class CollisionMarker extends DebugMarker{
		Hit h;
		
		public CollisionMarker(Hit h){
			
		}

		@Override
		public Renderer makeRenderer(){
			Renderer ret= new Renderer(){
				@Override
				public void addToWorldRenderGroup(World world){
					world.rgMisc.add(this);
				}
				
				@Override
				public void render(Camera cam){
					// TODO Auto-generated method stub
					
				}

				@Override
				public void onDestruct(){
					// TODO Auto-generated method stub
					
				}
			};
			
			return ret;
		}
	}
}
