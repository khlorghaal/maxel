package core;

import gfx.Antialias.AA;

public class Settings{
	public static boolean undecorated=false, fullscreen=false;
	
	
	/**relative to whole screen*/
	public static float fontsize= .05f;
	public static int fontColor= 0xfdfdfdff;
	/**how many pages per wheelclick or arrowpress*/
	public static float	scrollyness= 1f/4f;
	
	public static int uiColorPrime= 0x22114499;
	public static int uiColorSecond= 0x11339988;
	public static boolean uiBorders= false;
	public static String cursor= "1";
	
	public static boolean profilerShow= true;
	public static boolean nightvision= true;	
	
	public static boolean pan= true, panOffWhileBuilding= true, panNonlinear= false;
	/**recommend with nonlinear panning*/
	public static float panDeadzone= 0;//.7f;
	/**0none 1inonly 2inout </br>
	 * recommend mutex with mousepan*/
	public static int cursorzoom= 2;
	
	public static boolean toolgrid= true;
	
	
	
	public static GraphicsSettings RECOMMENDEDGFX= new GraphicsSettings();
	public static GraphicsSettings RETROGFX= new GraphicsSettings();
	public static GraphicsSettings HDGFX= new GraphicsSettings();
	static{		
		RECOMMENDEDGFX.materialResolutionExp= 6;
		HDGFX.materialResolutionExp= 9;
		RETROGFX.materialResolutionExp= 4;
		
		RECOMMENDEDGFX.materialLinearMagnificationFilter= false;
		HDGFX.materialLinearMagnificationFilter= true;
		RETROGFX.materialLinearMagnificationFilter= false;
		RECOMMENDEDGFX.normalLinearMagnificationFilter= false;
		HDGFX.normalLinearMagnificationFilter= true;
		RETROGFX.normalLinearMagnificationFilter= false;
		
		RECOMMENDEDGFX.aatyp= AA.SUPERSAMPLE;
		HDGFX.aatyp= AA.SUPERSAMPLE;
		RETROGFX.aatyp= AA.NONE;
		RECOMMENDEDGFX.aalvl=2;
		HDGFX.aalvl=3;
		RETROGFX.aalvl=1;
		
		//		RECOMMENDEDGFX.=;
		//		HDGFX.=;
		//		RETROGFX.=;
	}
	public static GraphicsSettings graphics= RECOMMENDEDGFX;
	
	public static class GraphicsSettings {
		//Each selection box is represented as a
		//String[] containing the selectable values.
		//String[] = new String[]{};
		
		public String[] antialias= new String[]{"off", "fast-approximate","supersample","morphological"};
		public AA aatyp;
		public int aalvl;
		/**Pixels in a triangle*/
		public int tessellation= 32;
		
		public boolean refraction= true;
		
		public float minbright=.0f, maxbright=1.01f;
		public boolean bloom= true;
		public boolean bloomlinearmag= true;
		public int bloomradius=3;
		public float burn= 0.00005f;
		
		public float speculareyeheight=.4f;
		
		public int starcount= 0x20000; 
		
		//public String[] materialResolutions= new String[]{"1","4","8","16","32","64","128","256","512","1024"};
		public int materialResolutionExp;
		public int cloudResolutionFraction= 1;//matres/(1<<cloudresfrac)
		public int cloudZResolution= 6;
		public boolean materialLinearMagnificationFilter;
		public boolean normalLinearMagnificationFilter;
		public boolean effectLinearMagnificationFilter;
		public String[] mipmapLevels= new String[]{"off","1","2","all"};
		public int mipmaps;
		
		public String[] fluids= new String[]{"flat","perlin","navier-stokes"};
		public String[] aerosols= new String[]{"opaque textured","dense textured","density*perlin(time*flow)"};
		public String[] particles= new String[]{"off","low","high","cloud"};
		
		public String[] fpscap= new String[]{"30","60","120","rampant"};
		public float fps= 60f;
		public boolean vsync;
	}
}
