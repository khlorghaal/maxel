package core;

import java.util.HashSet;
import java.util.Set;

public class EventBus{
	public static class Event{}
	public static interface EventListener{
		public void update(Event e);
	}
	
	Set<EventListener> listeners= new HashSet<>();
	
	public void register(EventListener l){
		synchronized(listeners){
			listeners.add(l);
		}
	}
	public void remove(EventListener l){
		synchronized(listeners){
			listeners.remove(l);
		}
	}
	
	public void broadcast(Event e){
		synchronized(listeners){
			for(EventListener l : listeners){
				l.update(e);
			}
		}
	}
	
}
