package core;


/**Used for waiting threads and getting times<br/>
 * does things in miliseconds*/
public class Timer {
	public long tick=0;
	private long tStart, tInvoke, tinvokep;
	private int dt;
	public float fps;
	/**seconds per frame*/
	public float spf;
	
	private int targetTimems;

	public Timer(float fps){
		setMaxUpdatesPerSecond(fps);
		tStart= t();
	}

	/**@param cap -1 if infinite*/
	public void setMaxUpdatesPerSecond(float cap){
		targetTimems= (int)(1000f/cap);
	}

	public void invoke(){
		//start set (previously)
		//end set
		//(end-start)+wait=target
		//wait= target-(end-start)
		tick++;
		
		tinvokep= tInvoke;
		tInvoke= t();
		dt= (int)(tInvoke-tStart);
		if(dt<0)
			dt= 0;

		spf= (int)(tInvoke-tinvokep)/1000f;
		fps= 1f/spf;
		synchronized(this){
			prevspf[prevspfMark++]= spf;
			if(prevspfMark>=prevspf.length)
				prevspfMark= 0;
		}
		
		int w;		
		if(halttime!=0){
			w= halttime-dt;
			halttime= 0;
		}
		else
			w= targetTimems-dt;
		
		if(w>0)
			try {
				sleepingThread= Thread.currentThread();
				Thread.sleep(w);
			}catch (InterruptedException e){}
		tStart= t();
	}
	final long t(){
		return System.nanoTime()/1000000;
	}
	
	Thread sleepingThread;
	/**upon the next invocation*/
	int halttime=0;
	/**@param timems -1 to wait until interrupt*/
	public void halt(Thread thread, int timems){
			this.sleepingThread= thread;
			if(timems==-1)
				timems= 0x7fffffff;
			halttime= timems;
	}
	public void dehalt(){
		sleepingThread.interrupt();
	}
	
	/**Always has size==samplesize*/
	float[] prevspf;
	int prevspfMark= 0;
	/**for averaging over previous frames*/
	private synchronized void setSamplesize(int s){
		prevspf= new float[s];
		prevspfMark= 0;
		for(int i=0; i!=s; i++)
			prevspf[i]= 1f;
	}
	{setSamplesize(30);}
	/**Over the last second*/
	public synchronized float getAverageSpf(){
		float a=0;
		for(Float t : prevspf)
			a+=t;
		a/= prevspf.length;
		return a;
	}
	public int getAverageFps(){
		return (int)(1f/getAverageSpf());
	}
	
	/**thisrate * k = thatrate*/
	public float getRelativeRate(Timer that){
		return that.spf/this.spf;
	}
}
