package core;

import gfx.texture.NoiseTextures;

import java.io.File;
import java.util.concurrent.CountDownLatch;

import material.Planetology;
import world.World;

public class Main{	
	private static boolean run= true;
	public static boolean isRunning(){return run;}
	
	private final Timer maintimer= new Timer(3);
	
	public static CountDownLatch worldCdl; 
	
	public static void main(String[] args){
		for(int i=0; i!=args.length; i++){
			if(args[i].equalsIgnoreCase("server")){
				if(i+1!=args.length){
					String fp= args[i+1];
					new Server(new File(fp));
				}
				else
					new Server();
				return;
			}
		}
		
		//adjust native path
		String os= System.getProperty("os.name");
		os= os.substring(0, os.indexOf(" "));//cull version
		if(os.equals("Mac"))
			os= "macosx";
		System.setProperty("org.lwjgl.librarypath", new File("lwjgl-2.9.0/native/"+os).getAbsolutePath());
		
//		if(System.getProperty("debug")!=null)
//			System.setProperty("org.lwjgl.util.Debug", "true");
		
		new Main();
	}
	public Main(){
		NoiseTextures.init();
		worldCdl= new CountDownLatch(2);
		//2 for both localref set and threaded init
		new World("default");
		worldCdl.countDown();
		Client.init();

		while(run){
			if(maintimer.tick%(3*5)==0)
				checkSanity();			
			maintimer.invoke();
		}
	}
		
	public void checkSanity(){
		
	}
	
	static void onClientClose(){
		run= false;
		World.instance.terminate();
		Planetology.terminate();
	}
}
