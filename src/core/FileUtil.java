package core;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

public class FileUtil {
	public static final String newline= System.getProperty("line.separator");
	
	public static void copy(File sourceFile, File destFile) throws IOException {
	    if(!destFile.exists()) {
	        destFile.createNewFile();
	    }

	    FileChannel source = null;
	    FileChannel destination = null;

	    try {
	        source = new FileInputStream(sourceFile).getChannel();
	        destination = new FileOutputStream(destFile).getChannel();
	        destination.transferFrom(source, 0, source.size());
	    }
	    finally {
	        if(source != null) {
	            source.close();
	        }
	        if(destination != null) {
	            destination.close();
	        }
	    }
	}
	/**{@link #blockify(String)}*/
	public static String[] blockify(byte[] in){
		return blockify(new String(in));
	}
	/**Removes comments and separates by \n\n
	 * @return Strings with startends of nonwhitespace but unaltered internal whitespaces*/
	public static String[] blockify(String in){
		in= in.replaceAll("\\r", "");//only \n for newline
		in= in.replaceAll("//.*\\n", "");//remove comments
		String[] r= in.split("\\n\\n");
		int num=0;
		for(int i=0; i!=r.length; i++){
			r[i]= r[i].replaceAll("(\\A\\s+)|(\\s+\\z)", "");//remove trailing whitespace
			if(r[i].length()!=0)
				num++;
		}
		String[] ret= new String[num];
		num=0;
		for(int i=0; i!=r.length; i++){//plane nonblank entries <- what?
			if(r[i].length()!=0)
				ret[num++]= r[i];
		}
		return ret;
	}

	public static String getExtension(String filename){
		for(int i=1; i!=filename.length(); i++){
			if(filename.charAt(i)=='.'){
				return filename.substring(i+1, filename.length());
			}
		}
		return "";
	}
}
