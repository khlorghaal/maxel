package core;


public class Selector{
	public boolean active;
	
	private int pos=0;
	public int pos(){return pos;}
	
	public int max;
	public int w;
	
	public Selector(int max, int w){
		this.max= max;
		this.w= w;
	}
	
	
	private void bound(){
		if(pos<0)
			pos+= max;
		pos%= max;
	}
	public void set(int i){
		pos= i;
		bound();
	}
	public void left(){
		pos--;
		bound();
	}
	public void right(){
		pos++;
		bound();
	}
	public void up(){
		pos+= w;
		bound();
	}
	public void down(){
		pos-= w;
		bound();
	}
	
}
