package core;

import java.util.ConcurrentModificationException;
import java.util.List;


/**Divides up the iteration of a list among threads.
 * 
 *	creates a pool, each thread has an offset 'slice'
 *	each poolthread waits for invocation
 *
 *	invocation waits invoking thread	
 *	poolthreads sent list and function to iteratively apply to their slice
 *	poolthreads notified
 *	
 *	poolthread counts down then waits
 *	invoking thread is notified
 * */
public class Threaderator{
	
	private static final int cores= Runtime.getRuntime().availableProcessors();
	private final Thread[] threads= new Thread[cores];
	private final Ran[] rans= new Ran[cores];
	private final ThreadGroup tg;
	public Threaderator(String poolName){
		tg= new ThreadGroup(poolName);
		tg.setDaemon(true);
		for(int t=0; t<cores; t++){
			rans[t]= new Ran(t);
			threads[t]= new Thread(tg, rans[t], String.valueOf(t));
			threads[t].start();
		}
	}
	
	private class Ran implements Runnable{
		boolean alive= true;
		Runnable load= null;
		final int offs;
		Ran(int offset){
			this.offs= offset;
		}
		
		/**The main runnable per thread*/
		@Override
		public void run(){
			while(alive){
				
				if(load!=null){
					load.run();						
					load= null;
				}
				countdown();
				synchronized(this){
					try{
						this.wait();
					}catch(InterruptedException e){ e.printStackTrace(); }
				}
			}
			
		}
		
		synchronized <T> void do_(final List<T> l, final Function<T> f){
			//using an anonymous runnable makes the parameterization work nicely
			this.load= new Runnable(){
				@Override
				public void run(){
					for(int i=offs; i<lst; i+=cores)
						f.use(l.get(i));
				}
			};
			this.notify();
		}
		
		synchronized void kill(){
			alive= false;
			this.notify();
		}
	}
	
	
	private int count= cores;
	private final Object countlock= new Object();
	private void countdown(){
		synchronized(countlock){
			count--;
			synchronized(this){
				if(count==0)
					this.notify();
			}
		}
	}
	synchronized void await(){
		boolean done;
		synchronized(countlock){
			done= count==0;
		}
		if(!done)
			try{
				this.wait();
			}catch(InterruptedException e){ e.printStackTrace(); }
	}
	
	int lst= -1;
	/**List should be approximately sorted by computation time.
	 * List may not be inserted/removed before finishing.
	 * Waits calling thread until all children finish computing.
	 * @param a
	 * @param r*/
	public <T> void invoke(List<T> list, Function<T> f){
		lst= list.size();
		await();//needed if invoking from multiple threads
		synchronized(countlock){
			count= cores;//reset countdown
		}
		for(int t=0; t<cores; t++)
			rans[t].do_(list, f);
		await();
		if(lst!=list.size())
			throw new ConcurrentModificationException(
					"Must not add or remove from list during threaderator invocation");
	}
	
	public void terminate(){
		for(Ran r : rans)
			r.kill();
	}
	
	
	public static abstract class Function<T>{
		public abstract void use(T t);
	}
}
