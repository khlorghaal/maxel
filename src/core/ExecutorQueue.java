package core;

import java.util.ArrayList;
import java.util.concurrent.Executor;

/**Not a SingleThreadExecutor.
 * Stores runnables until invokeAll is called by an arbitrary thread*/
public class ExecutorQueue implements Executor{
	private final ArrayList<Runnable> queue= new ArrayList<Runnable>();
	@Override
	public void execute(Runnable command){
		synchronized(queue){
			queue.add(command);
		}
	}
	
	public void flush(){
		Runnable[] cqueue;
		synchronized(queue){
			cqueue= queue.toArray(new Runnable[0]);
			queue.clear();
		}
		for(Runnable r:cqueue)
			r.run();
	}
	
	public boolean pop(){
		Runnable pop;
		synchronized(queue){
			if(queue.size()==0)
				return false;
			pop= queue.get(0);
			queue.remove(0);
		}
		pop.run();
		return true;
	}
}
