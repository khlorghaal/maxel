package core;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Stack;

import ui.gui.Console;

public class Profiler{
	private static Hashtable<String, Profiler> map= new Hashtable<>();
	private static Stack<Profiler> running= new Stack<>();
	public static int metaduration;
	static{
		start("");
		metaduration= stop();
	}
	
	public String name;
	private long start, stop;
	public int duration;
	
	private Profiler(String name){
		this.name= name;
		map.put(name, this);
	}
	
	public synchronized void start(){
		running.push(this);
		start= System.nanoTime();
	}
	
	public static void start(String name){
		Profiler got= map.get(name);
		if(got!=null)
			got.start();
		else
			new Profiler(name).start();
	}
	
	/**Pops a timer and stops it*/
	public synchronized static int stop(){
		long stop= System.nanoTime();
		if(running.empty()){
			System.err.println("No profilers are started");
			return 0;
		}
		Profiler p= running.pop();
		int t= (int)(stop-p.start);
		p.duration= t;
		p.stop= stop;
		if(p.name.equals(""))
			return t;
		
		StringBuilder s= new StringBuilder(Integer.toString(t));
		//length+pad==9
		int pad= 9-s.length();
		if(pad>0){
			char[] arr= new char[pad];
			Arrays.fill(arr, '0');
			s.insert(0, arr);
		}
		s.insert(s.length()-3, ' ');
		s.insert(s.length()-7, ' ');
		s.insert(s.length()-11, '.');
		s.insert(0, p.name+": ");
		Console.out.println(s.toString());
		return t;
	}
	
	public static Profiler[] getAllSorted(){
		start("metaprofiling: getAllSorted()");
		Enumeration<Profiler> elements= map.elements();
		ArrayList<Profiler> all= new ArrayList<>();
		while(elements.hasMoreElements())
			all.add(elements.nextElement());
		Profiler[] ret= all.toArray(new Profiler[0]);
		
		Arrays.sort(ret, new Comparator<Profiler>(){
			@Override
			public int compare(Profiler p1, Profiler p2){
				return p1.duration-p2.duration;
			}
		});
		
		for(int i=0; i!=all.size(); i++){
		}
		stop();
		return ret;
	}
}
