package core;

public class ReferenceGlue{
	
	public static class Tuple1<T>{
		public T val;
		public Tuple1(){}
		public Tuple1(T val){
			this.val= val;
		}
	}
	public static class Tuple2<T>{
		public T a,b;
		public Tuple2(){}
		public Tuple2(T a, T b){
			this.a= a;
			this.b= b;
		}
	}
	
	public static interface BoolGetter{
		public boolean get();
	}
	public static interface IntGetter{
		public int get();
	}
	public static interface FloatGetter{
		public float get();
	}
}
