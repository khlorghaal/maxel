package core;

import java.util.HashSet;
import java.util.Set;

public class UpdateBus{
	public static class UpdateEvent{}
	public static interface Updateable{
		public void update();
	}
	
	Set<Updateable> listeners= new HashSet<>();
	
	public void register(Updateable l){
		synchronized(listeners){
			listeners.add(l);
		}
	}
	public void remove(Updateable l){
		synchronized(listeners){
			listeners.remove(l);
		}
	}
	
	public final void broadcast(){
		synchronized(listeners){
			for(Updateable l : listeners){
				l.update();
			}
		}
	}
}