
uniform sampler2D back;
uniform bool axis;
uniform int r;

layout(pixel_center_integer) in vec4 gl_FragCoord;
ivec2 xy= ivec2(gl_FragCoord.xy);

out vec4 color;

void main(){
	color= vec4(0);
	//lumpx = dist^-2;
	//2*integrate(lumpx ddist, 1,r)
	//= -2/x [1,r]
	//= -2/r -2
	//= (-2 -2r)/r
	//^-1 == r/(-2-2*r) = -.5*r/(1+r)
	//float normalizer= -.425*r/(1+r);
	
	//lumpx = dist^-1
	//2*integrate(lumpx ddist, 1,r)
	//2*log(x) [1,r]
	//2*(log(r)-log(1))
	//^-1== .5/log(r)
	//float normalizer= 1/r;
	
	//integrate(tau*r^2+1 dr, 0,1)
	//
	
	/*for(int x=-r; x!=r+1; x++){
		int slice= int(sqrt(float(r*r-x*x)));
		for(int y=-slice; y!=slice+1; y++){
			ivec2 txy= ivec2(x,y);
			float l= length(vec2(txy));
			float c= normalizer/(l+1);
			color+= c*texelFetch(back, xy+txy,0);
		}
	}*/
	
	//lumpx = 1/(1+d^2)
	//1/sum(1/(1+d^2),-r,r)
	//sum(1, -r, r) + sum(d^2, -r, r)
	//2r+1 + r(r+1)(2r+1)/6
	float normalizer= .2;//2*r+1 + (r+1)*(2*r+1)*r/6;
	for(int i=-r; i!=r+1;i++){
		ivec2 d;
		if(axis)
			d= ivec2(0,i);
		else
			d= ivec2(i,0);
		color+= normalizer*(texelFetch(back, xy+d,0))/(i*i+1);
	}
}