uniform mat3x3 mvp;

layout(location=0) in vec2 posin;

void main(){
	gl_Position= vec4( (mvp*vec3(posin.xy, 1)).xy, 0, 1 );
}