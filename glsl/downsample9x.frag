
uniform sampler2D super;

layout(pixel_center_integer) in vec4 gl_FragCoord;

out vec4 color;

void main(){
		ivec2 xy= ivec2(gl_FragCoord.xy)*3;
		vec4 s00= texelFetch(super, xy + ivec2(0,0),0);
		vec4 s10= texelFetch(super, xy + ivec2(1,0),0);
		vec4 s20= texelFetch(super, xy + ivec2(2,0),0);
		vec4 s01= texelFetch(super, xy + ivec2(0,1),0);
		vec4 s11= texelFetch(super, xy + ivec2(1,1),0);
		vec4 s21= texelFetch(super, xy + ivec2(2,1),0);
		vec4 s02= texelFetch(super, xy + ivec2(0,2),0);
		vec4 s12= texelFetch(super, xy + ivec2(1,2),0);
		vec4 s22= texelFetch(super, xy + ivec2(2,2),0);
		
		color=(s00+s10+s20+s01+s11+s21+s02+s12+s22)/9;
}