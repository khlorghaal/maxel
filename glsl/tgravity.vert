uniform vec4 attractor;
layout(location=0) in vec3 posin;
layout(location=1) in vec3 vin;

out vec3 pos, v;

void main(){
	float G= attractor.w;
	
	vec3 disp= attractor.xyz-posin;
	float d= length(disp)+.0001;
	float gmag= G/(d*d);
	vec3 g= normalize(disp)*gmag;
	
	v= vin+g;
	float vmag= length(v);
	pos= posin+v;
	
	//outer sphere
	float ro= 2;
	float bmaxmag= (length(posin)-ro)*2;
	if(bmaxmag<0)
		bmaxmag=0;
	v-= bmaxmag*normalize(posin)*clamp(dot(v,normalize(posin)),0,1);
	v= normalize(v)*vmag;
	
	//mouse sphere
	float ri= .5;
	if(d<ri){
		v-= g;//negate static g
		vmag= length(v);
		float soft= clamp((ri-d)*10,0,1);
		v-= soft*normalize(disp)*abs(dot(v,normalize(disp)));
		v= vmag*normalize(v);
		v+= 1.01*normalize(disp)*vmag*vmag/d;
		
		//pos+x=rvec  r-pos=x  
		//rvec= normalize(disp)*r
		//pos+= normalize(disp)*-(r-d);
	}
}