uniform sampler2D tex;

smooth in vec2 uv;
out vec4 outcolor;

void main(){
	outcolor= texture(tex, uv);
}