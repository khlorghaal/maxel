uniform vec3 color, colordawn,colordusk;
uniform vec2 soln;

smooth in vec2 uv;

out vec4 outcolor;//alpha affects starfield
void main(){
	vec3 c;
	float mag= length(uv);
	
	float z= sqrt(1-mag*mag);
	float incl= .25;
	mag= 1-pow(mag,8);
	
	vec3 uvz= normalize(vec3(uv,z));
	vec3 sol= normalize(vec3(soln,incl));
	float d= dot(uvz,sol);
	d= pow(d, 1);
	c= color*clamp(d,0,1);
	
	float left= dot(uvz,cross(sol,vec3(0,0,-1)));
	float t= left/2+.5;
	t= smoothstep(0,1,t);
	t= smoothstep(0,1,t);
	//t= sign(t)*sqrt(max(0,t));
	vec3 colortwi= mix(colordusk, colordawn, t);
	
	float twi= 1-abs(d-.12);
	twi= pow(twi, 5);
	twi= smoothstep(0,1, twi);
	c+= twi*colortwi;
	
	c*= mag;
	
	float a= mag*max(0,d);
	outcolor= vec4(c, a);
}