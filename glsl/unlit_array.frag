uniform sampler2DArray tex;
uniform int layer;

smooth in vec2 uv;
out vec4 outcolor;

void main(){
	outcolor= texture(tex, vec3(uv, layer));
}