
uniform sampler2D back;
uniform float mul;

layout(pixel_center_integer) in vec4 gl_FragCoord;
ivec2 xy= ivec2(gl_FragCoord.xy);

out vec4 color;

void main(){
		color= texelFetch(back, xy,0)*mul;
}