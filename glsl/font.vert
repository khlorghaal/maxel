#extension GL_ARB_shading_language_packing : enable

uniform vec2 corner;
uniform vec2 scale;
uniform int dp;

layout(location=0) in vec2 pos;
layout(location=1) in int ch;
layout(location=2) in int stylein;
layout(location=3) in uint colorin;

flat out int character;
flat out vec4 color;
flat out int style;

void main(){
	gl_Position= vec4(pos*scale+corner, 0., 1.);
	gl_PointSize= dp;
	
	character= ch;
	style= stylein;
	
	#ifdef GL_ARB_shading_language_packing
		color= unpackUnorm4x8(colorin);
	#else
		color= vec4(
			(colorin&0xFF000000)>>030,
			(colorin&0x00FF0000)>>020,
			(colorin&0x0000FF00)>>010,
			(colorin&0x000000FF));
	#endif
	
	color= vec4(1);
}