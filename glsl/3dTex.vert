uniform mat4 mvp;
uniform sampler3D tex;
uniform vec3 texoffs;

layout(location=0) in vec3 pos;
flat out vec3 uvw;
flat out vec4 color;
void main(){
	gl_Position= mvp*vec4( pos, 1.);
	uvw= pos/2+.5;
	float alpha= texture(tex, uvw+texoffs).x;
	if(alpha<.3)
		gl_Position= vec4(1./0.);
		
	gl_PointSize= 12./gl_Position.w;
	float parr= gl_PointSize*gl_PointSize;	
	
	color= vec4( mix(vec3(0,0,1),vec3(0,1,0), alpha), .01/parr);
	color.r= gl_Position.w/7;
}