
uniform sampler2D materials;

smooth in vec2 uv;

layout(location=0) out vec4 color;

void main(){
	color= texture(materials, uv);
}