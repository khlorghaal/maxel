
uniform mat4x4 mvp;

layout(location=0) in vec3 posin;
layout(location=1) in vec3 vin;

flat out vec3 v;
flat out float mag;

void main(){
	vec4 pos= vec4(posin, 1);
	gl_Position= pos*mvp;;
	gl_PointSize= 1;///(gl_Position.w+.5);
	
	v= vin;
	mag= 1;
}