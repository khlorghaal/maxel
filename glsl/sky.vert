uniform mat3x3 mvp;

layout(location=0) in vec2 pos;

smooth out vec2 uv;
void main(){
	uv= pos;
	vec2 xy= (mvp*vec3(pos.xy, 1)).xy;
	gl_Position= vec4( xy, .97, 1 );
}