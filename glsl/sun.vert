
uniform mat3x3 mvp;

layout(location=0) in vec2 posv;

smooth out vec2 uv;
void main(){
	vec2 xy= (mvp*vec3(posv, 1)).xy;
	gl_Position= vec4( xy, -.98, 1);
	
	uv= posv;
}