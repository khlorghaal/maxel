
uniform sampler2D super;

layout(pixel_center_integer) in vec4 gl_FragCoord;

out vec4 color;

void main(){
		ivec2 xy= ivec2(gl_FragCoord.xy)*2;
		vec4 xpyp= texelFetch(super, xy + ivec2(1,1),0);
		vec4 xpyn= texelFetch(super, xy + ivec2(1,0),0);
		vec4 xnyp= texelFetch(super, xy + ivec2(0,1),0);
		vec4 xnyn= texelFetch(super, xy + ivec2(0,0),0);
		color= (xpyp+xpyn+xnyp+xnyn)/4;
}