uniform vec3 color;
uniform sampler3D noise;
uniform float zoom;
uniform float t;

//linear filtering on z
float tex(sampler3D s, in vec3 coord){
	float sz= 1./textureSize(s,0).z;
	float z= mod(coord.z, sz)/sz;
	float a= texture(s, coord).x;
	float b= texture(s, coord+vec3(0,0,sz)).x;
	return mix(a,b,z);
}

smooth in vec2 uv;
out vec3 colorout;
void main(){
	float n;
	n= tex(noise, vec3(uv/3,t/2))*1.5;
	n+= tex(noise, vec3(uv*4,t))*1.5;
	n+= tex(noise, vec3(uv*8,t*8))*1;
	n= -n/2;
	colorout= color+n*4;
	colorout*=4;
	if(length(uv)>.995)
		colorout.xyz= vec3(-.1);
}