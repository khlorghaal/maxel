
uniform sampler2D normals;

smooth in vec2 uv;

out vec4 color;

void main(){
	color= texture(normals, uv)/2+.5;
}