uniform sampler3D tex;
uniform float layer;

smooth in vec2 uv;
out vec4 outcolor;

void main(){
	outcolor= texture(tex, vec3(uv, layer));
}