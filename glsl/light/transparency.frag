uniform usampler2D material;
uniform sampler2D normal;
uniform sampler2D opaque;

ivec2 fc= ivec2(gl_FragCoord.xy);

out vec4 colora;
void main(){	
	vec3 background;
	
	#ifdef REFRACTION
	vec3 geom= normalize(texelFetch(normal, fc,0).xyz);
	ivec2 rfrfc= ivec2( refract(vec3(0,0,1), geom, .5).xy *100 );
	background= texelFetch(opaque, rfrfc+fc,0).xyz;
	#else
	background= texelFetch(opaque, fc,0).xyz;
	#endif
	
	uint tsiz= (1u<<8u)-1u;
	float alpha= float(
	(texelFetch(material, fc,0).x>>5u)&tsiz
	)/float(tsiz);
	colora= vec4(background, alpha);
}