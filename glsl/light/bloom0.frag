
uniform sampler2D back;
uniform float lummax;

layout(pixel_center_integer) in vec4 gl_FragCoord;
ivec2 xy= ivec2(gl_FragCoord.xy);

out vec3 color;
void main(){
		color= texelFetch(back, xy,0).xyz;
		vec3 cabs= abs(color);
		
		if(cabs.x<=lummax)
			color.x= 0;//dont discard, must overwrite to clear
		else
			color.x-= lummax/2;//i dont know why /2
			
		if(cabs.y<=lummax)
			color.y= 0;
		else
			color.y-= lummax/2;
			
		if(cabs.z<=lummax)
			color.z= 0;
		else
			color.z-= lummax/2;
}