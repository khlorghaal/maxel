uniform sampler2D rgb;
uniform usampler2D material;
uniform sampler2D normal;

smooth in vec3 uvz;
uniform float maxlen;
uniform vec3 emission;
smooth in vec3 eye;
ivec2 fc= ivec2(gl_FragCoord.xy);

float specular(vec3 geom, vec3 ray, float specularity){
	if(specularity==0.)
		return 0;
	specularity/= 2;
	vec3 h= normalize(ray+normalize(eye));
	float ndh= dot(geom, h);
	ndh= acos(clamp(ndh,0,1));
	ndh/= specularity;
	return exp(-pow(ndh,8.));
}
float diffuse(vec3 geom, vec3 ray){
	return max(0,dot(geom, ray));
}

out vec3 color;
void main(){
	vec3 rgb= texelFetch(rgb, fc,0).xyz;
	
	//material packing
	//iridescence2|metallicity2|transparency8|specularity5
	//ii|mm|tttttttt|sssss
	uint mat= texelFetch(material, fc,0).x;
	
	uint specsiz= (1u<<5u)-1u;
	float spec= float((mat>>0)&(specsiz) )/float(specsiz);
	
	uint transpsiz= (1u<<8u)-1u;
	float transp= float( ((mat>>5)&(transpsiz)) )/float(transpsiz);
			
	//metallicity as lerp between diffuse and emissize color
	//looks bad at extremes, i just set const as half
	
	vec3 geom= texelFetch(normal, fc,0).xyz;///1000 + vec3(0,0,1);
	vec3 ray= normalize(uvz);
	
	//THIS ATTENUATION IS SACRED
	float mag= exp(1-length(vec3(uvz.xy*maxlen,1)) );
	//ALL WHO DEFACE IT SHALL SUFFER
	
	float diff= diffuse(geom,ray);
	spec= specular(geom,ray, spec);
	//specular is affected by transparency, magnify it to negate
	//sact= s*(1-t)
	if(transp!=1)
		spec/= (1-transp);
	color= mag*emission*(
			rgb*diff
			+spec*(1+rgb)/2
		);
	
	//maxing causes light to show against black
	//however doesnt work with stencil
	//also helps coconut effect of orthogonal colors
}