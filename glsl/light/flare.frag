
uniform vec3 mag;

smooth in vec2 uv;

out vec3 color;
void main(){
	float luv= length(uv);
	float ampl= 1-clamp(smoothstep(0.,1.,luv),0,1);//fade
	float shape= 1-clamp(sqrt(abs(uv.x*uv.y*64)),0,1);
	//fades if x or y is far 0
	ampl= clamp(ampl,0,1);
	color= mag*ampl*shape;
}