
uniform mat3x3 cam;
uniform float z;
uniform float depth;
uniform float aspect;
uniform float seh;//specular eye height

in layout(location=0) vec2 posin;
in layout(location=1) vec2 uvin;

smooth out vec3 uvz;
smooth out vec3 eye;

void main(){
	vec3 pos= cam*vec3(posin.xy, 1.);
	gl_Position= vec4(pos, 1.);
	gl_Position.z= depth;
	uvz= vec3((uvin*2-1), z);
	eye= vec3(gl_Position.xy*vec2(aspect,1), seh);
}