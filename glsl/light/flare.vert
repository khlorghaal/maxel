
uniform mat3x3 cam;
uniform float depth;

in layout(location=0) vec2 posin;
in layout(location=1) vec2 uvin;

smooth out vec2 uv;
void main(){
	vec3 pos= cam*vec3(posin.xy, 1.);
	gl_Position= vec4(pos, 1.);
	gl_Position.z= depth;
	uv= uvin*2-1;
}