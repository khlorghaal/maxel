uniform sampler2D light;
uniform float saturation;

layout(pixel_center_integer) in vec4 gl_FragCoord;
ivec2 fxy= ivec2(gl_FragCoord.xy);
smooth in vec2 uv;

uniform float lummin, lumrange, t;

#ifdef BLOOM
uniform sampler2D bloom;
#endif

float hash(vec3 n){
    return fract(sin(dot( n,vec3(39322.9898,41528.233,.16215589) )) * 1334.15453);
}

out vec3 color;
void main(){
	color= texelFetch(light, fxy,0).xyz;
	#ifdef BLOOM
	color+= texture(bloom, uv).xyz;
	#endif
	
	//map color to [0,1]
	color-= lummin;
	color/= lumrange;
	
	//desaturate dims
	vec3 lumr= vec3(.2126, 0.7152, 0.0722);
	float lum= dot(color, lumr);
	color= mix( vec3(lum), color, saturation);
	
	//default fbo is srgb, no need to tone map
	
	//dither 
	color+= (hash( vec3(uv, t) )-.5)/64;
}