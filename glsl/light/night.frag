uniform vec3 gold, blue;
uniform vec2 soln;
uniform float daymag;
uniform float nightmag;


smooth in vec2 uv;

out vec3 outcolor;
void main(){
	float luv= length(uv);
	float mag= luv;
	float z= sqrt(1-mag*mag);
	float incl= .25;
	
	vec3 uvz= normalize(vec3(uv,z));
	vec3 sol= normalize(vec3(soln,incl));
	
	float d= dot(uvz,sol);
	
	float day= max(nightmag,d);
	//fade shadow when into space 
	//= increase daylight at high mag low dot
	//mag [0,1]->[.95,1]
	//daylight+= (1-daylight-amb)*max(0, (mag-.95)/.01);
	//mag= mag*mag;
	//mag= 1-mag;
	
	float h= d/2+.5;
	vec4 b= vec4(.47,.54,.69, .78);
	vec3 mags= clamp( (h-b.xyz)/(b.yzw-b.xyz), 0,1);
	mags= smoothstep(0,1,mags);
	vec3 add= vec3(0);
	add= mix(add, gold, mags.x);
	add= mix(add, blue, mags.y);
	add= mix(add, vec3(0,0,0), mags.z);
	
	float left= dot(uvz,cross(sol,vec3(0,0,-1)));
	left= smoothstep(-1,1,left);
	add*= max(0,left);
	
	outcolor= daymag*(day + add);
}

/*
float d2= dot( uvz,cross(sol,vec3(0,0,1)) );
	float left= d2>0? 0:1;
	left*= smoothstep(0,.2,luv);
	float gold= ( 1-abs(d-.05) )*left;
	gold= pow(gold, 6);
	gold= smoothstep(0,1, gold);
	gold= smoothstep(0,1, gold);
	gold*=5;
	
	float blue= 1-abs(d-.2);
	blue= pow(blue, 2);
	//blue= smoothstep(0,1, blue);
	//blue= smoothstep(0,1, blue);
	blue*=.5;
*/