uniform sampler2DArray noise;

smooth in vec2 uv;
flat in float mag1, mag2;
flat in vec2 scl1, scl2;//scale as uv multiplier
flat in int n1, n2;//noise layer, function
flat in int f1, f2;

#define NULL -1
#define PERLIN 0
#define WORLEY 1

#define ABS 1
#define XSQ 2
#define SQRT 4
#define XSQN 8
#define SIG 16
bool bt(int dat, int indx){
	return (dat&indx)==indx;
}

ivec2 o;//sampling offset
float func(vec2 xy, int n, int f){
	float ret;
	switch(n){
		case NULL:
		default:
			ret=0;
			break;
			
		case PERLIN:
			ret= textureOffset(noise, vec3(xy,PERLIN),o).x;
			break;
	
		case WORLEY:
			ret= textureOffset(noise, vec3(xy,WORLEY),o).x;
			break;
	}
	if(bt(f, ABS))
		ret= abs(ret);
	ret= ret/2+.5;
	if(bt(f, SQRT))
		ret= sqrt(ret);
	if(bt(f, XSQ))
		ret= ret*ret;
	if(bt(f, XSQN))
		ret= -ret*ret+2*ret;//f'(1)=-2x+2
	if(bt(f, SIG))//f(0,1)=(0,1)  f'(0,.5,1)=(0,1,0)
		ret= smoothstep(0,1,ret);
	return ret;
}

float sum(vec2 xy){
	float f1= mag1*func(xy*scl1, n1, f1); 
	float f2= mag2*func(xy*scl2, n2, f2);
	return f1+f2;
}

float dcolor(float mag){
	return mag;
}

out vec3 normal;
void main(){
	o= ivec2(0);
	float t0=  sum(gl_PointCoord);
	o= ivec2(1,0);
	float tdx= sum(gl_PointCoord);
	o= ivec2(0,1);
	float tdy= sum(gl_PointCoord);
	
	normal= vec3(tdx, tdy, -1./textureSize(noise,0).x);
	normal.xy-= t0;
	normal= normalize(-normal);
}