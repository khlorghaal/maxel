flat in vec3 intensity;
smooth in vec2 gl_PointCoord;

float shape(){
	vec2 p= gl_PointCoord*2-1;
	return 1-length(p)/sqrt(2.);
}

out vec3 color;
void main(){
	//TODO twinkle
	color= intensity*shape();
}