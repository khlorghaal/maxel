
uniform mat3x3 mvp;

layout(location=0) in vec3 posin;
layout(location=1) in vec2 uvin;

smooth out vec2 uv;

void main(){
	vec2 xy= (mvp*vec3(posin.xy, 1)).xy;
	gl_Position= vec4( xy, posin.z, 1 );
	
	uv= uvin;
}