
uniform mat3x3 mvp;
uniform vec2 down;
uniform vec2 wind;
uniform float t;

layout(location=0) in vec2 posin;
void main(){
	vec2 xy= (mvp*vec3(posin.xy, 1)).xy;
	float z=(mod(gl_VertexID, 4)/4. -.01)*2-1;
	gl_Position= vec4( xy, z, 1 );
}