smooth in vec2 uv;
smooth in float dist;
layout(location=0) out vec3 rgb;
layout(location=1) out uint material;
layout(location=2) out vec3 normal;
void main(){
	rgb= vec3(0, uv);
	//material packing
	//iridescence2|metallicity2|transparency8|specularity5
	//ii|mm|tttttttt|sssss
	material= 0xffu<<5u;
	material= 0x0u;
	normal= vec3(0,0,1);
}