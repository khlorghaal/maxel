
uniform sampler2D backbuffer;
uniform sampler3D noise;

uniform vec3 color;
uniform float time;

in vec2 uv;


out layout(location=0) vec3 rgb;
out layout(location=1) uint material;
out layout(location=2) vec3 normal;
void main(){
	vec3 bump= vec3(
		texture(noise, vec3(uv,time)).x,
		texture(noise, vec3(uv+dx,time)).x,
		texture(noise, vec3(uv+dy,time)).x
	);
	//todo bicubic
	vec3 norm= normalize( bump
	
	vec2 ruv= uv+.1*refract(vec3(0,0,1), norm, .5).xy;
	vec3 bb= texture(backbuffer, ruv).xyz;
	rgb= vec4(color*bb, 0);
	material= 0u;
	normal= vec3(0,0,1);
}