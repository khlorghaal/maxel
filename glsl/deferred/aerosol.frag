uniform sampler3D noise;

uniform vec3 color;

smooth in vec2 uv;

out layout(location=0) vec3 rgb;
out layout(location=1) uint material;
out layout(location=2) vec3 normal;
void main(){
	float a;
	float d= 1./10;
	for(float z=0; z<=1; z+=d){
		a+= 
		d*(texture(noise, vec3(uv/2,z)).x/2
		  +texture(noise, vec3((uv+.5)/2,z)).x/2
		  +texture(noise, vec3(uv/1.5,z)).x/2
		  +texture(noise, vec3(uv*2,z)).x/4);
	}
	a= (a-.01);
	a= -exp(-a)+1;
	a+= sqrt(texture(noise, vec3(uv*12.9,0)).x/2+.5)/16;
	
	rgb= color*.9;
	material= 0u;
	normal= vec3(0,0,1);
}