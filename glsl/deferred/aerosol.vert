uniform mat3 mvp;

layout(location=0) in vec3 pos;
smooth out vec2 uv;
void main(){
	vec2 xy= (mvp*vec3(pos.xy, 1)).xy;
	gl_Position= vec4( xy, -.99, 1 );
	uv= pos.xy/2+.5;	
}