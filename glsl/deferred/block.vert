uniform mat3x3 mvp;
uniform samplerBuffer brgb;
uniform usamplerBuffer bmat;

layout(location=0) in vec3 posin;
layout(location=1) in float uvs;
layout(location=2) in int idin;

flat out int id;
flat out vec3 vrgb;
flat out uint vmat;
smooth out vec2 uv;
void main(){
	vec2 xy= (mvp*vec3(posin.xy, 1)).xy;
	gl_Position= vec4( xy, posin.z, 1 );
	
	id= idin;
	vrgb= texelFetch(brgb, id).xyz;
	vmat= texelFetch(bmat, id).x;
	uv= posin.xy*uvs;
}