uniform sampler2D tex;

out layout(location=0) vec3 rgb;
out layout(location=1) uint material;
out layout(location=2) vec3 normal;
void main(){
	vec2 xy= gl_PointCoord*2-1;
	float l= length(xy);
	normal= normalize(vec3(xy, sqrt(2.-l*l)));
	//normal= vec3(xy,0);
	rgb= normal/2+.5;
	
	rgb= vec3(1);
	normal= vec3(0,0,1);
	
	//material packing
	//iridescence2|metallicity2|transparency8|specularity5
	//ii|mm|tttttttt|sssss
	float trf= .7;
	uint tr= uint(trf*((1<<8)-1))<<5;
	float spf= .0;
	uint sp= uint(spf*((1<<5)-1));
	material= tr|sp;
}