layout(vertices = 1) out;

uniform mat3x3 mvp;
uniform ivec2 res;

bool unvisible(vec2 xy){
	xy= (mvp*vec3(xy,1)).xy;
	return any( greaterThan( vec4(xy, -xy), vec4(1) ) );
}

struct knot{
	vec2 pos;
	vec2 norm;
};

in knot knv[];
patch out knot left,right;
void main(){
	left=  knv[0];
	right= knv[1];
	
	/*
	gl_TessLevelOuter[0]= gl_TessLevelOuter[2]= 1;
	gl_TessLevelOuter[1]= gl_TessLevelOuter[3]= 1;
	gl_TessLevelInner[0]= 1;
	gl_TessLevelInner[1]= 1;
	*/
	
	/*
	if(all(bvec2( unvisible(left.pos), unvisible(right.pos) ))){
		gl_TessLevelOuter[0]= 
		gl_TessLevelOuter[1]= 
		gl_TessLevelOuter[2]= 
		gl_TessLevelOuter[3]= 
		gl_TessLevelInner[0]= 
		gl_TessLevelInner[1]= 0;
		return;
	}
	*/

	int len= int(length(
	( mvp*vec3(left.pos-right.pos,0) ).xy*res ));
	//w=0 because ignore translation
	
	gl_TessLevelOuter[3]= gl_TessLevelOuter[1]= len+1;
	gl_TessLevelOuter[0]= gl_TessLevelOuter[2]= 1;
	gl_TessLevelInner[0]=
	gl_TessLevelInner[1]= 1;
}