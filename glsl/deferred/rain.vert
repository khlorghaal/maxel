uniform mat3x3 mvp;
uniform float scale;
uniform int count;
uniform vec2 down;
uniform vec2 wind;
uniform float t;

vec2 offset(vec2 pos){
	pos.y-= t*1;
	pos.y= mod(pos.y, 2.)-1;
	pos.x-= t*(1+float(gl_VertexID)/count)/5;
	pos.x= mod(pos.x, 2.)-1;
	return pos;
}

const int lsiz= 6;
float logs(float f){ return log(f)/log(lsiz); }
float exps(float f){ return exp(f*log(lsiz)); }

float getlvl(float scale){
	//level moves particles outward as zoom decreases
	//each level is lsiz the scale as the last
	float num= float(gl_VertexID)/count ;
	int level= int( logs(scale) + num);
	//level= (level&0x7fffffff)|((level&1)<<31);//sign= odd? 1:0
	
	return (exps(level)+1);
}

layout(location=0) in vec2 posin;
void main(){
	float lvl= getlvl(scale);
	vec2 pos= 256*offset(posin);
	
	
	vec2 xy= (mvp*vec3(pos, 1)).xy;
	float z=(gl_VertexID&3)/3.;//4 layers
	gl_Position= vec4( xy, z*2-1, 1 );
	gl_PointSize= max(1+z, 40.*(1+z/3)/scale);
}