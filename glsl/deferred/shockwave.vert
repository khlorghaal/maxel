uniform mat3x3 mvp;

struct knot{
    vec2 pos;
	vec2 norm;
};

layout(location=0) in vec2 xyL;
layout(location=1) in vec2 xy;
layout(location=2) in vec2 xyR;
out knot knv;
void main(){	
	vec2 l= xyL;
	vec2 r= xyR;
	knv.pos= xy;
	knv.norm= normalize( knv.pos - (r+l)/2 );
	
	/*vec2 l= ( mvp*vec3(xyL,1) ).xy;
	vec2 r= ( mvp*vec3(xyR,1) ).xy;
	knv.pos= ( mvp*vec3(xy,1) ).xy;
	knv.norm= normalize( knv.pos - (r+l)/2 );*/
}