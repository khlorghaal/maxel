uniform mat3x3 mvp;
uniform float z;

layout(location=0) in vec2 xyL;
layout(location=1) in vec2 xy;
layout(location=2) in vec2 xyR;

smooth out vec2 uv;
smooth out float dist;
void main(){
	dist= length(xyR-xy)/10;
	
	vec2 pos= (mvp*vec3(xy, 1)).xy;
	gl_Position= vec4( pos, z, 1 );
	uv= abs(normalize(xy));
}