uniform sampler2DArray atlasn;

flat in int id;
flat in vec3 vrgb;
flat in uint vmat;
smooth in vec2 uv;

layout(location=0) out vec3 rgb;
layout(location=1) out uint material;
layout(location=2) out vec3 normal;
void main(){
	rgb= vrgb;
	material= vmat;
	normal= texture(atlasn, vec3(uv, id)).xyz;
	//oddly, renormalizing is bad.
	//Causes issues with high frequencies
}