uniform sampler2D texm;
uniform sampler2D texg;

smooth in vec2 uv;

out layout(location=0) vec3 rgb;
out layout(location=1) uint material;
out layout(location=2) vec3 normal;
void main(){
	rgb= vec3(.25);
	material= 0u;
	normal= vec3(0,0,1);
}