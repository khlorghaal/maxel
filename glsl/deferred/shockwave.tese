uniform mat3x3 mvp;
uniform float z;
uniform float w;
layout(quads, equal_spacing, cw) in;

struct knot{
	vec2 pos;
	vec2 norm;
};

patch in knot left,right;
smooth out vec2 uv;
void main(){
	vec2 xy= mix(left.pos, right.pos, gl_TessCoord.x);
	float s= gl_TessCoord.x*2-1;
	s= sqrt(1-s*s);
	vec2 n= mix(left.norm, right.norm, gl_TessCoord.x);
	vec2 nn= normalize(n);
	xy+= -gl_TessCoord.y*n*w;
	xy*= length(nn)/length(n);//n*k=nn; //TODO deshitify
	gl_Position= vec4( (mvp*vec3(xy,1)).xy, z,1 );
	
	uv= gl_TessCoord.xy;
}