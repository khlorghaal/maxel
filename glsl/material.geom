//passthough, assigns layers

layout(points) in;
layout(points, max_vertices=1) out;

uniform int res;

flat in float mag1g[], mag2g[];
flat in vec2 scl1g[], scl2g[];
flat in int n1g[], n2g[];
flat in int f1g[], f2g[];

flat out float mag1, mag2;
flat out vec2 scl1, scl2;
flat out int n1, n2;
flat out int f1, f2;

void main(){
	
	gl_Layer= gl_PrimitiveIDIn;
	
	gl_Position= vec4(0,0,0,1);
	gl_PointSize= res;
	
	//passthrough
	mag1= mag1g[0];
	mag2= mag2g[0];
	scl1= scl1g[0];
	scl2= scl2g[0];
	n1= n1g[0];
	n2= n2g[0];
	f1= f1g[0];
	f2= f2g[0];
	EmitVertex();
	EndPrimitive();
}