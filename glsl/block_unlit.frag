uniform sampler2DArray atlasn;

flat in int id;
flat in vec3 vrgb;
flat in uint vmat;
smooth in vec2 uv;

layout(location=0) out vec4 color;
void main(){
	vec3 normal= texture(atlasn, vec3(uv, id)).xyz;
	color.xyz= vrgb*normal.z;
	color.w= 1;
	//oddly, renormalizing is bad.
	//Causes issues with high frequencies
}