
flat in vec3 v;
flat in float mag;
out vec4 color;

vec3 hsl2rgb(vec3 c)
{
    vec3 K = vec3(3./3., 2./3., 1./3.);
    vec3 p = abs(fract(c + K)*6-3);
    return c.z*mix(vec3(1), clamp(p - 1, 0.0, 1.0), c.y);
}

void main(){
	color= 
	//vec4( hsl2rgb(vec3(gl_PrimitiveID/4200000., 1, 1)), mag);
	vec4( 1,1,1,mag);
	//vec4( mix(vec3(1),vec3(1,0,0),gl_PrimitiveID/4200000.), mag);
	//vec4( hsl2rgb(vec3(mag, 1, 1)), .02);
	//vec4(normalize(abs(v.xyz)),mag);
}