uniform sampler2DArray tex;

flat in int character;
flat in vec4 color;
flat in int style;
out vec4 colorout;
void main(){
	colorout= texture(tex, vec3(gl_PointCoord, character))*color;
}