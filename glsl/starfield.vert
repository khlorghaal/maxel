uniform mat2x2 cam;//purely rotational

layout(location=0) in vec2 posin;
layout(location=1) in vec3 intensityin;

flat out vec3 intensity;

void main(){
	gl_Position= vec4( cam*posin.xy, .99, 1 );
	intensity= intensityin;
}