layout(location=0) in vec3 coe1in;
layout(location=1) in vec2 nf1in;
layout(location=2) in vec3 coe2in;
layout(location=3) in vec2 nf2in;

flat out float mag1g, mag2g;
flat out vec2 scl1g, scl2g;
flat out int n1g, n2g;
flat out int f1g, f2g;

void main(){
	mag1g= coe1in.x;
	mag2g= coe2in.x;
	scl1g= vec2(1<<int(coe1in.y), 1<<int(coe1in.z));
	scl2g= vec2(1<<int(coe2in.y), 1<<int(coe2in.z));
	n1g= int(nf1in.x);
	n2g= int(nf2in.x);
	f1g= int(nf1in.y);
	f2g= int(nf2in.y);
}