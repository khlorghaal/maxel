
layout(location=0) in vec2 xy;
layout(location=1) in vec4 colorin;

smooth out vec4 color;

void main(){
	gl_Position= vec4(xy, 0, 1.);
	color= colorin;
}