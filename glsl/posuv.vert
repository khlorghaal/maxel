layout(location=0) in vec2 xy;
layout(location=1) in vec2 uvin;

smooth out vec2 uv;

void main(){
	gl_Position= vec4(xy, 0, 1.);
	uv= uvin;
}