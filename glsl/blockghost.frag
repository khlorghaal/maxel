
uniform sampler2DArray atlasm;
uniform sampler2DArray atlasn;
uniform bool invalid;

flat in int id;
smooth in vec2 uv;

layout(location=0) out vec4 color;

void main(){
	vec4 material= texture(atlasm, vec3(uv, id));
	vec3 normal= normalize(texture(atlasn, vec3(uv, id)).xyz);
	color= vec4(material.xyz*normal.z,.7);
	if(invalid){
		color.xyz*=(0.299, 0.587, 0.114);
		color.x= (color.x+color.y+color.z)/.299;
		color.yz= vec2(0);
	}
}