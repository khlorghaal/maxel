Ordering is rough chronological, 

"+"= done

Phase 1
+Chunks
copy and paste
+grid snap
+Tileset defscript


+Entities made out of a chunk
+entity spawning
+Chunk-chunk collision
+Collision mipping
Destruction mipping
Entity spatial hashing
+Controllable entities

+Terrain gen
Mineral gen

Atmospheres
Gravity fields, orbiting planets
+Skies

+GUIs
Settings Menu
Control modes, 
	regular, walking, weapons, interaction
	build
	command, crude prototype
	admin- basically creative mode, resource spawning, creature spawning, fly
	
	
+Collision destruction
+Material physical properties
Sound

+Normal maps
+Bumpmap>Normalmap converter
+Deferred pipeline
+Refraction
Dynamic range
Shadows

Dynamic lighting from emissive blocks
Chunk cleaving
Multi-Z

+Inventory
Hand use interface
Picking up blocks
Manufacturing benches
Blueprints
Material recipe defscript

Vehicle controls
Thrusters

Rotational Kinematics
Weapon swinging
Kinematic player limbs
Player sprite downsampling

//Phase 2
////////
Chemistry

Astrophysics

Dynamic chunk streaming, loading

Multiplayer
Update all 'important' events such as keypress and block breaking
Ping compensation
Kinematic updates
SCTP?
Allow clients variable timestep, disallow on server?, match client to server time dialation


///Phase 3
////////
Crops, wildlife
Hunger, eating, cooking

Special blocks
Explosives: high explosive, only detonated by other explosions; low explosive, by a harsh impact
	Explosion works via expanding particles
Thermal
	Fire; add combustible block property
	melting
Mechanical blocks, a subchunk which slides freely, while copying its parents rotation, optimized collision accordingly
Liquids, first terraria-like, then SPH

Electrons, a special entity that occupies a single block
Pervade all blocks defined as conductive, hurt organics on direct contact
Transistor, when hit by a signal, if it has >=two adjacent blocks, toggles their state and its own
Electron amplitude
Electron activated blocks, effect varies depending on the side hit and the direction of e's previous block
	Rotors, generate a torque when hit by an e, attach a chunk to them 
	Laser, pewpewpew
Cell, holds electrons, has attached entity to track them
Switch, when hit mechanicly, toggles conductivity
Generator, charges cells
Sensor, outputs very small charge based on location of nearby player


//Phase 4
/////////
Dynamic biosphere, animal reproduction

AI players
AI morale, stats, whims
Pathing algorithms
Building AI
Combat AI
Command AI
Command Mode

