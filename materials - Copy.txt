//This file defines the properties and appearance of all materials

//To make a version of this file unique to a single world, simply create a materials.txt in its root directory
//Reload all materials anytime while ingame by pressing f5.

//Format
//Comments start with // must be whole line
//Tokens separated by single spaces
//Newline ends the current set of properties
//Newlines end a definition

//Material definitions
//physical properties - gameplay affectors
//color - diffuse rgb and specularity
//noise - becomes the normal map - mag*noise(uv*scl,func))^exp
//noise2 - optional, added to the other noise bumpmap.
//
//name type hardness heatconductivity meltingpoint
//rgb s
//mag1 scl1 exp1 func1
//mag2 scl2 exp2 func2
//
//name- unique name of the material ingame. Implementation block IDs are automatically generated
//type- family of material. Applies to world generation and reactions.
//  syn- Synthetic. Does not appear in world.
//  ground- See planetology
//  mineral- Generated when rock is removed.
//  dosh- Execptionally valuable mineral. Abundance modified directly by how dosh the region is
//  metal- Found as ore, sometimes pure in small amounts as dosh.
//  plant- Planetology specifies how plants form out of which plant materials. It also specifies which plant materials are edible
//abundance- a float weighted relative to all other material abundances within its type.
//
//rgb- diffuse color, given as hex rrggbb
//s- specularity
//noise-
//  mag- magnitude
//  scl- 2^scl = minification. Should always tesselate.
//  exp- exponent applied after func before mag. experiment with >1, <1, and negatives
//  func- a function applied first before other noise properties, always returns [0,1]
//  funcs supported: abs sig grain line worley

//Noise definitions, define it once and reuse it.
//defined
//!name mag scl exp func
//referenced
//name

////////////////////
////////////////////
////////////////////

stone rock 1 .1 2000
777766 .04
.04 0 0 00010 p
rough2

dirt rock .1 .2 1500
777766 .04
.04 0 0 00010 p
rough2

sand rock .1 .2 1500
eed277 0 
.08 1 1 00010 p

grass syn .05 2 150
11aa11 .2
.5 0 2 000001 p
.3 4 0 000101 p

leaf syn .05 2 150
3a9933 .05
.18 0 0 0001000 p
.2 1 1 10 p

wood syn .1 2 50
b07544 0
.02 6 1 1 p
.015 2 1 1 p

steel metal .5 100 1000
dddddd .25
.025 6 1 000000 p

test syn 1 1 1
eed277 0 
.08 1 1 00010 p

////////////////////
////////////////////
////////////////////

!rough .05 2 2 000001 p
!rough2 .03 1 1 000000 p
!wood1 .1 1 1 000000 p
!wood2 .1 2 1 000000 p





////////////////////
////////////////////
////////////////////

//IDs are automagically generated
//A removed material will have its ID preserved until manually removed from this map. Fragmentation has no performance difference.
//Savefiles store ID only; swapping material-IDs will result in those materials being swapped.
#IDs:
8 sand
7 leaf
6 wood
5 grass
2 stone
4 dirt
3 steel
1 test
